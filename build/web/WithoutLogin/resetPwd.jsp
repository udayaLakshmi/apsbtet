<%-- 
    Document   : Teachersprint
    Created on : Jun 5, 2018, 7:39:44 PM
    Author     : 1042564
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 0;

        %>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <!--<script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">-->
    </script>
    <script type="text/javascript" language="javascript" src="js/demo.js">
    </script>
    <script type="text/javascript" class="init">
        function onlyNumbers(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Allow Numbers Only");
                return false;
            }
            return true;
        }
        $(document).ready(function() {
            var table = $('#example').DataTable({
//                    scrollY: "300px",
                scrollX: true,
//                    scrollCollapse: true,
//                    paging: true,
//                    fixedColumns: true
            });
        });
    </script>
    <style type="text/css">
        table {
            border-collapse: collapse !important;
            border-spacing: 0px !important;
        }
        table.altrowstable1 th {
            background-color: #f59042 !important;
            border: 1px #1e2ab7 solid !important;
            font-size: 13px !important;
            color: #000000 !important;
            border-collapse: collapse !important;
            border-spacing: 0px !important;
            text-align: left !important;
            padding: 5px 15px;
            word-break: keep-all;
            white-space: nowrap;
        }

        table.altrowstable1 td {
            text-align: left;
            border: 1px #1e2ab7 solid !important;
            vertical-align: middle;
            padding-left: 3px;
            padding-right: 3px;
            font-size: 13px;
            font-family: verdana;
            font-weight: normal;
            height: 20px;
            padding: 7px;
            background: #e6e8ff;

        }

        input {
            width: 90% !important;
            padding: 3px !important; 
        }
        tr.payrad td {
            text-align: left !important;
        }
        tr.payrad input[type="radio"] {
            float: left;
            text-align: left !important;
            width: 19% !important;
        }
        input[type="radio"], input[type="checkbox"]
        {
            width: 30px !important;
            float: left;
        }

        select {
            border-radius: 0;
            margin-bottom: 12px !important;
            border: 1px solid #005396;
            box-shadow: none;
        }

        .form-control{
            display: block;
            width: 100% !important;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
        }
        .btn
        {
            padding: 6px 12px !important;
            width: auto !important;
        }
        .h3_text{
            background: #1a9200;
            font-size: 15px;
            padding: 5px;
            text-align: center;
            color: #fff;
            line-height: 26px;
            box-shadow: 5px 5px 5px #ccc;
        }
        #example_wrapper {
            width: 100% !important;
            padding: 20px;
        }
        table.dataTable thead th, table.dataTable tbody td {
            padding: 8px 10px;
            border: 1px solid #ccc;
            font-size: 13px;
        }
    </style>




    <script>
        window.onload = function() {
            $('input, :input').attr('autocomplete', 'off');
            $("form").attr('autocomplete', 'off');
            document.getElementById("userName").placeholder = "Enter User Name";
            var seconds = 10;
            setTimeout(function() {
                if (document.getElementById("msg") !== null)
                    document.getElementById("msg").style.display = "none";
            }, seconds * 1000);
            $('input[type=text], textarea ,password').bind("cut copy paste", function(e) {
                alert("Cut copy paste not allowed here");
                e.preventDefault();
            });
        };
        function space(evt, thisvalue)
        {
            var number = thisvalue.value;
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (number.length < 1) {
                if (evt.keyCode == 32) {
                    return false;
                }
            }
            return true;
        }
        function mysearch() {
            if ($('#userName').val() == "") {
                alert("Please Select UserName");
                $('#userName').focus();
                return false;
            } else {
                document.forms[0].mode.value = "getOTP";
                document.forms[0].submit();
            }

        }

        function validateOtp() {
            if ($("#passwordNew").val() == "") {
                alert("Please Enter OTP Number");
                $("#otp").focus().css({'border': '1px solid red'});
                return false;
            }
            document.forms[0].mode.value = "updatePassword";
            document.forms[0].submit();
        }
    </script>
</head>
<body>
    <div class="row mainbodyrow">
        <div class="container">
            <div class="col-xs-12">
                <div class="maindodycnt">

                    <h3 class="h3_background"><b><center>Reset Your Password</center></b></h3>

                    <span></span>
                    <html:form action="/resetPwd" >
                        <html:hidden property="mode"/>

                        <logic:present name="result1">
                            <span id="msg" > <center> <font color="green" style="font-weight: bold">${result1}</font></center></span>
                                </logic:present>
                                <logic:present name="result">
                            <span id="msg" > <center> <font color="red" style="font-weight: bold">${result}</font></center></span>
                                </logic:present>
                        <br>
                        <logic:present name="form">
                            <table align="center" cellpadding="0" cellspacing="0" border="0" width="60%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                <tr>
                                    <th>User Name<font color="red">*</font></th>
                                    <td><html:text  property="userName" styleId="userName" onkeydown="return space(event,this);" /></td>
                                    <td>
                                        <input type='submit' value='SUBMIT' id="SUBMIT" onclick="return mysearch();" class="button button2" style=" width: auto !important;  padding: 5px !important;"/>
                                    </td>
                                </tr>
                            </table>
                        </logic:present>        
                        <br>

                        <logic:present name="smsForm">
                                <table align="center" cellpadding="0" cellspacing="0" border="0" width="60%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                    <tr>
                                        <th >Mobile No : <font color="red">*</font></th>
                                        <td colspan="2">${maskmobile}
                                            <html:hidden  property="status" styleId="mobile" value="${mobile}"/>
                                            <html:hidden  property="userName" styleId="userName" value="${userName}"/>
                                        </td>
                                    </tr>  
                                    <tr>
                                        <th>Enter OTP recevied to your registered Mobile No<font color="red">*</font></th>
                                        <td><html:text  property="passwordNew" styleId="passwordNew" maxlength="5" onkeypress='return onlyNumbers(event);' /></td>
                                        <td><input onkeypress='return onlyNumbers(event);' type="button" onclick="return validateOtp();" value="SUBMIT" style="text-align: center;" /></td></tr>
                                </table>
                        </logic:present>    


                    </html:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>                                       