<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/style1.css">
        <style>
            .ui-datepicker-calendar thead{
                background-color:#2dc1c9 !important;
            }
            .feedback-form {
                padding: 22px 0px;
                min-height: 384px;
            }
        </style>
        <script type="text/javascript">
            function space(evt, thisvalue) {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function telephoneValidation() {
                var mobileNo = $("#mobile").val();
                var len = mobileNo.length;
                if (len < 10) {
                    alert("Mobile Number Should be 10 digits");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim().charAt(0) != '9'
                        && $("#mobile").val().trim().charAt(0) != '8' &&
                        $("#mobile").val().trim().charAt(0) != '7' &&
                        $("#mobile").val().trim().charAt(0) != '6')) {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allow Numbers Only");
                    return false;
                }
                return true;
            }
            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
            });

            function getData() {
                if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter Registration No");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#grade1").val().length != 10) {
                    $("#grade1").val("");
                    alert("Please Enter Valid Registered Mobile Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#grade1").val() == "0" || $("#grade1").val() == "") {
                    alert("Please Enter Registered Mobile Number");
                    $("#grade1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    document.forms[0].mode.value = "getDownloadHallticket";
                    document.forms[0].submit();
                }
            }

        </script>
        <script type="text/javascript">
            function blockSpecialChar(e) {
                var k = e.keyCode;
                if (k == 36 || k == 44 || k == 39 || k == 38) {
                    return false;
                }

            }


        </script>
    </head>
    <body>
        <div class="page-title title-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Download Hall Ticket</h1>
                        <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
                            <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                            <span>Download Hall Ticket</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feedback-form">
            <div class="container">
                <div class="row">
                    <h3 class="block-head-News">DTOs Office Automation Course Hall Ticket Download</h3>
                    <div class="line-border"></div>
                    <br>
                    <div class="row">
                        <div class=" col-md-12">
                            <!--<div class="login-form">-->
                            <div>
                                <html:form action="/downloadBeforeLoginHallticketCCIC"  method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <logic:present name="result1">
                                        <span id="msg"><center><font color="red" style="font-weight: bold">${result1}</font></center></span>
                                    </logic:present><br>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Registration No<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:text  styleClass="form-control" property="aadhar1" styleId="aadhar1" maxlength="20"  onkeydown="return space(event, this);"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Registered Mobile No<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:text  styleClass="form-control" property="grade1" styleId="grade1" maxlength="10"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group row">
                                            <input type="button" onclick="getData();" value="DOWNLOAD" class="btn btn-primary"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </html:form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div> 
</div>
</body>
</html>