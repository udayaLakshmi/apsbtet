
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <style>
            ul {
    list-style-type: none;
}
        </style>
        <script>
            function paymentInsert(id) {
                if (id != null) {
                    document.forms[0].mode.value = "payment";
                    document.forms[0].submit();
                }

            }
        </script>
    <div class="page-title title-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Registration </h1>
                    <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">

                        <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                        <span>Registration</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="feedback-form">
        <div class="container">
            <div class="row">
<br/>
<br/>
                <h3 class="block-head-News">Payment Details</h3>
                <div class="line-border"></div>
                <br>
                <div class="row">
                    <div class=" col-md-12">
                        <!--<div class="login-form">-->
                        <div>
                            <html:form action="/dtoregister"  method="post" enctype="multipart/form-data">
                               <html:hidden property="mode"/>
                               <html:hidden property="grade" value="${grade}"/>
                                <logic:present name="errMsg">
                                    <p style="text-align: center;color:green;font-weight: bold" > ${errMsg}</p>
                                    <br/>
                                </logic:present>
                                <logic:present name="result1">
                                    <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                                    <br/>
                                    </logic:present>
<br/>
                                <!--                                    <h5 class="block-head-News">Personal Details (As per SSC Certificate)</h5>
                                                                    <div class="line-border"></div>-->
                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Aadhar No  </label>
                                            <div class="col-sm-8">
                                                <html:text property="fname" styleId="fname" value="  ${aadhaar}" styleClass="form-control" readonly="true"/>
                                                <html:hidden styleClass="form-control" property="aadhar" styleId="aadhar"  value="${aadhar}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label"> Name  of the Candidate</label>
                                            <div class="col-sm-8">
                                                <html:text property="bname" styleId="bname" value="${bname}" readonly="true" styleClass="form-control"/>
                                                <%--<html:text  styleClass="form-control" property="fname" styleId="fname" maxlength="150" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');"/>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Mobile Number </label>
                                            <div class="col-sm-8">
                                                <html:text property="mobile" styleId="mobile" value=" ${mobile}"  readonly="true" styleClass="form-control"/>
                                                <%--<html:text property="dob" styleId="dob" styleClass="dob" readonly="true" />--%>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Amount<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:text property="ecenter" styleId="ecenter" value="  ${amount}"  styleClass="form-control" readonly="true"/>
                                                <html:hidden property="amount" styleId="amount" value='${amount}' styleClass="form-control"/> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Payment Mode<font color="red">*</font> </label>
                                            <div class="col-sm-8">
                                                <ul>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="DC" checked="checked"> Debit Card </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="CC" > Credit Card </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBS" >Net Banking (SBI) </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBI"> Net Banking (ICICI) </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBH"> Net Banking (HDFC) </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="EBS"> Net Banking (Others)</li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="PTM"> Net Banking (Paytm)</li>
                                                </ul>

                                                <!--<textarea rows="3" class="form-control"></textarea>-->

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                
                               
                                <br/>

                                <div class="row">
                                    <div class="col-md-12 center">
                                        <!--<center><a class="submit" href="#">Pay</a></center>-->
                                        <center> <input type="submit" value="Pay" class="submit" style="max-width: 96px;font-size:24px;" onclick="paymentInsert('${aadhar}');"/></center>
                                   <br/>
                                   <br/>
                                    </div>
                                </div>
                            </div>

                        </html:form>
                    </div>
                </div> 
            </div>
        </div>
    </div>	
</div>
</body>
</html>
