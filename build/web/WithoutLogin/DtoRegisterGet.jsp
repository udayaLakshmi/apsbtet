
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>
        <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <style>
            .ui-datepicker-calendar thead{
                background-color:#2dc1c9 !important;
            }
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }
            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit{
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
            input[type="text"] {
                width: 100%;
                padding: 8px;
                border: 1px solid #ccc;
            }
            .form-control {
                background: #fff !important;
            }

        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
                $("form").attr('autocomplete', 'off');
                $("#highdiv").hide();
                $("#brDiv").show();
                $("#aadharDiv").hide();
                $(".viifile").hide();
                $("#myImg").hide();
//                $('input[type=text], textarea').bind("cut copy paste", function(e) {
//                    alert("Cut copy paste not allowed here");
//                    e.preventDefault();
//                });
                $("#startDiv").show();
                $("#DistrictDiv").show();
                $("#DistrictDiv1").hide();
                $("#MandalDiv").show();
                $("#MandalDiv1").hide();
                $(".MainDiv").show();
                $('.sscPass').show();
                $('.InterIIT').hide();
                $('.degreeWithChe').hide();
                $('.InterPass').hide();
                $('.anyDegree').hide();
                $('.sscPassORFailOREqil').hide();
                $('.yogaPass').hide();
                $(function() {
                    $("#photo").change(function() {
                        $("#myImg").show();
                        if (this.files && this.files[0]) {
                            var reader = new FileReader();
                            reader.onload = imageIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                    $("#dialog").dialog({
                        autoOpen: false,
                        modal: true,
                        width: 600,
                        height: 500,
                        closeText: "X",
                        position: {my: "center top", at: "center top+120px", of: window}
//                        my: 'center top-1000', 
//                        at: 'center top-100' 


//                            show: {
//                    effect: "slide",
//                            duration: 1500
//                            },
//                            hide: {
//                    effect: "fade",
//                            duration: 1000
//                            }
                    });
                });
                function imageIsLoaded(e) {
                    $('#myImg').attr('src', e.target.result);
                }
                var angle = 0;
                $(".toggle").click(function() {
                    angle += 90;
                    $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
//                    $("#imageview").toggleClass('flip');
                });

  $('#bname').keyup(function() {
            this.value = this.value.toUpperCase();
        });

            });
            function fileupload(evt, thisvalue) {

                var files = $('#' + thisvalue).prop("files")
                if (files && files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded1;
                    reader.readAsDataURL(files[0]);
                }
            }
            function imageIsLoaded1(e) {
                $('#imageview').attr('src', e.target.result);
                $("#dialog").dialog("open");
            }
            function space(evt, thisvalue)
            {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allows only Numerics");
                    return false;
                }
                return true;
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function telephoneValidation() {

                var mobileNo = $("#mobile").val();
                var len = mobileNo.length;
                if (len < 10) {
                    alert("Mobile Number Should be 10 digits");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim()) == '9999999999'
                        || ($("#mobile").val().trim()) == '8888888888' ||
                        ($("#mobile").val().trim()) == '7777777777' ||
                        ($("#mobile").val().trim()) == '6666666666') {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim().charAt(0) != '9'
                        && $("#mobile").val().trim().charAt(0) != '8' &&
                        $("#mobile").val().trim().charAt(0) != '7' &&
                        $("#mobile").val().trim().charAt(0) != '6')) {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
            }
            function CheckfilePdfOrOther(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 1001) {
                        return true;
                    } else {
                        alert("File size should be less than 1MB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 1MB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }
            function CheckfilePdfOrOther40kb(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 41) {
                        return true;
                    } else {
                        alert("File size should be less than 40KB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 40KB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }
            function examinationDistricts() {
                var data = "district=" + $("#apdistrict").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./dtoregister.do?mode=getCenters&district=" + $("#apdistrict").val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#ecenter").empty().append(response);
                        $("#ecenter").val();
                    },
                    error: function(e) {
                    }
                });
            }

            function getDistricts() {
                $("#district").val("0");
                $("#mandal").val("0");
                $("#district1").val("");
                $("#mandal1").val("");
                if (($("#state").val() == "Andra Pradesh") || ($("#state").val() == "Telagana")) {
                    $("#DistrictDiv").show();
                    $("#DistrictDiv1").hide();
                    $("#MandalDiv").show();
                    $("#MandalDiv1").hide();
                    var data = "district=" + $("#state").val().trim();
                    $.ajax({
                        type: "POST",
                        url: "./dtoregister.do?mode=getDistrictListAt&state=" + $("#state").val().trim(),
                        data: data,
                        cache: true,
                        contentType: true,
                        processData: true,
                        success: function(response) {
                            $("#district").empty().append(response);
                        },
                        error: function(e) {
                        }
                    });
                } else {
                    $("#DistrictDiv").hide();
                    $("#DistrictDiv1").show();
                    $("#MandalDiv").hide();
                    $("#MandalDiv1").show();
                }
            }
            function getMandals() {
                var data = "district=" + $("#state").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./dtoregister.do?mode=getMandalsAt&state=" + $("#state").val().trim() + "&district=" + $("#district").val().trim(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#mandal").empty().append(response);

                    },
                    error: function(e) {
                    }
                });
            }

            function isEmail() {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test($("#email").val())) {
                    $("#email").val("");
                    alert("Invalid eMail")
                    return true;  //wrong mail
                } else {
                    return false;  //correct mail

                }
            }
            function isPincode() {
                var pincode = $("#pincode").val();
                if (pincode.length < 6) {
                    $("#pincode").val("");
                    alert("Pincode must be 6 digits")
                    return true;  //wrong mail
                } else if (pincode == "000000") {
                    $("#pincode").val("");
                    alert("Invalid Pincode")
                    return true;
                }
                else {
                    return false;

                }
            }

            function adharDuplicatevalidation() {

                var paraData = "aadhaar=" + $("#aadhaar").val();
                $.ajax({
                    type: "POST",
                    url: "dtoregister.do?mode=validatingaadharNum&aadharNum=" + $("#aadhar").val() + "&mobile=" + $("#mobile").val() + "&email=" + $("#email").val(),
                    data: paraData,
                    success: function(response) {
                        if (response != 0) {
                            alert("Mobile Number already Registered for another Aadhar")
                            $("#mobile").val("")
//                            $("#email").val("")

                        } else {
                        }
                    }


                });

            }

            var angle = 0;
            function rotate() {
                angle += 90;
                $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
            }
            function firstLettterCheck(val, id) {
                var firstChar = val.charAt(0);
                var letters = /^[A-Za-z]+$/;
                if (firstChar.match(letters)) {
                    return true;
                } else {
                    alert("First Character Must be Letter");
                    $("#" + id).val("");
                    return false;
                }
            }
        </script>

        <script type="text/javascript">
            function SubmitForm() {
                if (document.getElementById("declaration").checked === false) {
                    alert("Accept Declaration ");
                    $("#declaration").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#bname").val() === undefined || $("#bname").val() === "") {
                    alert("Please enter Name of the applicant");
                    $("#bname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#fname").val() === undefined || $("#fname").val() === "") {
                    alert("Please enter Father Name of the applicant");
                    $("#fname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#dob").val() === undefined || $("#dob").val() === "") {
                    alert("Select  Date of Birth");
                    $("#dob").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#gender").val() === undefined || $("#gender").val() === "" || $("#gender").val() === "0") {
                    alert("Please Select Gender.");
                    $("#gender").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#aadhar").val() === undefined || $("#aadhar").val() === "" && $("#aadhar").val().length < 12) {
                    alert("Enter  valid  Aadhar Number");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#category").val() === undefined || $("#category").val() === "" || $("#category").val() === "0") {
                    alert("Select Category");
                    $("#category").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#apdistrict").val() === undefined || $("#apdistrict").val() === "" || $("#apdistrict").val() === "0") {
                    alert("Select Studied District");
                    $("#apdistrict").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#institute").val() === undefined || $("#institute").val() === "" || $("#institute").val() === "0") {
                    alert("Select Institution");
                    $("#institute").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#course").val() === undefined || $("#course").val() === "" || $("#course").val() === "0") {
                    alert("Select Course");
                    $("#course").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#category").val() === "Backlog" && $("#subjectCode").val() === undefined) || ($("#category").val() === "Backlog" && $("#subjectCode").val() === "") || ($("#category").val() === "Backlog" && $("#subjectCode").val() === "0")) {
                    alert("Select Subject");
                    $("#subjectCode").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#apdistrict").val() === undefined || $("#apdistrict").val() === "" || $("#apdistrict").val() === "0") {
                    alert("Select Studied District");
                    $("#apdistrict").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#ecenter").val() === undefined || $("#ecenter").val() === "" || $("#ecenter").val() === "0") {
                    alert("Select Examination Center");
                    $("#ecenter").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#sscflag").val() == "N") && ($("#upload1").val() === undefined || $("#upload1").val() === "")) {
                    alert("Upload ssc/equivalent  Certificate.");
                    $("#upload1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#upload2").val() === undefined || $("#upload2").val() === "") {
                    alert("Upload Service  Certificate issued by DTO ");
                    $("#upload2").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#houseno").val() === undefined || $("#houseno").val() === "" || ($.trim($("#houseno").val()).length === 0)) {
                    alert("Enter Present Address house Number");
                    $("#houseno").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#locality").val() === undefined || $("#locality").val() === "" || ($.trim($("#locality").val()).length === 0)) {
                    alert("Enter Present Address Locality");
                    $("#locality").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#village").val() === undefined || $("#village").val() === "" || ($.trim($("#village").val()).length === 0)) {
                    alert("Enter Present Address Village/Town");
                    $("#village").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#state").val() === undefined || $("#state").val() === "" || $("#state").val() === "0") {
                    alert("Enter  Present Address State");
                    $("#state").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#state").val() == "Andra Pradesh" || $("#state").val() == "Telagana") && ($("#district").val() == "0" || $("#district").val() === "")) {
                    alert("Enter Present Address District");
                    $("#district").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#state").val() == "Andra Pradesh" || $("#state").val() == "Telagana") && ($("#mandal").val() == "0" || $("#mandal").val() === "")) {
                    alert("Enter Present Address Mandal");
                    $("#mandal").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ((($("#state").val() != "Andra Pradesh") && ($("#state").val() != "Telagana")) && ($("#district1").val() == "" || ($.trim($("#district1").val()).length === 0))) {
                    alert("Enter Present Address District");
                    $("#district1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ((($("#state").val() != "Andra Pradesh") && ($("#state").val() != "Telagana")) && ($("#mandal1").val() == "" || ($.trim($("#mandal1").val()).length === 0))) {
                    alert("Enter Present Address Mandal");
                    $("#mandal1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#pincode").val() === undefined || $("#pincode").val() === "" || ($.trim($("#pincode").val()).length === 0)) {
                    alert("Enter Present Address Pincode");
                    $("#pincode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#mobile").val() === undefined || ($.trim($("#mobile").val()).length === 0 ||
                        $("#mobile").val().length < 10 || parseInt($("#mobile").val().trim()) < 6000000000)) {
                    alert("Enter Mobile Number");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#email").val() === undefined || $("#email").val() === "" || ($.trim($("#email").val()).length) < 10) {
                    alert("Enter EMail");
                    $("#email").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#signature").val() === undefined || $("#signature").val() === "") {
                    alert("Upload your Signature.");
                    $("#signature").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#photo").val() === undefined || $("#photo").val() === "") {
                    alert("Upload your Photo.");
                    $("#photo").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    var x = confirm("Do you Want to Proceed?")
                    if (x == false) {
                        return false
                    } else {

                        document.forms[0].mode.value = "submitData";
                        document.forms[0].submit();
                    }
                }
            }
            function showData() {
                $("#bname").val('${bname}')
                $("#fname").val('${fname}')
                $("#dob").val('${dob}')
                $("#dob1").val('${dob}')
                $("#gender").val('${gender}')
                $("#gender1").val('${gender}')
                $("#aadhar").val('${aadhar}')
                $("#sschallticket").val('${sschallticket}')
                $("#sscyear").val('${sscyear}')
                if ('${Examinationstatus}' == "PASS") {
                    $("#sscflag").val("Y");
                    $("#sscdiv").hide();
                    $("#gendercaste").hide();
                    $("#gendercaste1").show();
                    $(".dateDiv1").show();
                    $(".dateDiv").hide();
                    $("#bname").prop('readonly', true);
                    $("#fname").prop('readonly', true);
                } else {
                    $("#sscflag").val("N");
                    $("#gender").val("0");
                    $("#sscdiv").show();
                    $("#gendercaste").show();
                    $("#gendercaste1").hide();
                    $(".dateDiv1").hide();
                    $(".dateDiv").show();
                }
            }


        </script>
        <style>
            .multi-select-button {
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

        </style>
    </head>
    <body onload="showData()">
        <div class="page-title title-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">CCIC </h1>
                        <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
                            <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                            <span>CCIC REGISTRATION FORM </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feedback-form">
            <div class="container">
                <div class="row">
                    <h3 class="block-head-News">CCIC REGISTRATION FORM </h3>
                    <div class="line-border"></div>                   
                    <div class="row">
                        <div class=" col-md-12">
                            <div>
                                <html:form action="/dtoregister" styleId="d" method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <html:hidden property="statusresult"/>
                                    <html:hidden property="sscflag" styleId="sscflag" value="N"/>
                                    <logic:present name="result2">
                                        <center> <font color="green" style="font-weight: bold">${result2}</font>  </center>  
                                        </logic:present>
                                        <logic:present name="result">
                                        <span id="msg"><center><font color="red" style="font-weight: bold">${result}</font></center></span>
                                    </logic:present><br>
                                    <%-- Main Formm  --%>


                                    <h5 class="block-head-News">Personal Details</h5>
                                    <div class="line-border"></div>

                                    <div id="dialog" title="View Photo">
                                        <center><img class="toggle" id="imageview" src="#"  width="500" height="350"/></center>
                                        <br/>
                                        <br/>
                                        <!--<center> <button class="btn btn-success" style="text-align:center;margin: 10px;" onclick="return rotate();">Rotate <i class="fa fa-rotate-right"></i></button</center>-->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Name of the applicant<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control-plaintext" property="bname" styleId="bname" maxlength="50" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Father Name <font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control-plaintext" property="fname" styleId="fname" maxlength="50" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Date of Birth <font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <div class="dateDiv1">
                                                        <html:text property="dob1" styleId="dob1" styleClass="form-control" readonly="true" />
                                                    </div>
                                                    <div class="dateDiv">
                                                        <html:text property="dob" styleId="dob" styleClass="form-control" readonly="true" />
                                                        <Script>
            $(document).ready(function() {
                var today = new Date();
                yrRange = '1960' + ":" + '2008';
                $("#dob").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    maxDate: new Date(2008, 08 - 1, 01),
                    minDate: new Date(1960, 12 - 04, 1),
                    yearRange: yrRange,
                    changeYear: true
                });
            });
                                                        </Script>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="row">
                                            <div id="gendercaste">
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">Gender<font color="red">*</font> </label>
                                                        <div class="col-sm-8">
                                                            <html:select property="gender" styleId="gender" styleClass="form-control">
                                                                <html:option value="0">--Select Gender--</html:option>
                                                                <html:option value="MALE">MALE</html:option>
                                                                <html:option value="FEMALE">FEMALE</html:option>
                                                                <html:option value="TRANSGENDER">TRANSGENDER</html:option>
                                                            </html:select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="gendercaste1">
                                                <div class="col-md-4">

                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">Gender<font color="red">*</font> </label>
                                                        <div class="col-sm-8">
                                                            <html:text property="gender1" styleId="gender1" styleClass="form-control"  readonly="true"/>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Aadhar Number<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="aadhar" styleId="aadhar" maxlength="12" readonly="true"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">SSC HallTicket<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="sscyear" styleId="sscyear"/>
                                                        <html:text  styleClass="form-control-plaintext" property="sschallticket" styleId="sschallticket" maxlength="12"   readonly="true"/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <br/>
                                        <h5 class="block-head-News">Examination Details</h5>
                                        <div class="line-border"></div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Category<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text property="category" styleId="category" value="Regular" readonly="true"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Studied District<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="institute" styleId="institute" value="9999"></html:hidden>
                                                        <html:select property="apdistrict" styleId="apdistrict" styleClass="form-control" onchange="examinationDistricts()">
                                                            <html:option value="0">--Select District--</html:option> 
                                                            <html:optionsCollection property="distLists" label="district_Name" value="district_ID"/>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Course<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="course" styleId="course" value="OA"/>
                                                        <html:text property="coursedesc" styleId="coursedesc" value="Office Automation" styleClass="form-control" readonly="true"/>
                                                        <%-- <html:select property="course" styleId="course" styleClass="form-control" onchange="fileStatus();">
                                                             <html:option value="0">--Select Course--</html:option> 
                                                             <html:optionsCollection property="courseList" label="gname" value="gcode"/>
                                                         </html:select>--%>
                                                    </div>
                                                </div>
                                            </div>




                                        </div>

                                        <div class="row">



                                            <%--      <div class="col-md-4" id="hideSuject">
                                                      <div class="form-group row">
                                                          <label for="name" class="col-sm-4 col-form-label">Subject<font color="red">*</font> </label>
                                                          <div class="col-sm-8">
                                                              <html:select property="subjectCode" styleId="subjectCode" styleClass="form-control" onchange="centersList();">
                                                                  <html:option value="0">--Select Subject--</html:option> 
                                                                  <html:optionsCollection property="subjectList" label="gname" value="gcode"/>
                                                              </html:select>
                                                          </div>
                                                      </div>
                                                  </div>--%>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <html:hidden property="subjectCode" styleId="subjectCode"  value="0"/>
                                                    <label for="name" class="col-sm-4 col-form-label">Examination Center<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:select property="ecenter" styleId="ecenter" styleClass="form-control">
                                                            <html:option value="0">--Select--</html:option>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="sscdiv">
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <div class="sscPass"><label for="name" class="col-sm-4 col-form-label">SSC/Equivalent Certificate<font color="red">*</font></label></div>
                                                        <div class="col-sm-8">
                                                            <html:file property="upload1" styleId="upload1" onchange="return CheckfilePdfOrOther('upload1');"/><a href="#" onclick="fileupload(event, 'upload1')"/>View File</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <div class="sscPass"><label for="name" class="col-sm-4 col-form-label">Service Certificate issued by DTO<font color="red">*</font></label></div>
                                                    <div class="col-sm-8">
                                                        <html:file property="upload2" styleId="upload2" onchange="return CheckfilePdfOrOther('upload2');"/><a href="#" onclick="fileupload(event, 'upload2')"/>View File</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>

                                        <h5 class="block-head-News">Communication Details</h5>
                                        <div class="line-border"></div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">House No<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="houseno" styleId="houseno" maxlength="250" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Street<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="locality" styleId="locality" maxlength="250" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');" onchange="firstLettterCheck(this.value,this.id)"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Village/Town<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="village" styleId="village" maxlength="100" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');" onchange="firstLettterCheck(this.value,this.id)"/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">State<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="state1" styleId="state1" ></html:hidden>
                                                        <html:select property="state" styleId="state" styleClass="form-control" onchange="getDistricts()">
                                                            <html:option value="0">--Select State--</html:option> 
                                                            <html:optionsCollection property="stateList" label="sname" value="scode"/>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="DistrictDiv" class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">District<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:select property="district" styleId="district" styleClass="form-control" onchange="getMandals()">
                                                            <html:option value="0">--Select--</html:option>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="MandalDiv" class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Mandal<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:select property="mandal" styleId="mandal" styleClass="form-control" >
                                                            <html:option value="0">--Select--</html:option>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="DistrictDiv1" class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">District<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="district1" styleId="district1" maxlength="100" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="MandalDiv1" class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Mandal<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="mandal1" styleId="mandal1"   maxlength="100"  onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Pincode<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="pincode" styleId="pincode" maxlength="6" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="isPincode()"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Mobile No<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="mobile" styleId="mobile"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="telephoneValidation(),adharDuplicatevalidation()" maxlength="10"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">E-Mail<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="email" styleId="email"  onkeydown="return space(event, this);" maxlength="100" onchange='return isEmail(this)'/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Upload Signature (should not be more than 40KB)<font color="red">*</font></label>
                                                    <div class="col-sm-8">                                           
                                                        <html:file property="signature" styleId="signature" onchange="return CheckfilePdfOrOther40kb('signature');"/><a href="#" onclick="fileupload(event, 'signature')"/>View File</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Upload Photo (Should not be more than 40KB)<font color="red">*</font></label>
                                                    <div class="col-sm-8">                                           
                                                        <html:file property="photo" styleId="photo" onchange="return CheckfilePdfOrOther40kb('photo');"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-8">                                           
                                                        <center>  <img id="myImg" src="#"  width="100" height="70"/></center>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <br/>

                                        <div><input type="checkbox" id="declaration" style="max-width: 15px;max-height: 15px;font-size:24px;"/>&nbsp;&nbsp;&nbsp;I promise to abide by the rules/regulations and the orders of the CCIC, its Authorities and Officers.I do hereby declare that the information furnished in this application is true to the best of my knowledge and belief.
                                            I am aware that in the event of any information being found to be false or untrue, I shall be liable to such action by CCIC.
                                            <br/>
                                        </div>

                                        <br/>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-12 center">
                                                <center><input type="button" onclick="return SubmitForm();" value="SUBMIT"  class="submit"/></center>
                                                <br/>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <br/>

                            </html:form>
                        </div>
                    </div> 
                </div>	
                </body>
                </html>