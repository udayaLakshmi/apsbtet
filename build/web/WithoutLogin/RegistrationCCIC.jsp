
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>
        <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <style>
            .ui-datepicker-calendar thead{
                background-color:#2dc1c9 !important;
            }
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }
            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit{
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
            input[type="text"] {
                width: 100%;
                padding: 8px;
                border: 1px solid #ccc;
            }
            .form-control {
                background: #fff !important;
            }
            textarea {
                overflow: auto;
                width: 246px;
            }
            label
            {
                font-size: 13px !important;
            }

        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
                $("form").attr('autocomplete', 'off');
                $("#highdiv").hide();
                $("#brDiv").show();
                $("#aadharDiv").hide();
                $(".viifile").hide();
//                $('input[type=text], textarea').bind("cut copy paste", function(e) {
//                    alert("Cut copy paste not allowed here");
//                    e.preventDefault();
//                });
                $("#startDiv").show();
                $("#DistrictDiv").show();
                $("#DistrictDiv1").hide();
                $("#MandalDiv").show();
                $("#MandalDiv1").hide();
                $("#yesDiv").hide();
                $("#noDiv").hide();
                $(".blindDiv").hide();
                $(function() {
                    $("#photo").change(function() {
                        $("#myImg").show();
                        if (this.files && this.files[0]) {
                            var reader = new FileReader();
                            reader.onload = imageIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                    $("#dialog").dialog({
                        autoOpen: false,
                        modal: true,
                        width: 600,
                        height: 500,
                        closeText: "X",
                        position: {my: "center top", at: "center top+120px", of: window}
//                        my: 'center top-1000', 
//                        at: 'center top-100' 


//                            show: {
//                    effect: "slide",
//                            duration: 1500
//                            },
//                            hide: {
//                    effect: "fade",
//                            duration: 1000
//                            }
                    });
                });
                function imageIsLoaded(e) {
                    $('#myImg').attr('src', e.target.result);
                }
                var angle = 0;
                $(".toggle").click(function() {
                    angle += 90;
                    $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
//                    $("#imageview").toggleClass('flip');
                });

                $('#bname').keyup(function() {
                    this.value = this.value.toUpperCase();
                });

            });
            function fileupload(evt, thisvalue) {

                var files = $('#' + thisvalue).prop("files")
                if (files && files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded1;
                    reader.readAsDataURL(files[0]);
                }
            }
            function imageIsLoaded1(e) {
                $('#imageview').attr('src', e.target.result);
                $("#dialog").dialog("open");
            }
            function space(evt, thisvalue) {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allows only Numerics");
                    return false;
                }
                return true;
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function telephoneValidation() {

                var mobileNo = $("#mobile").val();
                var len = mobileNo.length;
                if (len < 10) {
                    alert("Mobile Number Should be 10 digits");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim()) == '9999999999'
                        || ($("#mobile").val().trim()) == '8888888888' ||
                        ($("#mobile").val().trim()) == '7777777777' ||
                        ($("#mobile").val().trim()) == '6666666666') {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim().charAt(0) != '9'
                        && $("#mobile").val().trim().charAt(0) != '8' &&
                        $("#mobile").val().trim().charAt(0) != '7' &&
                        $("#mobile").val().trim().charAt(0) != '6')) {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
            }
            function validateAdharDetails(aadharId) {
                if ($("#" + aadharId).val() == "") {
                    $("#" + aadharId).val("");
                    alert("Please Enter Aadhaar Card Number");
                    $("#" + aadharId).focus().css({'border': '1px solid red'});
                } else if ($("#" + aadharId).val().length != 12) {
                    $("#" + aadharId).val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#" + aadharId).focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#" + aadharId).val() == "999999999999" || $("#" + aadharId).val() == "333333333333") {
                    $("#" + aadharId).val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#" + aadharId).focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#" + aadharId).val()) != true) {
                    $("#" + aadharId).val("");
                    $("#" + aadharId).focus().css({'border': '1px solid red'});
                    return false;
                }
            }
            function CheckfilePdfOrOther(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 1001) {
                        return true;
                    } else {
                        alert("File size should be less than 1MB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 1MB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }
            function CheckfilePdfOrOther40kb(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 41) {
                        return true;
                    } else {
                        alert("File size should be less than 40KB only");
                        if (myval === "photo")
                            $("#myImg").hide();
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 40KB");
                    $("#" + myval).val('').clone(true);
                    if (myval === "photo")
                        $("#myImg").hide();
                    $("#" + myval).focus();
                    return false;
                }
            }

            function fileStatus() {
                $("#upload1").val("");
                $("#upload2").val("");
                var course1 = $("#course1").val();
                if (course1 !== "0") {
                    var course = $("#course1").val();
                    if (course == "OA" || course == "PC" || course == "MA" || course == "WD" || course == "FP" || course == "FO" || course == "FB" || course == "AO" || course == "FD" || course == "FG" || course == "ID") {
                        $("#lowdiv").show();
                        $('.sscPass').show();
                        $('.InterIIT').hide();
                        $('.degreeWithChe').hide();
                        $('#degreeWithCheExp').hide();
                        $('.InterPass').hide();
                        $('.anyDegree').hide();
                        $('.sscPassORFailOREqil').hide();
                        $('.yogaPass').hide();
                    }
                    else if (course == "AC") {
                        $("#lowdiv").show();
                        $('.sscPass').hide();
                        $('.InterIIT').show();
                        $('.degreeWithChe').hide();
                        $('#degreeWithCheExp').hide();
                        $('.InterPass').hide();
                        $('.anyDegree').hide();
                        $('.sscPassORFailOREqil').hide();
                        $('.yogaPass').hide();
                    }
                    else if (course == "IS") {
                        $("#lowdiv").show();
                        $('.sscPass').hide();
                        $('.InterIIT').hide();
                        $('.degreeWithChe').show();
                        $('#degreeWithCheExp').show();
                        $('.InterPass').hide();
                        $('.anyDegree').hide();
                        $('.sscPassORFailOREqil').hide();
                        $('.yogaPass').hide();
                    }
                    else if (course == "FS" || course == "YO") {
                        $("#lowdiv").show();
                        $('.sscPass').hide();
                        $('.InterIIT').hide();
                        $('.degreeWithChe').hide();
                        $('#degreeWithCheExp').hide();
                        $('.InterPass').show();
                        $('.anyDegree').hide();
                        $('.sscPassORFailOREqil').hide();
                        $('.yogaPass').hide();
                    } else if (course == "CS") {
                        $("#lowdiv").show();
                        $('.sscPass').hide();
                        $('.InterIIT').hide();
                        $('.degreeWithChe').hide();
                        $('#degreeWithCheExp').hide();
                        $('.InterPass').hide();
                        $('.anyDegree').show();
                        $('.sscPassORFailOREqil').hide();
                        $('.yogaPass').hide();
                    } else if (course == "LS") {
                        $("#lowdiv").show();
                        $('.sscPass').hide();
                        $('.InterIIT').hide();
                        $('.degreeWithChe').hide();
                        $('#degreeWithCheExp').hide();
                        $('.InterPass').hide();
                        $('.anyDegree').hide();
                        $('.sscPassORFailOREqil').show();
                        $('.yogaPass').hide();
                    } else if (course == "AY") {
                        $("#lowdiv").show();
                        $('.sscPass').hide();
                        $('.InterIIT').hide();
                        $('.degreeWithChe').hide();
                        $('#degreeWithCheExp').hide();
                        $('.InterPass').hide();
                        $('.anyDegree').hide();
                        $('.sscPassORFailOREqil').hide();
                        $('.yogaPass').show();
                    }
                } else if (course1 == "0") {
                    $("#lowdiv").hide();
                    $('.sscPass').hide();
                    $('.InterIIT').hide();
                    $('.degreeWithChe').hide();
                    $('#degreeWithCheExp').hide();
                    $('.InterPass').hide();
                    $('.anyDegree').hide();
                    $('.sscPassORFailOREqil').hide();
                    $('.yogaPass').show();
                }
            }
            function examinationDistricts() {
                var data = "district=" + $("#apdistrict").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./registerCCIC.do?mode=getCentersDistrict&examination=" + $("#examination").val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        alert(response);
                        $("#apdistrict").empty().append(response);
                        $("#apdistrict").val(${loginDistrict});
                    },
                    error: function(e) {
                    }
                });
            }

            function getDistricts() {
                $("#district").val("0");
                $("#mandal").val("0");
                $("#district1").val("");
                $("#mandal1").val("");
                if (($("#state").val() == "Andra Pradesh") || ($("#state").val() == "Telagana")) {
                    $("#DistrictDiv").show();
                    $("#DistrictDiv1").hide();
                    $("#MandalDiv").show();
                    $("#MandalDiv1").hide();
                    var data = "district=" + $("#state").val().trim();
                    $.ajax({
                        type: "POST",
                        url: "./registerCCIC.do?mode=getDistrictListAt&state=" + $("#state").val().trim(),
                        data: data,
                        cache: true,
                        contentType: true,
                        processData: true,
                        success: function(response) {
                            $("#district").empty().append(response);
                        },
                        error: function(e) {
                        }
                    });
                } else {
                    $("#DistrictDiv").hide();
                    $("#DistrictDiv1").show();
                    $("#MandalDiv").hide();
                    $("#MandalDiv1").show();
                }
            }
            function getMandals() {
                var data = "district=" + $("#state").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./registerCCIC.do?mode=getMandalsAt&state=" + $("#state").val().trim() + "&district=" + $("#district").val().trim(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#mandal").empty().append(response);

                    },
                    error: function(e) {
                    }
                });
            }

            function isEmail() {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test($("#email").val())) {
                    $("#email").val("");
                    alert("Invalid eMail")
                    return true;  //wrong mail
                } else {
                    return false;  //correct mail

                }
            }
            function isPincode() {
                var pincode = $("#pincode").val();
                if (pincode.length < 6) {
                    $("#pincode").val("");
                    alert("Pincode must be 6 digits")
                    return true;  //wrong mail
                } else if (pincode == "000000") {
                    $("#pincode").val("");
                    alert("Invalid Pincode")
                    return true;
                }
                else {
                    return false;

                }
            }

            function adharDuplicatevalidation() {

                var paraData = "aadhaar=" + $("#aadhaar").val();
                $.ajax({
                    type: "POST",
                    url: "registerCCIC.do?mode=validatingaadharNum&aadharNum=" + $("#aadhar").val() + "&mobile=" + $("#mobile").val() + "&email=" + $("#email").val(),
                    data: paraData,
                    success: function(response) {
                        if (response != 0) {
                            alert("Mobile Number already Registered for another Aadhar")
                            $("#mobile").val("")
//                            $("#email").val("")
                        } else {
                        }
                    }
                });
            }
            function getTimeStatus() {
                if ($("#grade").val() == "TEL" || $("#grade").val() == "TTL" || $("#grade").val() == "THL" || $("#grade").val() == "TUL" ||
                        $("#grade").val() == "TEH" || $("#grade").val() == "TTH" || $("#grade").val() == "THH" || $("#grade").val() == "TUH" || $("#grade").val() == "TEJ" || $("#grade").val() == "TEHS" || $("#grade").val() == "TTHS") {
                    var paraData = "aadhaar=" + $("#aadhaar1").val();
                    $.ajax({
                        type: "POST",
                        url: "registerCCIC.do?mode=getTimeStatus&aadharNum=" + $("#aadhar").val() + "&date=" + $("#edate").val() + "&batch=" + $("#ebatch").val() + "&grade=" + $("#grade").val(),
                        data: paraData,
                        success: function(response) {
                            if (response == 1) {
                                alert("Already Applied in this Batch For Other Language Examination.")
                                $("#ebatch").val("0");
                                $("#edate").val("0");
//                                $("#grade").val("0");
                            }
                        }
                    });
                }
            }
            var angle = 0;
            function rotate() {
                angle += 90;
                $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
            }
            function firstLettterCheck(val, id) {
                var firstChar = val.charAt(0);
                var letters = /^[A-Za-z]+$/;
                if (firstChar.match(letters)) {
                    return true;
                } else {
                    alert("First Character Must be Letter");
                    $("#" + id).val("");
                    return false;
                }
            }
        </script>

        <script type="text/javascript">
            function getData() {
                if ($("#hallticket").val() == "") {
                    alert("Enter Your Old HallTicket No");
                    $("#hallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar2").val().length != 12) {
                    $("#aadhar2").val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#aadhar2").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar2").val() == "999999999999" || $("#aadhar2").val() == "333333333333") {
                    $("#aadhar2").val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#aadhar2").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#aadhar2").val()) != true) {
                    $("#aadhar2").val("");
                    $("#aadhar2").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if ($("#course1").val() == "0" || $("#course1").val() == undefined) {
//                    alert("Enter Your Registration Number");
//                    $("#hallticket").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else {
                    document.forms[0].mode.value = "editPage";
                    document.forms[0].submit();
                }
            }

            function SubmitForm() {
                if ($("#bname").val() === undefined || $("#bname").val() === "") {
                    alert("Please enter Name of the applicant");
                    $("#bname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#fname").val() === undefined || $("#fname").val() === "") {
                    alert("Please enter Father Name of the applicant");
                    $("#fname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#dob").val() === undefined || $("#dob").val() === "") {
                    alert("Select  Date of Birth");
                    $("#dob").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#gender").val() === undefined || $("#gender").val() === "" || $("#gender").val() === "0") {
                    alert("Please Select Gender.");
                    $("#gender").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#aadhar").val() === undefined || $("#aadhar").val() === "" && $("#aadhar").val().length < 12) {
                    alert("Enter  valid  Aadhar Number");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#examinationname").val() === undefined || $("#examinationname").val() === "" || $("#examinationname").val() === "0") {
                    alert("Select Category");
                    $("#examinationname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#apdistrict").val() === undefined || $("#apdistrict").val() === "" || $("#apdistrict").val() === "0") {
                    alert("Select Studied District");
                    $("#apdistrict").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#institutionCode").val() === undefined || $("#institutionCode").val() === "" || $("#institutionCode").val() === "0") {
                    alert("Select Institution");
                    $("#institutionCode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#course").val() === undefined || $("#course").val() === "" || $("#course").val() === "0") {
                    alert("Select Course");
                    $("#course").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#examinationname").val() === "Backlog" && $("#subjectCode").val() === undefined) || ($("#examinationname").val() === "Backlog" && $("#subjectCode").val() === "") || ($("#examinationname").val() === "Backlog" && $("#subjectCode").val() === "0")) {
                    alert("Select Subject");
                    $("#subjectCode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#ecenter").val() === undefined || $("#ecenter").val() === "" || $("#ecenter").val() === "0") {
                    alert("Select Examination Center");
                    $("#ecenter").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#attendance").val() === undefined || $("#attendance").val() === "") {
                    alert("Enter Attendance");
                    $("#attendance").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#sscflag").val() == "N") && ($("#fileFlag").val() == "3") && ($("#upload1").val() === undefined || $("#upload1").val() === "")) {
                    alert("Upload SSC Certificate.");
                    $("#upload1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#interflag").val() == "N") && ($("#fileFlag").val() == "4") && ($("#upload1").val() === undefined || $("#upload1").val() === "")) {
                    alert("Upload InterMediate Certificate.");
                    $("#upload1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#course").val() === "IS" && ($("#qualification").val() === undefined || $("#qualification").val() === "0")) {
                    alert("Please Select Qualification");
                    $("#qualification").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#course").val() === "IS" && ($("#upload1").val() === undefined || $("#upload1").val() === "")) {
                    alert("Upload Certificate.");
                    $("#upload1").focus().css({'border': '1px solid red'});
                    return false;
                }
                
                else if ($("#course").val() === "IS" && ($("#qualification").val() === "DEGREE" || $("#qualification").val() === "DIPLOMA")
                        && ($("#upload2").val() == "")) {
                    alert("Upload Atleast One Experience Certificate.");
                    $("#upload2").focus().css({'border': '1px solid red'});
                    return false;
                }
                
                else if ($("#course").val() === "IS" && ($("#qualification").val() === "DEGREE" || $("#qualification").val() === "DIPLOMA")
                        && ($("#upload2").val() == "" && $("#upload3").val() === "" && $("#upload4").val() === "" && $("#upload5").val() === "")) {
                    alert("Upload Atleast One Experience Certificate.");
                    $("#upload2").focus().css({'border': '1px solid red'});
                    return false;
                }

                else if ($("#houseno").val() === undefined || $("#houseno").val() === "" || ($.trim($("#houseno").val()).length === 0)) {
                    alert("Enter Present Address house Number");
                    $("#houseno").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#locality").val() === undefined || $("#locality").val() === "" || ($.trim($("#locality").val()).length === 0)) {
                    alert("Enter Present Address Locality");
                    $("#locality").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#village").val() === undefined || $("#village").val() === "" || ($.trim($("#village").val()).length === 0)) {
                    alert("Enter Present Address Village/Town");
                    $("#village").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#state").val() === undefined || $("#state").val() === "" || $("#state").val() === "0") {
                    alert("Enter  Present Address State");
                    $("#state").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#state").val() == "Andra Pradesh" || $("#state").val() == "Telagana") && ($("#district").val() == "0" || $("#district").val() === "")) {
                    alert("Enter Present Address District");
                    $("#district").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#state").val() == "Andra Pradesh" || $("#state").val() == "Telagana") && ($("#mandal").val() == "0" || $("#mandal").val() === "")) {
                    alert("Enter Present Address Mandal");
                    $("#mandal").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ((($("#state").val() != "Andra Pradesh") && ($("#state").val() != "Telagana")) && ($("#district1").val() == "" || ($.trim($("#district1").val()).length === 0))) {
                    alert("Enter Present Address District");
                    $("#district1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ((($("#state").val() != "Andra Pradesh") && ($("#state").val() != "Telagana")) && ($("#mandal1").val() == "" || ($.trim($("#mandal1").val()).length === 0))) {
                    alert("Enter Present Address Mandal");
                    $("#mandal1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#pincode").val() === undefined || $("#pincode").val() === "" || ($.trim($("#pincode").val()).length === 0)) {
                    alert("Enter Present Address Pincode");
                    $("#pincode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#mobile").val() === undefined || ($.trim($("#mobile").val()).length === 0 ||
                        $("#mobile").val().length < 10 || parseInt($("#mobile").val().trim()) < 6000000000)) {
                    alert("Enter Mobile Number");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#email").val() === undefined || $("#email").val() === "" || ($.trim($("#email").val()).length) < 10) {
                    alert("Enter eMail");
                    $("#email").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#signature").val() === undefined || $("#signature").val() === "") {
                    alert("Upload your Signature.");
                    $("#signature").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#photo").val() === undefined || $("#photo").val() === "") {
                    alert("Upload your Photo.");
                    $("#photo").focus().css({'border': '1px solid red'});
                    return false;
                } else if (document.getElementById("declaration").checked === false) {
                    alert("Accept Declaration ");
                    $("#declaration").focus().css({'border': '1px solid red'});
                    return false;
                } else {
                    var x = confirm("Do you Want to Proceed?")
                    if (x == false) {
                        return false
                    } else {
                        document.forms[0].mode.value = "submitData";
                        document.forms[0].submit();
                    }
                }
            }

            function getInstituteList(district) {
                var data = "district=" + district;
                $.ajax({
                    type: "POST",
                    url: "./registerCCIC.do?mode=getInstituteByDistrict&district=" + district,
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#institutionCode").empty().append(response);
                        $("#institutionCode").val('${instituteCode}');
                    },
                    error: function(e) {
                    }
                });
            }

            function getCoursesList(district, instituteCode) {
                var data = "instCode=" + instituteCode + "&district=" + district;
                $.ajax({
                    type: "POST",
                    url: "./registerCCIC.do?mode=getCoursesByInstCode&instCode=" + instituteCode + "&district=" + district,
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#course1").empty().append(response);
                        $("#course").empty().append(response);
                    },
                    error: function(e) {
                    }
                });
            }

            function getCoursesList1(district, instituteCode, id) {
                var data = "instCode=" + instituteCode + "&district=" + district + "&hallticket=" + $("#" + id).val();
                $.ajax({
                    type: "POST",
                    url: "./registerCCIC.do?mode=getCoursesByInstCodeAndHallticket&instCode=" + instituteCode + "&district=" + district + "&hallticket=" + $("#" + id).val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {

                        if (response.trim() == "0") {
                            alert("No courses available with given Registration No");
                            $("#course1").empty().append("<option value='0'>--Select Course--</option>");
                        } else {
                            $("#course1").empty().append(response);
                        }

                    },
                    error: function(e) {
                    }
                });
            }

            function centersList(loginDistrict) {
                var data = "district=" + loginDistrict;
                $.ajax({
                    type: "POST",
                    url: "./registerCCIC.do?mode=getCenters&district=" + loginDistrict,
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#ecenter").empty().append(response);
                        $("#ecenter").val('${ccode}');
                    },
                    error: function(e) {
                    }
                });
            }

            function showStatus() {
                var statusflag = $("input[name='statusflag']:checked").val();
                if (statusflag == "1") {
                    $("#category").val("Backlog");
                    $("#examinationname").val("Backlog");
                    $("#startDiv").hide();
                    $("#yesDiv").show();
                    $("#brDiv").show();
                    $("#noDiv").hide();
                    $("#aadharDiv").hide();
                } else if (statusflag == "2") {
                    $('.MainDiv').hide();
                    $("#noDiv").show();
                    $("#brDiv").show();
                    $("#yesDiv").hide();
                    $("#hallticket").val("");
                    $("#startDiv").hide();
                    $("#aadharDiv").show();
                    $("#brDiv").show();
                }
            }

            $(document).ready(function() {

                $('#SSCFLAG').hide();
                $('#INTERFLAG').hide();
                $('#sscyeardiv').hide();
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);

                $('.MainDiv').hide();
                $("#yesDiv").hide();
//                getDistricts();
//                examinationDistricts();
//                getInstituteList('${loginDistrict}');
                getCoursesList('${loginDistrict}', '${instituteCode}');
//                centersList('${loginDistrict}');
                $('#hideSuject').hide();


                $("#lowdiv").hide();
                $('.sscPass').hide();
                $('.InterIIT').hide();
                $('.degreeWithChe').hide();
                $('#degreeWithCheExp').hide();
                $('#degreeWithCheExp1').hide();
                $('.InterPass').hide();
                $('.anyDegree').hide();
                $('.sscPassORFailOREqil').hide();
                $('.yogaPass').hide();

                $("#myImg").hide();

            });

            function getMainDiv() {
                fileStatus();
                $('.MainDiv').show();
                $("#noDiv").hide();
                $("#yesDiv").hide();
                $("#examinationname").val("Regular");
                $("#category").val("Regular");
                $('#aadhar').val($('#aadhar1').val());
                $('#aadhar').attr('readonly', true);
            }

            function getSubjectsByCourses(district, instituteCode, backlogCourseId) {
                if ($("#hallticket").val() == undefined || $("#hallticket").val() == "") {
                    alert("Please Enter Old Registration No");
                    $("#hallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar2").val() == undefined || $("#aadhar2").val() == "") {
                    alert("Please Enter Aadhar No");
                    $("#aadhar2").focus().css({'border': '1px solid red'});
                    return false;
                }
                var backlogCourseId1 = $("#" + backlogCourseId).val().split("-")[1];
                var data = "instCode=" + instituteCode + "&district=" + district + "&backlogCourseId=" + backlogCourseId1 + "&oldHallTicketNo=" + $("#hallticket").val();
                $.ajax({
                    type: "POST",
                    url: "./registerCCIC.do?mode=getSubjectsByCourses&instCode=" + instituteCode + "&district=" + district + "&backlogCourseId=" + backlogCourseId1 + "&oldHallTicketNo=" + $("#hallticket").val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#subjectCode1").empty().append(response.trim());
                    },
                    error: function(e) {
                    }
                });
            }

            function checkCourseStatus() {
                var aadhar = $('#aadhar1').val();
                if ($('#aadhar1').val() == "") {
                    alert("Please Enter Aadhar Number");
                    $('#course1').val("0");
                    $('#aadhar1').focus();
                    return false;
                } else if (validateAdharDetails('aadhar1') == false) {
                    $('#course1').val("0");
                    $('#aadhar1').focus();
                    return false;
                } else {

                    //Clear data
                    //$('#checkbox').attr('checked', true);


                    $("#sschallticket").val("");
                    $("#sscyear").val("0");
                    $("#myImg").hide();
                    $('#gender').val("0");
                    $('#upload1').val("");
                    $('#upload2').val("");
                    $('#photo').val("");
                    $('#signature').val("");
                    $('#bname').val("");
                    $('#fname').val("");
                    $('#dob').val("");
                    $('#attendance').val("");
                    $('#houseno').val("");
                    $('#locality').val("");
                    $('#village').val("");
                    $('#state').val("0");
                    $("#mandal").empty().append("<option value='0'> --Select-- </option>");
                    $("#district").empty().append("<option value='0'> --Select-- </option>");
                    $('#pincode').val("");
                    $('#mobile').val("");
                    $('#email').val("");
                    //Clear Data end

                    var course1 = $('#course1').val();

                    if (course1 == "IS") {
                        $('#degreeWithCheExp').show();
                        $('#degreeWithChe').show();
                        $('#qualification').val("0");
                    } else {
                        $('#degreeWithCheExp').hide();
                        $('#degreeWithCheExp1').hide()
                        $('#degreeWithChe').hide();
                    }

                    if (course1 !== "0") {
                        var data = "courseId=" + course1 + "&aadhar=" + $('#aadhar1').val();
                        $.ajax({
                            type: "POST",
                            url: "./registerCCIC.do?mode=getCheckCourseStatus&courseId=" + course1 + "&aadhar=" + $('#aadhar1').val(),
                            data: data,
                            cache: true,
                            contentType: true,
                            processData: true,
                            success: function(response) {
                                var id = response.split("~")[0];
                                var msg = response.split("~")[1];
                                var fileFlag = response.split("~")[2];
                                $("#fileFlag").val(fileFlag);
                                var fileFlagName = response.split("~")[3];
                                $("#brDiv").hide();
                                $("#aadhar").val(aadhar);
                                $("#gendercaste").show();
                                $("#gendercaste1").hide();
                                $(".dateDiv").show();
                                $(".dateDiv1").hide();
                                $("#sscyeardiv").hide();
                                $("#bname").prop('readonly', false);
                                $("#fname").prop('readonly', false);
                                if (id == "0") {
                                    if (fileFlag === "2") { //direct
                                        $('.MainDiv').hide();
                                        $('#INTERFLAG').hide();
                                        $('#SSCFLAG').show();
                                        $('#sscyeardiv').show();
                                        $("#brDiv").show();
//                                         fileStatus();
                                    } else if (fileFlag === "3") {
                                        $('#SSCFLAG').show();
                                        $('#sscyeardiv').show();
                                        $('#INTERFLAG').hide();
                                        $('.MainDiv').hide();
                                        $("#brDiv").show();
                                    } else if (fileFlag == "4") {
                                        $('#INTERFLAG').show();
                                        $('#SSCFLAG').hide();
                                        $('.MainDiv').hide();
                                        $("#brDiv").show();
                                    } else if (fileFlag == "5") {
                                        $('.MainDiv').show();
                                        $('#INTERFLAG').hide();
                                        $('#SSCFLAG').hide();
                                        $("#brDiv").hide();
                                        fileStatus();
                                    } else if (fileFlag == "6") {
                                        $('.MainDiv').show();
                                        $('#INTERFLAG').hide();
                                        $('#SSCFLAG').hide();
                                        $("#brDiv").hide();
                                        fileStatus();
                                    }
                                    $('#course').val($('#course1').val());
                                    $('#courseName').val($('select[name=course1]').find(':selected').text());
                                } else if (id == "3") {
                                    alert("Already institutes seats filled for this course");
                                    $("#brDiv").show();
                                } else if (id == "4") {
                                    alert("Aadhar already mapped with another institute");
                                    $("#brDiv").show();
                                } else {
                                    document.forms[0].statusresult.value = id;
                                    document.forms[0].mode.value = "getData";
                                    document.forms[0].submit();
                                }
                            },
                            error: function(e) {
                            }
                        });
                    }
                }
            }

            function checkCourseInBacklogList() {
                var myStr = $('#aadhar2').val();
                if (myStr === "") {
                    alert("Enter Aadhar No");
                    $('#hallticket').val("");
                    return false;
                }
                if (validateAdharDetails('aadhar2') !== false) {
                    var aadhar = $('#aadhar2').val();
                    var course1 = $('#hallticket').val().substring(0, 2);
                    if (course1 !== "0") {
                        var data = "courseId=" + course1 + "&aadhar=" + aadhar;
                        $.ajax({
                            type: "POST",
                            url: "./registerCCIC.do?mode=checkCourseInBacklogList&courseId=" + course1 + "&aadhar=" + aadhar,
                            data: data,
                            cache: true,
                            contentType: true,
                            processData: true,
                            success: function(response) {
                                alert(response);
                                var id = response.split("~")[0];
                                var msg = response.split("~")[1];
                                if (id == "0") {
                                    $('#hallticket').val("");
                                    alert(msg);
                                }
                            },
                            error: function(e) {
                            }
                        });
                    }
                }
            }
            function validateAdharDetailsQualification() {
                if (($("#fileFlag").val() == "3" || $("#fileFlag").val() == "2") && ($.trim($("#sschallticket").val()).length === 0)) {
                    $("#sschallticket").val("");
//                    alert("Please Enter SSC Hall Ticket ");
                    $("#sschallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#fileFlag").val() == "3" || $("#fileFlag").val() == "2") && ($("#sscyear").val() == "0")) {
                    $("#sscyear").val("0");
                    $("#sscyear").focus().css({'border': '1px solid red'});
                    return false;
                }

                else if (($("#fileFlag").val() == "4") && (($.trim($("#interhallticket").val()).length === 0))) {
                    $("#interhallticket").val("");
//                    alert("Please Enter Inter Hall Ticket ");
                    $("#interhallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#fileFlag").val() == "4") && ($("#intermonth").val() == "0")) {
                    $("#intermonth").val("0");
//                    alert("Please Select Inter pass out month ");
                    $("#intermonth").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#fileFlag").val() == "4") && ($("#interyear").val() == "0")) {
                    $("#interyear").val("0");
//                    alert("Please Select Inter pass out Year ");
                    $("#interyear").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    var aadharNum = $("#aadhar1").val();
                    var course1 = $("#course1").val();
                    var courseName = $("#courseName").val();
                    var sschallticket = $("#sschallticket").val();
                    var fileFlag = $("#fileFlag").val();
                    var sscyear = $("#sscyear").val();

                    var url = "";
                    var interhallticket = "";
                    var intermonth = "";
                    var interyear = "";
                    var paraData = "";

                    if (course1 == "IS") {
                        $('#degreeWithCheExp').show();
                        $('.degreeWithChe').show();
                        $('#qualification').val("0");
                    } else {
                        $('#degreeWithCheExp').hide();
                        $('#degreeWithCheExp1').hide();
                    }

                    if ($("#fileFlag").val() == "4") {
                        interhallticket = $("#interhallticket").val();
                        intermonth = $("#intermonth").val();
                        interyear = $("#interyear").val();
                        url = "registerCCIC.do?mode=getStatusQualification&aadharNum=" + $("#aadhar1").val() + "&course=" + $("#course1").val() + "&interhallticket=" + $("#interhallticket").val() + "&intermonth=" + $("#intermonth").val() + "&interyear=" + $("#interyear").val() + "&qualification=INTER";
                    } else {
                        url = "registerCCIC.do?mode=getStatusQualification&aadharNum=" + $("#aadhar1").val() + "&course=" + $("#course1").val() + "&sschallticket=" + $("#sschallticket").val() + "&lowerreghallticket=" + $("#lowerreghallticket").val() + "&qualification=0";
                    }

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: paraData,
                        success: function(response) {
                            $("#d")[0].reset()
                            $("#sscflag").val("N")
                            $("#interflag").val("N")
                            $("#aadhar1").val(aadharNum)
                            $("#course1").val(course1)
                            $("#sschallticket").val(sschallticket)
                            $('#aadhar').val(aadharNum);
                            $('#sscyear').val(sscyear);
                            $('#fileFlag').val(fileFlag);
                            if ($("#fileFlag").val() == "4") {
                                $("#interhallticket").val(interhallticket);
                                $("#intermonth").val(intermonth);
                                $("#interyear").val(interyear);
                            }
                            $("#gendercaste").show();
                            $("#gendercaste1").hide();
                            $(".dateDiv").show();
                            $(".dateDiv1").hide();
                            $('#course').val(course1);
                            $('#courseName').val(courseName);
                            $("#bname").prop('readonly', false);
                            $("#fname").prop('readonly', false);
                            var sample = response.trim().split("_")
                            var resssc = sample[0];
                            $("#brDiv").hide();
                            if ($("#fileFlag").val() == "3" || $("#fileFlag").val() == "2") {
                                if ((resssc == "1")) {
                                    var estatus = sample[6];
                                    if (course1 == "LS") {
                                        $("#sscflag").val("Y")
                                        $("#bname").val(sample[1])
                                        $("#fname").val(sample[2])
                                        $("#gender1").val(sample[3])
                                        $("#caste1").val(sample[4])
                                        $("#gender").val(sample[3])
                                        $("#caste").val(sample[4])
                                        $("#dob").val(sample[5])
                                        $("#dob1").val(sample[5])
                                        $("#gendercaste").hide();
                                        $("#gendercaste1").show();
                                        $(".dateDiv1").show();
                                        $(".dateDiv").hide();
                                        $("#bname").prop('readonly', true);
                                        $("#fname").prop('readonly', true);
                                        $(".MainDiv").show();
                                    } else if (estatus == "PASS") {
                                        $('.degreeWithChe').hide();
                                        $('#lowdiv').hide();
                                        $('#degreeWithCheExp1').hide();
                                        $("#sscflag").val("Y")
                                        $("#bname").val(sample[1])
                                        $("#fname").val(sample[2])
                                        $("#gender1").val(sample[3])
                                        $("#caste1").val(sample[4])
                                        $("#gender").val(sample[3])
                                        $("#caste").val(sample[4])
                                        $("#dob").val(sample[5])
                                        $("#dob1").val(sample[5])
                                        $("#gendercaste").hide();
                                        $("#gendercaste1").show();
                                        $(".dateDiv1").show();
                                        $(".dateDiv").hide();
                                        $("#bname").prop('readonly', true);
                                        $("#fname").prop('readonly', true);
                                        $(".MainDiv").show();
//                                    dateChange();
//                                    statusexam();
                                    } else if ((resssc == 1) && (estatus == "FAIL")) {
                                        alert("Not Eligible")
                                        $("#sschallticket").val("");
                                        $("#sscyear").val("0");
                                        $(".MainDiv").hide();
                                        $("#brDiv").show();
                                    }
                                }
                                else if (resssc == 0) {
                                    $("#sscflag").val("N")
                                    $("#bname").val("");
                                    $("#fname").val("");
                                    $("#dob").val("");
                                    $("#gender").val("0");
                                    $("#gender1").val("0");
                                    $("#caste").val("0");
                                    $("#caste1").val("0");
                                    fileStatus();
                                    $(".MainDiv").show();
//                                    dateChange();
//                                    statusexam();
                                } else if (resssc == 2) {
                                    alert("Entered SSC Hall Ticket Number Already Tagged to Another Candidate")
                                    $("#sschallticket").val("");
                                    $(".MainDiv").hide();
                                    $("#brDiv").show();
//                                            dateChange();
//                                            statusexam();
                                }
                                else {
                                    $(".MainDiv").show();
                                    fileStatus();
//                                    dateChange();
//                                    statusexam();
                                }
                            } else if ($("#fileFlag").val() == "4") {
                                if (resssc == "11") {
                                    $("#interflag").val("Y")
                                    $("#gendercaste").hide();
                                    $("#gendercaste1").show();
                                    $("#bname").val(sample[1])
                                    $("#fname").val(sample[2])
                                    $("#gender1").val(sample[3])
                                    $("#caste1").val(sample[4])
                                    $("#gender").val(sample[3])
                                    $("#caste").val(sample[4])
                                    $("#bname").prop('readonly', true);
                                    $("#fname").prop('readonly', true);
                                    $(".MainDiv").show();
                                    $("#brDiv").hide();
                                } else if (resssc == "10") {
                                    $("#interflag").val("N")
                                    alert("Not Eligible");
                                    $(".MainDiv").hide();
                                    $("#brDiv").show();
                                }
                                else if (resssc == "12") {
                                    fileStatus();
                                    $(".MainDiv").show();
                                    $("#brDiv").hide();
                                } else if (resssc == "13") {
                                    $("#interflag").val("N")
                                    alert("Entered Hallticket tagged to another Aadhar Number.Enter Valid Hall TIcket");
                                    $("#interhallticket").val("");
                                    $(".MainDiv").hide();
                                    $("#brDiv").show();
                                } else {
                                    $(".MainDiv").hide();
                                    $("#brDiv").show();
                                }
                            } else {
                                $(".MainDiv").hide();
                                $("#brDiv").show();
//                                fileStatus();
//                                dateChange();
//                                statusexam();
                            }
                        }

                    })
                }
            }
            function validPoints() {
                if ((parseFloat($("#attendance").val()) < 65) || (parseFloat($("#attendance").val()) > 100)) {
                    alert("Not Eligible");
                    $("#attendance").val("")
                }
            }
            function iSQualification(selectedId) {
                $("#upload1").val("");
                $("#upload2").val("");
                $("#upload3").val("");
                $("#upload4").val("");
                $("#upload5").val("");
                if ($("#" + selectedId).val() == "" || $("#" + selectedId).val() == undefined || $("#" + selectedId).val() == "0") {
                    $("#" + selectedId).val("0");
                    alert("Select Qualification");
                    $("#degreeWithCheExp1").hide();
                    $("#" + selectedId).focus();
                } else if ($("#" + selectedId).val() == "ENGINEER") {
                    $("#degreeWithCheExp1").hide();
                } else {
                    $("#degreeWithCheExp1").show();
                }
            }
        </script>
    </head>
    <body>

        <br/>
        <div class="feedback-form">
            <div class="container">
                <div class="row">

                    <h3 class="block-head-News">CCIC, Craft & Other Certificate Courses Registration Form</h3>
                    <div class="line-border"></div>
                    <br>
                    <div class="row">
                        <div class=" col-md-12">
                            <div>
                                <html:form action="/registerCCIC" styleId="d" method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <html:hidden property="fileFlag" styleId="fileFlag"/>
                                    <html:hidden property="statusresult" styleId="statusresult"/>
                                    <logic:present name="result2">
                                        <span id="msg"><center> <font color="green" style="font-weight: bold">${result2}</font>  </center></span>  
                                            </logic:present>
                                            <logic:present name="result">
                                        <span id="msg"><center><font color="red" style="font-weight: bold">${result}</font></center></span>
                                    </logic:present><br>
                                    <div id="startDiv">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Did the candidate appear for Craft & Short term Certificate Course in any of the sessions?<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:radio styleId="statusflag" property="statusflag" value="1" onclick="showStatus()"/>&nbsp;Yes
                                                    &nbsp;&nbsp;&nbsp;<html:radio styleId="statusflag" property="statusflag" value="2" onclick="showStatus()"/>&nbsp;No
                                                    <br/><br/><br/><br/>
                                                    <br/><br/><br/><br/>
                                                    <br/><br/><br/><br/>
                                                    <br/><br/><br/><br/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- If NO--%>
                                    <div id="noDiv">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label"> Aadhar No<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="aadhar1" id="aadhar1" onchange="validateAdharDetails('aadhar1');" class="form-control-plaintext"  maxlength="12"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Course<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:select property="course1" styleId="course1" styleClass="form-control" onchange="checkCourseStatus();">
                                                            <html:option value="0">--Select Course--</html:option> 
                                                            <<html:optionsCollection property="courseList1" label="course_Name" value="course_Id"/>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4" id="SSCFLAG">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-5 col-form-label" id="sscMan">SSC/EQUIVALENT HALL.NO<font color="red">*</font></label>
                                                    <div class="col-sm-7">
                                                        <html:hidden property="sscflag" styleId="sscflag" value="N"/>
                                                        <html:text  styleClass="form-control" property="sschallticket" styleId="sschallticket" maxlength="12"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" onchange="validateAdharDetailsQualification();"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="sscyeardiv">
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">Passout<br/> Year<font color="red">*</font></label>
                                                        <div class="col-sm-8">
                                                            <html:select property="sscyear" styleId="sscyear" styleClass="form-control" onchange="validateAdharDetailsQualification();">
                                                                <html:option value="0">--Select Year--</html:option>
                                                                <html:option value="2021">2021</html:option>
                                                                <html:option value="Previous">Before 2021</html:option>
                                                            </html:select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="INTERFLAG">
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">Inter/ITI/Diploma Hall Ticket<font color="red">*</font></label>
                                                        <div class="col-sm-8">
                                                            <html:hidden property="interflag" styleId="interflag" value="N"/>
                                                            <html:text property="interhallticket" styleId="interhallticket" styleClass="form-control"   onkeydown="return space(event, this);" onchange="validateAdharDetailsQualification()"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">Passout Month<font color="red">*</font></label>
                                                        <div class="col-sm-8">
                                                            <html:select property="intermonth" styleId="intermonth" styleClass="form-control" onchange="validateAdharDetailsQualification()">
                                                                <option value="0">--Select Month--</option>
                                                                <html:option value="March">March</html:option>
                                                                <html:option value="June">June</html:option>
                                                            </html:select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">Passout Year<font color="red">*</font></label>
                                                        <div class="col-sm-8">
                                                            <html:select property="interyear" styleId="interyear" styleClass="form-control" onchange="validateAdharDetailsQualification()">
                                                                <option value="0">--select Year--</option>
                                                                <%
                                                                    for (int a = 2020; a > 1975; a--) {%>
                                                                <html:option value="<%=Integer.toString(a)%>"><%=Integer.toString(a)%></html:option>
                                                                <% }
                                                                %>

                                                            </html:select>    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--<div class="col-md-1">
                                                <div class="form-group row">
                                                    <input type="button" onclick="getMainDiv();" value="SUBMIT" class="btn btn-primary"/>
                                                </div>
                                            </div>--%>

                                        </div>
                                    </div>

                                    <%-- If Yes--%>                    
                                    <div id="yesDiv">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Aadhar No<font color="red">*</font></label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="aadhar2" id="aadhar2"  class="form-control-plaintext"  maxlength="12"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-5 col-form-label">Old HallTicket No<font color="red">*</font></label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="hallticket" id="hallticket"  class="form-control-plaintext" maxlength="20" onkeydown="return space(event, this);" />
                                                    </div>
                                                </div>
                                            </div>


                                            <%-- <div class="col-md-4">
                                                 <div class="form-group row">
                                                     <label for="name" class="col-sm-4 col-form-label">Course<font color="red">*</font> </label>
                                                     <div class="col-sm-8">
                                                      <html:select property="course1" styleId="course1" styleClass="form-control" >
                                                             <html:option value="0">--Select Course--</html:option> 
                                                             <<html:optionsCollection property="courseList1" label="gname" value="gcode"/>
                                                         </html:select>
                                                     </div>
                                                 </div>
                                             </div>--%>


                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label></label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="button" onclick="getData();" value="SUBMIT" class="btn btn-primary"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="brDiv">
                                        <br/><br/><br/><br/><br/><br/>
                                        <br/><br/><br/><br/><br/><br/>
                                    </div>


                                    <%-- Main Formm  --%>
                                    <div class="MainDiv">
                                        <p class="p_textred" style="float: right; color: #f00;">All uploads must be in  JPG Format<br/>
                                            Registered  Mobile Number and email will be used for all future communications </p>
                                        <br/>
                                        <br/>

                                        <h5 class="block-head-News">Personal Details</h5>
                                        <div class="line-border"></div>

                                        <div id="dialog" title="View Photo">
                                            <center><img class="toggle" id="imageview" src="#"  width="500" height="350"/></center>
                                            <br/>
                                            <br/>
                                            <!--<center> <button class="btn btn-success" style="text-align:center;margin: 10px;" onclick="return rotate();">Rotate <i class="fa fa-rotate-right"></i></button</center>-->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Name<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="bname" styleId="bname" maxlength="50" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Father Name <font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="fname" styleId="fname" maxlength="50" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Date of Birth <font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <div class="dateDiv1">
                                                            <html:text property="dob1" styleId="dob1" styleClass="form-control" readonly="true" />
                                                        </div>
                                                        <div class="dateDiv">
                                                            <html:text property="dob" styleId="dob" styleClass="form-control" readonly="true" />
                                                            <Script>
            $(document).ready(function() {
                var today = new Date();
                yrRange = '1960' + ":" + '2008';
                $("#dob").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    maxDate: new Date(2008, 08 - 1, 01),
                    minDate: new Date(1960, 12 - 04, 1),
                    yearRange: yrRange,
                    changeYear: true
                });
            });
                                                            </Script>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div id="gendercaste">
                                                <div class="col-md-4">

                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">Gender<font color="red">*</font> </label>
                                                        <div class="col-sm-8">
                                                            <html:select property="gender" styleId="gender" styleClass="form-control">
                                                                <html:option value="0">--Select Gender--</html:option>
                                                                <html:option value="MALE">MALE</html:option>
                                                                <html:option value="FEMALE">FEMALE</html:option>
                                                                <html:option value="TRANSGENDER">TRANSGENDER</html:option>
                                                            </html:select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="gendercaste1">
                                                <div class="col-md-4">

                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">Gender<font color="red">*</font> </label>
                                                        <div class="col-sm-8">
                                                            <html:text property="gender1" styleId="gender1" styleClass="form-control"  readonly="true"/>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Aadhar<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text property="aadhar" styleId="aadhar" styleClass="form-control" readonly="true"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <h5 class="block-head-News">Examination Details</h5>
                                        <div class="line-border"></div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Category<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="category" styleId="category" value="Regular" ></html:hidden>
                                                            <input name="examinationname" id="examinationname" value="Regular" class="form-control" readonly>                                              
                                                        <%--  <html:select property="examinationname" styleId="examinationname" styleClass="form-control">
                                                              <html:option value="0">--Select Category--</html:option> 
                                                              <html:option value="Regular">Regular</html:option> 
                                                              <html:option value="Backlog">Backlog</html:option> 
                                                          </html:select> --%>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Studied District<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="apdistrict" styleId="apdistrict" value="${loginDistrict}"></html:hidden>
                                                        <html:text property="apdistrict1" styleId="apdistrict1" value="${sessionScope.distname}" readonly="true"/>
                                                        <%--   <html:select property="apdistrict" styleId="apdistrict" styleClass="form-control" >
                                                               <html:option value="0">--Select District--</html:option> 
                                                               <html:optionsCollection property="distLists" label="district_Name" value="district_ID"/>
                                                           </html:select>--%>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Institution<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="institutionCode" styleId="institutionCode" value="${instituteCode}"></html:hidden>
                                                        <html:textarea property="institute" styleId="institute" value="${sessionScope.instituteName}" readonly="true"/>
                                                        <%--  <html:select property="institutionCode" styleId="institutionCode" styleClass="form-control" onchange="getCoursesList();">
                                                              <html:option value="0">--Select Institution--</html:option> 
                                                              <html:optionsCollection property="institutionList" label="inst_Name" value="inst_Id"/>
                                                          </html:select>--%>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Course<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="course" styleId="course"></html:hidden>
                                                            <input name="courseName" id="courseName" class="form-control" readonly>                                              
                                                        <%--   <html:select property="course" styleId="course" styleClass="form-control" onchange="fileStatus(),checkCourseStatus();">
                                                            <html:option value="0">--Select Course--</html:option> 
                                                        <%--  <html:optionsCollection property="courseList" label="gname" value="gcode"/>
                                                    </html:select>--%>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-4" id="hideSuject">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Subject<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:text property="subjectCode" styleId="subjectCode" styleClass="form-control" value="ALL"/>
                                                        <%--   <html:option value="0">--Select Subject--</html:option> 
                                                           <html:optionsCollection property="subjectList" label="gname" value="gcode"/>
                                                       </html:select>--%>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Examination Center<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="ecenter" styleId="ecenter" value="${ccode}"></html:hidden>
                                                        <html:textarea property="ecenter1" styleId="ecenter1" value="${sessionScope.cname}" readonly="true"/>
                                                        <%--<html:select property="ecenter" styleId="ecenter" styleClass="form-control">
                                                            <html:option value="0">--Select--</html:option>
                                                        </html:select>--%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Attendance<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:text property="attendance" styleId="attendance"  maxlength="5" onkeypress="return inputLimiter(event, 'ogpaExp');"  onchange="return validPoints()"/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-4" id="degreeWithCheExp">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Qualification<font color="red">*</font> </label>
                                                    <div class="col-sm-8">    
                                                        <html:select property="qualification" styleId="qualification" styleClass="form-control" onchange="return iSQualification('qualification')">
                                                            <html:option value="0">--Select--</html:option>
                                                            <html:option value="DEGREE">--Degree With 2 Yrs Exp--</html:option>
                                                            <html:option value="DIPLOMA">--Any Diploma with 2 Yrs Exp--</html:option>
                                                            <html:option value="ENGINEER">--Any Engineering--</html:option>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-5" id="lowdiv">
                                                <div class="form-group row">
                                                    <div class="sscPass"><label for="name" class="col-sm-6 col-form-label">SSC Passed Certificate(Should not be more than 1MB)<font color="red">*</font></label></div>
                                                    <div class="InterIIT"><label for="name" class="col-sm-6 col-form-label">Inter/ITI/Diploma (Passed) (Should not be more than 1MB)<font color="red">*</font></label></div>
                                                    <!--<div class="degreeWithChe"><label for="name" class="col-sm-6 col-form-label">Degree with Physics or Chemistry subjects with one year experience/any Diploma with 2 years experience/any Engg. (Should not be more than 1MB)<font color="red">*</font></label></div>-->
                                                    <div class="degreeWithChe"><label for="name" class="col-sm-6 col-form-label">Degree/Diploma/Engg Certificate (Should not be more than 1MB)<font color="red">*</font></label></div>
                                                    <div class="InterPass"><label for="name" class="col-sm-6 col-form-label">Intermediate(Passed) (Should not be more than 1MB)<font color="red">*</font></label></div>
                                                    <div class="anyDegree"><label for="name" class="col-sm-6 col-form-label">Any Degree/Diploma/Engineering (Should not be more than 1MB)<font color="red">*</font></label></div>
                                                    <div class="sscPassORFailOREqil"><label for="name" class="col-sm-6 col-form-label">S.S.C-(Pass/Fail/equivalent)(Should not be more than 1MB)<font color="red">*</font></label></div>
                                                    <div class="yogaPass"><label for="name" class="col-sm-6 col-form-label">Yoga (Passed) (Should not be more than 1MB)<font color="red">*</font></label></div>
                                                    <div class="col-sm-6">
                                                        <html:file property="upload1" styleId="upload1" onchange="return CheckfilePdfOrOther('upload1');"/><a href="#" onclick="fileupload(event, 'upload1')"/>View File</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="degreeWithCheExp1">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-6 col-form-label">Experience Certificate (Should not be more than 1MB)<font color="red">*</font></label>
                                                    <div class="col-sm-5">
                                                        <html:file property="upload2" styleId="upload2" onchange="return CheckfilePdfOrOther('upload2');"/><a href="#" onclick="fileupload(event, 'upload2')"/>View File</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-6 col-form-label">Experience Certificate (Should not be more than 1MB)</label>
                                                    <div class="col-sm-6">
                                                        <html:file property="upload3" styleId="upload3" onchange="return CheckfilePdfOrOther('upload3');"/><a href="#" onclick="fileupload(event, 'upload3')"/>View File</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-6 col-form-label">Experience Certificate (Should not be more than 1MB)</label>
                                                    <div class="col-sm-6">
                                                        <html:file property="upload4" styleId="upload4" onchange="return CheckfilePdfOrOther('upload4');"/><a href="#" onclick="fileupload(event, 'upload4')"/>View File</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-6 col-form-label">Experience Certificate (Should not be more than 1MB)</label>
                                                    <div class="col-sm-6">
                                                        <html:file property="upload5" styleId="upload5" onchange="return CheckfilePdfOrOther('upload5');"/><a href="#" onclick="fileupload(event, 'upload5')"/>View File</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <br/>

                                        <h5 class="block-head-News">Communication Details</h5>
                                        <div class="line-border"></div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">House No<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="houseno" styleId="houseno" maxlength="250" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Street<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="locality" styleId="locality" maxlength="250" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');" onchange="firstLettterCheck(this.value,this.id)"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Village/Town<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="village" styleId="village" maxlength="100" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');" onchange="firstLettterCheck(this.value,this.id)"/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">State<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:select property="state" styleId="state" styleClass="form-control" onchange="getDistricts()">
                                                            <html:option value="0">--Select State--</html:option> 
                                                            <html:optionsCollection property="stateList" label="sname" value="scode"/>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="DistrictDiv" class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">District<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:select property="district" styleId="district" styleClass="form-control" onchange="getMandals()">
                                                            <html:option value="0">--Select--</html:option>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="MandalDiv" class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Mandal<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:select property="mandal" styleId="mandal" styleClass="form-control" >
                                                            <html:option value="0">--Select--</html:option>
                                                        </html:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="DistrictDiv1" class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">District<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="district1" styleId="district1" maxlength="100" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="MandalDiv1" class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Mandal<font color="red">*</font> </label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="mandal1" styleId="mandal1"   maxlength="100"  onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Pincode<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="pincode" styleId="pincode" maxlength="6" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="isPincode()"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Mobile No<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="mobile" styleId="mobile"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="telephoneValidation(),adharDuplicatevalidation()" maxlength="10"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">E-Mail<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control-plaintext" property="email" styleId="email"  onkeydown="return space(event, this);" maxlength="100" onchange='return isEmail(this)'/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Upload Signature (should not be more than 40KB)<font color="red">*</font></label>
                                                    <div class="col-sm-8">                                           
                                                        <html:file property="signature" styleId="signature" onchange="return CheckfilePdfOrOther40kb('signature');"/><a href="#" onclick="fileupload(event, 'signature')"/>View File</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Upload Photo (Should not be more than 40KB)<font color="red">*</font></label>
                                                    <div class="col-sm-8">                                           
                                                        <html:file property="photo" styleId="photo" onchange="return CheckfilePdfOrOther40kb('photo');"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <div class="col-sm-8">                                           
                                                        <center>  <img id="myImg" src="#"  width="100" height="70"/></center>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <br/>

                                        <div><input type="checkbox" id="declaration" style="max-width: 15px;max-height: 15px;font-size:24px;"/>&nbsp;&nbsp;&nbsp;I promise to abide by the rules/regulations and the orders of the CCIC, its Authorities and Officers.I do hereby declare that the information furnished in this application is true to the best of my knowledge and belief.
                                            I am aware that in the event of any information being found to be false or untrue, I shall be liable to such action by CCIC.
                                            <br/>
                                        </div>

                                        <br/>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-12 center">
                                                <center><input type="button" onclick="return SubmitForm();" value="SUBMIT"  class="submit"/></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                            </html:form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </body>
</html>