<%-- 
    Document   : paymentSuccess
    Created on : Oct 23, 2020, 3:15:06 PM
    Author     : 1582792
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    int i = 1, j = 1, k = 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <script src="./js/jquery.min.js"></script>
        <script src ="./js/jquery.js"></script>
         <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
           <link rel="stylesheet" href="css/style1.css">
        <title>Payment Success</title>

        <style type="text/css">
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
      table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color:  #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
             .h3_text{
                background: #1a9200;
                font-size: 15px;
                padding: 5px;
                text-align: center;
                color: #fff;
                line-height: 26px;
                box-shadow: 5px 5px 5px #ccc;
            }
        </style>
    </head>
   <script>
          $(document).ready(function(){
    $("#logoDiv").hide();
    $("#datediv").hide();
})
        function getPdf() {
           
                var table = document.getElementById('showpdf').innerHTML;
                var table1 = document.getElementById('logoDiv').innerHTML;
            var table2 = document.getElementById('datediv').innerHTML;      
        var myWindow = window.open('', '', 'width=800, height=600');
                myWindow.document.write(table1);
                myWindow.document.write(table2);
                myWindow.document.write(table);
                myWindow.print();


        }
    </script>
    <body>
        <html:form action="/paymentSuccess" method="post">
            <html:hidden property="mode"/>
            <html:hidden property="reqId"/> 
            <html:hidden property="aadhar" /> 
            <%--<logic:present name="failed">--%>
                <!--<h3><font color="red">${failed}</font></h3>-->
            <%--</logic:present>--%>
            <br/> <br/>  <br/> <br/><br/><br/>
            
           
          
           
               <h3 class="block-head-News"><center>TWSH Registration</center></h3>
              <%--   <logic:present name="result2">
                            <center> <font color="green" style="font-weight: bold">${result2}</font>  <a href="editDegree.do">Click Here</a>  </center>  
                            </logic:present>
                           <logic:present name="result1">
                                <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                                </logic:present>--%>
           <div class="col-xs-12 col-sm-7" id="logoDiv">
                <center> <img src="assets/img/logo.png" style="max-width:100%"></center>
                <br/>
  <center>GOVERNMENT OF ANDHRA PRADESH, VIJAYAWADA </center>
               <center>TWSH Payment Receipt</center>
                    <br/>
                </div>
                              
                <logic:present name="paymentres">
                    <logic:iterate name="paymentres" id="list">
                        <center>
                            <!--<h3><font color="green">Payment Success</font></h3>-->
                            <div class="container  inner_page">
                                <div style="padding:15px;">
                                      <div id="showpdf">
                                    <table  border="1" cellpadding="0" cellspacing="0"  width="80%"  align="center" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;" >

                                        <tr>
                                            <th style="text-align:left">
                                                Registration No  
                                            </th>
                                            <td style="text-align:left">
                                                ${list.reqId}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="text-align:left">
                                               Aadhar No
                                            </th>
                                            <td style="text-align:left">
                                                ${list.aadhar}
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <th style="text-align:left">
                                                Aadhar Number  
                                            </th>
                                            <td style="text-align:left">
                                                ${list.aadhar}
                                                <html:hidden property="aadhar" value="${list.aadhar}"/> 
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <th style="text-align:left"> Applicant Name </th>
                                            <td style="text-align:left">
                                                ${list.customerName}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="text-align:left">
                                                Gender
                                            </th>
                                            <td style="text-align:left">
                                                ${list.gender}
                                            </td>
                                        </tr>
                                        <tr>
                                             <th style="text-align:left"> Social Category(Caste) :  </th>
                                            <td style="text-align:left">
                                                ${list.caste}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="text-align:left">
                                                Mobile Number
                                            </th>
                                            <td style="text-align:left">
                                                ${list.mobile}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="text-align:left">Visually Impaired</th>
                                            <td style="text-align:left">
                                                ${list.ph}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="text-align:left">
                                                Amount 
                                            </th>
                                            <td style="text-align:left">
                                                ${list.baseAmt}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="text-align:left">
                                                Payment Ref No 
                                            </th>
                                            <td style="text-align:left">
                                                ${list.PgRefNo}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="text-align:left">
                                                Payment Date 
                                            </th>
                                            <td style="text-align:left">
                                                ${list.gst}
                                            </td>
                                        </tr>
                                    </table>
</div>
                                            <div id="datediv"><p class="p_textred" style="float: right; color: blue;">
                                  <%=new Date()%></div>
                         <br/>
                           
                                        <center> <input type="button" readonly="true" name="signUp" class="btn btn-success" style="max-width: 96px;font-size:24px;"  value="Print" onClick="getPdf()" /></center>
                                        
                                </div>
                            </div>
                        </center>
                    </logic:iterate>
                </logic:present>
            </div>
        </html:form>
    </body>
</html>
