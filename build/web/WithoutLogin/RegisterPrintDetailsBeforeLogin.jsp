<%-- 
    Document   : RegisterPrintDetailsBeforeLogin
    Created on : Apr 22, 2021, 7:39:44 PM
    Author     : 1042564
--%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>
        <script src="./js/jquery.min.js"></script>
        <script src ="./js/jquery.js"></script>
        <script src ="./js/validateDegree.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">

        
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
        </style>
        <style>
            .box {
                width: 40%;
                margin: 0 auto;
                background: rgba(255, 255, 255, 0.2);
                padding: 35px;
                border: 2px solid #fff;
                border-radius: 20px/50px;
                background-clip: padding-box;
                text-align: center;
            }

            span#customHTMLFooter {
                display: none;
            }

            .panel-default>.panel-heading {
                color: #fff;
                background-color: #f79400;
                border-color: #f79400;
            }

            .button {
                font-size: 1em;
                padding: 10px;
                color: #fff;
                border: 2px solid #06D85F;
                border-radius: 20px/50px;
                text-decoration: none;
                cursor: pointer;
                transition: all 0.3s ease-out;
            }

            .button:hover {
                background: #06D85F;
            }

            .overlay {
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                z-index: 111;
                background: rgba(0, 0, 0, 0.7);
                transition: opacity 500ms;
                visibility: hidden;
                opacity: 0;
            }

            .overlay:target {
                visibility: visible;
                opacity: 1;
            }

            .popup {
                margin: 20px auto;
                /* padding: 40px; */
                background: #fff;
                border-radius: 5px;
                width: 70%;
                position: relative;
                transition: all 5s ease-in-out;
            }


            .popup .content {
                width: 930px !important;
                overflow-x: scroll !important;
                height: 450px;
                overflow-y: scroll !important;
            }

            .popup h2 {
                margin-top: 0;
                color: #333;
                font-family: Tahoma, Arial, sans-serif;
            }

            .popup .close {
                position: absolute;
                top: 20px;
                right: 30px;
                transition: all 200ms;
                font-size: 30px;
                font-weight: bold;
                text-decoration: none;
                color: #333;
            }

            .popup .close:hover {
                color: #06D85F;
            }


            @media screen and (max-width: 700px) {
                .box {
                    width: 80%;
                }
                .popup {
                    width: 80%;
                }

            }

            .custexthidden{
                background: none;
                border: 0;
                outline: none;
            }
            .cusselecthidden{
                -webkit-appearance: none;
                -moz-appearance: none;
                text-indent: 1px;
                text-overflow: '';
                border: 0;
                background: none;

            }
            select {

            }
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>
        <script type="text/javascript">
            function getDocumentDetailsData() {
                document.forms[0].mode.value = "getCanData";
                document.forms[0].submit();
            }
        </script>
        <script>
            $(document).ready(function() {
                $("#logoDiv").hide();
                $("#datediv").hide();
            });
            function getPdf() {
                var table = document.getElementById('dataDiv').innerHTML;
                var table1 = document.getElementById('logoDiv').innerHTML;
                var table2 = document.getElementById('datediv').innerHTML;
                var myWindow = window.open('', '', 'width=800, height=600');
                myWindow.document.write(table1);
                myWindow.document.write(table2);
                myWindow.document.write(table);
                myWindow.print();
            }
            function goHome(){
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
        </script>
    </head>
    <body>
        <div class="page-title title-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Print Registration</h1>
                        <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
                            <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                            <span>Print Registration</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mainbodyrow">
            <div class="container" style="padding-top: 40px;">
                <div class="col-xs-12">
                    <div class="maindodycnt" style="min-height: 460px;background: #fbf9f9;box-shadow: 0px 0px 2px #717171; padding: 10px;">
                        <div class="col-xs-12 col-sm-7" id="logoDiv">
                            <center> <img src="assets/img/logo.png" style="max-width:100%"></center>
                            <br/>
                        </div>
                        <html:form action="/formPrint"  method="post" enctype="multipart/form-data">
                            <html:hidden property="mode"/>
                            <html:hidden property="dob" styleId="dob" value="${dob}"/>
                            <html:hidden property="caste" styleId="caste" value="${caste}"/>
                            <div style="text-align: right;"><button onclick="goHome();">BACK</button></div>
                            <div id="dataDiv">
                                <h3 class="block-head-News">Print For Registration Form </h3> 
                                <div class="line-border"></div>
                                <div>
                                    <h5 class="block-head-News">Personal Details (As per SSC Certificate)</h5> 
                                    <div class="line-border"></div>
                                    <span> </span>
                                </div>

                                <table align="center" cellpadding="0" cellspacing="0" border="1" width="100%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                    <tr>
                                        <th>Name</th>
                                        <td>${name}</td> 
                                        <th>Father Name</th>
                                        <td>${fname}</td> 
                                        <th>Date Of Birth</th>
                                        <td>${dob}</td> 
                                        <td colspan="2" rowspan="2"><img width="80" height="80" style="float:right;margin-right: 20px;" alt="NO FILE" src="data:image/jpg;base64,${photo}" /><br><img width="80" height="80" style="float:right;margin-right: 20px;height:40px;" alt="NO FILE" src="data:image/jpg;base64,${signature}" /></td>
                                    </tr>
                                    <tr>
                                        <th>Gender</th>
                                        <td>${gender}</td> 
                                        <th>Community</th>
                                        <td>${caste}</td> 
                                        <th>Are you Visually Impaired?</th>
                                        <td>${visImp}</td>
                                    </tr>
                                  <%--  <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <td colspan="2" rowspan="2"><img width="80" height="80" style="float:right;margin-right: 20px;" alt="NO FILE" src="data:image/jpg;base64,${photo}" /></td>
                                    </tr> --%>
                                </table>
                                <br>
                                <div>
                                    <h5 class="block-head-News">Examination Details</h5>
                                    <div class="line-border"></div>
                                    <span></span>
                                </div>
                                <table align="center" cellpadding="0" cellspacing="0" border="1" width="100%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                    <tr>
                                        <th>Institution</th>
                                        <td>${institution}</td>  
                                        <th>Examination Appearing</th>
                                        <td >${examinationAppearing}</td>  
                                        <th>Language</th>
                                        <td>${language}</td> 
                                        <th>Grade</th>
                                        <td>${grade}</td>
                                    </tr>
                                    <tr>
                                        <th>Examination District</th>
                                        <td>${examinationDistrict}</td> 
                                        <th>Examination Center</th>
                                        <td>${examinationCenter}</td>  
                                        <th>Examination Date</th>
                                        <td>${examinationDate}</td>  
                                        <th>Examination Batch</th>
                                        <td>${examinationBatch}</td>
                                    </tr>
                                </table>
                                <br>
                                <div>
                                    <h5 class="block-head-News">Communication Details</h5>
                                    <div class="line-border"></div>
                                    <span></span>
                                </div>
                                <table align="center" cellpadding="0" cellspacing="0" border="1" width="100%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                    <tr>
                                        <th>House No</th>
                                        <td>${hNo}</td>  
                                        <th>Street</th>
                                        <td >${street}</td>  
                                        <th>Village</th>
                                        <td>${village}</td> 
                                        <th>State</th>
                                        <td>${state}</td>
                                    </tr>
                                    <tr>
                                        <th>District</th>
                                        <td>${district}</td> 
                                        <th>Mandal</th>
                                        <td>${mandal}</td>  
                                        <th>Pincode</th>
                                        <td colspan="3">${pincode}</td>  
                                    </tr>
                                    <tr>
                                        <th>Mobile No</th>
                                        <td>${mobile}</td> 
                                        <th>eMail</th>
                                        <td>${email}</td>  
                                        <th>Aadhar Number</th>
                                        <td  colspan="3">${aadhar}</td>  
                                    </tr>
                                </table>
                                <br>
                                <div>
                                    <h5 class="block-head-News">Payment Details</h5>
                                    <div class="line-border"></div>
                                    <span></span>
                                </div>
                                <table align="center" cellpadding="0" cellspacing="0" border="1" width="100%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                    <tr>
                                        <th>Registration No</th>
                                        <td>${RegNo}</td>
                                        <th>Ref.No</th>
                                        <td>${paymentRefNo}</td>
                                        <th>Amount</th>
                                        <td>${paymentAmount}</td>
                                        <th>Date</th>
                                        <td>${paymentDate}</td>
                                    </tr>
                                    
                                </table>
                                <br><br><br><br><br>
                                <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable" id="altrowstable" style="margin: 0px auto;">
                                    <tr>
                                        <td colspan="4"><b  style="color: black;">SIGNATURE OF CANDIDATE</b></td>
                                        <td  colspan="4" style="text-align: right;    padding-right: 13px;"><b style="color: black;">SIGNATURE OF INSTITUTE'S </b></td>  
                                    </tr>
                                    <tr>
                                        <td colspan="7"></td>
                                        <td  colspan="1" style="text-align: right;"><b  style="color: black;">PRINCIPAL/GAZETTED OFFICER</b></td>  
                                    </tr>
                                </table>
                                
                            </div>
                            <div id="datediv"><p class="p_textred" style="float: right; color: blue;">${date}</div>
                            <center> <input type="button" readonly="true" name="signUp" class="btn btn-success" style="max-width: 96px;font-size:24px;"  value="Print" onClick="getPdf();" /></center>
                        </div><br><br>
                        
                    </html:form>
                </div>
            </div>
        </div>
    </body>
</html>