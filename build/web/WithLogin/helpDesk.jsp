<%-- 
    Document   : Teachersprint
    Created on : Jun 5, 2018, 7:39:44 PM
    Author     : 1042564
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 0;

        %>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <!--<script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">-->
    </script>
    <script type="text/javascript" language="javascript" src="js/demo.js">
    </script>
    <script type="text/javascript" class="init">
        function onlyNumbers(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Allow Numbers Only");
                return false;
            }
            return true;
        }
        $(document).ready(function() {
            var table = $('#example').DataTable({
//                    scrollY: "300px",
                scrollX: true,
//                    scrollCollapse: true,
//                    paging: true,
//                    fixedColumns: true
            });
        });
    </script>
    <style type="text/css">
        table {
            border-collapse: collapse !important;
            border-spacing: 0px !important;
        }
        table.altrowstable1 th {
            background-color: #9fa7ff !important;
            border: 1px #1e2ab7 solid !important;
            font-size: 13px !important;
            color: #000000 !important;
            border-collapse: collapse !important;
            border-spacing: 0px !important;
            text-align: left !important;
            padding: 5px 15px;
            word-break: keep-all;
            white-space: nowrap;
        }

        table.altrowstable1 td {
            text-align: left;
            border: 1px #1e2ab7 solid !important;
            vertical-align: middle;
            padding-left: 3px;
            padding-right: 3px;
            font-size: 13px;
            font-family: verdana;
            font-weight: normal;
            height: 20px;
            padding: 7px;
            background: #e6e8ff;

        }

        input {
            width: 90% !important;
            padding: 3px !important; 
        }
        tr.payrad td {
            text-align: left !important;
        }
        tr.payrad input[type="radio"] {
            float: left;
            text-align: left !important;
            width: 19% !important;
        }
        input[type="radio"], input[type="checkbox"]
        {
            width: 30px !important;
            float: left;
        }

        select {
            border-radius: 0;
            margin-bottom: 12px !important;
            border: 1px solid #005396;
            box-shadow: none;
        }

        .form-control{
            display: block;
            width: 100% !important;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
        }
        .btn
        {
            padding: 6px 12px !important;
            width: auto !important;
        }
        .h3_text{
            background: #1a9200;
            font-size: 15px;
            padding: 5px;
            text-align: center;
            color: #fff;
            line-height: 26px;
            box-shadow: 5px 5px 5px #ccc;
        }
        #example_wrapper {
            width: 100% !important;
            padding: 20px;
        }
        table.dataTable thead th, table.dataTable tbody td {
            padding: 8px 10px;
            border: 1px solid #ccc;
            font-size: 13px;
        }
    </style>




    <script>
        function space(evt, thisvalue)
        {
            var number = thisvalue.value;
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (number.length < 1) {
                if (evt.keyCode == 32) {
                    return false;
                }
            }
            return true;
        }
        function mysearch() {
            if ($('#programmeId').val() == "00") {
                alert("Please Select Stream");
                $('#programmeId').focus();
                return false;
            } else if ($('#programmeName').val() == "") {
                alert("Please Enter Hall Ticket Number");
                $('#programmeName').focus();
                return false;
            } else {
                document.forms[0].mode.value = "getData";
                document.forms[0].submit();
            }

        }
    </script>
</head>
<body>
    <div class="row mainbodyrow">
        <div class="container">
            <div class="col-xs-12">
                <div class="maindodycnt">

                    <h3 class="h3_text"><b><center>Check Applications Status</center></b></h3>

                    <span></span>
                    <html:form action="/helpDesk" >
                        <html:hidden property="mode"/>

                        <logic:present name="result1">
                            <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                            </logic:present>
                            <logic:present name="result">
                            <center> <font color="red" style="font-weight: bold">${result}</font></center>
                            </logic:present>
                        <br>
                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="60%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                            <tr>
                                <th>&nbsp;&nbsp;&nbsp;<b>Stream :</b>&nbsp;&nbsp;&nbsp;</th>
                                <td>
                                    <html:select property="programmeId" styleId="programmeId" >
                                        <html:option value='00'>Select Stream</html:option>
                                        <html:option value='BiPC'>BiPC</html:option>
                                        <html:option value='MPC'>MPC</html:option>
                                    </html:select>  
                                </td>
                                <th>&nbsp;&nbsp;&nbsp;<b>Hall Ticket No :</b>&nbsp;&nbsp;&nbsp;</th>
                                <td>
                                    <html:text property="programmeName" styleId="programmeName" maxlength="12" onkeydown="return space(event,this);" onkeypress='return onlyNumbers(event);'/>
                                </td>
                                <td>
                                    <input type='submit' value='SUBMIT' id="SUBMIT" onclick="return mysearch();" class="button button2" style=" width: auto !important;  padding: 5px !important;"/>
                                </td>
                            </tr>
                        </table>

                        <br>

                        <logic:present  name="masterData">
                            <table align="center" cellpadding="0" cellspacing="0" border="0" width="60%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                <logic:iterate name="masterData" id="list">
                                    <tr>
                                        <th>Hall Ticket</th>
                                        <td>${list.HALLTICKET}</td>
                                    </tr>
                                    <tr>
                                        <th>Candidate Name</th>
                                        <td>${list.name}</td>
                                    </tr>  
                                    <tr>
                                        <th>Major Subject Name</th>
                                        <td>${list.mjS}</td> 
                                    </tr>

                                    <tr>
                                        <th>Registration Status</th>
                                        <td>${list.Registration}</td> 
                                    </tr>
                                    <tr>
                                        <th>Payment Status</th>
                                        <td>${list.payment}</td> 
                                    </tr>
                                </logic:iterate>
                            </table>
                        </logic:present>
                    </html:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>                                       