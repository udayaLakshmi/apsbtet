<%-- 
    Document   : CourseStatusReport
    Created on : Nov 23, 2020, 1:51:42 PM
    Author     : 1582792
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%int i = 0;%>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <script type="text/javascript">

            $(document).ready(function() {
             
                 var oTable = $('#example').DataTable({
                     paging:false,
        stateSave: true,
         "scrollX": true
    });
    var allPages = oTable.cells( ).nodes( );
                $('#selectall').on('click', function() {
//    $('.selectedId').attr('checked', $(this).is(":checked"));
     if ($('#selectall').hasClass('allChecked')) {
            $('input[type="checkbox"]', allPages).prop('checked', false);
            $("select", allPages).val("0");
        } else {
            $('input[type="checkbox"]', allPages).prop('checked', true);
                $("select", allPages).val("1");
        }
         $('#selectall').toggleClass('allChecked');
});
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .box {
                width: 40%;
                margin: 0 auto;
                background: rgba(255, 255, 255, 0.2);
                padding: 35px;
                border: 2px solid #fff;
                border-radius: 20px/50px;
                background-clip: padding-box;
                text-align: center;
            }

            span#customHTMLFooter {
                display: none;
            }

            .panel-default>.panel-heading {
                color: #fff;
                background-color: #f79400;
                border-color: #f79400;
            }

            .button {
                font-size: 1em;
                padding: 10px;
                color: #fff;
                border: 2px solid #06D85F;
                border-radius: 20px/50px;
                text-decoration: none;
                cursor: pointer;
                transition: all 0.3s ease-out;
            }

            .button:hover {
                background: #06D85F;
            }

            .overlay {
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                z-index: 111;
                background: rgba(0, 0, 0, 0.7);
                transition: opacity 500ms;
                visibility: hidden;
                opacity: 0;
            }

            .overlay:target {
                visibility: visible;
                opacity: 1;
            }

            .popup {
                margin: 20px auto;
                /* padding: 40px; */
                background: #fff;
                border-radius: 5px;
                width: 70%;
                position: relative;
                transition: all 5s ease-in-out;
            }


            .popup .content {
                width: 930px !important;
                overflow-x: scroll !important;
                height: 450px;
                overflow-y: scroll !important;
            }

            .popup h2 {
                margin-top: 0;
                color: #333;
                font-family: Tahoma, Arial, sans-serif;
            }

            .popup .close {
                position: absolute;
                top: 20px;
                right: 30px;
                transition: all 200ms;
                font-size: 30px;
                font-weight: bold;
                text-decoration: none;
                color: #333;
            }

            .popup .close:hover {
                color: #06D85F;
            }


            @media screen and (max-width: 700px) {
                .box {
                    width: 80%;
                }
                .popup {
                    width: 80%;
                }

            }

            .custexthidden{
                background: none;
                border: 0;
                outline: none;
            }
            .cusselecthidden{
                -webkit-appearance: none;
                -moz-appearance: none;
                text-indent: 1px;
                text-overflow: '';
                border: 0;
                background: none;

            }
            select {

            }
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>
    </head>
    <script>
        function getPrint(aadhar, grade, dob, regno) {
            document.forms[0].grade.value = grade;
            document.forms[0].hallticket.value = regno;
            document.forms[0].dob.value = dob;
            document.forms[0].aadhar.value = aadhar;
            document.forms[0].mode.value = "getDataPrint";
            document.forms[0].submit();
        }
         
        function submitData() {
            var checkedBoxes = [];
            var regno = "";
            var aadhar = "";
            var grade = "";
             var hallticketstatus = "";
            var r = false;
            var chkbox = document.getElementsByName('checkid');
            if (chkbox.length > 0) {
                for (var i = 0, n = chkbox.length; i < n; i++) {
                    if (chkbox [i].checked)
                    {
                        checkedBoxes.push(chkbox [i].value);
                        if (($("#hstatus" + chkbox[i].value).val() == null || $("#hstatus" + chkbox[i].value).val() == '0')) {
                            alert("Please Select confirm Status");
                            $("#hstatus" + chkbox[i].value).focus();
                            return false;
                        }
                        aadhar = aadhar + $("#aadhar" + chkbox[i].value).val() + "~";
                        grade = grade + $("#grade" + chkbox[i].value).val() + "~";
                        regno = regno + $("#regno" + chkbox[i].value).val() + "~";
                     hallticketstatus = hallticketstatus + $("#hstatus" + chkbox[i].value).val() + "~";
                        r = true;

                    }
                }
                if (r == false) {
                    alert("please select at least one check box");
                    return false;
                }
                else {
                    var returnval = confirm("Do You want to proceed");
                    if (returnval == true) {
                        $("#checkedBoxes").val(checkedBoxes);
                        $("#aadhardetails").val(aadhar);
                        $("#gradedetails").val(grade);
                        $("#regnodetails").val(regno);
                        $("#hallticketdetails").val(hallticketstatus);
                        document.forms[0].mode.value = "submitDetails";
                        document.forms[0].submit();
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
   function checkuncheck(value){
       if($("#checkid"+value).is(':checked')==false){
           $("#hstatus"+value).val("0")
       }
   }
    </script>
</head>
<body>
    <div class="row mainbodyrow">
        <div class="container" style="padding-top: 40px;">
            <div class="col-xs-12">
                <div class="maindodycnt">
                    <h3 class="block-head-News">HallTicket Confirmation</h3>
                    <div class="line-border"></div>
                    <html:form action="/hallTicketConfirmation" >
                        <html:hidden property="mode"/>
                      <input type="hidden" name="checkedBoxes" id="checkedBoxes">
                        <input type="hidden" name="aadhardetails" id="aadhardetails">
                        <input type="hidden" name="gradedetails" id="gradedetails">
                        <input type="hidden" name="regnodetails" id="regnodetails">
                        <input type="hidden" name="hallticketdetails" id="hallticketdetails">
                        <logic:present name="result2">
                            <center> <font color="green" style="font-weight: bold">${result2}</font></center>
                            </logic:present>
                            <logic:present name="result">
                            <center> <font color="red" style="font-weight: bold">${result}</font></center>
                            </logic:present>
                            <logic:present  name="listData">
                            <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Select All<input type="checkbox"  name="selectall" id='selectall'/> </th>
                                        <th>Registration Number</th>
                                        <th>Name Of the Candidate</th>
                                        <th>Date of Birth</th>
                                        <th>Mobile</th>
                                        <th>Caste</th>
                                        <th>Examination</th>
                                        <th>Grade</th>
                                        <th>confirmationstatus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%int l = 0;%>
                                    <logic:present name = "listData">
                                        <logic:iterate name="listData" id="list">
                                            <tr>
                                                 <%++l;%>
                                                <input type="hidden" name="aadhar<%=l%>"  id="aadhar<%=l%>" value ="${list.aadhar}" />
                                                <input type="hidden" name="grade<%=l%>"  id="grade<%=l%>"  value ="${list.grade}" />
                                                <input type="hidden" name="regno<%=l%>"  id="regno<%=l%>"  value ="${list.regno}"/>
                                             <td><input type="checkbox" class="checkbox" style=" margin: auto;" styleClass="selectedId" name="checkid" id="checkid<%=l%>" value="<%=l%>" onclick="checkuncheck(this.value)"></td>
                                                <td>${list.regno}</td>
                                                <td>${list.bname}</td>
                                                <td>${list.dob}</td>
                                                <td>${list.mobile}</td>
                                                <td>${list.caste}</td>
                                                <td>${list.examination}</td>
                                                <td>${list.grade}</td>
                                                <td style="width: 110px;">
                                             <select  name="hstatus<%=l%>" id="hstatus<%=l%>" styleClass="form-control">
                                                 <option value="0">--select--</option>
                                                 <option value="1">Approved</option>
                                                 <option value="2">Rejected</option>
                                             </select>
                                                </td>
                                            </tr>
                                        </logic:iterate>
                                           
                                    </logic:present>
                                    <logic:notPresent name = "listData">
                                        <tr><td colspan="8">NO Data</td></tr>
                                    </logic:notPresent>
                                </tbody>
                            </table>
                              <logic:present name = "listData">
                               <div class="row">
                                    <div class="col-md-12 center">
                                        <center>  <button class="btn btn-primary" id="COnfirm" type="submit" value="Confirm" onclick="return submitData();">CONFIRM</button></center>
                                </div>
                               </div></logic:present>
                        </logic:present>
                    </html:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>                                       