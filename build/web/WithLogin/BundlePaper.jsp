<!DOCTYPE html>
<html>
    <head>
        <%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
        <%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
//                    "scrollY": 300,
                    "scrollX": true
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control-plaintext{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }

            * {
                box-sizing: border-box;
            }

            ul {
                list-style-type: none;
                padding: 0;
                margin: 0;
            }

            ul li {
                border: 1px solid #ddd;
                margin-top: -1px; /* Prevent double borders */
                background-color: #f6f6f6;
                padding: 12px;
                text-decoration: none;
                font-size: 18px;
                color: black;
                display: block;
                position: relative;
            }

            ul li:hover {
                background-color: #eee;
            }

            .close {
                cursor: pointer;
                position: absolute;
                top: 50%;
                right: 0%;
                padding: 12px 16px;
                transform: translate(0%, -50%);
            }

            .close:hover {background: #bbb;}
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $(document).ajaxStart(function() {
                    $("body").addClass("loading");
                });
                $(document).ajaxComplete(function() {
                    $("body").removeClass("loading");
                });
                var seconds = 5;
                setTimeout(function() {
                    if ($(".msg") !== null)
                        $(".msg").hide();
                }, seconds * 1000);
                $("form").attr('autocomplete', 'off');
                $("#showDiv").hide();
                $("#showDiv1").hide();
                $("#buttonDiv").hide();
                $("#bundleDiv").hide();
                $("#bookletsDiv").hide();
                $("#brDiv").show();
                $("#brDiv1").hide();
            });
            var paperlist = [];

            function disableData(id) {
                $("#" + id).prop('readonly', true);
            }
            function SubmitForm() {
                if (parseInt($("#epaper").val()) >= 40 && (paperlist.length < 40)) {
                    alert("40 Booklets Not Mapped.Please Map")
                    $("#barcode").focus().css({'border': '1px solid red'});
                } else if ((parseInt($("#epaper").val()) < 40) && (paperlist.length < (parseInt($("#epaper").val())))) {
                    alert("Maximum Limit Booklets Not Mapped.Please Map")
                    $("#barcode").focus().css({'border': '1px solid red'});
                } else if ($("#grade").val() == "0") {
                    alert("Please Select Examination Grade")
                    $("#grade").focus().css({'border': '1px solid red'});
                } else if ($("#ecenter").val() == "0") {
                    alert("Please Select Examination Paper")
                    $("#ecenter").focus().css({'border': '1px solid red'});
                }
                else if ($("#bundlecode").val().length === 0) {
                    alert("Please Scan BundleCode")
                    $("#bundlecode").focus().css({'border': '1px solid red'});
                } else {
                    var x = confirm("Do you Want to Proceed?")
                    if (x == false) {
                        return false
                    } else {
                        var bundlelistsample = "";
                        for (var i = 0; i < bundlelist.length; i++) {
                            bundlelistsample = bundlelistsample + bundlelist[i] + "~"
                        }
//                        alert(bundlelistsample);
                        $("#bundle").val(bundlelistsample);
                        document.forms[0].mode.value = "submitDetails";
                        document.forms[0].submit();
                    }
                }
            }
            function rangeCheck() {
                if ((parseInt($("#bundlecode").val()) < 69450) || (parseInt($("#bundlecode").val()) > 75000)) {
                    alert("Invalid BundleCode")
                    $("#bundlecode").val("");
                } else {
                    var paraData = "bundlecode=" + $("#bundlecode").val();
                    $.ajax({
                        type: "POST",
                        url: "bundlePaper.do?mode=getBundleStatus&bundlecode=" + $("#bundlecode").val().trim(),
                        data: paraData,
                        success: function(response) {
                            var sample = response.trim().split("_");
                            var res = sample[0];
                            if (res > 0) {
                                alert("Bundle Already Scanned");
                                $("#bundlecode").val();
                            }
                        }

                    });
                }
            }
            function resetData() {
                bundlelist = [];
                paperlist = [];
                $("#bundle").val("");
                $("#epaper").val("0");
                $("#gradename").val("0");
                $("#institute").val("0");
                $("#instaddress").val("0");
                $("#instcode").val("0");
                $("#instname").val("0");
                $("#bookletsDiv").hide();
            }
        </script>
    </head>
    <br/><br/>
    <body>
        <div class="feedback-form">
            <div class="container">
                <div class="row">

                    <h3 class="block-head-News">Booklets Bundling</h3>
                    <div class="line-border"></div>
                    <br>
                    <div class="row">
                        <div class=" col-md-12">
                            <!--<div class="login-form">-->
                            <div>
                                <html:form action="/bundlePaper"  styleId="d" method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <html:hidden property="bundle" styleId="bundle"/>
                                    <html:hidden property="hallticket" styleId="hallticket"/>
                                    <html:hidden property="epaper" styleId="epaper"/>
                                    <logic:present name="result2">
                                        <span class="msg"><center> <font color="green" style="font-weight: bold">${result2}</font>  </center>  </span>
                                            </logic:present>
                                            <logic:present name="result">
                                        <span class="msg"><center> <font color="red" style="font-weight: bold">${result}</font></center></span>
                                    </logic:present><br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination  Grade<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:select property="grade" styleId="grade" styleClass="form-control" onchange="resetData();addDetails();">
                                                        <html:option value="0">--Select Grade--</html:option> 
                                                        <html:optionsCollection property="gradeslist" label="gname" value="gcode"/>
                                                    </html:select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination  Paper<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:select property="ecenter" styleId="ecenter" styleClass="form-control" onchange="resetData();addDetails();">
                                                        <html:option value="0">--Select Paper--</html:option> 
                                                        <html:option value="I">PAPER-I</html:option> 
                                                        <html:option value="II">PAPER-II</html:option> 
                                                    </html:select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination Batch<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:select property="examinationname" styleId="examinationname" styleClass="form-control" onchange="resetData();addDetails();">
                                                        <html:option value="0">--Select Batch--</html:option> 
                                                        <html:option value="B1">Batch-I</html:option> 
                                                        <html:option value="B2">Batch-II</html:option> 
                                                        <html:option value="B3">Batch-III</html:option> 
                                                        <html:option value="B4">Batch-IV</html:option> 
                                                    </html:select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Scan Barcode<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:text  styleClass="form-control" property="barcode" styleId="barcode" maxlength="100" onchange="addDetails();"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="bundleDiv">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label"><font color="red">Scan Bundle Code*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:text  styleClass="form-control" property="bundlecode" styleId="bundlecode" maxlength="6" onchange="rangeCheck()"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div id="bookletsDiv">
                                        <table class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Total Booklets</th>
                                                    <th>To be Bundled Booklets</th>
                                                    <th>Bundles Formed Booklets</th>
                                                    <th>Scanned Booklets For Current Bundle</th>
                                                    <th>To be Scanned Booklets</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                            <td> <html:text  styleClass="form-control" property="gradename" styleId="gradename" maxlength="100" readonly="true"/></td>
                                            <td> <html:text  styleClass="form-control" property="institute" styleId="institute" maxlength="100" readonly="true"/></td>
                                            <td><html:text  styleClass="form-control" property="instaddress" styleId="instaddress" maxlength="100" readonly="true"/></td>
                                            <td> <html:text  styleClass="form-control" property="instcode" styleId="instcode" maxlength="100" readonly="true"/></td>
                                            <td> <html:text  styleClass="form-control" property="instname" styleId="instname" maxlength="100" readonly="true"/></td>
                                            </tbody>
                                        </table>
                                    </div>


                                    <div id="brDiv">
                                        <br/> <br/> <br/> <br/> <br/>
                                        <br/> <br/> <br/> <br/> <br/>
                                        <br/> <br/> <br/> <br/> <br/>
                                        <br/> <br/> <br/> <br/> <br/>
                                    </div>
                                    <div id="showDiv1">
                                        <table class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                            <tbody>
                                            <td style="text-align: center;"><html:text styleClass="form-control" property="ebatch" styleId="ebatch" /></td>
                                            </tr>
                                            <%-- </logic:iterate>--%>
                                            </tbody>
                                        </table>
                                        <br/>
                                        <!--<table id="tablecontainer" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%"></table>-->
                                        <br/>
                                        <br/>
                                    </div>
                                    <div id="showDiv">
                                        <ul id="list"></ul>

                                    </div>
                                    <div id="brDiv1">
                                        <br/><br/><br/>
                                        <br/><br/><br/>
                                        <br/><br/><br/>
                                        <br/><br/><br/>
                                        <br/><br/><br/>
                                    </div>
                                    <br/>

                                    <div id="buttonDiv">
                                        <div class="row">
                                            <div class="col-md-12 center">
                                                <center><input type="button" onclick="return SubmitForm()" value="submit" class="btn btn-primary" style="max-width: 96px;"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                                <br/>
                            </html:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
            var bundlelist = [];
            function addDetails() {
                if ($("#grade").val() == "0") {
                    $("#grade").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#ecenter").val() == "0") {
                    $("#ecenter").focus().css({'border': '1px solid red'});

                    return false;
                }
                else if ($("#examinationname").val() == "0") {
                    $("#examinationname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#barcode").val().length === 0 && $("#bundlecode").val().length === 0) {
                    $("#barcode").val("");
                    return false;
                }
                else {
                    var paraData = "barcode=" + $("#barcode").val();
                    $.ajax({
                        type: "POST",
                        url: "bundlePaper.do?mode=getStatus&barcode=" + $("#barcode").val().trim() + "&grade=" + $("#grade").val().trim() + "&ecenter=" + $("#ecenter").val().trim()+ "&examinationname=" + $("#examinationname").val().trim(),
                        data: paraData,
                        success: function(response) {
                            var sample = response.trim().split("_");
                            var res = sample[0];
                            if (res == 1) {
                                $("#brDiv").hide();
                                $("#bookletsDiv").show();
                                var count = sample[1];
                                var regno1 = sample[2];
                                var instaddress = sample[3];
                                var gradename = sample[4];
                                $("#institute").val(count);
                                $("#hallticket").val(regno1);
                                $("#instaddress").val(instaddress);
                                $("#gradename").val(gradename);
                                var tobescannedcount = parseInt($("#institute").val()) - parseInt($("#instaddress").val());
                                $("#epaper").val(tobescannedcount);
                                if (tobescannedcount === 0) {
                                    alert("Bundling of Booklets Completed")
                                    $("#grade").val("0");
                                    $("#ecenter").val("0");
                                } else {
                                    if (tobescannedcount >= 40) {
                                        if (paperlist.length < 40) {
                                            if (paperlist.indexOf(regno1) === -1) {
                                                paperlist.push($("#hallticket").val());
                                                $("#instcode").val(paperlist.length);
                                                var bcode = $("#barcode").val();
                                                var regno = "'" + $("#hallticket").val() + "'";
                                                var bcode1 = "'" + $("#barcode").val() + "'";
                                                bundlelist.push(bcode)
                                                var tobescanned = parseInt($("#institute").val()) - (parseInt($("#instaddress").val()) + paperlist.length);
                                                $("#instname").val(tobescanned);
                                                $("#showDiv").hide();
                                                if (paperlist.length == 40) {
                                                    $("#brDiv1").hide();
                                                    $("#bundleDiv").show();
                                                    $("#buttonDiv").show();
                                                    $("#bundlecode").focus().css({'border': '1px solid red'});
                                                    $("#barcode").prop('readonly', true);
                                                } else {
                                                    $("#brDiv1").show();
                                                    $("#bundleDiv").hide();
                                                    $("#buttonDiv").hide();
                                                    $("#barcode").prop('readonly', false);
                                                }
                                                $("#list").append('<li id=' + (regno) + '>' + bcode + '<a href="#" onclick="deleteitem(' + (regno) + ',' + (bcode1) + ')"><span class="close">&times;</span></a></li>');
//                                        $("#list").append('<li id=' + regno + '>' + bcode + '<a href="#" onclick="deleteitem(' + regno + ')"><span class="close">&times;</span></a></li>');
                                                $("#barcode").val("");
                                            }
                                            else {
                                                alert("Booklet Already Scanned");
                                                $("#barcode").val("");
                                            }
                                        } else {
                                            $("#buttonDiv").show();
                                            $("#bundleDiv").show();
                                            $("#brDiv1").hide();
                                            $("#bundlecode").focus().css({'border': '1px solid red'});
                                            $("#barcode").val("");
                                            alert("Maximum Count reached.Please Scan BundleCode")
                                        }
                                    } else if (paperlist.length < tobescannedcount) {
                                        if (paperlist.indexOf(regno1) === -1) {
                                            paperlist.push($("#hallticket").val());
                                            var bcode = $("#barcode").val();
                                            var regno = "'" + $("#hallticket").val() + "'";
                                            var bcode1 = "'" + $("#barcode").val() + "'";
                                            bundlelist.push(bcode)
                                            $("#instcode").val(paperlist.length);
                                            var tobescanned = parseInt($("#institute").val()) - (parseInt($("#instaddress").val()) + paperlist.length);
                                            $("#instname").val(tobescanned);
                                            $("#showDiv").hide();
                                            if (paperlist.length == tobescannedcount) {
                                                $("#brDiv1").hide();
                                                $("#bundleDiv").show();
                                                $("#buttonDiv").show();
                                                $("#bundlecode").focus().css({'border': '1px solid red'});
                                                $("#barcode").prop('readonly', true);
                                            } else {
                                                $("#brDiv1").show();
                                                $("#bundleDiv").hide();
                                                $("#buttonDiv").hide();
                                                $("#barcode").prop('readonly', false);
                                            }
                                            $("#list").append('<li id=' + (regno) + '>' + bcode + '<a href="#" onclick="deleteitem(' + (regno) + ',' + (bcode1) + ')"><span class="close">&times;</span></a></li>');
//                                        $("#list").append('<li id=' + regno + '>' + bcode + '<a href="#" onclick="deleteitem(' + regno + ')"><span class="close">&times;</span></a></li>');
                                            $("#barcode").val("");
                                        } else {
                                            alert("Booklet Already Scanned");
                                            $("#barcode").val("");
                                        }
                                    } else {
                                        $("#buttonDiv").show();
                                        $("#bundleDiv").show();
                                        $("#brDiv1").hide();
                                        $("#bundlecode").focus().css({'border': '1px solid red'});
                                        $("#barcode").val("");
                                        alert("Maximum Count reached.Please Scan BundleCode")
                                    }
                                }
                            } else if (res == 2) {
                                alert("Booklet Already Mapped For another Bundle")
                                $("#barcode").val("");
                            }
                            else {
                                alert("Invalid Barcode")
                                $("#barcode").val("");
                            }
                        }
                    });
                }
            }
            function finalDetails() {

            }
            function deleteitem(id, barc) {
                paperlist = jQuery.grep(paperlist, function(value) {
                    return value != id;
                });
                bundlelist = jQuery.grep(bundlelist, function(value) {
                    return value != barc;
                });
                $("#instcode").val(paperlist.length);
                var tobescanned = parseInt($("#institute").val()) - paperlist.length;
                $("#instname").val(tobescanned);
                $("#" + id).remove();
                $("#barcode").prop('readonly', false);
                $("#barcode").focus().css({'border': '1px solid red'});
            }
    </script>
</body>
</html>
