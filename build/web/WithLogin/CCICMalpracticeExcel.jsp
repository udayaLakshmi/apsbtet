<%-- 
    Document   : MalpracticeExcel
    Created on : Jul 29, 2021, 11:51:35 PM
    Author     : 1820530
--%>

<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            int i = 0;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <!--<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">-->

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/demo.js">
        </script>
        <script type="text/javascript" class="init">

            $(document).ready(function() {
                var table = $('#example').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    fixedColumns: true
                });
            });
        </script>
        <style type="text/css">
            th, td { white-space: nowrap; }
            div.dataTables_wrapper {
                margin: 0 auto;
            }
            /* 
               div.container {
                   width: 60%;
               }*/
            table.dataTable
            {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.dataTable thead th {
                padding: 10px 18px;
                border: 1px solid #115400 !important;
                color: #fff;
                background-color: #1d7806 !important;
            }
            table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #115400 !important;
            }
            .h3_head{
                background: #337ab7;
                color: #fff;
                font-size: 16px;
                padding: 10px !important;
                text-align: center;
                box-shadow: 5px 5px 5px #ccc;
            }
        </style>
        <style type="text/css">
            #example_wrapper
            {
                width: 70% !important;
            }
        </style>

    </head>
    <body>


        <h3 class="block-head-News">CCIC Malpractice Report</h3>

        <logic:present  name="listData">

            <div id="dataDiv">
                <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="80%">
                    <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Reg.No/Hall ticket No</th>
                            <th>Name of the Candidate</th>
                            <th>Father Name</th>
                            <th>Date of Birth</th>
                            <th>Institute Code/Private</th> 

                            <!--<th>Institute Name</th>-->
                            <!--<th>Institute Address</th>-->
                            <th>Center Code</th>
                            <th>Center Name</th>
                            <th>Course</th>
                            <th>Subject</th> 
                            <th>Category</th>
                            <th>Phone.No</th>
                            <th>Exam Date</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <logic:iterate name="listData" id="list">
                            <tr>
                                <td style="text-align: center;">${list.sno}</td>
                                <td style="text-align: left;">${list.regNo}</td>
                                <td style="text-align: center;">${list.name}</td>     
                                <td style="text-align: center;">${list.fatherName}</td>
                                <td style="text-align: center;">${list.dob}</td>
                                <td style="text-align: center;">${list.instcode}</td>
                                <!--<td style="text-align: center;">${list.instName}</td>-->

                                <!--<td style="text-align: left;">${list.instAddress}</td>-->
                                <td style="text-align: left;">${list.centerC}</td>
                                <td style="text-align: center;">${list.centerN}</td>     
                                <td style="text-align: center;">${list.courseId}</td>
                                <td style="text-align: center;">${list.subject}</td>
                                <td style="text-align: center;">${list.category}</td>
                                <td style="text-align: center;">${list.mobile}</td>
                                <td style="text-align: center;">${list.examDate}</td>

                                <td style="text-align: center;">${list.remarks}</td>
                            </tr>
                        </logic:iterate>
                    </tbody>

                </table>

            </div>
        </logic:present>
    </body>
</html>
