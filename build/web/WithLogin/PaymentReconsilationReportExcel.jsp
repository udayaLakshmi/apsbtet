<%-- 
    Document   : PaymentReconsilationReportExcel
    Created on : Jul 12, 2021, 7:10:17 PM
    Author     : 1820530
--%>
<%@page contentType="application/vnd.ms-excel" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<!DOCTYPE html>
<html>
    <head>
        <%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>

        <title>Payment Reconsilation</title>
    </head>
    <body>

        <h3 class="block-head-News">Payment Reconciliation Report</h3>
        <br>
        <%int count = 1;%>
        <logic:present name="nodata">
        <center><font color="red">${nodata}</font></center>
        </logic:present>

    <logic:present  name="paymentReconsilationReport">

        
        <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
         
                <tr>
                    <th>S.NO</th>
                    <th>PGRefNo</th>
                    <th>Request Id</th>
                    <th>Base Amount</th>
                    <th>Amount to be retained</th>
                    <th>Amount to be remitted</th>
                    <th>Convience Charges</th>
                    <th>GST</th>
                    <th>Customer Name</th>
                    <th>Payment Mode</th>
                    <th>Phone Number</th>
                    <th>Created Date</th>
                    <th>Course</th>
                                            <th>No.of Semesters</th>
                </tr>
           
                <logic:iterate name="paymentReconsilationReport" id="list">
                    <tr>
                        <td style="text-align: center;"><%=count++%></td>
                        <td style="text-align: left;">${list.pgRefNo}</td>
                        <td style="text-align: center;">${list.requestId}</td>     
                        <td style="text-align: right;">${list.baseAmount}</td>
                        <td style="text-align: right;">${list.amountRetained}</td>
                        <td style="text-align: right;">${list.amountRemitted}</td>
                        <td style="text-align: right;">${list.convineanceCharge}</td>
                        <td style="text-align: center;">${list.gst}</td>
                        <td style="text-align: left;">${list.customerName}</td>
                        <td style="text-align: center;">${list.paymentMode}</td>
                        <td style="text-align: right;">${list.phoneNum}</td>
                        <td style="text-align: right;">${list.createdDate}</td>
                         <td style="text-align: right;">${list.course}</td>
                                                    <td style="text-align: right;">${list.semesters}</td>
                    </tr>
                </logic:iterate>
            

        </table>
    </logic:present>
</body>
</html>
