
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <Style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control-plaintext{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
        </style>
        <style>
            .overlay{
                display: none;
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                z-index: 999;
                background: rgba(255,255,255,0.8) url("img/LOADER.gif") center no-repeat;
            }
            /* Turn off scrollbar when body element has the loading class */
            body.loading{
                overflow: hidden;   
            }
            /* Make spinner image visible when body element has the loading class */
            body.loading .overlay{
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $(document).ajaxStart(function() {
                    $("body").addClass("loading");
                });
                $(document).ajaxComplete(function() {
                    $("body").removeClass("loading");
                });

            });
            function space(evt, thisvalue)
            {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allows only Numerics");
                    return false;
                }
                return true;
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function telephoneValidation() {

                var mobileNo = $("#mobile").val();
                var len = mobileNo.length;
                if (len < 10) {
                    alert("Mobile Number Should be 10 digits");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim()) == '9999999999'
                        || ($("#mobile").val().trim()) == '8888888888' ||
                        ($("#mobile").val().trim()) == '7777777777' ||
                        ($("#mobile").val().trim()) == '6666666666') {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim().charAt(0) != '9'
                        && $("#mobile").val().trim().charAt(0) != '8' &&
                        $("#mobile").val().trim().charAt(0) != '7' &&
                        $("#mobile").val().trim().charAt(0) != '6')) {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
            }
            function SubmitForm() {
//            adharDuplicatevalidation();
                if ($("#instprincipal").val() === undefined || $("#instprincipal").val() === "") {
                    alert("Please enter Principal Name");
                    $("#instprincipal").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#mobile").val() === undefined || $("#mobile").val() === "") {
                    alert("Please enter Mobile No");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#mobile").val() === undefined || ($.trim($("#mobile").val()).length === 0 ||
                        $("#mobile").val().length < 10 || parseInt($("#mobile").val().trim()) < 6000000000)) {
                    alert("Enter Mobile Number");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#instname").val() === undefined || $("#instname").val() === "") {
                    alert("Please enter institute name");
                    $("#instname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#instaddress").val() === undefined || $("#instaddress").val() === "") {
                    alert("Please enter Institute Address");
                    $("#instaddress").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#insttown").val() === undefined || $("#insttown").val() === "") {
                    alert("Please enter Institute Town");
                    $("#insttown").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#instpincode").val() === undefined || $("#instpincode").val() === "") {
                    alert("Please enter Institute Pincode");
                    $("#instpincode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    var x = confirm("Do you want to proceed?")
                    if (x == false) {
                        return false
                    } else {

                        document.forms[0].mode.value = "submitData";
                        document.forms[0].submit();
                    }
                }
            }
            function showData() {
                $("#instcode").val('${instcode}')
                $("#instreg").val('${instreg}')
                $("#instname").val('${instname}')
                $("#instaddress").val('${instaddress}')
                $("#insttown").val('${insttown}')
                $("#instdist").val('${instdist}')
                $("#instpincode").val('${instpincode}')
                $("#instprincipal").val('${instprincipal}')
                $("#mobile").val('${instmobile}')

            }
            function isPincode() {
                var pincode = $("#instpincode").val();
                if (pincode.length < 6) {
                    $("#pincode").val("");
                    alert("Pincode must be 6 digits")
                    return true;  //wrong mail
                } else if (pincode == "000000") {
                    $("#instpincode").val("");
                    alert("Invalid Pincode")
                    return true;
                }
                else {
                    return false;

                }
            }
            function goHome(){
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
               function firstLettterCheck(val, id) {
                var firstChar = val.charAt(0);
                var letters = /^[A-Za-z]+$/;
                if (firstChar.match(letters)) {
                    return true;
                } else {
                    alert("First Character Must be Letter");
                    $("#" + id).val("");
                    return false;
                }
            }

        </script>
    <br/>
    <br/>
    <body onload="showData()">
        <div class="feedback-form">
            <div class="container">
                <div class="row">
                    <h3 class="block-head-News">Institute Details Updation<span style="padding-left: 1100px;"><input onclick="goHome();" type="button" name="BACK" value="BACK" class="btn btn-success" style="background-color: #06447d"></span> </h3>
                    <div class="line-border"></div>
                    <br>
                    <div class="row">
                        <div class=" col-md-12">
                            <!--<div class="login-form">-->
                            <div>
                                <html:form action="/instituteedit"  method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <html:hidden property="instreg" styleId="instreg"/>
                                    <html:hidden property="instcode" styleId="instcode"/>
                                    <logic:present name="result2">
                                        <center> <font color="green" style="font-weight: bold">${result2}</font>  </center>  
                                        </logic:present>
                                        <logic:present name="result">
                                        <center> <font color="red" style="font-weight: bold">${result}</font></center>
                                    </logic:present><br>
<!--                                    <h5 class="block-head-News">Institute Details</h5>
                                    <div class="line-border"></div>-->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Principal Name<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="instprincipal" styleId="instprincipal" maxlength="150" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');"  />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Mobile No<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="mobile" styleId="mobile"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="telephoneValidation()" maxlength="10"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Institute Name<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="instname" styleId="instname"  maxlength="200" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'HouseNo');" onchange="firstLettterCheck(this.value,this.id)"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Institute Address<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="instaddress" styleId="instaddress" maxlength="200" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'HouseNo');"  onchange="firstLettterCheck(this.value,this.id)" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Institute Town<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="insttown" styleId="insttown" maxlength="100" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'HouseNo');"   onchange="firstLettterCheck(this.value,this.id)"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Institute Pincode<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="instpincode" styleId="instpincode" maxlength="6" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="isPincode()"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Institute District<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="instdist" styleId="instdist" readonly="true"/>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-12 center">

                                                <center>   <input type="button" onclick="return SubmitForm();" value="SUBMIT"  class="submit"/></center>
                                            </div>
                                        </div>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </html:form>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>	
            </div>
    </body>
</html>
