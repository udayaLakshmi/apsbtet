
<%-- 
    Document   : CourseStatusReport
    Created on : Jul 23, 2021, 1:51:42 PM
    Author     : APTOL301294
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html>
    <head>
        <%int i = 0;%>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <script type="text/javascript">

            $(document).ready(function() {
                $("#datediv").hide();
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }
            select option { color: black !important; }
            select:invalid {
                color: green;
            }
        </style>
        <script type="text/javascript">

            function validFields() {
                if ($('#course').val() == "0") {
                    alert("Select Year/Semester");
                    $('#course').focus();
                    return false;
                } else if ($('#batchCode').val() == "0") {
                    alert("Select Branch");
                    $('#batchCode').focus();
                    return false;
                } else {
                    document.forms[0].mode.value = "getData";
                    document.forms[0].submit();
                }

            }

            function getBranchList(courseId) {
                var data = "courseId=" + $('#' + courseId).val();
                $.ajax({
                    type: "POST",
                    url: "./semWiseSubjectsReport.do?mode=getBranchList&courseId=" + $('#' + courseId).val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        if (response.trim() == "0") {
                            alert("No Subject available with given Registration No");
                            $("#batchCode").empty().append("<option value='0'>--Select Branch--</option>");
                        } else {
                            $("#batchCode").empty().append(response);
                        }
                        $("#centerCode").empty().append("<option value='0'>--Select--</option>");

                    },
                    error: function(e) {
                    }
                });
            }

            function validFields1() {
                if ($('#course').val() == "0") {
                    alert("Select Year/Semester");
                    $('#course').focus();
                    return false;
                } else if ($('#batchCode').val() == "0") {
                    alert("Select Branch");
                    $('#batchCode').focus();
                    return false;
                } else if ($('#centerCode').val() == "0") {
                    alert("Select Center Code");
                    $('#centerCode').focus();
                    return false;
                } else {
                    document.forms[0].mode.value = "getData";
                    document.forms[0].submit();
                }

            }

            function hideData() {
                $('.DataDiv').hide();
            }

            function getSubjectList(courseId) {
                var data = "courseId=" + $('#' + courseId).val();
                $.ajax({
                    type: "POST",
                    url: "./semWiseSubjectsReport.do?mode=getSubject&courseId=" + $('#' + courseId).val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        if (response.trim() == "0") {
                            alert("No Subject available with given Registration No");
                            $("#subjectCode").empty().append("<option value='0'>--Select Subject--</option>");
                        } else {
                            $("#subjectCode").empty().append(response);
                        }

                    },
                    error: function(e) {
                    }
                });
            }
            function getCenterList(courseId, batchCode) {
                if ($('#role').val() == "18") {
                } else {
                    var data = "courseId=" + $('#' + courseId).val() + "&branchC=" + $('#' + batchCode).val();
                    $.ajax({
                        type: "POST",
                        url: "./semWiseSubjectsReport.do?mode=getCenterList&courseId=" + $('#' + courseId).val() + "&branchC=" + $('#' + batchCode).val(),
                        data: data,
                        cache: true,
                        contentType: true,
                        processData: true,
                        success: function(response) {
                            if (response.trim() == "0") {
                                $("#centerCode").empty().append("<option value='0'>--Select--</option>");
                            } else {
                                $("#centerCode").empty().append(response);
                            }

                        },
                        error: function(e) {
                        }
                    });
                }
            }

        </script>

    </head>
    <body>
        <div class="row mainbodyrow">
            <div class="container" style="padding-top: 15px;">
                <div class="col-xs-12">
                    <div class="maindodycnt">
                        <h3 class="block-head-News">Subject Wise Report</h3>
                        <div class="line-border"></div>
                        <html:form action="/semWiseSubjectsReport" >
                            <html:hidden property="mode"/>
                            <input type="hidden" name="role" id="role" value="${role}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Year/<br>Semester<font color="red">*</font> </label>
                                        <div class="col-sm-6">
                                            <html:select property="course" styleId="course" styleClass="form-control" onchange="hideData(),getBranchList('course');">
                                                <html:option value="0">--Select--</html:option>
                                                <logic:present name="courseWiseList">
                                                    <html:optionsCollection property="courseWiseList" label="courseName" value="ccd"/>
                                                </logic:present>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Branch<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="batchCode" styleId="batchCode" styleClass="form-control" onchange="hideData(),getCenterList('course','batchCode')">
                                                <html:option value="0">--Select Branch--</html:option> 
                                                <html:optionsCollection property="batchlist" label="braN" value="braC"/>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                                <logic:notPresent name="centerAndInstLogin">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-2 col-form-label">Exam Center<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:select property="centerCode" styleId="centerCode" styleClass="form-control" onchange="hideData()">
                                                    <html:option value="0">--Select--</html:option> 
                                                    <logic:present name="centerlist">
                                                        <html:optionsCollection name="centerlist" label="centerName" value="centercode"/>
                                                    </logic:present>
                                                </html:select>
                                            </div>
                                        </div>
                                    </div>
                                </logic:notPresent>
                            </div>

                            <logic:notPresent name="centerAndInstLogin">
                                <table align="center"  cellpadding="0" cellspacing="0" border="0" id="altrowstable1"  class="table"  style="margin: 0px auto;"> 
                                    <tr>
                                        <td style="text-align:  center;padding: 5px !important;" colspan="4"  >
                                            <input type="Submit" value="Submit" style="width: auto" class="btn btn-primary" onclick="return validFields1();"></td>
                                    </tr>
                                </table>
                            </logic:notPresent>

                            <logic:present name="centerAndInstLogin">
                                <table align="center"  cellpadding="0" cellspacing="0" border="0" id="altrowstable1"  class="table"  style="margin: 0px auto;"> 
                                    <tr>
                                        <td style="text-align:  center;padding: 5px !important;" colspan="4"  >
                                            <input type="Submit" value="Submit" style="width: auto" class="btn btn-primary" onclick="return validFields();"></td>
                                    </tr>
                                </table>

                            </logic:present>


                            <logic:present name="result">
                                <center> <font color="red" style="font-weight: bold">${result}</font></center>
                                </logic:present>
                            <div class="DataDiv">

                                <logic:present  name="listData">
                                    <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                        <a href="./semWiseSubjectsReport.xls?mode=downloadExcel&courceCode=<%=request.getAttribute("courceCode")%>&uName=<%=request.getAttribute("uName")%>&cCode=<%=request.getAttribute("cCode")%>&subCode=<%=request.getAttribute("subCode")%>"><img src="img/excel.png" style="width: 35px"/></a>
                                        <!--<a href="./semWiseSubjectsReport.do?mode=getPdfData&courceCode=<%=request.getAttribute("courceCode")%>&uName=<%=request.getAttribute("uName")%>&cCode=<%=request.getAttribute("cCode")%>&subCode=<%=request.getAttribute("subCode")%>"><img src="img/pdf.png" style="width: 30px"/></a>-->
                                    </div>
                                    <div id="dataDiv">
                                        <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>S.NO</th>
                                                    <th>PIN NUMBER</th>
                                                    <th>Name</th>
                                                    <th>SUB1</th>
                                                    <th>SUB2</th>
                                                    <th>SUB3</th>
                                                    <th>SUB4</th>
                                                    <th>SUB5</th>
                                                    <th>SUB6</th>
                                                    <th>SUB7</th>
                                                    <th>SUB8</th>
                                                    <th>SUB9</th>
                                                    <th>SUB10</th>
                                                    <th>SUB11</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <logic:iterate name="listData" id="list">
                                                    <tr>
                                                        <td style="text-align: center;"><%=++i%></td>
                                                        <td style="text-align: center;">${list.rollNo}</td>
                                                        <td style="text-align: center;">${list.name}</td>     
                                                        <td style="text-align: center;">${list.sub1}</td>
                                                        <td style="text-align: center;">${list.sub2}</td>
                                                        <td style="text-align: center;">${list.sub3}</td>
                                                        <td style="text-align: center;">${list.sub4}</td>     
                                                        <td style="text-align: center;">${list.sub5}</td>
                                                        <td style="text-align: center;">${list.sub6}</td>
                                                        <td style="text-align: center;">${list.sub7}</td>
                                                        <td style="text-align: center;">${list.sub8}</td>
                                                        <td style="text-align: center;">${list.sub9}</td>
                                                        <td style="text-align: center;">${list.sub10}</td>
                                                        <td style="text-align: center;">${list.sub11}</td>
                                                    </tr>
                                                </logic:iterate>
                                            </tbody>
                                            <tfoot>
                                            <td colspan="3" style="text-align: center;">Total</td>
                                            <td style="text-align: center;">${list.totalsub1}</td>
                                            <td style="text-align: center;">${list.totalsub2}</td>
                                            <td style="text-align: center;">${list.totalsub3}</td>
                                            <td style="text-align: center;">${list.totalsub4}</td>     
                                            <td style="text-align: center;">${list.totalsub5}</td>
                                            <td style="text-align: center;">${list.totalsub6}</td>
                                            <td style="text-align: center;">${list.totalsub7}</td>
                                            <td style="text-align: center;">${list.totalsub8}</td>
                                            <td style="text-align: center;">${list.totalsub9}</td>
                                            <td style="text-align: center;">${list.totalsub10}</td>
                                            <td style="text-align: center;">${list.totalsub11}</td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </logic:present>
                            </div>
                        </html:form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>                                       