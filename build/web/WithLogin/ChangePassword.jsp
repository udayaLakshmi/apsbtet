<%-- 
    Document   : Teachersprint
    Created on : Jun 5, 2018, 7:39:44 PM
    Author     : 1042564
--%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<form>
    <head>
        <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
        <script src="./js/jquery.min.js"></script>
        <script src ="./js/jquery.js"></script>
        <script src ="./js/md5/md5.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #f59042 !important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: white !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }
            .update{
                width: 30% !important;
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
            .h3_head{
                background: #0085b7;
                font-size: 15px;
                padding: 5px;
                text-align: center;
                color: #fff;
                line-height: 26px;
                box-shadow: 5px 5px 5px #ccc;
            }
            .login-form{
                background: #f1f0f0;
                padding: 35px;
            }
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }
        </style>
        <script>
            function passwordPolicy(id) {
                if ($("#" + id).val().length < 8) {
                    $("#" + id).val("");
                    alert("The password does not meet the password policy requirements");
                    $("#" + id).focus().css({'border': '1px solid red'});
                    return false;
                } else {
                    var y = $("#" + id).val();
                    if (y.search(/[a-z]/) < 0) {
                        $("#" + id).val("");
                        alert("The password does not meet the password policy requirements");
                        $("#" + id).focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if (y.search(/[A-Z]/) < 0) {
                        $("#" + id).val("");
                        alert("The password does not meet the password policy requirements");
                        $("#" + id).focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if (y.search(/[0-9]/) < 0) {
                        $("#" + id).val("");
                        alert("The password does not meet the password policy requirements");
                        $("#" + id).focus().css({'border': '1px solid red'});
                        return false;
                    }
                    var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                    if (!format.test(y)) {
                        $("#" + id).val("");
                        alert("The password does not meet the password policy requirements");
                        $("#" + id).focus().css({'border': '1px solid red'});
                        return false;
                    }
                }
            }
            function submitData() {
                if ($("#oldPassword").val() == "") {
                    alert("Please Enter Old Password");
                    $("#oldPassword").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#newPassword").val() == "") {
                    alert("Please Enter New Password");
                    $("#newPassword").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#newPassword").val().length < 8) {
                    alert("Please Enter New Password length greater than 8");
                    $("#newPassword").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#confirmPassword").val() == "") {
                    alert("Please Enter Confirm Password");
                    $("#confirmPassword").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#oldPassword").val() == $("#newPassword").val()) {
                    alert("Please check password can not be same with previous or old password");
                    $("#newPassword").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#newPassword").val() !== $("#confirmPassword").val()) {
                    alert("Please Enter Password should match");
                    $("#newPassword").focus().css({'border': '1px solid red'});
                    return false;
                } else {

                    var y = $("#newPassword").val();
                    if (y.search(/[a-z]/) < 0) {
                        alert("Your password needs a lower case letter");
                        $("#newPassword").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if (y.search(/[A-Z]/) < 0) {
                        alert("Your password needs an uppser case letter");
                        $("#newPassword").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if (y.search(/[0-9]/) < 0) {
                        alert("Your password needs a number");
                        $("#newPassword").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                    if (format.test(y)) {
                        var newpass = calcMD5($("#newPassword").val());
                        document.forms[0].newNormPassword.value = newpass;
                        document.forms[0].mode.value = "changePassword";
                        document.forms[0].submit();
                    } else {
                        alert("Your password must contains one special characters");
                        $("#newPassword").focus().css({'border': '1px solid red'});
                        return false;
                    }
                }
            }
            function onlyNumbersWithchar(e) {
                var keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
                var ret = ((keyCode !== 32) || (specialKeys.indexOf(e.keyCode) !== -1 && e.charCode !== e.keyCode));
                if (ret === false)
                    alert("This key not allowed");
                return ret;
            }
            window.onload = function() {
                $('input, :input').attr('autocomplete', 'off');
                $("form").attr('autocomplete', 'off');
                document.getElementById("oldPassword").placeholder = "Old Password";
                document.getElementById("newPassword").placeholder = "New Password";
                document.getElementById("confirmPassword").placeholder = "Confirm Password";
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
            };
            function space(evt, thisvalue) {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
        </script>
    </head>
    <body>
        <div class="row mainbodyrow">
            <div class="container" style="padding-top: 40px;">
                <div class="col-xs-12">
                    <div class="maindodycnt">
                        <h3 class="block-head-News">Change Password</h3>
                        <div class="line-border"></div>
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">

                                    <div class="login-form">
                                        <html:form action="/changePassword" method="post">
                                            <html:hidden property="mode"/>
                                            <html:hidden property="newNormPassword"/>
                                            <logic:present name="result1">
                                                <span id="msg" ><center> <font color="green" style="font-weight: bold">${result1}</font></center></span>
                                                    </logic:present>
                                                    <logic:present name="result">
                                                <span id="msg" ><center> <font color="red" style="font-weight: bold">${result}</font></center></span>
                                            </logic:present><br>
                                               <div>
                                                    <span style="color: red;"><b>Password Policy</b></span><br>
                                                    <b>P</b>assword must be at least 8 characters.
                                                    <b>A</b>t least one upper case letter.
                                                    <b>A</b>t least one lower case letter.
                                                    <b>A</b>t least one numeric digit.
                                                    <b>A</b>t least one special character like (!,#,$,@,%,*).
                                                </div><br>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Old Password<font color="red">*</font></label>
                                                    <html:password  property="oldPassword" styleId="oldPassword" maxlength="12" onkeydown="return space(event,this);" styleClass="form-control"/>
                                            </div>       
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">New Password<font color="red">*</font></label>
                                                    <html:password  property="newPassword" styleId="newPassword" maxlength="12" onkeydown="return space(event,this);" onchange="return passwordPolicy('newPassword');" styleClass="form-control"/>
                                            </div>       
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Confirm Password<font color="red">*</font></label>
                                                    <html:password property="confirmPassword" styleId="confirmPassword" maxlength="12" onkeydown="return space(event,this);" onchange="return passwordPolicy('newPassword');" styleClass="form-control"/>
                                            </div>   
                                            <div class="col-md-12" >
                                                <html:submit onclick="return submitData();" value="UPDATE" styleClass="btn btn-primary" />
                                            </div> 
                                            <div class="clearfix"></div>

                                        </html:form>
                                    </div> 
                                </div>
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </body>
</form>    