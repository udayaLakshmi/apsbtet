<%-- 
    Document   : ExamCentreWiseTWReport
    Created on : Jul 28, 2021, 12:48:15 PM
    Author     : 1820530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>
       
       
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  


        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">


        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[13, -1], [13, "All"]]
                });
                $('#example1').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
            tfoot {
             background-color: #6699ff!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>
    </head>
    <body>
         <html:form action="/examCentreWiseTW">
            
             <div class="row mainbodyrow">
                <div class="container" style="padding-top: 15px; " >
                    <div class="col-xs-12 ">
                        <div class="maindodycnt">
                            <h3 class="block-head-News">TWSH Exam Center Wise TW Report</h3>
                            <div class="line-border"></div>


                            <%int count = 1;%>
                            <logic:present name="nodata">
                                <center><font color="red">${nodata}</font></center>
                                </logic:present>

                                <logic:present name="ExamCenterTWList">
                                <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                    <a href="./examCentreWiseTW.xls?mode=downloadExcel ">
                                        <img src="img/excel.png" style="width: 35px"/></a>
                                </div>
                                <br>
                              
                                <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center" rowspan="2">S.NO</th>
                                            <th style="text-align: center" rowspan="2"> Exam Center Code</th>
                                            <th style="text-align: center" rowspan="2">Exam center Name</th>
                                            <th style="text-align: center" colspan="4">Typewriting English Lower</th>
                                            <th style="text-align: center" colspan="4">Typewriting English Higher</th>
                                            <th style="text-align: center" colspan="4">Typewriting Telugu Lower</th>
                                            <th style="text-align: center" colspan="4">Typewriting Telugu Higher</th>
                                            <th style="text-align: center" colspan="2">Typewriting English Junior</th>
                                            <th style="text-align: center" >Typewriting Hindi Lower</th>
                                            <th style="text-align: center" >Typewriting Hindi Higher</th>
                                            <th style="text-align: center" >Typewriting English High Speed</th>
                                            <th style="text-align: center" >Typewriting Telugu High Speed</th>
                                            <th style="text-align: center" rowspan="2" >Total Strength</th>
                                           
                                        </tr>
                                        <tr>
                                            <th>B1</th>
                                            <th>B2</th>
                                            <th>B3</th>
                                            <th>B4</th>
                                            
                                            <th>B1</th>
                                            <th>B2</th>
                                            <th>B3</th>
                                            <th>B4</th>
                                            
                                            <th>B1</th>
                                            <th>B2</th>
                                            <th>B3</th>
                                            <th>B4</th>
                                            
                                            <th>B1</th>
                                            <th>B2</th>
                                            <th>B3</th>
                                            <th>B4</th>
                                            
                                            <th>B1</th>
                                            <th>B2</th>
                                            
                                            <th>B1</th>
                                            <th>B1</th>
                                            <th>B1</th>
                                            <th>B1</th>
                                        </tr>
                                        <bean:define id="total1" value="0"/>
                                        <bean:define id="total2" value="0"/>
                                        <bean:define id="total3" value="0"/>
                                        <bean:define id="total4" value="0"/>
                                        <bean:define id="total5" value="0"/>
                                        <bean:define id="total6" value="0"/>
                                        <bean:define id="total7" value="0"/>
                                        <bean:define id="total8" value="0"/>
                                        <bean:define id="total9" value="0"/>
                                        <bean:define id="total10" value="0"/>
                                        <bean:define id="total11" value="0"/>
                                        <bean:define id="total12" value="0"/>
                                        <bean:define id="total13" value="0"/>
                                        <bean:define id="total14" value="0"/>
                                        <bean:define id="total15" value="0"/>
                                        <bean:define id="total16" value="0"/>
                                        <bean:define id="total17" value="0"/>
                                        <bean:define id="total18" value="0"/>
                                        <bean:define id="total19" value="0"/>
                                        <bean:define id="total20" value="0"/>
                                        <bean:define id="total21" value="0"/>
                                        <bean:define id="total22" value="0"/>
                                        <bean:define id="total23" value="0"/>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="ExamCenterTWList" id="list">
                                            <tr>
                                                <td style="text-align: center;"><%=count++%></td>
                                                <td style="text-align: left;">${list.centerCode}</td>
                                                <td style="text-align: left;">${list.centerName}</td>     
                                                <td style="text-align: right;">${list.typeWritingEngLowerB1}</td>
                                                <td style="text-align: right;">${list.typeWritingEngLowerB2}</td>
                                                <td style="text-align: right;">${list.typeWritingEngLowerB3}</td>
                                                <td style="text-align: right;">${list.typeWritingEngLowerB4}</td> 
                                                <td style="text-align: right;">${list.typeWritingEngHigherB1}</td>
                                                <td style="text-align: right;">${list.typeWritingEngHigherB2}</td>
                                                <td style="text-align: right;">${list.typeWritingEngHigherB3}</td> 
                                                <td style="text-align: right;">${list.typeWritingEngHigherB4}</td>
                                                <td style="text-align: right;">${list.typeWritingteluguLowerB1}</td>
                                                <td style="text-align: right;">${list.typeWritingteluguLowerB2}</td>
                                                <td style="text-align: right;">${list.typeWritingteluguLowerB3}</td>
                                                <td style="text-align: right;">${list.typeWritingteluguLowerB4}</td> 
                                                
                                                <td style="text-align: right;">${list.typeWritingteluguHigherB1}</td>
                                                <td style="text-align: right;">${list.typeWritingteluguHigherB2}</td>
                                                <td style="text-align: right;">${list.typeWritingteluguHigherB3}</td>
                                                <td style="text-align: right;">${list.typeWritingteluguHigherB4}</td> 
                                                
                                                <td style="text-align: right;">${list.typeWritingEngJnrB1}</td>
                                                <td style="text-align: right;">${list.typeWritingEngJnrB2}</td>
                                                
                                                <td style="text-align: right;">${list.typeWritingHindiLowerB1}</td>
                                                <td style="text-align: right;">${list.typeWritingHindiHigherB1}</td>
                                                
                                                <td style="text-align: right;">${list.typeWritingEngHighSpeedB1}</td>
                                                <td style="text-align: right;">${list.typeWritingTeluguHighSpeedB1}</td>
                                                <td style="text-align: right;">${list.totalStrength}</td>
                                                
                                                
                                            </tr>
                                            
                                             <bean:define id="total1" value="${total1+list.typeWritingEngLowerB1}"/>
                                            <bean:define id="total2" value="${total2+list.typeWritingEngLowerB2}"/> 
                                            <bean:define id="total3" value="${total3+list.typeWritingEngLowerB3}"/> 
                                            <bean:define id="total4" value="${total4+list.typeWritingEngLowerB4}"/>
                                            
                                            <bean:define id="total5" value="${total5+list.typeWritingEngHigherB1}"/>
                                            <bean:define id="total6" value="${total6+list.typeWritingEngHigherB2}"/> 
                                            <bean:define id="total7" value="${total7+list.typeWritingEngHigherB3}"/> 
                                            <bean:define id="total8" value="${total8+list.typeWritingEngHigherB4}"/>
                                            
                                            <bean:define id="total9" value="${total9+list.typeWritingteluguLowerB1}"/> 
                                            <bean:define id="total10" value="${total10+list.typeWritingteluguLowerB2}"/>                                         
                                             <bean:define id="total11" value="${total11+list.typeWritingteluguLowerB3}"/>
                                            <bean:define id="total12" value="${total12+list.typeWritingteluguLowerB4}"/> 
                                            
                                            <bean:define id="total13" value="${total13+list.typeWritingteluguHigherB1}"/> 
                                            <bean:define id="total14" value="${total14+list.typeWritingteluguHigherB2}"/>
                                            <bean:define id="total15" value="${total15+list.typeWritingteluguHigherB3}"/>
                                            <bean:define id="total16" value="${total16+list.typeWritingteluguHigherB4}"/> 
                                            
                                            <bean:define id="total17" value="${total17+list.typeWritingEngJnrB1}"/> 
                                            <bean:define id="total18" value="${total18+list.typeWritingEngJnrB2}"/>
                                            <bean:define id="total19" value="${total19+list.typeWritingHindiLowerB1}"/> 
                                            <bean:define id="total20" value="${total20+list.typeWritingHindiHigherB1}"/> 
                                            
                                            <bean:define id="total21" value="${total21+list.typeWritingEngHighSpeedB1}"/>
                                            <bean:define id="total22" value="${total22+list.typeWritingTeluguHighSpeedB1}"/>
                                            <bean:define id="total23" value="${total23+list.totalStrength}"/>

                                        </logic:iterate>  
                                    </tbody>
                                    
                                    <foot>
                                         <tr  style="background-color: white;">
                                            <td style="text-align: center " colspan="3" >
                                                <b> Total </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total1} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total2} </b>
                                            </td> 
                                            <td style="text-align: right ">
                                                <b> ${total3} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total4} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total5} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total6} </b>
                                            </td> 
                                            <td style="text-align: right ">
                                                <b> ${total7} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total8} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total9} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total10} </b>
                                            </td>
                                       
                                        <td style="text-align: right ">
                                                <b> ${total11} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total12} </b>
                                            </td> 
                                            <td style="text-align: right ">
                                                <b> ${total13} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total14} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total15} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total16} </b>
                                            </td> 
                                            <td style="text-align: right ">
                                                <b> ${total17} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total18} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total19} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total20} </b>
                                            </td>
                                             <td style="text-align: right ">
                                                <b> ${total21} </b>
                                            </td>
                                             <td style="text-align: right ">
                                                <b> ${total22} </b>
                                            </td>
                                             <td style="text-align: right ">
                                                <b> ${total23} </b>
                                            </td>
                                        </tr> 
                                    
                                    </foot>
                                     

                                </table>
                              </logic:present>
                           

                        </div>
                    </div>
                </div>
            </div>
            
        </html:form>
    </body>
</html>
