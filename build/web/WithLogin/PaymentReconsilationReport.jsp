<%-- 
    Document   : PaymentReconsilationReport
    Created on : Jul 12, 2021, 7:09:24 PM
    Author     : 1820530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  


        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">


        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[13, -1], [13, "All"]]
                });
                $('#example1').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>

        <Script>

            function validFields() {

                var date1 = document.getElementById("fromDate").value;
                var date2 = document.getElementById("toDate").value;

                var submitFromDate = date1.substring(6, 10) + "-" + date1.substring(3, 5) + "-" + date1.substring(0, 2);
                var submitToDate = date2.substring(6, 10) + "-" + date2.substring(3, 5) + "-" + date2.substring(0, 2);

               // alert("hi" + date1 + ":" + date2 + " :" + submitFromDate + " :" + submitToDate);
                if ($("#fromDate").val() === undefined || $("#fromDate").val() === "") {
                    alert("Please Select From Date");
                    $("#fromDate").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#toDate").val() === undefined || $("#toDate").val() === "") {
                    alert("Please Select To Date");
                    $("#toDate").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {

//                    document.forms[0].submitFrom.value = document.getElementById("fromDate").value;
//                    document.forms[0].toDate.value = submitToDate;

                    document.forms[0].mode.value = "getReconsilationReport";
                    document.forms[0].submit();
                }

            }

        </Script>

        <script>
            $(function() {
                var today = new Date();
                yrRange = '1998' + ":" + '2021';
                $('#fromDate').datepicker({
                    format: 'dd-mm-yyyy',
                    changeMonth: true,
                    minDate: new Date(1998, 12 - 04, 1),
                    autoclose: true,
                    changeYear: true,
                    yearRange: yrRange,
                    endDate: "today",
                    maxDate: today
                    
                    
                }).on('fromDate', function(ev) {
                    $(this).datepicker('hide');
                });


                $('#fromDate').keyup(function() {
                    if (this.value.match(/[^0-9]/g)) {
                        this.value = this.value.replace(/[^0-9^-]/g, '');
                    }
                });
            });</script>
        <script>
            $(function() {
                var today = new Date();
                 yrRange = '1998' + ":" + '2021';
                $('#toDate').datepicker({
                    format: 'dd-mm-yyyy',
                    changeMonth: true,
                    minDate: new Date(1998, 12 - 04, 1),
                    autoclose: true,
                    changeYear: true,
                    yearRange: yrRange,
                    endDate: "today",
                    maxDate: today
                    
                    
                }).on('toDate', function(ev) {
                    $(this).datepicker('hide');
                });


                $('#toDate').keyup(function() {
                    if (this.value.match(/[^0-9]/g)) {
                        this.value = this.value.replace(/[^0-9^-]/g, '');
                    }
                });
            });</script>

    </head>
    <body>
        <html:form action="/paymentReconsilation">
            <html:hidden property="mode"/>  
            <%--<html:hidden property="fromDate" />--%>  
            <%--<html:hidden property="toDate" />--%> 
            
             <html:hidden property="submitFrom" /> 


            <div class="row mainbodyrow">
                <div class="container" style="padding-top: 15px;">
                    <div class="col-xs-12">
                        <div class="maindodycnt">
                            <h3 class="block-head-News">Payment Reconciliation Report</h3>
                            <div class="line-border"></div>
                            
                            <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">From Date<font color="red">*</font> </label>
                                                <div class="col-sm-6">
                                                      <input type="text" Class="form-control readonly datepicker-here" data-language='en' id="fromDate" name="fromDate" value="${fromDate}" readonly="true" style="width: 200px;" placeholder="dd-mm-yyyy">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">TO Date <font color="red">*</font></label>
                                                <div class="col-sm-6">
                                                    <input type="text"  Class="form-control readonly datepicker-here" data-language='en' id="toDate" name="toDate" value="${toDate}"readonly="true" placeholder="dd-mm-yyyy" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                          <%--  <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <input type="Submit" value="Submit" style="width: auto" class="btn btn-primary" onclick="return validFields();">
                                                </div>
                                            </div>
                                        </div>  --%>
                                    </div>
                            <table align="center"  cellpadding="0" cellspacing="0" border="0" id="altrowstable1"  class="table"  style="margin: 0px auto;"> 
                                <tr>
                                    <td style="text-align:  center;padding: 5px !important;" colspan="4"  >
                                        <input type="Submit" value="Submit" style="width: auto" class="btn btn-primary" onclick="return validFields();"></td>
                                </tr>
                            </table>

                        <%--        <div class="col-md-6">
                                    <div class="form-group row" >
                                        <label for="fromDate" class="col-sm-3 col-form-label">From Date<font color="red">*</font></label>
                                        <div class="col-sm-6">                                           
                                            <html:text property="fromDate" styleId="fromDate" styleClass="form-control" readonly="true" />                                                                                    
                                            <input type="text" id="fromDate" name="fromDate" readonly="true" style="width: 50px;">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="toDate" class="col-sm-3 col-form-label">To Date<font color="red">*</font></label>
                                        <div class="col-sm-6" >                                           
                                            
                                            <input type="text" id="toDate" name="toDate" readonly="true" >
                                        </div>

                                    </div>
                                </div> --%>


<!--                            <table align="center"  cellpadding="0" cellspacing="0" border="0" id="altrowstable1"  class="table"  style="margin: 0px auto;"> 
                                <tr>
                                    <td style="text-align:  center;padding: 5px !important;" colspan="4"  >
                                        <input type="Submit" value="Submit" style="width: auto" class="btn btn-primary" onclick="return validFields();"></td>
                                </tr>
                            </table>-->

                            <%int count = 1;%>
                            <logic:present name="nodata">
                                <center><font color="red">${nodata}</font></center>
                                </logic:present>

                            <logic:present  name="paymentReconsilationReport">
                                <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                    <a href="./paymentReconsilation.xls?mode=downloadExcel&submitFrom=<%=request.getAttribute("fromDate")%>&submitTo=<%=request.getAttribute("toDate")%>&flag=<%=request.getAttribute("flag")%>" >
                                        <img src="img/excel.png" style="width: 35px"/></a>
                                </div>
                                <br>
                                <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>PGRefNo</th>
                                            <th>Request Id</th>
                                            <th>Base Amount</th>
                                            <th>Amount to be retained</th>
                                            <th>Amount to be remitted</th>
                                            <th>Convience Charges</th>
                                            <th>GST</th>
                                            <th>Customer Name</th>
                                            <th>Payment Mode</th>
                                            <th>Phone Number</th>
                                            <th>Created Date</th>
                                            <th>Course</th>
                                            <th>No.of Semesters</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="paymentReconsilationReport" id="list">
                                            <tr>
                                                <td style="text-align: center;"><%=count++%></td>
                                                <td style="text-align: left;">${list.pgRefNo}</td>
                                                <td style="text-align: center;">${list.requestId}</td>     
                                                <td style="text-align: right;">${list.baseAmount}</td>
                                                <td style="text-align: right;">${list.amountRetained}</td>
                                                <td style="text-align: right;">${list.amountRemitted}</td>
                                                <td style="text-align: right;">${list.convineanceCharge}</td>
                                                <td style="text-align: center;">${list.gst}</td>
                                                <td style="text-align: left;">${list.customerName}</td>
                                                <td style="text-align: center;">${list.paymentMode}</td>
                                                <td style="text-align: right;">${list.phoneNum}</td>
                                                <td style="text-align: right;">${list.createdDate}</td>
                                                  <td style="text-align: right;">${list.course}</td>
                                                    <td style="text-align: right;">${list.semesters}</td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>

                                </table>
                            </logic:present>

                        </div>
                    </div>
                </div>
            </div>


        </html:form>
    </body>
    <script src="./js/datePicker.js"></script>
</html>
