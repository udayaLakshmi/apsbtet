<%-- 
    Document   : practicalMarksEntry
    Created on : Jul 20, 2021, 9:01:36 PM
    Author     : 1582792
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable th {
                background-color: #20d0e29e !important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #82d7e01c;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }
            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style type="text/css">
            .modal-dialog {
                width: 85% !important;
                margin: 15px auto;
            }

            body {
                padding-right: 0 !important;
            }
            .expand-small-on-hover:hover {
                max-width : 200px; 
                text-overflow: ellipsis;
            }

            .expand-maximum-on-hover:hover {
                max-width : initial; 
            }
        </style>



    </head>

    <script>
      function mysearch() {
            if ($("#course").val() == "0") {
                alert("Please select Course");
                $("#course").focus();
                return false;
            }
            document.forms[0].mode.value = "getData";
            document.forms[0].target="";
            document.forms[0].submit();

        }
        
         function onPrint() {
            if ($("#course").val() == "0") {
                alert("Please select Course");
                $("#course").focus();
                return false;
            }
            document.forms[0].mode.value = "getPrintData";
            document.forms[0].target="_blank";
            document.forms[0].submit();

        }
        function marksvalidation(marks, maxMarks) {
//                  alert(marks+"  "+maxMarks)    
            var maxMaksVal = document.getElementById(maxMarks).value;
            var marksVal = document.getElementById(marks).value;
//                 alert(maxMaksVal+"  "+marksVal)
            if (parseInt(marksVal) > parseInt(maxMaksVal)) {
                alert("Marks Scored should be less than or equal to Max Marks");
                document.getElementById(marks).value = "";
                document.getElementById(marks).focus();
            }

        }
        function marksvalidation1(marks, maxMarks) {
            alert(marks + "  " + maxMarks)
            var maxMaksVal = document.getElementById(maxMarks).value;
            var marksVal = document.getElementById(marks).value;
//                 alert(maxMaksVal+"  "+marksVal)
            var scoredmarks = marksVal.split(".");
            var beforeDecimalValue = scoredmarks[0];
            var afterDecimalValue = scoredmarks[1];
            if (afterDecimalValue === "1" || afterDecimalValue === "2" || afterDecimalValue === "3" || afterDecimalValue === "4") {
                alert("Scored Marks should be .5");
                document.getElementById(marks).value = "";
                document.getElementById(marks).focus();
            }
            if (afterDecimalValue > 5) {
                alert("Scored Marks should not more than .5");
                document.getElementById(marks).value = "";
                document.getElementById(marks).focus();
            }
            if (parseFloat(marksVal) > parseFloat(maxMaksVal)) {
                alert("Marks Scored should be less than or equal to Max Marks");
                document.getElementById(marks).value = "";
                document.getElementById(marks).focus();
            }
        }
        function inputLimiter(e, allow) {
            var AllowableCharacters = '';
            if (allow == 'Letters') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            }
            if (allow == 'Numbers') {
                AllowableCharacters = '1234567890';
            }
            if (allow == 'landline') {
                AllowableCharacters = '1234567890-';
            }
            if (allow == 'NameCharacters') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
            }
            if (allow == 'NameCharactersAndNumbers') {
                AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
            }
            if (allow == 'website') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
            }

            var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
            if (k != 13 && k != 8 && k != 0) {
                if ((e.ctrlKey == false) && (e.altKey == false)) {
                    return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
        function inputLimiter1(e, allow) {
            var AllowableCharacters = '';
            if (allow == 'Letters') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            }
            if (allow == 'Numbers') {
                AllowableCharacters = '1234567890';
            }
            if (allow == 'landline') {
                AllowableCharacters = '1234567890-';
            }
            if (allow == 'NameCharacters') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
            }
            if (allow == 'NameCharactersAndNumbers') {
                AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
            }
            if (allow == 'website') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
            }
            var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
            if (k != 13 && k != 8 && k != 0) {
                if ((e.ctrlKey == false) && (e.altKey == false)) {
                    return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
        function space(evt, thisvalue)
        {
            var number = thisvalue.value;
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (number.length < 1) {
                if (evt.keyCode == 32) {
                    return false;
                }
            }
            return true;
        }

        function submitData() {
            var checkedBoxes = [];
            var applicationid = "";
            var maxmarks = "";
            var obtainedmarks = "";
            var paperdts = "";
            var coursedts = "";

            var paper = '${epaper}';
            var course = $("#course").val();
            var r = false;
            var chkbox = document.getElementsByName('checkid');
            if (chkbox.length > 0) {
                for (var i = 0, n = chkbox.length; i < n; i++) {
                    if (chkbox [i].checked)
                    {
                        checkedBoxes.push(chkbox [i].value);
                        if (($("#obtainedMarks" + chkbox[i].value).val() == null || $("#obtainedMarks" + chkbox[i].value).val() == '')) {
                            alert("Please Enter Practical Marks");
                            $("#obtainedMarks" + chkbox[i].value).focus();
                            return false;
                        }
                        if (parseFloat(($("#obtainedMarks" + chkbox[i].value).val()) > parseFloat($("#maxMarks" + chkbox[i].value).val()))) {
                            alert("Scored Marks Less than Max Marks");
                            $("#obtainedMarks" + chkbox[i].value).focus();
                            return false;
                        }

                        applicationid = applicationid + $("#applicationno" + chkbox[i].value).val() + "~";
                        maxmarks = maxmarks + $("#maxMarks" + chkbox[i].value).val() + "~";
                        obtainedmarks = obtainedmarks + $("#obtainedMarks" + chkbox[i].value).val() + "~";
                        paperdts = paperdts + paper + "~";
                        coursedts = coursedts + course + "~";

                        r = true;
                    }
                }
                if (r == false) {
                    alert("please select at least one check box");
                    return false;
                }
                else {
                    var returnval = confirm("Do You want to proceed");
                    if (returnval == true) {

                        $("#applicationdts").val(applicationid);
                        $("#maxmarksdts").val(maxmarks);
                        $("#obtaineddts").val(obtainedmarks);
                        $("#stype").val(paperdts);
                        $("#coursetype").val(coursedts);

                        document.forms[0].mode.value = "submitOfficeDetails";
                        document.forms[0].submit();
                    } else {
                        return false;
                    }
                }
            }
            else {
                return false;
            }
        }


        function getHideData() {
            $('.hideDataDiv').hide();
        }


        function upTOTen(id, max) {
//            alert("id"+id+"max"+max);
            var value = $('#' + id).val();
            var maxValue = $('#' + max).val();
            if (isNaN(value)) {
                alert("Please enter valid input");
                $('#' + id).val("");
                $('#' + id).focus();
                return false;
            }
            if (value !== "") {
                if (value.indexOf('.') == "0") {
                    alert("Please enter max 10 ");
                    $('#' + id).val("");
                    $('#' + id).focus();
                    return false;
                }
                if (parseFloat(value) > parseFloat(maxValue)) {
                    alert("Scored Marks Less than Max Marks");
                    $('#' + id).val("");
                    $('#' + id).focus();
                    return false;
                }
                return true;
            }
            return true;
        }
    </script>
</head>
<body padding ="0 !important">
    <div class="row mainbodyrow">
        <div class="container">
            <div class="col-xs-12">
                <div class="maindodycnt">

                    <h3 class="h3_background"><b><center>CCIC Practical Marks Report</center></b></h3>
                    <span></span>

                    <html:form action="/practicalsCCICReport" >
                        <html:hidden property="mode"/>
                        <input type="hidden" name="checkedBoxesupdate" id="checkedBoxesupdate">
                        <input type="hidden" name="applicationdts" id="applicationdts">
                        <input type="hidden" name="obtaineddts" id="obtaineddts">
                        <input type="hidden" name="maxmarksdts" id="maxmarksdts">
                        <input type="hidden" name="stype" id="stype">
                        <input type="hidden" name="coursetype" id="coursetype">


                        <br><br>
                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="50%" class="altrowstable" id="altrowstable" style="margin: 0px auto;">

                            <tr>
                                <th>&nbsp;&nbsp;&nbsp;<b>Course :</b>&nbsp;&nbsp;&nbsp;</th>
                                <td style="text-align:center;">
                                    <html:select property="course"  styleId="course" onchange="getHideData();" style="width: 100%;">
                                        <html:option value="0">--Select--</html:option>
                                        <html:optionsCollection property="courseList" label="courseName" value="courseId"/>
                                    </html:select>  
                                </td>  
                                <td>  
                                    <input type='submit' value='SUBMIT' id="SUBMIT" onclick="return mysearch();" class="btn btn-info" style=" width: auto !important;  padding: 5px !important;"/>
                                </td>
                            </tr>
                        </table>
                        <div class="hideDataDiv">
                            <logic:present name="result1">
                                <br><br>
                                <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                                <br>
                            </logic:present>
                            <logic:present name="result">
                                <br><br>
                                <center> <font color="red" style="font-weight: bold">${result}</font></center>
                                <br>
                            </logic:present>
                            
                            <logic:present name="doculist">
                                <div style="float:right;margin-right: 44px"> <input class="btn btn-primary" type="submit" value="Print" onClick="onPrint();"></div>
                               <br>
                                <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th rowspan="3">S.no</th>
                                            <th rowspan="3">Application No</th>
                                            <th rowspan="3">Name of Applicant</th>
                                            <th colspan="2">Practicals</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">${epaper}</th>  
                                        </tr>
                                        <tr>
                                            <th>Max Marks</th>
                                            <th>Obtained Marks </th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <%int l = 0;%>
                                        <c:forEach var="list" items="${doculist}" >
                                            <tr>
                                                <td> <%=i++%></td>
                                                <!--<td><input type="checkbox" class="checkbox" style=" margin: auto;" name="checkid" id="checkid<%=l%>" value="<%=l%>"></td>-->
                                                <!--<td><%=l%></td>-->
                                                <td>
                                                    ${list.applicantid}
                                                    <input type="hidden" name="applicationno<%=l%>" id="applicationno<%=l%>"  value="${list.applicantid}">
                                                <td class="cell expand-maximum-on-hover">${list.applicantname}</td>
                                                <td>${list.maxMarks}</td>
                                        <input type="hidden" name="maxMarks<%=l%>" id="maxMarks<%=l%>"  value="${list.maxMarks}">
                                        <td>
                                            
                                             ${list.practicalScored}
                                            <!--<input type="text" name="obtainedMarks<%=l%>" id="obtainedMarks<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('obtainedMarks<%=l%>', 'maxMarks<%=l%>');"/>-->
                                        </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>

                                <!--<center> <button onclick="return submitData();" class="btn btn-primary">Submit</button></center>-->
                                </logic:present>
                            <!-- modal popup -->
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" style="width: 23px;opacity: 2; background: #3ee514;" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"></h4>
                                        </div>
                                        <div class="modal-body">

                                        </div>
                                        <div class="modal-footer">
                                            <center>
                                                <button  class="close" style="width: 62px;opacity: 2;float:none;background: #b9c4e3;border:1!important;" data-dismiss="modal">Close</button>
                                            </center>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- modal popup -->
                        </div>

                    </html:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script> 
<script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
<script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script> 
<script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
        $("form").attr('autocomplete', 'off');
</script>      

</html>     