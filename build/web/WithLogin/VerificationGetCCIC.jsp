<%-- 
    Document   : Teachersprint
    Created on : Jun 5, 2018, 7:39:44 PM
    Author     : 1042564
--%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">

        <style type="text/css">
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
        </style>
        <script>
$(document).ready(function () {
$("form").bind("keypress", function (e) {
if (e.keyCode == 13) {
return false;
}
});
});
        </script>
        <script>

            function backSubmit() {
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
            function onchangeEligibleShow() {
                var arr = [];
                var cfrmstatus = ['upload1Confirm', 'upload2Confirm', 'upload3Confirm', 'upload4Confirm', 'upload5Confirm'];
                for (var i = 0; i < cfrmstatus.length; i++) {
                    arr.push($("#" + cfrmstatus[i]).val());
                }
                if (arr.includes("1") && arr.includes("2") == false && arr.includes("3") == false) {
                    $(".finalconfirmation").show();
                    $("#finaleligibleRemarks").val("");
                    $("#eligibleConfirm").empty().append("<option value='3'>Eligible</option>");
                } else if (arr.includes("2")) {
                    $(".finalconfirmation").hide();
                    $("#finaleligibleRemarks").val("");
                    $("#eligibleConfirm").empty().append("<option value='1'>Reupload</option>");
                } else if (arr.includes("3") && arr.includes("2") == false) {
                    $(".finalconfirmation").show();
                    $("#finaleligibleRemarks").val("");
                    $("#eligibleConfirm").empty().append("<option value='4'>Not Eligible</option>");
                }
            }


            function verficationDocuments() {
                var arr = [];
                var cfrmstatus = ['upload1Confirm', 'upload2Confirm', 'upload3Confirm', 'upload4Confirm', 'upload5Confirm'];
                var alertstatus = [ 'Certificate', 'Experience Certificate','Experience Certificate II','Experience Certificate III','Experience Certificate IV'];
                var remaks = ['upload1Remarks', 'upload2Remarks', 'upload3Remarks', 'upload4Remarks', 'upload5Remarks'];
                var remaksu = ['upload1RemarksU', 'upload2RemarksU', 'upload3RemarksU', 'upload4RemarksU', 'upload5RemarksU'];
                var relstatus = "1";
                for (var i = 0; i < cfrmstatus.length; i++) {
                    if ($("#" + cfrmstatus[i]).val() == "0") {
                        alert("Please Select " + alertstatus[i] + " and Verify");
                        $("#" + cfrmstatus[i]).focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if (($("#" + cfrmstatus[i]).val() == "2")&&($("#" + remaksu[i]).val() == "")) {
                        alert("Please enter " + alertstatus[i] + " Remarks");
                        $("#" + remaksu[i]).focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if (($("#" + cfrmstatus[i]).val() == "3")&&($("#" + remaks[i]).val() == "0")) {
                        alert("Please Select " + alertstatus[i] + " Remarks");
                        $("#" + remaks[i]).focus().css({'border': '1px solid red'});
                        return false;
                    }
                    arr.push($("#" + cfrmstatus[i]).val());
                }
                if ((arr.includes("1") || arr.includes("3")) && arr.includes("2") == false) {
                    if ($("#finaleligibleRemarks").val() == "") {
                        alert("Please Enter Final  Remarks");
                        $("#finaleligibleRemarks").focus().css({'border': '1px solid red'});
                        return false;
                    } else {
                        document.forms[0].mode.value = "submitAcademicsDocumentVerificationData";
                        document.forms[0].submit();
                    }
                } else {
                    $("#eligibleConfirm").empty().append("<option value='1'>Reupload</option>")
                    document.forms[0].mode.value = "submitAcademicsDocumentVerificationData";
                    document.forms[0].submit();
                }
            }
            function getPhotosListAjax(aaddhar, course, filename) {
                var paraData = "aadhar=" + aaddhar + "&course=" + course + "&filename=" + filename;
                $.ajax({
                    type: "GET",
                    url: "verificationCCIC.do?mode=getImageInBase64Format",
                    data: paraData,
                    success: function(response) {
                        var img = response.split('~~');
                        var image0 = img[0];
                        $("#dialog").dialog("open");
                        $('#imageview').attr('src', "data:image/jpg;base64," + image0);
                    },
                    error: function(e) {
                    }
                });
            }


            var angle = 0;
            function rotate() {
                angle += 90;
                $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
            }

            function getDocumentDetailsData() {
                document.forms[1].mode.value = "getDocumentDetails";
                document.forms[1].submit();
            }

            function getRemarksList(remarksid, remarksclass, cfrmid,remarksclassu) {
                //if Reupload 
                if ($("#" + cfrmid).val() == '3') {
                    $("." + remarksclass).show();
                    $("." + remarksclassu).hide();
                    $("#" + remarksid).empty().append("<option value='0'>--Select--</option><option value='Relevant document is not enclosed'>Relevant document is not enclosed</option>");

                } else if ($("#" + cfrmid).val() == '2') {
                    $("." + remarksclassu).show();
                    $("." + remarksclass).hide();
                    $("#" + remarksid+"U").val("");
                }
                else {
                    $("." + remarksclass).hide();
                    $("#" + remarksid).empty().append("<option value=''>--Select--</option>");

                }
            }

            function getDiv(id, classid, value, vstatus) {
                if (value == '99') {
                    $("#" + id).empty().append("<option value='99'>Not Eligible</option>");
                    $("." + classid).hide();
                } else {
                    $("." + classid).show();
                    if (vstatus == 0) {
                        $("#" + id).empty().append("<option value='0' selected>--Select--</option><option value='1'>Verified/Approved</option><option value='2'>Re-Upload</option><option value='3'>Rejected/Not Approved</option>");
                        $("#" + id).val(value);
                    } else {
                        if(value==1||value==3){
                        $("#" + id).empty().append("<option value='0' selected>--Select--</option><option value='1'>Verified/Approved</option><option value='3'>Rejected/Not Approved</option>");
                       $("#"+id).val(value);
                     }else{
                     $("#" + id).empty().append("<option value='0' selected>--Select--</option><option value='1'>Verified/Approved</option><option value='3'>Rejected/Not Approved</option>");
                        }
                    }

                }
            }


function getRemarksList1(remarksid,remarksclass,cfrmid,remarksvalue) {
                if (cfrmid == '3') {
                    $("." + remarksclass).show();
                    $("#" + remarksid).empty().append("<option value='0'>--Select--</option><option value='Relevant document is not enclosed'>Relevant document is not enclosed</option>");
                        $("#" + remarksid).val(remarksvalue);
                } 
                else {
                    $("." + remarksclass).hide();
                    $("#" + remarksid).empty().append("<option value=''>--Select--</option>");

                }
            }
            function getFileStatus() {
//             alert(${upload14cfrm});
                 $(".finalconfirmation").hide();
                $("#vstatus").val('${vstatus}')
                
                getDiv("upload1Confirm", "upload1class", '${upload11cfrm}', '${vstatus}');
                getDiv("upload2Confirm", "upload2class", '${upload12cfrm}', '${vstatus}');
                getDiv("upload3Confirm", "upload3class", '${upload13cfrm}', '${vstatus}');
                getDiv("upload4Confirm", "upload4class", '${upload14cfrm}', '${vstatus}');
                getDiv("upload5Confirm", "upload5class", '${upload15cfrm}', '${vstatus}');
                
                getRemarksList1("upload1Remarks", "upload1rmks", '${upload11cfrm}', '${upload1rmks}');
                getRemarksList1("upload2Remarks", "upload2rmks", '${upload12cfrm}', '${upload2rmks}');
                getRemarksList1("upload3Remarks", "upload3rmks", '${upload13cfrm}', '${upload3rmks}');
                getRemarksList1("upload4Remarks", "upload4rmks", '${upload14cfrm}', '${upload4rmks}');
                getRemarksList1("upload5Remarks", "upload5rmks", '${oldregistrationcfrm}', '${upload5rmks}');
                if('${updatedby}'=="9999"){
                    $(".otherdiv").hide();
                    $(".dtodiv").show();
                }else{
                      $(".otherdiv").show();
                    $(".dtodiv").hide(); 
                }
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function space(evt, thisvalue)
            {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allows only Numerics");
                    return false;
                }
                return true;
            }
            $(document).ready(function() {
                 $('input[type=text], textarea').bind("cut copy paste", function(e) {
                    alert("Cut copy paste not allowed here");
                    e.preventDefault();
                });
                $("#dialog").dialog({
                    autoOpen: false,
                    modal: true,
                    width: 600,
                    height: 600,
                    closeText: "X"
//                            show: {
//                    effect: "slide",
//                            duration: 1500
//                            },
//                            hide: {
//                    effect: "fade",
//                            duration: 1000
//                            }
                });
                var angle = 0;
                $(".toggle").click(function() {
                    angle += 90;
                    $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
//                    $("#imageview").toggleClass('flip');
                });
            });
             function getImageDownload(aadhar,course,filename) {
               $("#aadhar").val(aadhar);
               $("#course").val(course);
            $("#filename").val(filename);
            document.forms[0].mode.value = "downloadFiles";
            document.forms[0].submit();
        }
        </script>
        <style>
            .box {
                width: 40%;
                margin: 0 auto;
                background: rgba(255, 255, 255, 0.2);
                padding: 35px;
                border: 2px solid #fff;
                border-radius: 20px/50px;
                background-clip: padding-box;
                text-align: center;
            }

            span#customHTMLFooter {
                display: none;
            }

            .panel-default>.panel-heading {
                color: #fff;
                background-color: #f79400;
                border-color: #f79400;
            }

            .button {
                font-size: 1em;
                padding: 10px;
                color: #fff;
                border: 2px solid #06D85F;
                border-radius: 20px/50px;
                text-decoration: none;
                cursor: pointer;
                transition: all 0.3s ease-out;
            }

            .button:hover {
                background: #06D85F;
            }

            .overlay {
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                z-index: 111;
                background: rgba(0, 0, 0, 0.7);
                transition: opacity 500ms;
                visibility: hidden;
                opacity: 0;
            }

            .overlay:target {
                visibility: visible;
                opacity: 1;
            }

            .popup {
                margin: 20px auto;
                /* padding: 40px; */
                background: #fff;
                border-radius: 5px;
                width: 70%;
                position: relative;
                transition: all 1s ease-in-out;
            }


            .popup .content {
                width: 930px !important;
                overflow-x: scroll !important;
                height: 450px;
                overflow-y: scroll !important;
            }

            .popup h2 {
                margin-top: 0;
                color: #333;
                font-family: Tahoma, Arial, sans-serif;
            }

            .popup .close {
                position: absolute;
                top: 20px;
                right: 30px;
                transition: all 200ms;
                font-size: 30px;
                font-weight: bold;
                text-decoration: none;
                color: #333;
            }

            .popup .close:hover {
                color: #06D85F;
            }


            @media screen and (max-width: 700px) {
                .box {
                    width: 80%;
                }
                .popup {
                    width: 80%;
                }

            }
            .custexthidden{
                background: none;
                border: 0;
                outline: none;
            }
            .cusselecthidden{
                -webkit-appearance: none;
                -moz-appearance: none;
                text-indent: 1px;
                text-overflow: '';
                border: 0;
                background: none;

            }
            select {

            }

        </style>
    </head>
    <body onload="getFileStatus();">
        <div class="row mainbodyrow">
            <div class="container">
                <div class="col-xs-12">
                    <div class="maindodycnt">
                        <center>
                            <h3 class="block-head-News">Verification of Applications for CCIC, 2020-21</h3>
                        </center>
                        <br/>
                        <span></span>
                        <html:form action="/verificationCCIC" >
                            <html:hidden property="mode"/>
                            <input type="hidden" name="aadhar" id="aadhar"/>
                            <input type="hidden" name="course" id="course" value="${course}"/>
                            <input type="hidden" name="filename"  id="filename"/>
                            <html:hidden property="applicationno" value="${regno}"/>
                            <html:hidden property="vstaus" styleId="vstaus" value="${vstatus}"/>
                            <logic:present name="result1">
                                <br/> <br/> <br/>
                                <center> <font color="green" style="font-weight: bold;font-size: 20px;" >${result1}</font></center>
                                <br/><br/>  
                            </logic:present>
                            <logic:present name="result">
                                <br/><br/><br/>
                                <center> <font color="red" style="font-weight: bold;font-size: 20px;">${result}</font></center>
                                <br/><br/>
                            </logic:present>
                            <c:if test="${doculist<1}">
                                <br/><br/><br/>
                                <center> <font color="red" style="font-weight: bold;font-size: 20px;">No Data Available</font></center>
                                <br/><br/>
                            </c:if>
                            <c:if test="${doculist>0}">
                                <div id="dataDiv"><br>
                                     <div id="dialog" title="View Photo">
                                            <img class="toggle" id="imageview" src="#"  width="500" height="450"/>
                                            <br/>
                                            <br/>
                                            <center> <button class="btn btn-success" style="text-align:center;margin: 10px;" onclick="return rotate();">Rotate <i class="fa fa-rotate-right"></i></button</center>
                                        </div>
                                    <div class="mainheading">
                                        <h2>Personal Details</h2> 
                                        <span> </span>
                                    </div><br>
                                    <table align="center" cellpadding="0" cellspacing="0" border="1" width="100%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                        <tr>
                                            <th>Name of the applicant</th>
                                            <td>${bname}</td> 
                                            <th>Date Of Birth</th>
                                            <td>${dob}</td> 
                                            <th>Gender</th>
                                            <td>${gender}</td> 
                                            <td colspan="2"> <img width="80" height="80" style="float:right;margin-right: 20px;" alt="NO FILE" src="data:image/jpg;base64,${photo}" /></td>
                                        </tr>
                                        <tr>
                                            <th>Father Name</th>
                                            <td>${fname}</td> 
                                            <th>mobile</th>
                                            <td>${mobile}</td>
                                             <th>Institution</th>
                                            <td>${Institute}</td> 
                                             <th>Category</th>
                                            <td>${category}</td> 
                                        </tr>
                                        <tr>
                                            <th>Course</th>
                                            <td>${coursename}</td>
                                             <th>email</th>
                                            <td>${email}</td>
                                            <th>RegistrationNo</th>
                                            <td>${regno}</td>
                                        </tr>
                                    </table>
                                    <br>
                                    
                                    <div class="mainheading">
                                        <h2>Documents Verification</h2>
                                        <span></span>
                                    </div>  
                                    <br/>
                                    <div>
                                        <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable1" id="altrowstable1 photodiv" style="margin: 0px auto;">
                                            <tr class="upload1class">
                                                <% if ((request.getAttribute("fileflag").toString()).equalsIgnoreCase("2")
                                                           ) {%>
                                                <th>SSC /Equivalent Certificate</th>
                                                    <%} else if ((request.getAttribute("fileflag").toString()).equalsIgnoreCase("3")
                                                           ) {%>
                                                <th>SSC Certificate</th>
                                                    <%} else if ((request.getAttribute("fileflag").toString()).equalsIgnoreCase("4")) {%>
                                                <th>Intermediate Certificate</th>
                                                 <%} else if ((request.getAttribute("fileflag").toString()).equalsIgnoreCase("4") &&(request.getAttribute("course").toString()).equalsIgnoreCase("AC")) {%>
                                                <th>Intermediate/Equivalent Certificate</th>
                                                    <%} else if ((request.getAttribute("fileflag").toString()).equalsIgnoreCase("5")) {%>
                                                <th>Degree/Diploma/Engineering Certificate</th>
                                                    <%}else if ((request.getAttribute("fileflag").toString()).equalsIgnoreCase("6")) {%>
                                                 <th>Yoga Certificate</th>
                                                 <%}else {%>
                                                  <th>Upload Certificate</th>
                                                 <%}%>
                                                <td>
                                                    <a href="#dialog"  class="btn btn-default" onclick="getPhotosListAjax('${aadhar}', '${course}', '${upload1file}');">View File</a>
                                                </td>
                                                    <td style="text-align: center;">
                                                <a href="#" onclick="getImageDownload('${aadhar}', '${course}', '${upload1file}')"><button class="btn btn-default">Download File</button></a>
                                            </td>
                                                <td>
                                                    <select name="upload1Confirm" id="upload1Confirm" onchange=" onchangeEligibleShow();
                getRemarksList('upload1Remarks', 'upload1rmks', 'upload1Confirm','upload1rmksupload');">
                                                        <option value="0">--Select--</option>
                                                    </select>
                                                </td>       
                                                <td class="upload1rmks"  colspan="2" style="display:none">
                                                    <select name="upload1Remarks" id="upload1Remarks">
                                                        <option value="">--Select--</option>
                                                    </select>
                                                </td>
                                                      <td class="upload1rmksupload"  colspan="2" style="display:none">
                                                  <textarea name="upload1RemarksU"id="upload1RemarksU" rows="3" cols="50" maxlength="1000" onkeypress="return inputLimiter(event, 'Letters1');" ></textarea>
                                                </td>
                                            </tr>
                                            <tr class="upload2class">
                                          <th>  <div class="otherdiv"> Experience Certificate</div><div class="dtodiv"> Service Certificate</div></th>
                                                <td>
                                                    <a href="#dialog"  class="btn btn-default" onclick="getPhotosListAjax('${aadhar}', '${course}', '${upload2file}');">View File</a>
                                                </td>
                                                <td style="text-align: center;">
                                                <a href="#" onclick="getImageDownload('${aadhar}', '${course}', '${upload2file}')"><button class="btn btn-default">Download File</button></a>
                                            </td>
                                                <td>
                                                    <select name="upload2Confirm" id="upload2Confirm" onchange=" onchangeEligibleShow();
                getRemarksList('upload2Remarks', 'upload2rmks', 'upload2Confirm','upload2rmksupload');">
                                                        <option value="0">--Select--</option>
                                                    </select>
                                                </td>       
                                                <td class="upload2rmks"  colspan="2" style="display:none">
                                                    <select name="upload2Remarks" id="upload2Remarks">
                                                        <option value="">--Select--</option>
                                                    </select>
                                                </td>
                                                     <td class="upload2rmksupload"  colspan="2" style="display:none">
                                                  <textarea name="upload2RemarksU"id="upload2RemarksU" rows="3" cols="50" maxlength="1000" onkeypress="return inputLimiter(event, 'Letters1');"></textarea>
                                                </td>
                                            </tr>
                                             <tr class="upload3class">
                                                <th>Experience Certificate II</th>
                                                <td>
                                                    <a href="#dialog"  class="btn btn-default" onclick="getPhotosListAjax('${aadhar}', '${course}', '${upload3file}');">View File</a>
                                                </td>
                                                <td style="text-align: center;">
                                                    <a href="#" onclick="getImageDownload('${aadhar}', '${course}', '${upload3file}')"><button class="btn btn-default">Download File</button></a>
                                                </td>
                                                <td>
                                                    <select name="upload3Confirm" id="upload3Confirm" onchange=" onchangeEligibleShow();
                getRemarksList('upload3Remarks', 'upload3rmks', 'upload3Confirm', 'upload3rmksupload');">
                                                        <option value="0">--Select--</option>
                                                    </select>
                                                </td>       
                                                <td class="upload3rmks"  colspan="2" style="display:none">
                                                    <select name="upload3Remarks" id="upload3Remarks" >
                                                        <option value="">--Select--</option>
                                                    </select>
                                                </td>
                                                <td class="upload3rmksupload"  colspan="2" style="display:none">
                                                    <textarea name="upload3RemarksU"id="upload3RemarksU" rows="3" cols="50" maxlength="1000" onkeypress="return inputLimiter(event, 'Letters1');"></textarea>
                                                </td>
                                            </tr>
                                            <tr class="upload4class">
                                               <th>Experience Certificate III</th>
                                                <td>
                                                    <a href="#dialog"  class="btn btn-default" onclick="getPhotosListAjax('${aadhar}', '${course}', '${upload4file}');">View File</a>
                                                </td>
                                                <td style="text-align: center;">
                                                    <a href="#" onclick="getImageDownload('${aadhar}', '${course}', '${upload4file}')"><button class="btn btn-default">Download File</button></a>
                                                </td>
                                                <td>
                                                    <select name="upload4Confirm" id="upload4Confirm" onchange=" onchangeEligibleShow();
                getRemarksList('upload4Remarks', 'upload4rmks', 'upload4Confirm', 'upload4rmksupload');">
                                                        <option value="0">--Select--</option>
                                                    </select>
                                                </td>       
                                                <td class="upload4rmks"  colspan="2" style="display:none">
                                                    <select name="upload4Remarks" id="upload4Remarks" >
                                                        <option value="">--Select--</option>
                                                    </select>
                                                </td>
                                                <td class="upload4rmksupload"  colspan="2" style="display:none">
                                                    <textarea name="upload4RemarksU"id="upload4RemarksU" rows="3" cols="50" maxlength="1000" onkeypress="return inputLimiter(event, 'Letters1');"></textarea>
                                                </td>
                                            </tr>
                                            <tr class="upload5class">
                                                <th>Experience Certificate IV</th>
                                                <td>
                                                    <a href="#dialog"  class="btn btn-default" onclick="getPhotosListAjax('${aadhar}', '${course}', '${upload5file}');">View File</a>
                                                </td>
                                                <td style="text-align: center;">
                                                    <a href="#" onclick="getImageDownload('${aadhar}', '${course}', '${upload5file}')"><button class="btn btn-default">Download File</button></a>
                                                </td>
                                                <td>
                                                    <select name="upload5Confirm" id="upload5Confirm" onchange=" onchangeEligibleShow();
                getRemarksList('upload5Remarks', 'upload5rmks', 'upload5Confirm', 'upload5rmksupload');">
                                                        <option value="0">--Select--</option>
                                                    </select>
                                                </td>       
                                                <td class="upload5rmks"  colspan="2" style="display:none">
                                                    <select name="upload5Remarks" id="upload5Remarks">
                                                        <option value="">--Select--</option>
                                                    </select>
                                                </td>
                                                <td class="upload5rmksupload"  colspan="2" style="display:none">
                                                    <textarea name="upload5RemarksU"id="upload5RemarksU" rows="3" cols="50" maxlength="1000" onkeypress="return inputLimiter(event, 'Letters1');"></textarea>
                                                </td>
                                            </tr>
                                        </table>

                                        <table  style="display:none" align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable1 finalconfirmation" id="altrowstable1" style="margin: 10px auto;">
                                            <th> Please Confirm <font color="red">*</font></th> 
                                            <td>
                                                <select name="eligibleConfirm" id="eligibleConfirm">
                                                    <option value="0">--Select--</option>
                                                    <option value="1">Reupload</option>
                                                    <option value="3">Eligible</option>
                                                    <option value="4">Not Eligible</option>
                                                </select>
                                            </td>
                                            <th> Final Remarks <font color="red">*</font></th> 
                                            <td>
                                                <textarea name="finaleligibleRemarks" style="width:100%; height:50px;" id="finaleligibleRemarks" maxlength="500" onkeypress="return inputLimiter(event, 'Letters1');"></textarea>
                                            </td>
                                        </table>

                                        <br/>
                                        <center><input type="button" onclick="return verficationDocuments()" value="submit" class="btn btn-success" style="max-width: 96px;"/>
                                            <input type="button" onclick="return backSubmit()" value="Back" class="btn btn-primary" style="max-width: 96px;"/>
                                        </center>
                                        <br/>
                                        <br/>
                                        <br/>

                                       
                                        <!-- View Photo popup -->
                                        <div id="popup1" class="overlay">
                                            <div class="popup">
                                                <h4 style="
                                                    color: #fff;
                                                    font-weight: 500;
                                                    padding: 10px 10px;
                                                    margin: 0px;
                                                    background-color: #3c763d;
                                                    width: 100%;
                                                    display: block;
                                                    border-top-left-radius: 5px;
                                                    border-top-right-radius: 5px;
                                                    ">
                                                    View Photo <a class="close" style="position: absolute; top: 5px !important; right: 10px; transition: all 200ms; font-size: 30px; font-weight: bold; text-decoration: none; color: #000; display: block;" href="#photodiv">�</a>
                                                </h4>

                                                <div class="content">
                                                    <div>
                                                        <div>
                                                            <div>
                                                                <div media="slider" style="text-align: center;">
                                                                    <img style="width:800px;height:750px;" id="image0" src="" />


                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <center> <button class="btn btn-success" style="text-align:center;margin: 10px;" onclick="return rotateImage();">Rotate <i class="fa fa-rotate-right"></i></button</center>
                                            </div>
                                        </div>
                                    </c:if>
                                </html:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </body>
</html>                                     