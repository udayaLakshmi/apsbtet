<%-- 
    Document   : AbstractPaymentReportExcel
    Created on : Jul 16, 2021, 6:20:17 PM
    Author     : 1820530
--%>


<%@page contentType="application/vnd.ms-excel" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html>
<html>
    <head>
        <%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>

        <title>Abstract Payment Report</title>
        
            <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[13, -1], [13, "All"]]
                });
                $('#example1').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
            tfoot {
             background-color: #6699ff!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>

       
    </head>
    <body>
        <h3 class="block-head-News">Course Wise Report</h3>
        <br>
        <%int count = 1;%>
    <logic:present name="nodata">
        <center><font color="red">${nodata}</font></center>
    </logic:present>

    <logic:present name="paymentAbstractReport">
       
        <br>
        <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Course Name</th>
                                            <th>Course Code</th>
                                            <th>Limit</th>
                                            <th>Payment Done</th>
                                            <th>Payment Not Done</th>
                                            <th>Total</th>

                                        </tr>
                                         <bean:define id="limitTotal" value="0"/>
                                        <bean:define id="paymentDoneTotal" value="0"/>
                                        <bean:define id="paymentNotDoneTotal" value="0"/>
                                        <bean:define id="allTotal" value="0"/>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="paymentAbstractReport" id="list">
                                            <tr>
                                                <td style="text-align: center;"><%=count++%></td>
                                                <td style="text-align: left;">${list.courseName}</td>
                                                <td style="text-align: center;">${list.courseCode}</td>     
                                                <td style="text-align: right;">${list.limit}</td>
                                                <td style="text-align: right;">
                                                   ${list.paymentDone}
                                                </td>
                                                <td style="text-align: right;">
                                                    ${list.paymentNotDone}
                                                </td>
                                                <td style="text-align: right;">
                                                     ${list.total}
                                                </td>                                                                                      
                                            </tr>
                                            
                                            <bean:define id="limitTotal" value="${limitTotal+list.limit}"/>
                                            <bean:define id="paymentDoneTotal" value="${paymentDoneTotal+list.paymentDone}"/> 
                                            <bean:define id="paymentNotDoneTotal" value="${paymentNotDoneTotal+list.paymentNotDone}"/> 
                                            <bean:define id="allTotal" value="${allTotal+list.total}"/> 

                                        </logic:iterate>  
                                    </tbody>
                                     
                                        <tr>
                                            <th style="text-align: center " colspan="3" >
                                                Total
                                            </th>
                                            <th style="text-align: right; ">
                                                <b> ${limitTotal} </b>
                                            </th>
                                            <th style="text-align: right; ">
                                                <b> ${paymentDoneTotal} </b>
                                            </th> 
                                            <th style="text-align: right; ">
                                                <b> ${paymentNotDoneTotal} </b>
                                            </th>
                                            <th style="text-align: right; ">
                                                <b> ${allTotal} </b>
                                            </th>
                                        </tr>                                       
                                   
        </table>
    </logic:present>

    <logic:present name="paymentSubList">
        
        <br>
        <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>S.NO</th>
                    <th>Name</th>
                    <th>Father Name </th>
                    <th>Date Of Birth</th>
                    <th>Gender</th>
                    <th>Mobile </th>
                    <th>Course Name</th>
                    <th>category</th>
                    <th>Institute Name</th>
                    <th>Studied District</th>
                    <th>Exam Center</th>

                </tr>
            </thead>
            <tbody>
            <logic:iterate name="paymentSubList" id="list">
                <tr>
                    <td style="text-align: center;"><%=count++%></td>
                    <td style="text-align: left;">${list.name}</td>
                    <td style="text-align: left;">${list.fatherName}</td>     
                    <td style="text-align: right;">${list.dob}</td>
                    <td style="text-align: center;">${list.gender}</td>
                    <td style="text-align: right;">${list.mobileNum}</td>
                    <td style="text-align: left;">${list.courseName}</td>
                    <td style="text-align: left;">${list.category}</td>
                    <td style="text-align: left;">${list.institution}</td>
                    <td style="text-align: left;">${list.studiedDist}</td>
                    <td style="text-align: right;">${list.examCenterCode}</td>

                </tr>
            </logic:iterate>  
            </tbody>

        </table>
    </logic:present>

</body>
</html>
