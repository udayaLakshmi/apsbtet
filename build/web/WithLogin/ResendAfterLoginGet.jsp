
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <Style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control-plaintext{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
                    $("#blindDiv").hide();
                    $("#lowdiv").hide();
                    $("#highdiv").hide();
                    $("#highdiv1").hide();
                    $("#highdiv2").hide();
                    $("#highdiv3").hide();
                      $(".toggle").click(function() {
                    angle += 90;
                    $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
//                    $("#imageview").toggleClass('flip');
                });
 $("#dialog").dialog({
                        autoOpen: false,
                        modal: true,
                         width: 600,
                        height: 500,
                        closeText: "X",
                        position: { my: "center top", at: "center top+120px", of: window }
//                            show: {
//                    effect: "slide",
//                            duration: 1500
//                            },
//                            hide: {
//                    effect: "fade",
//                            duration: 1000
//                            }
                    });
            });
            function CheckfilePdfOrOther(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 1001) {
                        return true;
                    } else {
                        alert("File size should be less than 1MB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 1MB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }
            
            function SubmitForm() {
               if ($("#blindc").val() == "2" && $("#blindfile").val() === "") {
        $("#blindfile").val('').clone(true);
        alert("Upload Are you VIsually Impaired Candidate? Certificate");
        $("#blindfile").focus().css({'border': '1px solid red'});
        return false;
    }else  if ($("#upload1c").val() == "2" && $("#upload1").val() === "") {
        $("#upload1").val('').clone(true);
        alert("Upload  Certificate");
        $("#upload1").focus().css({'border': '1px solid red'});
        return false;
    } 
    else  if ($("#upload2c").val() == "2" && $("#upload2").val() === "") {
        $("#upload2").val('').clone(true);
        alert("Upload  Certificate");
        $("#upload2").focus().css({'border': '1px solid red'});
        return false;
    } else  if ($("#upload3c").val() == "2" && $("#upload3").val() === "") {
        $("#upload3").val('').clone(true);
        alert("Upload  Certificate");
        $("#upload3").focus().css({'border': '1px solid red'});
        return false;
    } else  if ($("#upload4c").val() == "2" && $("#upload4").val() === "") {
        $("#upload4").val('').clone(true);
        alert("Upload  Certificate");
        $("#upload4").focus().css({'border': '1px solid red'});
        return false;
    } else  if ($("#upload5c").val() == "2" && $("#upload5").val() === "") {
        $("#upload5").val('').clone(true);
        alert("Upload  Previous Session Hallticket");
        $("#upload5").focus().css({'border': '1px solid red'});
        return false;
    }   else {
                    var x = confirm("Do you Want to Proceed?")
                    if (x == false) {
                        return false
                    } else {

                        document.forms[0].mode.value = "submitData";
                        document.forms[0].submit();
                    }
                }
            }
            function showData() {
                $("#bname").val('${bname}')
                $("#fname").val('${fname}')
                $("#dob").val('${dob}')
                $("#gender").val('${gender}')
                $("#caste").val('${caste}')
                $("#blind").val('${blind}')
                $("#email").val('${email}')
                $("#mobile").val('${mobile}')
                $("#aadhar").val('${aadhar}')
                $("#aadhar1").val('${aadhar1}')
                $('#examination').val('${examination}');
                $('#language').val('${language}');
                $('#grade').val('${grade}');
                $('#gradename').val('${gradename}');
                $('#edate').val('${edate}');
                $('#ebatch').val('${ebatch}');
                $('#apdistrict').val('${edistrict}');
                $('#ecenter').val('${ecenter}');
                $('#institute').val('${institute}');
                $('#houseno').val('${address}');
                $("#blindn").val("${blindn}");
                $("#upload1n").val("${upload1n}");
                $("#upload2n").val("${upload2n}");
                $("#upload3n").val("${upload3n}");
                $("#upload4n").val("${upload4n}");
                $("#upload5n").val("${upload5n}");

                $("#blindc").val("${blindc}");
                $("#upload1c").val("${upload1c}");
                $("#upload2c").val("${upload2c}");
                $("#upload3c").val("${upload3c}");
                $("#upload4c").val("${upload4c}");
                $("#upload5c").val("${upload5c}");
              getDiv("blindDiv",'${blindc}');
              getDiv("lowdiv",'${upload1c}');
              getDiv("highdiv",'${upload2c}');
              getDiv("highdiv1",'${upload3c}');
              getDiv("highdiv2",'${upload4c}');
              getDiv("highdiv3",'${upload5c}');
                
                

            }
            function getDiv(classid,status){
                if(status=="2"){
                    $("#"+classid).show();
            }else{
               $("#"+classid).hide();  
            }
            }
            function fileupload(evt, thisvalue) {
                var files = $('#' + thisvalue).prop("files")
                if (files && files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded1;
                    reader.readAsDataURL(files[0]);
                }
            }
            function imageIsLoaded1(e) {
                $('#imageview').attr('src', e.target.result);
                $("#dialog").dialog("open");
            }
            var angle = 0;
            function rotate() {
                angle += 90;
                $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
            }
            
             function backTOReport() {
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
        </script>
<!--    <div class="page-title title-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Reupload </h1>
                    <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">

                        <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                        <span>Reupload</span>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <br/>
    <br/>
    <body onload="showData()">
        <div class="feedback-form">
            <div class="container">
                <div class="row">

                    <h3 class="block-head-News">TWSH REUPLOAD SERVICE  <span style="padding-left: 901px;"><input onclick="backTOReport();" type="button" name="BACK" value="BACK" class="btn btn-success" style="background-color: #06447d"></span></h3>
                    <div class="line-border"></div>
                    <br>
                    <div class="row">
                        <div class=" col-md-12">
                            <!--<div class="login-form">-->
                                <html:form action="/resendAfterLogin"  method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <html:hidden property="aadhar" styleId="aadhar"/>
                                     <html:hidden property="grade" styleId="grade"/>
                                    <html:hidden property="blindn" styleId="blindn"/>
                                    <html:hidden property="upload1n" styleId="upload1n"/>
                                    <html:hidden property="upload2n" styleId="upload2n"/>
                                    <html:hidden property="upload3n" styleId="upload3n"/>
                                    <html:hidden property="upload4n" styleId="upload4n"/>
                                    <html:hidden property="upload5n" styleId="upload5n"/>

                                    <html:hidden property="blindc" styleId="blindc"/>
                                    <html:hidden property="upload1c" styleId="upload1c"/>
                                    <html:hidden property="upload2c" styleId="upload2c"/>
                                    <html:hidden property="upload3c" styleId="upload3c"/>
                                    <html:hidden property="upload4c" styleId="upload4c"/>
                                    <html:hidden property="upload5c" styleId="upload5c"/>
                                    <logic:present name="result2">
                                        <center> <font color="green" style="font-weight: bold">${result2}</font>  </center>  
                                        </logic:present>
                                        <logic:present name="result">
                                        <center> <font color="red" style="font-weight: bold">${result}</font></center>
                                    </logic:present><br>
                    <p class="p_textred" style="float: right; color: #f00;">All uploads must be in  JPG Format and size should not be more than 1MB<br/>
                                    <br/>
                                    <br/>
                                    <h5 class="block-head-News">Personal Details (As per SSC Certificate)</h5>
                                    <div class="line-border"></div>
                                     <div id="dialog" title="View Photo">
                                        <center><img class="toggle" id="imageview" src="#"  width="500" height="350"/></center>
                                        <br/>
                                        <br/>
                                                                    <!--<center> <button class="btn btn-success" style="text-align:center;margin: 10px;" onclick="return rotate();">Rotate <i class="fa fa-rotate-right"></i></button</center>-->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Name of the candidate<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="bname" styleId="bname" readonly="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Father Name <font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="fname" styleId="fname"  readonly="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Date of Birth <font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text property="dob" styleId="dob" styleClass="form-control" readonly="true" />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Gender<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text property="gender" styleId="gender" styleClass="form-control" readonly="true"/>
                                                    <%--   <html:select property="gender" styleId="gender" styleClass="form-control">
                                                           <html:option value="0">--Select Gender--</html:option>
                                                           <html:option value="MALE">MALE</html:option>
                                                           <html:option value="FEMALE">FEMALE</html:option>
                                                           <html:option value="TRANSGENDER">TRANSGENDER</html:option>
                                                       </html:select>--%>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Community <font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text property="caste" styleId="caste" styleClass="form-control" readonly="true"/>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Mobile NO.<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text property="mobile" styleId="mobile" styleClass="form-control" readonly="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">EMail<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text property="email" styleId="email" styleClass="form-control" readonly="true"/>
                                                    <%--   <html:select property="gender" styleId="gender" styleClass="form-control">
                                                           <html:option value="0">--Select Gender--</html:option>
                                                           <html:option value="MALE">MALE</html:option>
                                                           <html:option value="FEMALE">FEMALE</html:option>
                                                           <html:option value="TRANSGENDER">TRANSGENDER</html:option>
                                                       </html:select>--%>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Aadhar No <font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text property="aadhar1" styleId="aadhar1" styleClass="form-control" readonly="true"/>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Are you Visually Impaired Candidate?<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text property="blind" styleId="blind" styleClass="form-control" readonly="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
<%--                                    <div  class="row">
                                        <div class="col-md-12">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-2 col-form-label">Address<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text property="houseno" styleId="houseno" styleClass="form-control" readonly="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <br/>
                                    <h5 class="block-head-News">Examination Details</h5>
                                    <div class="line-border"></div>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Institution<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="institute" styleId="institute"  readonly="true"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination Appearing<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text property="examination" styleId="examination" styleClass="form-control" readonly="true"/>
                                                    <%-- <html:select property="examination" styleId="examination" styleClass="form-control" onchange="examinationStatus();examinationDistricts()">
                                                         <html:option value="0">--Select--</html:option>
                                                         <html:option value="TW">Typewriting</html:option>
                                                         <html:option value="SH">Shorthand</html:option>
                                                     </html:select>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Language<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text property="language" styleId="language" styleClass="form-control" readonly="true"/>
                                                    <%-- <html:select property="language" styleId="language" styleClass="form-control" onchange="languageStatus()">
                                                         <html:option value="0">--Select--</html:option>
                                                     </html:select>--%>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Grade<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                   
                                                    <html:text property="gradename" styleId="gradename" styleClass="form-control" readonly="true"/>
                                                    <%--  <html:select property="grade" styleId="grade" styleClass="form-control" onchange="fileStatus(),dateChange(),statusexam()">
                                                          <html:option value="0">--Select--</html:option>
                                                      </html:select>--%>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination District <font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <%--  <html:hidden property="apdistrict" styleId="apdistrict" value="${sessionScope.distcd}"/>
                                                    <html:text property="apdistrict1" styleId="apdistrict1" styleClass="form-control" value="${sessionScope.distname}" readonly="true"/>
                                                    --%>
                                                    <html:text property="apdistrict" styleId="apdistrict" styleClass="form-control" readonly="true"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination Center<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text property="ecenter" styleId="ecenter" styleClass="form-control" readonly="true"/>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination Date<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text property="edate" styleId="edate" styleClass="form-control" readonly="true"/>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination Batch<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:text property="ebatch" styleId="ebatch" styleClass="form-control" readonly="true"/>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br/>

                                    <h5 class="block-head-News">Upload File Details</h5>
                                    <div class="line-border"></div>
                                      <div class="row" id="blindDiv">
                                    <div class="col-md-8" >

                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Are you Visually Impaired Candidate?<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:file property="blindfile" styleId="blindfile" onchange="return CheckfilePdfOrOther('blindfile');"/><a href="#" onclick="fileupload(event, 'blindfile')"/>View File</a>
                                            </div>
                                        </div>
                                    </div>
                                      </div>
                                              <div class="row" id="lowdiv">
 <div class="col-md-8">
                                        <div class="form-group row">
                                            <% if ((request.getAttribute("gradetype").toString()).equalsIgnoreCase("TTL")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TEL")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("THL")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TUL")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("STL")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEL")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SUL")) {%>
                                                <label for="name" class="col-sm-4 col-form-label">SSC Hallticket/Memo<font color="red">*</font></label>
                                                    <%} else if ((request.getAttribute("gradetype").toString()).equalsIgnoreCase("TTHS")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TEHS")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEHS150")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEHS180")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEHS200")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("STHS80")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("STHS100")) {%>
                                                 <label for="name" class="col-sm-4 col-form-label">Higher Grade Certificate<font color="red">*</font></label>
                                                    <%} else if ((request.getAttribute("gradetype").toString()).equalsIgnoreCase("TEJ")) {%>
                                                 <label for="name" class="col-sm-4 col-form-label">VII Bonafide Certificate<font color="red">*</font></label>
                                                    <%} else {%>
                                                 <label for="name" class="col-sm-4 col-form-label">Upload1 Certificate<font color="red">*</font></label>
                                                    <%}%>
                                           <div class="col-sm-8">
                                                <html:file property="upload1" styleId="upload1" onchange="return CheckfilePdfOrOther('upload1');"/><a href="#" onclick="fileupload(event, 'upload1')"/>View File</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="highdiv">
                                        <div class="col-md-8">

                                            <div class="form-group row">
                                                 <% if ((request.getAttribute("gradetype").toString()).equalsIgnoreCase("TTH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TEH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TUH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("THH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("STH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SUH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEI")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEH")) {%>
                                               <label for="name" class="col-sm-4 col-form-label">Lower Grade Certificate<font color="red">*</font></label>
                                                    <%}%>
                                                <div class="col-sm-8">
                                                    <html:file property="upload2" styleId="upload2" onchange="return CheckfilePdfOrOther('upload2');"/><a href="#" onclick="fileupload(event, 'upload2')"/>View File</a>
                                                </div>
                                        </div>
                                    </div>
                                                 </div>
  <div class="row" id="highdiv1">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                 <% if ((request.getAttribute("gradetype").toString()).equalsIgnoreCase("TTH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TEH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TUH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("THH")) {%>
                                                <label for="name" class="col-sm-4 col-form-label">SSC  Certificate<font color="red">*</font></label>
                                                    <%} else if ((request.getAttribute("gradetype").toString()).equalsIgnoreCase("STH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SUH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEI")
                                                        || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEH")) {%>
                                                <label for="name" class="col-sm-4 col-form-label">Graduation  Certificate<font color="red">*</font></label>
                                                    <%}%>
                                                <div class="col-sm-8">
                                                    <html:file property="upload3" styleId="upload3" onchange="return CheckfilePdfOrOther('upload3');"/><a href="#" onclick="fileupload(event, 'upload3')"/>View File</a>
                                                </div>
                                        </div>
                                    </div>
  </div>
                                <div class="row" id="highdiv2">

                                            <div class="col-md-8">
                                                <div class="form-group row">
                                                      <% if ((request.getAttribute("gradetype").toString()).equalsIgnoreCase("TTH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TEH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("TUH")
                                                            || (request.getAttribute("gradetype").toString()).equalsIgnoreCase("THH")) {%>
                                                            <label for="name" class="col-sm-4 col-form-label">Intermediate  Certificate<font color="red">*</font></label>
                                                    <%} else if ((request.getAttribute("gradetype").toString()).equalsIgnoreCase("SEH")) {%>
                                                <label for="name" class="col-sm-4 col-form-label">Vocational  Certificate<font color="red">*</font></label>
                                                    <%}%>
                                                      <div class="col-sm-8">
                                                        <html:file property="upload4" styleId="upload4" onchange="return CheckfilePdfOrOther('upload4');"/><a href="#" onclick="fileupload(event, 'upload4')"/>View File</a>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                            
                                                    <div class="row" id="highdiv3">
                                                            <div class="col-md-8">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Previous Session Hallticket<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                 <html:file property="upload5" styleId="upload5" onchange="return CheckfilePdfOrOther('upload5');"/><a href="#" onclick="fileupload(event, 'upload5')"/>View File</a>
                                                    <!--<textarea rows="3" class="form-control-plaintext"></textarea>-->

                                                </div>
                                            </div>
                                        </div>
                                                    </div>
                                            
                                        </div>
                                    </div>
                               
                                <br/>
                               
                                <br/>
                                <br/>
                                <div class="row">
                                    <div class="col-md-12 center">
                                        <center>   <input type="button" onclick="return SubmitForm();" value="SUBMIT"  class="submit"/></center>
                                    </div>
                                </div>
                                <br/>
                                <br/>
                            </html:form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>	
    </div>
</body>
</html>