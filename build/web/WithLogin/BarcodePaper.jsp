
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
//                    "scrollY": 300,
                    "scrollX": true
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <Style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control-plaintext{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
            /*            #imageview {
                            -moz-transition: transform 1s;
                            -webkit-transition: transform 1s;
                            transition: transform 1s;
                        }
            
                        .flip {
                            transform: rotate(-90deg);
                        }*/
        </style>
        <style>
            .overlay{
                display: none;
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                z-index: 999;
                background: rgba(255,255,255,0.8) url("img/LOADER.gif") center no-repeat;
            }
            /* Turn off scrollbar when body element has the loading class */
            body.loading{
                overflow: hidden;   
            }
            /* Make spinner image visible when body element has the loading class */
            body.loading .overlay{
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $(document).ajaxStart(function() {
                    $("body").addClass("loading");
                });
                $(document).ajaxComplete(function() {
                    $("body").removeClass("loading");
                });
                $("#barcode").focus()
                $('input[type=text], textarea').bind("cut copy paste", function(e) {
                    alert("Cut copy paste not allowed here");
                    e.preventDefault();
                });

//                document.onkeydown = function (e) {
//        return false;
//}
                var seconds = 5;
                setTimeout(function() {
                    if ($(".msg") !== null)
                        $(".msg").hide();
                }, seconds * 1000);
                $("form").attr('autocomplete', 'off');
                $("#showDiv").hide();
                $("#brDiv").show();
            });
            function moveCusor(id) {
                var idstring = parseInt(id.substring(id.length - 1, id.length)) + 1;
                if ($("#" + id).val().length === 8) {
                    $("#paper" + idstring).focus();
                }
            }
            function validateDetails1() {
                if ($("#barcode").val().length === 0) {
                    $("#barcode").val("");
                    alert("Please Scan barcode");
                }
                else {
                    var paraData = "barcode=" + $("#barcode").val();
                    $.ajax({
                        type: "POST",
                        url: "barcodePaper.xls?mode=getStatus&barcode=" + $("#barcode").val().trim(),
                        data: paraData,
                        success: function(response) {
                            var sample = response.trim().split("_");
                            var res = sample[0];
                            if (res == 1) {
                                var examination = sample[1];
                                var grade = sample[2];
                                var paper = sample[3];
                                var count1 = sample[4];
                                var hallticket = sample[5];
                                var ebatch = sample[6];
                                var ecenter = sample[7];
                                var totalbooklets = sample[8];
                                var scannedbooklets = sample[9];
                                var tobescannedbooklets = sample[10];
                                $("#examination").val(examination);
                                $("#grade").val(grade);
                                $("#examinationname").val(paper);
                                $("#ebatch").val(ebatch);
                                $("#ecenter").val(ecenter);
                                $("#institute").val(count1);
                                $("#hallticket").val(hallticket);
                                $("#totalbooklets").val(totalbooklets);
                                $("#scannedbooklets").val(scannedbooklets);
                                $("#tobescannedbooklets").val(tobescannedbooklets);
                                $("#barcode").prop('readonly', true);
                                $("#examination").prop('readonly', true);
                                $("#grade").prop('readonly', true);
                                $("#ebatch").prop('readonly', true);
                                $("#examinationname").prop('readonly', true);
                                $("#ecenter").prop('readonly', true);
                                $("#institute").prop('readonly', true);
                                $("#totalbooklets").prop('readonly', true);
                                $("#scannedbooklets").prop('readonly', true);
                                $("#tobescannedbooklets").prop('readonly', true);
                                $("#showDiv").show();
                                $("#brDiv").hide();
                                var paperarray = ['Paper-1', 'Paper-2', 'Paper-3', 'Paper-4', 'Paper-5', 'Paper-6', 'Paper-7', 'Paper-8']
                                var str = "<table width='100%' border='1'>";
                                str += "<thead>"
                                str += "<tr>"
                                str += "<th>S.No</th>"
                                str += "<th>Paper</td>"
                                str += "<th>Paper Barcode</th>"
                                str += "</thead>"
                                str += "<tr>"
                                for (var i = 0; i < count1; i++) {
                                    str += "<tr>"
                                    str += "<td style='text-align: center'>" + (i + 1) + "</td>"
                                    str += "<td style='text-align: center'>" + paperarray[i] + "</td>"
                                    str += "<td style='text-align: center'><input type='text' width='50%' styleClass='form-control' name=paper" + [i] + " id=paper" + [i] + " onchange='disableData(id),moveCusor(id)' maxlength='8'/></td>"
                                    str += "</tr>"
                                }
                                str += "</table>"
                                $("#tablecontainer").empty();
                                $("#tablecontainer").append(str)
                                $("#paper0").focus();
                            } else if(res==2){
                                alert("This Booklet has been Already Scanned")
                                $("#barcode").val("");
                                $("#barcode").prop('readonly', false);
                            }else if(res==3){
                                alert("Scanned Barcode either Malpracticed /Absentee")
                                $("#barcode").val("");
                                $("#barcode").prop('readonly', false);
                            }
                            else {
                                alert("Barcode not found")
                                $("#barcode").val("");
                                $("#barcode").prop('readonly', false);
                            }
                        }
                    });
                }
            }
             var paperlist=[];
             var innetlist="";
            function disableData(id) {
//        count++;       
 if (paperlist.indexOf($("#"+id).val()) === -1) {
                $("#" + id).prop('readonly', true);
                 paperlist.push($("#"+id).val());
                 innetlist=innetlist+$("#"+id).val()+"$";
 }else{
     alert("Page has been Already Scanned")
       $("#" + id).val("");
      $("#" + id).focus();
 }
            }
//            function disableData(id) {
////        count++;       
//                $("#" + id).prop('readonly', true);
////        if(count==8){
////             for(var i=0;i<count;i++){
////                    if($("#paper"+i).val().length===0){
////                        alert("please scan Paper"+(i+1)+" Barcode")
////                        $("#paper"+i).focus();
////                        return false;
////                    }else{
////                       document.forms[0].mode.value="submitDetails"
////                       document.forms[0].submit();
////                    }
////                } 
////            
////        }else{
////            
////        }
//            }
            function SubmitForm() {
                var flag = "";
                var paperarray = ['Paper-1', 'Paper-2', 'Paper-3', 'Paper-4', 'Paper-5', 'Paper-6', 'Paper-7', 'Paper-8']
                var count = $("#institute").val();
                for (var i = 0; i < count; i++) {
                    if ($("#paper" + i).val().length === 0) {
                        alert("please scan Paper" + (i + 1) + " Barcode")
                        $("#paper" + i).focus();
                        return false;
                    }
                }
                for (var i = 0; i < count; i++) {
                    if (($("#barcode").val().trim() + "0" + (i + 1)) == ($("#paper" + i).val())) {
                        flag = "Y"
                    } else {
                        flag = "N"
                        break;
                    }
                }
                if (flag == "Y") {
                    var x = confirm("Do you Want to Proceed?")
                    if (x == false) {
                        return false
                    } else {
                        $("#epaper").val(innetlist);
                        document.forms[0].statusresult.value = flag;
                        document.forms[0].mode.value = "submitDetails";
                        document.forms[0].submit();
                    }
                } else {
                    var x = confirm("Papers Mismatched.Do you Want to Proceed?")
                    if (x == false) {
                        $("#showDiv").hide();
//                        for (var i = 0; i < count; i++) {
//                            $("#paper" + i).val("");
//                        }
                        $("#institute").val("");
                        $("#examination").val("");
                        $("#grade").val("");
                        $("#ebatch").val("");
                        $("#ecenter").val("");
                        $("#epaper").val("");
                        $("#hallticket").val("");
                        $("#brDiv").show();
                        $("#barcode").val("");
                        $("#barcode").prop('readonly', false);
                         $("#barcode").focus();
                    } else {
                        $("#epaper").val(innetlist);
                        document.forms[0].statusresult.value = flag;
                        document.forms[0].mode.value = "submitDetails";
                        document.forms[0].submit();
                    }
                }
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }

        </script>
        <!--    <div class="page-title title-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Registration </h1>
                            <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
        
                                <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                                <span>Registration</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
    <br/>
    <br/>
    <body>
        <div class="feedback-form">
            <div class="container">
                <div class="row">

                    <h3 class="block-head-News">BARCODE SCAN</h3>
                    <div class="line-border"></div>
                    <br>
                    <div class="row">
                        <div class=" col-md-12">
                            <!--<div class="login-form">-->
                            <div>
                                <html:form action="/barcodePaper"  styleId="d" method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <html:hidden property="institute" styleId="institute"/>
                                    <html:hidden property="hallticket" styleId="hallticket"/>
                                    <html:hidden property="statusresult" styleId="statusresult"/>
                                    <html:hidden property="epaper" styleId="epaper"/>
                                    <logic:present name="result2">
                                        <span class="msg"><center> <font color="green" style="font-weight: bold">${result2}</font>  </center>  </span>
                                            </logic:present>
                                            <logic:present name="result">
                                        <span class="msg"><center> <font color="red" style="font-weight: bold">${result}</font></center></span>
                                    </logic:present><br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Scan Barcode<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="barcode" styleId="barcode" maxlength="6" onchange="validateDetails1();"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="brDiv">
                                        <br/> <br/> <br/> <br/> <br/>
                                        <br/> <br/> <br/> <br/> <br/>
                                        <br/> <br/> <br/> <br/> <br/>
                                        <br/> <br/> <br/> <br/> <br/>
                                    </div>
                                    <div id="showDiv">
                                        <%
                                            String sa[] = {"paper1", "paper2", "paper3", "paper4", "paper5", "paper6", "paper7", "paper8"};
                                            ArrayList<String> paperlist = new ArrayList<String>(Arrays.asList(sa));
                                        %>
                                        <table class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Examination</th>
                                                    <th>Grade</th>
                                                    <th>Paper</th>
                                                    <th>Examination Batch</th>
                                                    <th>Examination Center</th>
                                                    <th>Total Booklets</th>
                                                    <th>Scanned Booklets</th>
                                                    <th>To be Scanned Booklets</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%-- <logic:iterate name="listData" id="list">--%>
                                                <tr>
                                                    <%--                                           <td style="text-align: center;"><%=map.get("examination")%></td>
                                                                                            <td style="text-align: Center;"><%=map.get("grade")%></td>     
                                                                                            <td style="text-align: Center;"><%=map.get("paper")%></td>--%>
                                                    <td style="text-align: center;"><html:text styleClass="form-control" property="examination" styleId="examination" /></td>
                                                    <td style="text-align: center;"><html:text styleClass="form-control" property="grade" styleId="grade" /></td>     
                                                    <td style="text-align: center;"><html:text styleClass="form-control" property="examinationname" styleId="examinationname" /></td>
                                                    <td style="text-align: center;"><html:text styleClass="form-control" property="ebatch" styleId="ebatch" /></td>
                                                    <td style="text-align: center;"><html:text styleClass="form-control" property="ecenter" styleId="ecenter" /></td>
                                                    <td style="text-align: center;"><html:text styleClass="form-control" property="totalbooklets" styleId="totalbooklets" /></td>
                                                    <td style="text-align: center;"><html:text styleClass="form-control" property="scannedbooklets" styleId="scannedbooklets" /></td>
                                                    <td style="text-align: center;"><html:text styleClass="form-control" property="tobescannedbooklets" styleId="tobescannedbooklets" /></td>
                                                </tr>
                                                <%-- </logic:iterate>--%>
                                            </tbody>
                                        </table>
                                        <br/>
                                        <table id="tablecontainer" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%"></table>
                                        <br/>
                                        <br/>

                                        <div class="row">
                                            <div class="col-md-12 center">
                                                <center><input type="button" onclick="return SubmitForm();" value="SUBMIT"  class="btn btn-primary"/></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                            </html:form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>	
    </div>
</body>
</html>
