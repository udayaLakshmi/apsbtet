<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="icon" href="assets/img/aptonline.png" type="image/gif" sizes="32x32">
        <title>:: SBTET ::</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"> -->
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>
        <link rel="stylesheet" href="assets/css/slider.css">
        <style type="text/css">
            .table>tbody>tr>td
            {
                text-align: center;
            }
            .table>thead>tr>th
            {
                text-align: center;
                background: #058f96;
                color: #fff;
            }
            .blink_me {
                -webkit-animation-name: blinker;
                -webkit-animation-duration: 2s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-iteration-count: infinite;

                -moz-animation-name: blinker;
                -moz-animation-duration: 2s;
                -moz-animation-timing-function: linear;
                -moz-animation-iteration-count: infinite;

                animation-name: blinker;
                animation-duration: 2s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;

                font-size: 16px;
                text-align: right;
                font-weight: bold;

            }
            @-moz-keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }
            @-webkit-keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }
            @keyframes blinker {  
                0% { color: red; }
            50% { color: green; }
            100% { color: blue; }
            }

            .text_style {
                margin-left: 50px;
            }
        </style>
    </head>
    <body>
        <section class="top-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-5"> 
                        <!-- <div class="page_social_icons"> --> 
                        <!-- <a class="fa fa-facebook" href="#" title="Facebook"></a> --> 
                        <!-- <a class="fa fa-twitter" href="#" title="Twitter"></a> --> 

                        <!-- <a class="fa fa-instagram" href="#" title="Instagram"></a> --> 
                        <!-- <a class="fa fa-google" href="#" title="Google"></a> --> 
                        <!-- </div> -->

                        <div class="guidelines f-l">
                            <ul class="incrs-font">
                                <li><a href="">A+</a></li>
                                <li><a href="">A</a></li>
                                <li><a href="">A-</a></li>
                                <li><a href="">A</a></li>
                                <li style="background:#2f363c"><a href="">A</a></li>
                            </ul>
                            &nbsp;&nbsp;&nbsp; <a href="">Screen Reader</a>&nbsp;&nbsp;&nbsp; <a href="">Skip to Main Content</a>&nbsp;&nbsp;&nbsp; </div>
                    </div>
                    <div class="col-md-7">
                        <!--<div class="guidelines"> <a href=""><i class="fa fa-commenting" aria-hidden="true"></i>&nbsp; Feed Back</a>&nbsp;&nbsp;&nbsp;  </div>-->
                    </div>
                </div>
            </div>
        </section>
        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="#"><img src="assets/img/logo.png"></a> </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="Welcome.do">Home</a></li>
                        <!--<li class=""><a href="#">Govt Orders</a></li>-->
                        <!--<li class=""><a href="#"> Publications</a></li>-->
                        <!--<li class=""><a href="#"> Eligibility & Forms</a></li>-->
                        <!--<li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">ACT'S <span class="caret"></span></a>-->
                        <!--<ul class="dropdown-menu">-->
                        <!--            <li><a href="#">Acts & Rules</a></li>
                                    <li><a href="#">RTI Act</a></li>
                                    <li><a href="#">Citizen Chart</a></li>-->
                        <!--</ul>-->
                        <!--</li>-->
                        <!--        <li class="dropdown menu-large "> <a href="# " class="dropdown-toggle " data-toggle="dropdown ">Other Links <b class="caret "></b></a>
                                  <ul class="dropdown-menu megamenu row ">
                                    <li class="col-sm-3 ">
                                      <ul>
                                        <li><a href="# "> Polytechics In AP & TS</a> </li>
                                        <li class="disabled "><a href="# ">AICTE</a> </li>
                                        <li><a href="# ">DTEAP</a> </li>
                                        <li><a href="# ">Tenders</a> </li>
                                        <li><a href="# "> Downloads</a> </li>
                                        <li><a href="# ">Anti Raging</a> </li>
                                        <li><a href="# ">Media Releases</a> </li>
                                      </ul>
                                    </li>
                                    <li class="col-sm-3 ">
                                      <ul>
                                        <li><a href="# ">DTE Telangana</a> </li>
                                        <li><a href="# ">Polycet Telangana State</a> </li>
                                        <li><a href="# ">ITI Bridge Course</a> </li>
                                        <li><a href="# ">Diploma Results</a> </li>
                                        <li><a href="# ">Status of Biometric Devices In Private Polytechnic</a> </li>
                                        <li><a href="# ">Industrial Training Branch Wise Data</a> </li>
                                        <li><a href="# ">Requirement for CISCO training Programmes</a> </li>
                                      </ul>
                                    </li>
                                    <li class="col-sm-3 ">
                                      <ul>
                                        <li><a href="# ">Teaching experience (Subject wise) ? certain infor</a> </li>
                                        <li><a href="# ">PRACTICAL SUBJECTS HANDLING STAFF - DETAILS</a> </li>
                                        <li><a href="# ">lndustrial training for 2019-20 details of student</a> </li>
                                        <li><a href="# ">CISCO Conference Registration Link</a> </li>
                                        <li><a href="# ">Industrial Training Branch & Industry wise Details</a> </li>
                                      </ul>
                                    </li>
                                    <li class="col-sm-3 ">
                                      <ul>
                                        <li><a href="# ">Industrial Training Students Details from the Prls</a> </li>
                                        <li><a href="# ">LEARNING MANAGEMENT SYSTEM</a> </li>
                                        <li><a href="# ">POLYCET-2020</a> </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </li>-->
                        <li class=""><a href="#"> Contact</a></li>
                        <li class=""><a href="officialLogin.do" class="login"> &nbsp;Login<i class="fa fa-lock" aria-hidden="true"></i></a></li>
                        <!-- <li class=""><a href="#">Gallery</a></li> --> 
                        <!-- <li class=""><a href="#">CSR</a></li> -->

                    </ul>
                    <!-- <ul class="nav navbar-nav navbar-right"> --> 
                    <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> --> 
                    <!-- <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> --> 
                    <!-- </ul> --> 
                </div>
            </div>
        </nav>
        <div class="nav-border"></div>
        <div class="banner"> 
            <!--  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                 Indicators 
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                
                </ol>
            
                 Wrapper for slides 
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="assets/img/main-banner3-1366X700.jpg" alt="Los Angeles" style="width:100%;">
                            <div class="carousel-caption">
                                    <div class="main-banner-content">
                                    
                                    <h1 style="opacity: 1;" class="animated fadeInUp">State Board Of Technical Education & Training</h1>
                                    <br>
                                  <div class="btn-box animated fadeInUp" style="opacity: 1;"> <a href="#" class="default-btn pol-list" >Politechnic Collages in AP</a>  </div>
                                                     </div>
                            </div>
                  </div>
            
                  <div class="item">
                    <img src="assets/img/main-banner1-1366X700.jpg" alt="Los Angeles" style="width:100%;">
                            <div class="carousel-caption">
                                    <div class="main-banner-content">
                                    
                                    <h1 style="opacity: 1;" class="animated fadeInUp">State Board Of Technical Education & Training</h1>
                                    <br>
                                    <div class="btn-box animated fadeInUp" style="opacity: 1;"> <a href="#" class="default-btn pol-list" >Politechnical Collages in TS</a>  </div>
                                                     </div>
                                                     </div>
                  </div>
                
                </div>
            
              </div>
            </div>-->

            <div class="col-md-12">
                <!--   <marquee behaviour="alternate" class="blink_me"  style=" margin-top: 10px;color:#28278e;font-size: 15px; font-weight: bold; margin-top: 10px;" onmouseover="this.stop();" onmouseout="this.start();" scrollamount="6" scrolldelay="0">
                       <img src="img/new.gif" /> No batch change will be entertianed  after 2:00PM today i.e. 27th July 2021.Downloading of Hall Tickets for TWSH Examinations will be enabled on 28th July 2021
                    </marquee>-->
                                                

                <h3 style="text-align:center" > <span class="blink_me"><img src="img/new.gif" />&nbsp;&nbsp;Downloading of Hall Tickets for Diploma Examinations (Aug/Sept 2021) has been enabled</span></h3>
                <!--<h3 style="text-align:center" > <span class="blink_me"><img src="img/new.gif" />&nbsp;&nbsp;Payment Gateway and SMS services will not be functional from 20th Aug'21 5:00pm to 22nd Aug'21 9:00pm due to Annual Power shutdown/maintenance activity</span></h3>-->
                <!--<h3 style="text-align:center" > <span class="blink_me"><img src="img/new.gif" />&nbsp;&nbsp;Last date for Online fee payment (without late fee) for C-16 Diploma & 2019-20 ER 91 schemes has been extended upto <b>*20.08.2021*</> till *5:00pm*</span></h3>-->
                <!--<h3 style="text-align:center" > <span class="blink_me"><img src="img/new.gif" />&nbsp;&nbsp;Hall Tickets for CCIC, Crafts & Other Short term courses has been enabled</span></h3>--> 
                <!--<h3 style="text-align:center" > <span class="blink_me"><img src="img/new.gif" />&nbsp;&nbsp;Downloading of Hall Tickets for TWSH Examinations has been enabled </span></h3>--> 
                <!--<h3 style="text-align:center" > <span class="blink_me"><img src="img/new.gif" />&nbsp;&nbsp;Last date for Submission of Online Applications for CCIC, Crafts & Other Certificate Course (without late fee) has been extended upto<font color="red"> 28.07.2021</font></span></h3>--> 
                <!--<h3 style="text-align:center" > <span class="blink_me"><img src="img/new.gif" />&nbsp;&nbsp;Edit option is available in institute login candidates who have not done payment can use this service</span></h3>--> 

            </div>
            <div class="feedback-form">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="nav-border" style="position: initial !important;"></div>
                            <div class="side-box2">
                                <div class="how2 how2-cl4 flex-s-c" style="background: #2196f3;">
                                    <h3 class="f1-m-2 cl3 tab01-title">	APPLICATION FORMS</h3>
                                </div>
                                <ul class="services-list">
                                   
                                    <li><span><img src="img/new.gif" /></span>&nbsp;&nbsp;<a href="oep.do"> Click here to Download the Hall Ticket for Diploma Examinations (Aug/Sept 2021) </a></li>
                                      <li><span></span>&nbsp;&nbsp;<a href="onlineExaminationPayment.do"> Click here for Online fee payment for Diploma examinations </a></li>
                                    <!--<li><span><img src="img/new.gif" /></span>&nbsp;&nbsp;<a href="downloadHallticket.do">Click here to Download HallTicket<font color="red">(For Private Candidates only)</font>  </a></li>-->
                            <!--<li><span><img src="img/new.gif" /></span>&nbsp;&nbsp;<a href="registerCTwenty.do">Click here for Online fee payment for C20 examinations </a></li>-->
                                    <!--<li><span></span>&nbsp;&nbsp;<a href="register.do">TWSH Registration Form<font color="red">(For Private Candidates only)</font> </a></li>-->
                                    <!--<li><span></span>&nbsp;&nbsp;<a href="formPrint.do">Click here to Print TWSH Registration Form  </a></li>-->
                                    <!--<li><span></span>&nbsp;&nbsp;<a href="resend.do">Click here to know the TWSH application status/Re-upload Service </a></li>-->
                                    <!--<li><span></span>&nbsp;&nbsp;<a href="resendCCIC.do">Click here to know the CCIC application status/Re-upload Service </a></li>-->

                                    <!--<li><span><img src="img/new.gif" /></span>&nbsp;&nbsp;<a href="dtoregister.do">DTO CCIC Registration Form</a></li>-->
                                    <!--<li><span></span>&nbsp;&nbsp;<a href="dtoregister.do">DTOs Registration Form For Office Automation Course</a></li>-->
                                    <!--<li><span></span>&nbsp;&nbsp;<a href="formPrintCCIC.do">Click here to print CCIC, Craft & Other certificate course form</a></li>-->
                                    <!--<li><span><img src="img/new.gif" /></span>&nbsp;&nbsp;<a href="downloadHallticket.do">Click here to download private candidate TWSH HallTicket </a></li>-->
                                    <!--<li><span><img src="img/new.gif" /></span>&nbsp;&nbsp;<a href="downloadHallticket.do">Click here to Download TWSH Hall Ticket<font color="red">(For Private Candidates only)</font></a></li>-->
                                    <!--<li>&nbsp;&nbsp;<a href="downloadBeforeLoginHallticketCCIC.do">Click here to Download CCIC Hall Ticket<font color="red">(For DTO Candidates only)</font></a></li>-->
                                </ul>
                            </div>

                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="nav-border" style="position: initial !important;"></div>
                            <div class="side-box2">
                                <div class="how2 how2-cl4 flex-s-c" style="background: #FF9800;">
                                    <h3 class="f1-m-2 cl3 tab01-title">	INFORMATION BULLETIN </h3>
                                </div>
                                <ul class="services-list">
                                    
                                     <li style="text-transform:none!important"><img src="img/new.gif" /><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=dipNotification.pdf" target="_blank">Notification for Online Registration of C-16 Scheme Diploma & 2019-20 batch of ER-91 scheme of Pharmacy
                                        </a>
                                    </li>
                                      <li style="text-transform:none!important"><img src="img/new.gif" /><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=1yr5sem.pdf" target="_blank">C-16 - 1Yr, V & VII Semester Time Table
                                        </a>
                                    </li>
                                      <li style="text-transform:none!important"><img src="img/new.gif" /><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=3sem.pdf" target="_blank">C-16 - II, IV & VI Semester Time Table
                                        </a>
                                    </li>
                                      <li style="text-transform:none!important"><img src="img/new.gif" /><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=phtimetable.pdf" target="_blank">PH-II Year Time Table
                                        </a>
                                    </li>
<!--                                    <li style="text-transform:none!important"><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=ccicexamination.pdf" target="_blank">CCIC, Crafts & Other Short Term Courses Examination Schedule
                                        </a>
                                    </li>-->
<!--                                    <li style="text-transform:none!important"><img src="img/new.gif" /><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=twshexamination.pdf" target="_blank">TWSH Examination Schedule
                                        </a>
                                    </li>-->



<!--                                    <li style="display: flow-root;" ><font color="red">*Note.*</font><b>TWSH - Candidates who have applied through the Institutions can approach their respective Institution to collect their Hall Ticket</b>

                                    </li> -->
                                    <%--   <li style="text-transform:none!important"><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=eligibility.pdf" target="_blank">TWSH Eligibility and Fee Structure
                                                      </a>
                                                  </li>--%>
<!--                                    <li style="text-transform:none!important"><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=CCICeligibility.pdf" target="_blank">CCIC Eligibility and Fee Structure
                                        </a>
                                    </li>-->

                                    <%--                                        <li style="text-transform:none!important"><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=notification.pdf" target="_blank">Notification
                                                                                </a>
                                                                            </li>--%>
<!--                                    <li style="text-transform:none!important"><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=notificationCCIC.pdf" target="_blank">CCIC, Craft & Other certificate course Notification
                                        </a>
                                    </li>-->
                                    <%--  <li style="text-transform:none!important"><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=PrivateCandidates.pdf" target="_blank">User Manual for Private Candidates
                                                       </a>
                                                   </li> --%>
<!--                                    <li style="text-transform:none!important"><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=DTOUserManual.pdf" target="_blank">User Manual for CCIC, Craft & Other certificate course(for DTO Employees)
                                        </a>
                                    </li>-->
                                    <%-- <li style="text-transform:none!important"><a href="instituteMaster.do" target="_blank">List of Recognised Institutes for TWSH Examinations 2021
                                                    </a>
                                                </li> --%>
                                    <!--                              <li style="text-transform:none!important"><img src="img/new.gif" /><a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=PrivateCandidates.pdf" target="_blank">User Manual for Private Candidates
                                                                                </a>
                                                                            </li>-->
                                    <!--<li><a href="#">Notification </a></li>-->
                                    <!--                            <li><a href="#">PHOTO GALLERY </a></li>
                                                                <li><a href="#">NEWS &amp; EVENTS </a></li>-->
                                </ul>
                            </div>

                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="nav-border" style="position: initial !important;"></div>
                            <div class="side-box2">
                                <div class="how2 how2-cl4 flex-s-c" style="background: #1bb3bb;">
                                    <h3 class="f1-m-2 cl3 tab01-title">	IMPORTANT DATES </h3>
                                </div>
                                <ul class="services-list">
                                    
                                    <!--<li>Start Date for Online Fee payment for C-16 Diploma & 2019-20 ER-91 schemes is 12.08.2021</li>-->
                                    <!--<li><img src="img/new.gif" />Last date for Online fee payment (without late fee) for C-16 Diploma & 2019-20 ER 91 is *23.08.2021* </li>-->
                                    <!--<li><img src="img/new.gif" />Last Date for online submission of CCIC, Craft & Other certificate course application under TATKAL Scheme Rs.1800/- is 30.07.2021</li>-->
                                    <!--                            <li><a href="#">PHOTO GALLERY </a></li>
                                                                <li><a href="#">NEWS &amp; EVENTS </a></li>-->
                                </ul>
                            </div>
                            <!--             <div >
                                                                <img src="img/help.jpg" style="box-shadow: 1px 2px 1px #ccc; border: 1px solid #ccc;margin-left: 60px" />
                                                            </div> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="nav-border" style="position: initial !important;"></div>
                            <div class="side-box5">
                                <div class="how2 how2-cl4 flex-s-c" style="background: #2196f3;">
                                    <h3 class="f1-m-2 cl3 tab01-title"><i class="fa fa-phone"></i> Help Desk</h3>
                                </div>
                                <br>
                                <div class="table-responsive" style="overflow-x: scroll">
                                    <table class="table table-bordered  table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Domain Related Queries<br/>(Department)</th>
                                                <th rowspan="3">Technical Related Queries</th>
                                                <th>Email ID</th>
                                                <th>Queries Related Timings</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<!--                                            <tr>
                                                <td>TWSH - 7901620551</td>
                                                                              <td>CCIC - 7901620567</td>
                                                                              <td>CCIC - 7901620557</td>
                                                  </tr>-->
                                            <tr>
                                                <!--<td>TWSH - 7901620551</td>-->
                                                <td>POLYCET & Diploma - 7901620551</td>
                                                 <td rowspan="2" style="vertical-align: middle !important;">9154125801</td>
                                                <td rowspan="2" style="vertical-align: middle !important;" >systemanalyst.apsbtet@gmail.com</td>
                                                <td rowspan="2" style="vertical-align: middle !important;">(10.00 AM to 5.30 PM) on all working days</td>
                                        
                                             
                                                
                                                
                                            </tr>
                                            <tr>
                                                <!--<td>TWSH - 7901620551</td>-->
                                                <!--<td>CCIC - 7901620567</td>-->
                                                <td>POLYCET & Diploma - 7901620510</td>
                                                <!--<td >9154125801</td>-->
                                                <!--<td ></td>-->


                                                <!--<td >(10.00 AM to 5.30 PM) on all working days</td>-->
                                            </tr>
                                            <tr>
                                                 <td>TWSH & CCIC - 7901620557</td>
                                                <td  rowspan="2" style="vertical-align: middle !important;" >9154125801</td>
                                                <td rowspan="2" style="vertical-align: middle !important;" >apsbtet.helpdesk@gmail.com</td>
                                                <td rowspan="2" style="vertical-align: middle !important;" >(10.00 AM to 5.30 PM) on all working days</td>
                                                
                                            </tr>
                                             <tr>
                                                 <td>TWSH & CCIC - 7901620567</td>
                                                <!--<td  rowspan="2" style="vertical-align: middle !important;" >9154125801</td>-->
                                                <!--<td rowspan="2" style="vertical-align: middle !important;" >apsbtet.helpdesk@gmail.com</td>-->
                                                <!--<td rowspan="2" style="vertical-align: middle !important;" >(10.00 AM to 5.30 PM) on all working days</td>-->
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/><br/><br/><br/>


            <footer class="footer-area">
                <div class="container">
                    <div class="row">
                        <div class="new-footer-links"> 
                            <!--				  <a href="disclaimer.html">Disclaimer</a> |--> 
                            <!-- <a href="privacy_policy.html">Privacy Policy</a> | --> 
                            <!-- <a href="termsofuse.html">Terms of Use</a> | --> 
                            <!-- <a href="refund_policy.html">Refund &amp; Cancellation Policy</a> | --> 
                            <!-- <a href="accessibility.html">Accessibility</a> | --> 
                            <!-- <a href="#">FAQ's</a> | --> 
                            <!-- <a href="#">Help</a> | --> 
                            <!-- <a href="feedback.html">Feedback</a>  --> 
                        </div>
                    </div>
                    <div class="copyright-area">
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-sm-6 col-md-6">
                                <p> � Copy Rights Reserved with SBTET AP.</p>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-md-6">
                                <p class="text-right">Designed &amp; Developed by <img class="aptonline" src="assets/img/logo-3.png" alt="image"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <script src="assets/js/jquery.min.js"></script> 
            <script src="assets/js/bootstrap.min.js"></script> 
            <script src="assets/js/fontjs.js"></script> 
            <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script> 
            <script  src="assets/js/slider.js"></script> 
            <script>
                jQuery(document).ready(function() {
                    $(".dropdown").hover(
                            function() {
                                $('.dropdown-menu', this).stop().fadeIn("fast");
                            },
                            function() {
                                $('.dropdown-menu', this).stop().fadeOut("fast");
                            });
                });
            </script>
    </body>
</html>
