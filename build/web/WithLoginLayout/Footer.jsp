<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../favicon.ico">
<title>:: SBTET ::</title>
<!-- Fontawesome icon CSS -->
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="css/styles1.css" type="text/css">

 <style>
   @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700');
body, .toplabel, .middlebox .value
{
    font-family: 'Roboto Condensed', sans-serif;
}
	 .table-responsive {
    min-height: .01%;
    overflow-x: initial !important;
}
</style>
</head>
<body>
<div class="row footerrow">
<div class="container">
<div class="col-xs-12 col-sm-6">� Copy Rights Reserved with SBTET AP.</div>
<div class="col-xs-12 col-sm-6" style="text-align:right">Designed & Developed by <img src="assets/img/logo-3.png" class="img_footer" /></div>
</div>
</div>
</body>
</html>
