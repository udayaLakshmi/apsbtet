<%-- 
    Document   : LoginDetails
    Created on : Jul 8, 2021, 4:37:32 PM
    Author     : 1582792
--%>

<%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>
<%@page import="net.apo.angrau.db.ConnectionPool"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%--<%@page import="org.isms.common.CommonConistants"%>--%>
<%@page import="net.apo.angrau.db.DatabaseConnection"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.CallableStatement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" import="java.sql.Connection, java.sql.CallableStatement"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="<%=basePath%>js/alert/alert.css" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="<%=basePath%>Styles/scripts/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=basePath%>js/alert/alert.js" type="text/javascript"></script>
        <title>::Login Details::</title>
        <script>
            function validateForm() {
                var clientIP = document.getElementById("IPAddress").value;
//                if (((clientIP != null) && !(clientIP == '203.145.179.119' || clientIP == '125.16.9.132' || clientIP == '61.246.226.112'))) {
//                     alert("Accessing from Unknown IP Address is Not Allowed");
//                     return false;
//                } else
                    if (document.forms["myform"]["schoolCode"].value == "") {
                    alert("Please Enter the User ID");
                    return false;
                } else if (document.forms["myform"]["acyear"].value == "0") {
                    alert("Please select the acyear");
                    return false;
                }
            }
        </script>

        <style>
            .body{
                margin: 0px;
                padding: 0px;
                background-color: #5a5a5a;
                font-family: calibiri !important;
            }
            table, th, td {
                border: 2px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
                text-align: left;

            }
            table#t01 {
                width: 100%;    
                background-color: #f1f1c1;
            }
        </style>
        
        <!--Model PopUp Alert Started-->


    <script type="text/javascript">
        var ALERT_TITLE = "Alert Message";
        var ALERT_BUTTON_TEXT = "Ok";

        if (document.getElementById) {
            window.alert = function(txt) {
                createCustomAlert(txt);
            }
        }

        function createCustomAlert(txt) {
            d = document;

            if (d.getElementById("modalContainer"))
                return;

            mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
            mObj.id = "modalContainer";
            mObj.style.height = d.documentElement.scrollHeight + "px";

            alertObj = mObj.appendChild(d.createElement("div"));
            alertObj.id = "alertBox";
            if (d.all && !window.opera)
                alertObj.style.top = document.documentElement.scrollTop + "px";
            alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
            alertObj.style.visiblity = "visible";

            h1 = alertObj.appendChild(d.createElement("h1"));
            h1.appendChild(d.createTextNode(ALERT_TITLE));

            msg = alertObj.appendChild(d.createElement("p"));
            //msg.appendChild(d.createTextNode(txt));
            msg.innerHTML = txt;

            btn = alertObj.appendChild(d.createElement("a"));
            btn.id = "closeBtn";
            btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
            btn.href = "#";
            //btn.focus();
            btn.onclick = function() {
                removeCustomAlert();
                return false;

            }

            alertObj.style.display = "block";

        }

        function removeCustomAlert() {
            document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));

        }
        function ful() {
            alert('Alert this pages');
        }
    </script>
</head>
<style>
    #modalContainer {
        background-color:rgba(0, 0, 0, 0.3);
        position:absolute;
        width:100%;
        height:100%;
        top:0px;
        left:0px;
        z-index:10000;
        /* background-image:url(tp.png);  required by MSIE to prevent actions on lower z-index elements */
    }

    #alertBox {
        position:relative;
        width:500px;
        min-height:100px;
        margin-top:200px;
        border:1px solid #666;
        background-color:#fff;
        background-repeat:no-repeat;
        background-position:20px 30px;
        border-radius: 7px;
    }

    #modalContainer > #alertBox {
        position:fixed;
    }

    #alertBox h1 {
        margin:0;
        font:bold 0.9em verdana,arial;
        background-color: #FB4E01;
        /*background-color: #8d8d8d;*/
        color:#FFF;
        border-bottom: 1px solid #FF9E72;
        padding: 5px 0 6px 5px;
        border-radius: 5px;
        font-size: 13px;

    }

    #alertBox p {
        font:14px verdana,arial;
        /*height:50px;
        margin-left:55px;*/
        padding-left:5px;
        width: 100%;
        margin: 16px auto;
        text-align: center;
        color: #F00;

        font-weight: blod;
        font-style: italic;
        font-weight: bold;
    }

    #alertBox #closeBtn {
        display:block;
        position:relative;
        margin:5px auto;
        padding:7px;
        border:0 none;
        width:70px;
        font:0.7em verdana,arial;
        text-transform:uppercase;
        text-align:center;
        color:#FFF;
        background-color:#357EBD;
        border-radius: 3px;
        text-decoration:none;
    }

    /* unrelated styles */

    #mContainer {
        position:relative;
        width:600px;
        margin:auto;
        padding:5px;
        border-top:2px solid #000;
        border-bottom:2px solid #000;
        font:0.7em verdana,arial;
    }

    h1,h2 {
        margin:0;
        padding:4px;
        font:bold 1.5em verdana;
        border-bottom:1px solid #000;
    }

    code {
        font-size:1.2em;
        color:#069;
    }

    #credits {
        position:relative;
        margin:25px auto 0px auto;
        width:350px; 
        font:0.7em verdana;
        border-top:1px solid #000;
        border-bottom:1px solid #000;
        height:90px;
        padding-top:4px;
    }

    #credits img {
        float:left;
        margin:5px 10px 5px 0px;
        border:1px solid #000000;
        width:100px;
        height:179px;
    }

    .important {
        background-color:#F5FCC8;
        padding:2px;
    }

    code span {
        color:green;
    }

    .list_type li{
        line-height: 24px;
        font-size: 15px;
        font-weight: bold;
        color: black;
    }

    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 2s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;

        -moz-animation-name: blinker;
        -moz-animation-duration: 2s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;

        animation-name: blinker;
        animation-duration: 2s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;

        text-align: left;
        font-size: 18px;
        font-weight: bold;

    }

</style>


<!--Model PopAlert Ended-->
    </head>

    <%
        String schoolID = request.getParameter("schoolCode");
        StringBuffer resultBuffer = new StringBuffer();
        if (request.getParameter("submit") != null) {
            if (schoolID == null || schoolID.length() == 0) {
                out.println("Invalid User Code");
                return;
            }
            if (schoolID != null && schoolID.length() > 25) {
                out.println("User Code Should Be Less Than 25 Digits");
                return;
            }



            String acyear = request.getParameter("acyear");

            if (acyear == null || acyear.length() == 0) {
                out.println("Invalid AC Year ");
                return;
            }
            Connection conn = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            try {
                   conn = DatabaseConnection.getConnection(); 
               
                if (schoolID != null) {
                    schoolID = schoolID.trim();
                }

                pstmt = conn.prepareStatement("SELECT PassWord,RoleId,LoginFlag FROM  LoginDetails with(nolock)  Where UserName=? and status='Active'");
                pstmt.setString(1, schoolID);
                rs = pstmt.executeQuery();
                while (rs != null && rs.next()) {
                    resultBuffer.append("PassWord :" + rs.getString(1) + " RoleID :" + rs.getString(2) + " LoginFlag :" + rs.getString(3));
                }
            } catch (Throwable e) {
                out.println(e.getMessage());
            } finally {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            }
        }

        if (schoolID == null) {
            schoolID = "";
        }

    %>


    <body >
        <input type="hidden" name="IPAddress" id="IPAddress" value="<%=request.getRemoteAddr()%>" ></input>
        <fieldset>
            <legend style="font-size: 25px;">Login Details For <%=schoolID%></legend>

            <center>

                <h5 style="font-weight: bold;text-align: center;color: #ffffff;background: #005be0;padding: 7px;margin: 10px;box-shadow: 10px 7px 10px #D4D4D4;text-transform: uppercase; margin-bottom: 30px;"><%=schoolID%> Login Details </h5>
                <br>

                <form name="myform" onsubmit="return validateForm();" method="POST">

                    <table border="2" ali align="center;">

                        <tr>
                            <td>User ID*</td>
                            <td><input type="test" name="schoolCode" /></td>


                        </tr>
                        <tr>
                            <td>Academic Year *</td>
                            <td><select name="acyear">
                                    <option value="2018-19">2021-22</option>
                             </select>
                            </td>


                        </tr>

                        <tr >
                            <td colspan="2" style="text-align: center;">
                                <input type="submit" name="submit" value="Submit"/>
                            </td>
                        </tr>


                    </table>
                    <br>
                    <tbody>
                        <%
                            if (request.getParameter("submit") != null) {
                                if (resultBuffer.length() > 0) {
                                    out.println(resultBuffer.toString() + "<BR>");
                                    resultBuffer = null;
                                } else {
                                    out.println("NOT FOUND  <BR>");
                                }

                            }
                        %>
                    </tbody>
                </form>

            </center>
        </legend>
</body>
</html>