<%-- 
    Document   : 500
    Created on : Nov 23, 2018, 1:31:02 PM
    Author     : 1451421
--%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>::SBTET::</title>
        <style>
            .demo{
                width: 50%;
                margin: auto;
                padding: 40px 20px;
                background-color: #84d7f1;
                border: 1px solid #0fb8ec;
                margin-top: 15%;
                border-radius: 5px;
                box-shadow: 0px 0px 25px #5ca9ea;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="demo"> 
            <h1>
                "Ooops....Something went wrong" <br><br>
                Please Re-try after some time : <a href="<%=basePath%>" title="Click here">SBTET</a>
            </h1>
        </div>
    </body>
</html>
