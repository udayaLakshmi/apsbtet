<%-- 
    Document   : Welcome
    Created on : 22 Oct, 2013, 2:55:30 PM
    Author     : 484898
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<% int i = 1;%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
    <head>
        <title>:: SBTET ::</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Pragma" content="no-cache"/>
        <META HTTP-EQUIV="Expires" CONTENT="-1">
        <script src="js/custom1.js" type="text/javascript"></script>
        <script src="js/jscounter1.js" type="text/javascript"></script>
        <link rel="stylesheet" href="css/dashboard_style.css" type="text/css">
        <link rel="stylesheet" href="css/dashboard.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css" type="text/css">

        <style type="text/css">
            .DB{
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#000000;
                border: 1px solid #ccc;
                border-collapse: collapse;
            }


            .DB th {    
                height:25px;   
                font-size:14px;
                text-align:center;
                color:#005aa7;
                font-weight:bold;
                border: 1px solid #ccc;
                padding:5px;  
                background-color:#CEECF5;

            }


            .DB td {
                height:25px;
                font-size:12px;
                text-align:center;
                border: 1px solid #ccc;
                color:#464646;
                background-color: #F8FDFE;
                font-weight:normal;
            }	
            .SUBDB td {
                height:22px;
                font-size:12px;
                text-align:left;
                color:#464646;
                font-weight:normal;
                border:1px solid #ccc;
            }
            .SUBDBB td {
                height:25px;
                font-size:12px;
                text-align:center;
                font-weight:normal;
            }


            .blink_me {
                -webkit-animation-name: blinker;
                -webkit-animation-duration: 1s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-iteration-count: infinite;

                -moz-animation-name: blinker;
                -moz-animation-duration: 1s;
                -moz-animation-timing-function: linear;
                -moz-animation-iteration-count: infinite;

                animation-name: blinker;
                animation-duration: 1s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;

                font-weight: bold;
            }

            @-moz-keyframes blinker {  
                0% { color: #2980b9; }
            50% { color: #0E94C5; }
            100% { color: #e67e22; }
            }

            @-webkit-keyframes blinker {  
                0% { color: #2980b9; }
            50% { color: #0E94C5; }
            100% { color: #e67e22; }
            }

            @keyframes blinker {  
                0% { color: #2980b9; }
            50% { color: #0E94C5; }
            100% { color: #e67e22; }
            }


            .row.footerrow {
                position: absolute;
                border: 0;
            }

        </style>

        <link rel="stylesheet" href="css/DSDIGI.css" type="text/css">
        <script type="text/javascript">
            function  report2(flag, distId) {
                $('#backButtonSpan').hide();
                var roleId = $('#roleId').val();
                var modal = document.getElementById('myModal');
                modal.style.display = "none";
                var paraData = "flag=" + flag + "&distId=" + distId;
                modal.style.display = "block";
                $.ajax({
                    type: "POST",
                    url: "<%=basePath%>dashboardReport.do?mode=getMandalAjax",
                    data: paraData,
                    success: function(response) {
                        modal.style.display = "block";
                        var title = headingTitle(flag);
                        $('#headerId').empty().append(title);
                        $(".divcls").empty().append("<div>" + response + "</div>");
                    },
                    error: function(e) {
                        alert('Error: ' + e);
                    }
                });

                return false;
            }
            $(document).ready(function() {
                $('#backButtonSpan').hide();
                $('.close1').click(function() {
                    var modal = document.getElementById('myModal');
                    modal.style.display = "none";
                });
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                    });
                });
            });
        </script>

    </head>
    <body onload="getcount();">
        <!--<body>-->

    <html:form action="/home">
        <html:hidden property="mode"/>

        <div class="container" style="min-height: 550px !important;">


            <div style="border: 2px solid #30a6cc; min-height: 500px;  margin: 20px 0px; box-shadow: 5px 5px 5px #ddd;   border-radius: 5px;">





                <div class="row">



                    <div style="vertical-align: top;padding: 10px; padding-top: 0px">
                        <div style="width:95%; float: left; text-align: right; margin: 10px 20px;">
                            <h5 style="color:#FF6508; font-weight: bold; float:right; margin: 0px;">
                                Login ID : 
                                <span style="color:green; font-weight: bold; font-style: italic;">
                                    ${userName}
                                </span> 
                            </h5>
                            <br/>
                            <br/>
                            <% if (session.getAttribute("RoleId").equals("1")) {%>
                            <h5 style="color:#FF6508; font-weight: bold; float:right; margin: 0px;">
                                <a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=institutes.pdf" target="_blank">UserManual
                                </a>

                            </h5>
                            <%}%>
                        </div>

                    </div>
                    <div class="clearfix"></div><div class="clearfix"></div><br>

                    <% if (session.getAttribute("RoleId").equals("2")
                                || session.getAttribute("RoleId").equals("5")
                                || session.getAttribute("RoleId").equals("6")
                                || session.getAttribute("RoleId").equals("7")
                                || session.getAttribute("RoleId").equals("8")
                                || session.getAttribute("RoleId").equals("9")
                                || session.getAttribute("RoleId").equals("10")
                                || session.getAttribute("RoleId").equals("11")) {%>

                    <h3 class="h3_text">DIPLOMA</h3>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                    Total Applications 
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter17" style="font-size: 48px;color: white;"></span>
                                </div>
                                <a href="./diplomadistrictWiseReport.do" class="circle-tile-footer">Click Here<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading green">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded">
                                    IV SEMESTER
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter18" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading yellow">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content yellow">
                                <div class="circle-tile-description text-faded">
                                    V SEMESTER 
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter19" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    Supplementary (All Sems) 
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter20" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading yellow">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content yellow">
                                <div class="circle-tile-description text-faded">
                                    ER91
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter21" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-gray">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-gray">
                                <div class="circle-tile-description text-faded">
                                    Hall Ticket Downloaded
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter23" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                    NR Data
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter22" style="font-size: 48px;color: white;"> </span>
                                </div>
                               <a href="./nrDiploma.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading red">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content red">
                                <div class="circle-tile-description text-faded">
                                    Present <span class="diplomaExamDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter24" style="font-size: 48px;color: white;"> </span>
                                </div>
                               <!--<a href="./presentDiploma.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>-->
                               <a href="#" class="circle-tile-footer">More Info<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    Absentees <span class="diplomaExamDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter25" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./absentDiploma.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content purple">
                                <div class="circle-tile-description text-faded">
                                    Malpractice <span class="diplomaExamDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter26" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./malpraticeDiploma.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-gray">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-gray">
                                <div class="circle-tile-description text-faded">
                                    Buffer OMRs <span class="diplomaExamDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter27" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./bufferDiploma.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>

                <div class="row">
                    <h3 class="h3_text">CCIC</h3>

                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                    No. of Candidates Submitted
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter5" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./dashboardReportCCIC.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading green">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded">
                                    No. of Candidates Payment Made
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter6" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading orange">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">
                                    No. of Applications Verified
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter9" style="font-size: 48px;color: white;"></span>
                                </div>
                                <a href="./verificationReportCCIC.do" class="circle-tile-footer">Click Here<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    No. of Applications Verification Pending
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital"  id="counter10" style="font-size: 48px;color: white;"> </span>

                                </div>
                                <a href="./verificationReportCCIC.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading yellow">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content yellow">
                                <div class="circle-tile-description text-faded">
                                    Hall Ticket Downloaded
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter16" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info<i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    NR Data
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter15" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./ccicNRData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading red">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content red">
                                <div class="circle-tile-description text-faded">
                                    Present <span class="ccicExamDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter13" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./ccicPresentData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                    Absentees <span class="ccicExamDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter11" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./ccicAbsentData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content purple">
                                <div class="circle-tile-description text-faded">
                                    Malpractice <span class="ccicExamDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter12" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./ccicMalpracticeData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-gray">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-gray">
                                <div class="circle-tile-description text-faded">
                                    Buffer OMRs (SGs)<span class="ccicExamDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter14" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./ccicBufferOMRData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    
                    <h3 class="h3_text">TWSH</h3>
                    
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                    NR Data
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter1" style="font-size: 48px;color: white;"></span>
                                </div>
                                <p>  <a href="./nrReport.do" class="circle-tile-footer" style="float:left;">Click Here (TW) <i class="fa fa-chevron-circle-right"></i></a>
                                    <a href="./nrSHReport.do" class="circle-tile-footer">Click Here (SH) <i class="fa fa-chevron-circle-right"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading green">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded">
                                    Hall Ticket Downloaded
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter2" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading orange">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">
                                    Present <span class="examDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter7" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./presentData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    Absentees <span class="examDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter3" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./absentData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content purple">
                                <div class="circle-tile-description text-faded">
                                    Malpractice <span class="examDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter4" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./malpracticeData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading red">
                                    <i class="fa fa-edit fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content red">
                                <div class="circle-tile-description text-faded">
                                    Buffer OMRs (SGs)<span class="examDate" style="font-size: 13px;color: white;"> </span>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span class="info-box-number digital" id="counter8" style="font-size: 48px;color: white;"> </span>
                                </div>
                                <a href="./bufferOMRData.do" class="circle-tile-footer">Click Here <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>



                <% } else {%>
            </div>
            <% }%>
            <% if (session.getAttribute("RoleId").equals("1")) {%>
            <br/><br/><br/>
            <center><h3 style="color:brown;font-weight: bold">Welcome to ${sessionScope.institute}</h3></center>
                <%}%>

        </div>
    </div>
</html:form>
</body>
</html>