<%-- 
    Document   : Welcome
    Created on : 22 Oct, 2013, 2:55:30 PM
    Author     : 484898
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<% int i = 1;%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
    <head>
        <title>:: SBTET ::</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Pragma" content="no-cache"/>
        <META HTTP-EQUIV="Expires" CONTENT="-1">
        <script src="js/custom1.js" type="text/javascript"></script>
        <script src="js/jscounter1.js" type="text/javascript"></script>

        <style type="text/css">
            .DB{
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#000000;
                border: 1px solid #ccc;
                border-collapse: collapse;
            }


            .DB th {    
                height:25px;   
                font-size:14px;
                text-align:center;
                color:#005aa7;
                font-weight:bold;
                border: 1px solid #ccc;
                padding:5px;  
                background-color:#CEECF5;

            }


            .DB td {
                height:25px;
                font-size:12px;
                text-align:center;
                border: 1px solid #ccc;
                color:#464646;
                background-color: #F8FDFE;
                font-weight:normal;
            }	
            .SUBDB td {
                height:22px;
                font-size:12px;
                text-align:left;
                color:#464646;
                font-weight:normal;
                border:1px solid #ccc;
            }
            .SUBDBB td {
                height:25px;
                font-size:12px;
                text-align:center;
                font-weight:normal;
            }


            .blink_me {
                -webkit-animation-name: blinker;
                -webkit-animation-duration: 1s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-iteration-count: infinite;

                -moz-animation-name: blinker;
                -moz-animation-duration: 1s;
                -moz-animation-timing-function: linear;
                -moz-animation-iteration-count: infinite;

                animation-name: blinker;
                animation-duration: 1s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;

                font-weight: bold;
            }

            @-moz-keyframes blinker {  
                0% { color: #2980b9; }
            50% { color: #0E94C5; }
            100% { color: #e67e22; }
            }

            @-webkit-keyframes blinker {  
                0% { color: #2980b9; }
            50% { color: #0E94C5; }
            100% { color: #e67e22; }
            }

            @keyframes blinker {  
                0% { color: #2980b9; }
            50% { color: #0E94C5; }
            100% { color: #e67e22; }
            }


            .row.footerrow {
                position: absolute;
                border: 0;
            }

        </style>
        <link rel="stylesheet" href="css/DSDIGI.css" type="text/css">
        <script type="text/javascript">
            function  report2(flag, distId) {
                $('#backButtonSpan').hide();
                var roleId = $('#roleId').val();
                var modal = document.getElementById('myModal');
                modal.style.display = "none";
                var paraData = "flag=" + flag + "&distId=" + distId;
                modal.style.display = "block";
                $.ajax({
                    type: "POST",
                    url: "<%=basePath%>dashboardReport.do?mode=getMandalAjax",
                    data: paraData,
                    success: function(response) {
                        modal.style.display = "block";
                        var title = headingTitle(flag);
                        $('#headerId').empty().append(title);
                        $(".divcls").empty().append("<div>" + response + "</div>");
                    },
                    error: function(e) {
                        alert('Error: ' + e);
                    }
                });

                return false;
            }
            $(document).ready(function() {
                $('#backButtonSpan').hide();
                $('.close1').click(function() {
                    var modal = document.getElementById('myModal');
                    modal.style.display = "none";
                });
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                    });
                });
            });
        </script>

    </head>
    <body onload="getcount();">
        <!--<body>-->

    <html:form action="/home">
        <html:hidden property="mode"/>

        <div class="container" style="min-height: 550px !important;">

            <div class="row" style="margin: 0;
                 margin-top: 10px;
                 margin-bottom: 10px;">

                <div class="main_div" style="background-color:#fff;min-height:450px;box-shadow: 0 0 0px #ccc;">

                    <div style="vertical-align: top;padding: 10px; padding-top: 0px">
                        <div style="width:95%; float: left; text-align: right; margin: 10px 20px;">
                            <h5 style="color:#FF6508; font-weight: bold; float:right; margin: 0px;">
                                Login ID : 
                                <span style="color:green; font-weight: bold; font-style: italic;">
                                    ${userName}
                                </span> 
                            </h5>
                                <br/>
                                <br/>
                                 <% if (session.getAttribute("RoleId").equals("1")){%>
                                 <h5 style="color:#FF6508; font-weight: bold; float:right; margin: 0px;">
                                   <a href="./adimissionpdfs.do?mode=downloadPDFFile&filename=institutes.pdf" target="_blank">UserManual
                                            </a>
                                        
                            </h5>
                               <%}%>
                        </div>

                    </div><div class="clearfix"></div><div class="clearfix"></div><br>

                    <style>
                        .boxn1 {
                            background: #1e76dc;  
                        }
                        .boxn1 a:hover, a:focus{
                            color: #000 !important;
                            text-decoration: none !important;
                        }

                        .card h4{
                            font-size:16px;
                        }         
                        .card {
                            padding: 20px 12px;
                            min-height: 180px;
                        }
                        @media (min-width: 992px){
                            .col-md-4 {

                                padding: 0px 10px 0px 0px;
                                margin-bottom: 10px;
                            }
                        }
                        hr.myhr {
                            margin-top: 15px;
                            margin-bottom: 5px;
                            border: 0;
                            border-top: 2px solid #fff;
                        }
                        .card h4, .card h2, .card a {
                            color: #fff;
                            margin: 10px 0;
                        }               
                        i.fa.fa-edit.fa-4x {
                            font-size: 40px;
                            background: #fff;
                            margin-top: 15px;
                            padding: 20px;
                            text-align: center;
                            color: #1367c8;
                            border-radius: 50%;
                            vertical-align: middle;
                        }    
                    </style>  
                    <% if (session.getAttribute("RoleId").equals("2")||
                            session.getAttribute("RoleId").equals("5")||
                            session.getAttribute("RoleId").equals("6")||
                            session.getAttribute("RoleId").equals("7")||
                            session.getAttribute("RoleId").equals("8")||
                            session.getAttribute("RoleId").equals("9")||
                            session.getAttribute("RoleId").equals("10")||
                            session.getAttribute("RoleId").equals("11")
                            
        ) {%>
                <div class="container">

                       <div class="row">

                            <h3 style="text-align: left;color: #5361ff;font-weight: bold;margin-left: 20px;">TWSH</h3>

                            <div class="col-md-4">

                                <div class="card" style="background: linear-gradient(87deg,#5e72e4 0,#825ee4 100%)!important;border-radius: 10px;">

                                    <div class="row">

                                        <div class="col-md-8">
                                            <span class="info-box-number digital" id="counter1" style="font-size: 48px;color: white;"> </span><br><br>
                                            <p style="color: #fff;font-weight: bold;">No. of Candidates Submitted</p>  

                                        </div>
                                        <div class="col-md-4"><i class="fa fa-edit" style="
                                                                 font-size: 40px;
                                                                 background: #ffffff;
                                                                 padding: 15px;
                                                                 color: #815fe4;
                                                                 border-radius: 10px;
                                                                 border: 0px solid #5f71e4;

                                                                 "></i></div>
                                    </div>

                                    <div class="hline" style="
                                         border-top: 1px dashed  #fff;
                                         margin-top: 8px;
                                         margin-bottom: 15px;
                                         "></div>

                                    <a  href="./dashboardReport.do"  style="
                                        display: block;
                                        text-align: center;
                                        font-size: 18px;
                                        font-weight: bold;
                                        margin-bottom: 0;
                                        height:10px;
                                        ">Click Here</a> 
                                        
                                        

                                </div>

                            </div> 

                            <div class="col-md-4">

                                <div class="card" style="background: linear-gradient(87deg,#f5365c 0,#f56036 100%)!important;border-radius: 10px;">

                                    <div class="row">

                                        <div class="col-md-8">
                                            <span class="info-box-number digital" id="counter2" style="font-size: 48px;color: white;"> </span><br><br>
                                            <p style="
                                               color: #fff;
                                               font-weight: bold;
                                               ">No. of Candidates Payment Made</p>  

                                        </div>
                                        <div class="col-md-4"><i class="fa fa-edit" style="
                                                                 font-size: 40px;
                                                                 background: #ffffff;
                                                                 padding: 15px;
                                                                 color: #f55e37;
                                                                 border-radius: 10px;
                                                                 border: 0px solid #f55f37;
                                                                 "></i></div>
                                    </div>

                                    <div class="hline" style="
                                         border-top: 1px dashed  #fff;
                                         margin-top: 8px;
                                         margin-bottom: 15px;
                                         "></div>
                                </div>
                            </div> 

                            <div class="col-md-4">

                                <div class="card" style="background: linear-gradient(87deg,#0742a7 0,#19164a 100%)!important;border-radius: 10px;">

                                    <div class="row">

                                        <div class="col-md-8">
                                            <span class="info-box-number digital" id="counter3" style="font-size: 48px;color: white;"> </span><br><br>
                                            <p style="
                                               color: #fff;
                                               font-weight: bold;
                                               ">No. of Applications Verified</p>  

                                        </div>
                                        <div class="col-md-4"><i class="fa fa-edit" style="
                                                                 font-size: 40px;
                                                                 background: #ffffff;
                                                                 padding: 15px;
                                                                 color: #1a184d;
                                                                 border-radius: 10px;
                                                                 border: 0px solid #172a4d;
                                                                 "></i></div>
                                    </div>

                                    <div class="hline" style="
                                         border-top: 1px dashed  #fff;
                                         margin-top: 8px;
                                         margin-bottom: 15px;
                                         "></div>
                                </div>
                            </div> 
                            
                             <div class="col-md-4">

                                <div class="card" style="background: linear-gradient(87deg,#FF9800 0,#717d05 100%)!important;border-radius: 10px;">

                                    <div class="row">

                                        <div class="col-md-8">
                                            <span class="info-box-number digital" id="counter4" style="font-size: 48px;color: white;"> </span><br><br>
                                            <p style="
                                               color: #fff;
                                               font-weight: bold;
                                               ">No. of Applications Verification Pending</p>  

                                        </div>
                                        <div class="col-md-4"><i class="fa fa-edit" style="
                                                                 font-size: 40px;
                                                                 background: #ffffff;
                                                                 padding: 15px;
                                                                 color: #1a184d;
                                                                 border-radius: 10px;
                                                                 border: 0px solid #172a4d;
                                                                 "></i></div>
                                    </div>

                                    <div class="hline" style="
                                         border-top: 1px dashed  #fff;
                                         margin-top: 8px;
                                         margin-bottom: 15px;
                                         "></div>
                                </div>
                            </div> 

                         <!--   <div class="col-md-3">

                                <div class="card" style="background: linear-gradient(87deg,#FF9800 0,#717d05 100%)!important;border-radius: 10px;">

                                    <div class="row">

                                        <div class="col-md-8">
                                            <span class="info-box-number digital" id="counter4" style="font-size: 48px;color: white;"> </span><br><br>
                                            <p style="
                                               color: #fff;
                                               font-weight: bold;
                                               ">No. of Candidates Exam Attempts</p>  

                                        </div>
                                        <div class="col-md-4"><i class="fa fa-edit" style="
                                                                 font-size: 40px;
                                                                 background: #ffffff;
                                                                 padding: 15px;
                                                                 color: #f55e37;
                                                                 border-radius: 10px;
                                                                 border: 0px solid #f55f37;
                                                                 "></i></div>
                                    </div>

                                    <div class="hline" style="
                                         border-top: 1px dashed  #fff;
                                         margin-top: 8px;
                                         margin-bottom: 15px;
                                         "></div>
                                </div>
                            </div> -->

                      
                       </div>
                    <div class="row">
                            <h3 style="text-align: left;color: #5361ff;font-weight: bold;margin-left: 20px;">CCIC</h3>
                            <div class="col-md-4">
                                <div class="card" style="background: linear-gradient(87deg,#00ff00 0,#b3ffb3 100%)!important;border-radius: 10px;">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <span class="info-box-number digital" id="counter5" style="font-size: 48px;color: white;"> </span><br><br>
                                            <p style="color: #fff;font-weight: bold;">No. of Candidates Submitted</p>  
                                        </div>
                                        <div class="col-md-4"><i class="fa fa-edit" style="
                                                                 font-size: 40px;
                                                                 background: #ffffff;
                                                                 padding: 15px;
                                                                 color: #815fe4;
                                                                 border-radius: 10px;
                                                                 border: 0px solid #5f71e4;
                                                                 "></i></div>
                                    </div>
                                    <div class="hline" style="
                                         border-top: 1px dashed  #fff;
                                         margin-top: 8px;
                                         margin-bottom: 15px;
                                         "></div>
                                    <a  href="./dashboardReportCCIC.do"  style="
                                        display: block;
                                        text-align: center;
                                        font-size: 18px;
                                        font-weight: bold;
                                        margin-bottom: 0;
                                        height:10px;
                                        ">Click Here</a> 
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="card" style="background: linear-gradient(87deg,#ff8080 0,#f2ccff 100%)!important;border-radius: 10px;">

                                    <div class="row">

                                        <div class="col-md-8">
                                            <span class="info-box-number digital" id="counter6" style="font-size: 48px;color: white;"> </span><br><br>
                                            <p style="
                                               color: #fff;
                                               font-weight: bold;
                                               ">No. of Candidates Payment Made</p>  

                                        </div>
                                        <div class="col-md-4"><i class="fa fa-edit" style="
                                                                 font-size: 40px;
                                                                 background: #ffffff;
                                                                 padding: 15px;
                                                                 color: #f55e37;
                                                                 border-radius: 10px;
                                                                 border: 0px solid #f55f37;
                                                                 "></i></div>
                                    </div>

                                    <div class="hline" style="
                                         border-top: 1px dashed  #fff;
                                         margin-top: 8px;
                                         margin-bottom: 15px;
                                         "></div>
                                </div>
                            </div> 
                    </div>
                    </div> 
                    <% }%>
                    <% if(session.getAttribute("RoleId").equals("1")){ %>
                     <br/><br/><br/>
                      <center><h3 style="color:brown;font-weight: bold">Welcome to ${sessionScope.institute}</h3></center>
                      
                      <%}%>
                </div>
            </div>
        </div>

    </html:form>
</body>
</html>









