<%-- 
    Document   : PaymentReconsilationReport
    Created on : Jul 12, 2021, 7:09:24 PM
    Author     : 1820530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>

        <%
            int i = 0;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  


        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">


        <script type="text/javascript">

            function space(evt, thisvalue) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (evt.keyCode == 32) {
                    return false;
                } else
                    return true;

            }
            function onlyNumbers(id, evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;

                if (charCode == "46") {  // for dot allowed
                    var textvalue = $("#" + id).val();
                    if (textvalue.indexOf(".") > -1) {
                        alert("Not allowed dot more than one time");
                        return false;
                    } else
                        return true;
                }
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function onlyNumbers1(totalColWorkingDaysId, evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode == "46") {  // for dot allowed
                    var textvalue = $("#" + totalColWorkingDaysId).val();
                    if (textvalue.indexOf(".") > -1) {
                        alert("Not allowed dot more than one time");
                        $("#" + totalColWorkingDaysId).focus();
                        return false;
                    } else
                        return true;
                }
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function validatingColTotalWorkingDays(totalColWorkingDaysId, periodId) {
                $('.DataDiv').hide();
                var workingIdvalue = $('#' + totalColWorkingDaysId).val();
                var period = $('#' + periodId).val();
                if (period == "1") {
                    if (parseFloat(workingIdvalue) > 15) {
                        alert("Total College Working Days should not be greater than 15 days");
                        $('#' + totalColWorkingDaysId).val("");
                        $('#' + totalColWorkingDaysId).focus();
                        return false;
                    } else
                        return true;
                } else if (period == "2") {
                    if (parseFloat(workingIdvalue) > 16) {
                        alert("Total College Working Days should not be greater than 16 days");
                        $('#' + totalColWorkingDaysId).val("");
                        $('#' + totalColWorkingDaysId).focus();
                        return false;
                    } else
                        return true;
                } else
                    return false;
            }

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                    "paging": false,
                    "ordering": false

                });

                $("form").attr('autocomplete', 'off');
                $('input, :input').attr('autocomplete', 'off');
                $('input[type=text]').attr('autocomplete', 'off');
                $('input[type=text], textarea').bind("cut copy paste", function(e) {
                    alert("Cut copy paste not allowed here");
                    e.preventDefault();
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input btn-primary{
                width: 8% !important;
                padding: 3px !important; 
            }

            input[type="text"] {
                width: 48% !important;
                height: 20px !important;
                /*padding: 11px !important;*/
            }

            #subjectCode {
                width: 37% !important;
                height: 37px !important;
            }

            #grade {
                width: 100% !important;
                height: 34px !important;
                /*background: white;*/
            }



            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>

        <Script>

            function validFields() {
                if ($("#grade").val() === undefined || $("#grade").val() === "" || $("#grade").val() === "0") {
                    alert("Please Select Year/SEM");
                    $("#grade").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#batchCode").val() === undefined || $("#batchCode").val() === "" || $("#batchCode").val() === "0") {
                    alert("Please Select Branch");
                    $("#batchCode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#edate").val() === undefined || $("#edate").val() === "" || $("#edate").val() === "0") {
                    alert("Please Select Month");
                    $("#edate").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#epaper").val() === undefined || $("#epaper").val() === "" || $("#epaper").val() === "0") {
                    alert("Please Select Period");
                    $("#epaper").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#subjectCode").val() === undefined || $("#subjectCode").val() === "") {//|| $("#subjectCode").val() === "0") {
                    alert("Please Enter Total College Working Days");
                    $("#subjectCode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    document.forms[0].mode.value = "getCentrBasicData";
                    document.forms[0].submit();
                }
            }

            function check(workingid, attendedid, workingdays, attended, flag) {
                var workingIdvalue = $('#' + workingid).val();
                var period = $('#epaper').val();
                if (period == "1") {
                    if (parseFloat(workingIdvalue) > 15) {
                        alert("Working Days should not be greater than 15 days");
                        $('#' + workingid).val(workingdays);
                        $('#' + workingid).focus();
                        return false;
                    } else
                        return true;
                } else if (period == "2") {
                    if (parseFloat(workingIdvalue) > 16) {
                        alert("Working Days should not be greater than 16 days");
                        $('#' + workingid).val(workingdays);
                        $('#' + workingid).focus();
                        return false;
                    } else
                        return true;
                } else
                    return false;
            }

            function upTOTen(workingid, attendedid, workingdays, attended, flag) {

                var workingIdvalue = $('#' + workingid).val();
                var attendedIdvalue = $('#' + attendedid).val();

//                alert("flag-1 :  " + flag + " Working " + workingIdvalue + " attendedIdvalue " + attendedIdvalue);

                if (check(workingid, attendedid, workingdays, attended, flag) == true) {

//                     alert("flag-2 : " + flag + " Working " + workingIdvalue + " attended " + attendedIdvalue);

                    if (flag == "1") {
                        if (parseFloat(workingIdvalue) < parseFloat(attendedIdvalue)) {
                            alert("Working Days should be greater than or equal to No.Of Days Attended");
                            $('#' + workingid).val(workingdays);
                            $('#' + workingid).focus();
                            return false;
                        }
                    } else if (flag == "2") {
                        if (parseFloat(workingIdvalue) < parseFloat(attendedIdvalue)) {
                            alert("No.Of Days Attended should be less than or equal to Working Days");
                            $('#' + attendedid).val(attended);
                            $('#' + attendedid).focus();
                            return false;
                        }
                    }
                }
            }

            function saveData(status) {

                var pinArr = [];
                var nameArr = [];
                var schemeArr = [];

                var shiftArr = [];
                var sectionArr = [];

                var workingDaysArr = [];
                var attendedArr = [];


                if ($("#grade").val() === undefined || $("#grade").val() === "" || $("#grade").val() === "0") {
                    alert("Please Select Year/SEM");
                    $("#grade").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#batchCode").val() === undefined || $("#batchCode").val() === "" || $("#batchCode").val() === "0") {
                    alert("Please Select Branch");
                    $("#batchCode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#edate").val() === undefined || $("#edate").val() === "" || $("#edate").val() === "0") {
                    alert("Please Select Month");
                    $("#edate").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#epaper").val() === undefined || $("#epaper").val() === "" || $("#epaper").val() === "0") {
                    alert("Please Select Period");
                    $("#epaper").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#subjectCode").val() === undefined || $("#subjectCode").val() === "") {
                    alert("Please Enter Total College Working Days");
                    $("#subjectCode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {

                    var inputElements = document.getElementsByClassName('vcCheckBox');
                    for (var i = 0; i < inputElements.length; i++) {
                        var workingDaysEnableStatus = document.forms[0].elements["wdaysStatus" + inputElements [i].value].value;
                        if (workingDaysEnableStatus == "Y") {
                            if (document.forms[0].elements["instcode" + inputElements [i].value].value === "") {
                                alert("Please Enter Working Days");
                                document.forms[0].elements["instcode" + inputElements [i].value].focus();
                                return false;
                            }
                        }
                        if (document.forms[0].elements["barcode" + inputElements [i].value].value === "") {
                            alert("Please Enter No.Of Days Attended");
                            document.forms[0].elements["barcode" + inputElements [i].value].focus();
                            return false;
                        }

                        var pin = document.forms[0].elements["regno" + inputElements[i].value].value;
//                        console.log("pin " + pin);
                        var name = document.forms[0].elements["sname" + inputElements[i].value].value;
//                        console.log("name " + name);
                        var scheme = document.forms[0].elements["sscflag" + inputElements[i].value].value;
//                        console.log("scheme " + scheme);

                        var section = document.forms[0].elements["ncc" + inputElements[i].value].value;
//                        console.log("section " + section);

                        var shift = document.forms[0].elements["sports" + inputElements[i].value].value;
//                        console.log("shift " + shift);


                        var workingDays = document.forms[0].elements["instcode" + inputElements[i].value].value;
//                        console.log("workingDays " + workingDays);
                        var attended = document.forms[0].elements["barcode" + inputElements[i].value].value;
//                        console.log("attended " + attended);


                        if (parseFloat(workingDays) < parseFloat(attended)) {
                            alert("No.Of Days Attended should be less than or equal to Working Days");
                            document.forms[0].elements["barcode" + inputElements[i].value].focus();
                            return false;
                        }

                        pinArr = pinArr + "~" + pin;
                        nameArr = nameArr + "~" + name;
                        schemeArr = schemeArr + "~" + scheme;

                        shiftArr = shiftArr + "~" + shift;
                        sectionArr = sectionArr + "~" + section;


                        workingDaysArr = workingDaysArr + "~" + workingDays;
                        attendedArr = attendedArr + "~" + attended;

                    }

                    document.forms[0].sschallticket.value = pinArr;
                    document.forms[0].bname.value = nameArr;
                    document.forms[0].occupation.value = schemeArr;
                    document.forms[0].maxMarks.value = workingDaysArr;
                    document.forms[0].marks.value = shiftArr;

                    document.forms[0].maskmobile.value = sectionArr;
                    document.forms[0].attendance.value = attendedArr;

                    document.forms[0].vstatus.value = status;
                    document.forms[0].mode.value = "getSubmitData";
                    if (status == "1") {
                        document.forms[0].submit();
                    } else {
                        if (confirm("Once You Confirm Edit Will Be Disabled!") == true) {
                            document.forms[0].submit();
                        } else
                            return false;
                    }


                }
            }
        </Script>

        <script>
            $(function() {
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
            });

            function hideData() {
                $('.DataDiv').hide();
            }

            function getBranchList(courseId) {
                var data = "courseId=" + $('#' + courseId).val();
                var yearOrSem = $('#grade').val();
                if (yearOrSem !== "0") {
                    $.ajax({
                        type: "POST",
                        url: "./attendanceCTwentyEntry.do?mode=getBranchList&courseId=" + $('#' + courseId).val(),
                        data: data,
                        cache: true,
                        contentType: true,
                        processData: true,
                        success: function(response) {
                            if (response.trim() == "0") {
                                alert("No Subject available with given Registration No");
                                $("#batchCode").empty().append("<option value='0'>--Select--</option>");
                            } else {
                                $("#batchCode").empty().append(response);
                            }

                        },
                        error: function(e) {
                        }
                    });
                }
                if (yearOrSem == "0") {
                    $("#batchCode").empty().append("<option value='0'>--Select--</option>");
                }
                $("#epaper").empty().append("<option value='0'>--Select--</option>");
                $("#edate").empty().append("<option value='0'>--Select--</option>");
                $("#subjectCode").val("0");
            }

            function getSubjectList(courseId) {
                var data = "courseId=" + $('#' + courseId).val() + "&branchC=" + $('#batchCode').val();
                $.ajax({
                    type: "POST",
                    url: "./attendanceCTwentyEntry.do?mode=getSubject&courseId=" + $('#' + courseId).val() + "&branchC=" + $('#batchCode').val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        if (response.trim() == "0") {
                            alert("No Subject available with given Registration No");
                            $("#subjectCode").empty().append("<option value='0'>--Select--</option>");
                        } else {
                            $("#subjectCode").empty().append(response);
                        }

                    },
                    error: function(e) {
                    }
                });

                $("#subjectCode").val("0");
                $("#epaper").empty().append("<option value='0'>--Select--</option>");
                $("#edate").empty().append("<option value='0'>--Select--</option>");
            }



            function getExamDates() {
                if ($('#batchCode').val() === "0") {
                    $("#epaper").empty().append("<option value='0'>--Select--</option>");
                    $("#edate").empty().append("<option value='0'>--Select--</option>");
                    $("#subjectCode").val("0");
                } else {
                    $("#subjectCode").val("0");
                    $("#epaper").empty().append("<option value='0'>--Select--</option>");
                    var data = "courseId=" + $('#grade').val() + "&branchCode=" + $('#batchCode').val();
                    $.ajax({
                        type: "POST",
                        url: "./attendanceCTwentyEntry.do?mode=getExamDates&courseId=" + $('#grade').val() + "&branchCode=" + $('#batchCode').val(),
                        data: data,
                        cache: true,
                        contentType: true,
                        processData: true,
                        success: function(response) {
                            if (response.trim() == "0") {
                                alert("No exam  date available");
                                $("#edate").empty().append("<option value='0'>--Select--</option>");
                            } else {
                                $("#edate").empty().append(response);
                            }

                        },
                        error: function(e) {
                        }
                    });
                }
            }

            function getPeriod() {
                $("#subjectCode").val("0");
                if ($('#edate').val() === "0") {
                    $("#epaper").empty().append("<option value='0'>--Select--</option>");
                } else {
                    var data = "courseId=" + $('#grade').val() + "&branchCode=" + $('#batchCode').val() + "&edate=" + $('#edate').val();
                    $.ajax({
                        type: "POST",
                        url: "./attendanceCTwentyEntry.do?mode=getPeriod&subCode=" + $('#subjectCode').val() + "&courseId=" + $('#grade').val() + "&branchCode=" + $('#batchCode').val() + "&edate=" + $('#edate').val(),
                        data: data,
                        cache: true,
                        contentType: true,
                        processData: true,
                        success: function(response) {
                            if (response.trim() == "0") {
                                alert("No exam  date available");
                                $("#epaper").empty().append("<option value='0'>--Select--</option>");
                            } else {
                                $("#epaper").empty().append(response);
                            }

                        },
                        error: function(e) {
                        }
                    });
                }
            }

            function clearTotalColWorkingDays() {
                $("#subjectCode").val("0");
            }

        </script>

    </head>
    <body>
        <html:form action="/attendanceCTwentyEntry">

            <html:hidden property="mode"/>  

            <html:hidden property="sschallticket"/>
            <html:hidden property="bname"/>
            <html:hidden property="occupation"/>
            <html:hidden property="maxMarks"/>
            <html:hidden property="marks"/>

            <html:hidden property="maskmobile"/>
            <html:hidden property="attendance"/> 
            <html:hidden property="vstatus"/>

            <div class="row mainbodyrow">
                <div class="container" style="padding-top: 15px;">
                    <div class="col-xs-12">
                        <div class="maindodycnt">
                            <h3 class="block-head-News">C20 Attendance Entry 
                                <logic:present name="result">
                                    <span id="msg"><center><font color="green" style="font-size: 15px;">${result}</font></center></span>
                                        </logic:present>
                                        <logic:present name="result1">
                                    <span id="msg"><center><font color="red" style="font-size: 15px;">${result}</font></center></span>
                                        </logic:present>
                            </h3>
                            <div class="line-border"></div>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Year/Semester<font color="red">*</font> </label>
                                        <div class="col-sm-8">
                                            <%-- <html:select property="grade" styleId="grade" styleClass="form-control" onchange="hideData(),getBranchList('grade');">
                                                 <html:option value="0">--Select Year--</html:option> 
                                                 <html:optionsCollection property="gradeslist" label="gradename" value="gradecode"/>
                                             </html:select> --%>
                                            <html:text  styleClass="form-control" property="grade" styleId="grade" value="1YR" readonly="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Branch<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="batchCode" styleId="batchCode" styleClass="form-control" onchange="hideData(),getExamDates()">
                                                <html:option value="0">--Select--</html:option> 
                                                <html:optionsCollection property="batchlist" label="braN" value="braC"/>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Month<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="edate" styleId="edate" styleClass="form-control" onchange="hideData(),getPeriod();">
                                                <html:option value="0">--Select--</html:option> 
                                                <html:optionsCollection property="stateList" label="examN" value="examC"/>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Period <font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="epaper" styleId="epaper" styleClass="form-control" onchange="hideData(),clearTotalColWorkingDays();">
                                                <html:option value="0">--Select--</html:option> 
                                                <html:optionsCollection property="centerList" label="priN" value="priC"/>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>

                                <%--     <div class="col-md-4">

                                    <div class="form-group row">
                                        <!--<label for="name" class="col-sm-6 col-form-label">Total College Working Days<font color="red">*</font></label>-->
                                        <!--<input type="text" name="colleWorking" class="form-control-plaintext"/>-->
                                        <html:text  styleClass="form-control" property="subjectCode" styleId="subjectCode" maxlength="5" onkeydown="return space(event,this);" onkeypress="return onlyNumbers1('subjectCode',event);" onchange="return validatingColTotalWorkingDays('subjectCode', 'epaper');"/>
                                    </div>

                                </div>  --%>

                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-0 col-form-label"></label>
                                        <html:hidden value="0" styleClass="form-control" property="subjectCode" styleId="subjectCode" onkeydown="return space(event,this);" onkeypress="return onlyNumbers1('subjectCode',event);" onchange="return validatingColTotalWorkingDays('subjectCode', 'epaper');"/>
                                        <input type="Submit" value="Submit" style="width: auto" class="btn btn-primary" onclick="return validFields();">
                                    </div>

                                </div>

                            </div>

                            <table align="center"  cellpadding="0" cellspacing="0" border="0" id="altrowstable1"  class="table"  style="margin: 0px auto;"> 
                                <tr>
                                    <td style="text-align:  center;" colspan="4"  >
                                </tr>
                            </table>


                            <div class="DataDiv">    

                                <logic:present  name="listData">
                                    <div style="overflow-y: scroll;height: 610px; ">
                                        <!--                                    <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                                                                <a href="./attendanceCTwentyEntry.xls?mode=downloadExcel&period=<%=request.getAttribute("period")%>&month=<%=request.getAttribute("month")%>&yearSem=<%=request.getAttribute("yearSem")%>&branchCode=<%=request.getAttribute("branchCode")%>&totalWorkingDays=<%=request.getAttribute("totalWorkingDays")%>">
                                                                                <img src="img/excel.png" style="width: 35px"/></a>
                                                                            </div>-->
                                        <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>S.NO</th>
                                                    <th>PIN</th>
                                                    <th>Student Name</th>
                                                    <th>Academic Year</th>
                                                    <th>Scheme Code</th>
                                                    <th>Section</th>
                                                    <th>Shift</th>
                                                    <th>Working Days</th>
                                                    <th>No.Of Days Attended</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <logic:present  name="listData">
                                                    <logic:iterate name="listData" id="list">
                                                        <tr>
                                                            <html:hidden property="regno"  styleId="regno${list.pin}" value ="${list.pin}" styleClass="regno"/>
                                                            <html:hidden property="sname"  styleId="sname${list.pin}" value ="${list.name}" styleClass="sname"/>
                                                            <html:hidden property="sscyear"  styleId="sscyear${list.pin}" value ="${list.sem}" styleClass="sscyear"/>
                                                            <html:hidden property="sscflag"  styleId="sscflag${list.pin}" value ="${list.scheme}" styleClass="sscflag"/>

                                                            <html:hidden property="ncc"  styleId="ncc${list.pin}" value ="${list.sec}" styleClass="ncc"/>
                                                            <html:hidden property="sports"  styleId="sports${list.pin}" value ="${list.sft}" styleClass="sports"/>
                                                            <td style="text-align: center;">
                                                                <html:hidden property="vcCheckBox" styleId="vcId" styleClass="vcCheckBox" value="${list.pin}" />
                                                                <%=++i%></td>
                                                            <td style="text-align: left;">${list.pin}</td>
                                                            <td style="text-align: left;">${list.name}</td>     
                                                            <td style="text-align: left;">${list.sem}</td>
                                                            <td style="text-align: left;">${list.scheme}</td>
                                                            <td style="text-align: center;">${list.sec}</td>
                                                            <td style="text-align: center;">${list.sft}</td>
                                                            <td style="text-align: center;">
                                                                <c:set var="status" value="${list.editEnable}"/>
                                                                <html:hidden  property="statuscase" styleId="wdaysStatus${list.pin}"  value="${list.editEnable}"/>
                                                                <c:if test="${status=='Y'}">
                                                                    <html:text  property="instcode" styleId="instcode${list.pin}"  value="${list.workingDays}" maxlength="5" onkeydown="return space(event,this);" onkeypress="return onlyNumbers('instcode${list.pin}',event);" onchange="return upTOTen('instcode${list.pin}','barcode${list.pin}','${list.workingDays}','${list.attended}','1');" />
                                                                </c:if>
                                                                <c:if test="${status=='N'}">
                                                                    ${list.workingDays} <html:hidden  property="instcode" styleId="instcode${list.pin}"  value="${list.workingDays}" onkeydown="return space(event,this);" onkeypress="return onlyNumbers('instcode${list.pin}',event);" onchange="return upTOTen('instcode${list.pin}','barcode${list.pin}','${list.workingDays}','${list.attended}','1');" />
                                                                </c:if>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <html:text  property="barcode" styleId="barcode${list.pin}"  value="${list.attended}" maxlength="5" onkeydown="return space(event,this);" onkeypress="return onlyNumbers('barcode${list.pin}',event);" onchange="return upTOTen('instcode${list.pin}','barcode${list.pin}','${list.workingDays}','${list.attended}','2');" />
                                                                <%--<html:text  property="barcode" styleId="barcode${list.pin}"  value="${list.attended}" maxlength="5" onkeydown="return space(event,this);" onkeypress="return onlyNumbers('barcode${list.pin}',event);"  />--%>
                                                            </td>
                                                        </tr>
                                                    </logic:iterate>
                                                </logic:present>

                                            </tbody>

                                        </table>
                                    </div>
                                </logic:present>   

                                <div>
                                    <logic:present  name="listData1">
                                        <!--                                    <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                                                                <a href="./attendanceCTwentyEntry.xls?mode=downloadExcel&period=<%=request.getAttribute("period")%>&month=<%=request.getAttribute("month")%>&yearSem=<%=request.getAttribute("yearSem")%>&branchCode=<%=request.getAttribute("branchCode")%>&totalWorkingDays=<%=request.getAttribute("totalWorkingDays")%>">
                                                                                <img src="img/excel.png" style="width: 35px"/></a>
                                                                            </div>-->
                                        <div style="overflow-y: scroll;height: 610px; ">
                                            <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>S.NO</th>
                                                        <th>PIN</th>
                                                        <th>Student Name</th>
                                                        <th>Academic Year</th>
                                                        <th>Scheme Code</th>
                                                        <th>Section</th>
                                                        <th>Shift</th>
                                                        <th>Working Days</th>
                                                        <th>No.Of Days Attended</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <logic:present  name="listData1">
                                                        <logic:iterate name="listData1" id="list">
                                                            <tr>
                                                                <td style="text-align: center;">
                                                                    <%=++i%></td>
                                                                <td style="text-align: left;">${list.pin}</td>
                                                                <td style="text-align: left;">${list.name}</td>     
                                                                <td style="text-align: left;">${list.sem}</td>
                                                                <td style="text-align: left;">${list.scheme}</td>
                                                                <td style="text-align: center;">${list.sec}</td>
                                                                <td style="text-align: center;">${list.sft}</td>
                                                                <td style="text-align: center;">
                                                                    ${list.workingDays} 
                                                                </td>
                                                                <td style="text-align: center;">
                                                                    ${list.attended}
                                                                </td>
                                                            </tr>
                                                        </logic:iterate>
                                                    </logic:present>

                                                </tbody>

                                            </table>
                                        </div>
                                    </logic:present>   
                                    <logic:present  name="listData">
                                        <div style="text-align: center; margin: 10px;" id="myButton">
                                            <button class="btn btn-primary" id="save" type="submit" value="Save" onclick="return saveData('1');">SAVE</button>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-primary" id="save" type="submit" value="Save" onclick="return saveData('2');">CONFIRM</button>
                                        </div> 
                                    </logic:present>   
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </html:form>
    </body>
</html>
