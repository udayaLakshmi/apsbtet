<%-- 
    Document   : CourseStatusReport
    Created on : Jul 23, 2021, 1:51:42 PM
    Author     : APTOL301294
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html>
    <head>
        <%int i = 0;%>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[13,-1], [13, "All"]]
                });
                $('#example1').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10,25,50,-1], [10,25,50,"All"]]
                });
            });

        </script>
        <style type="text/css">
           
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            
            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }
            .table > thead > tr > th{
                    border-right: 1px solid #ddd !important;
                        font-weight: 400 !important;
            }
            .table > thead > tr > th{
            border-bottom: 0px !important;
            }
        </style>
    <script type="text/javascript">
        function getDistInstlist(aadhar) {
            document.forms[0].aadhar.value = aadhar;
            document.forms[0].mode.value = "getDistrictData";
            document.forms[0].submit();
        }
        function getDistInstlistExcel(distCode) {
            document.forms[0].aadhar.value = distCode;
            document.forms[0].mode.value = "getDistrictDataExcel";
            document.forms[0].submit();
        }
        function goBack(){
            document.forms[0].mode.value = "unspecified";
            document.forms[0].submit();
        }
    </script>
</head>
<body>
    <div class="row mainbodyrow">
        <div class="container" style="padding-top: 15px;">
            <div class="col-xs-12">
                <div class="maindodycnt">
                    <h3 class="block-head-News">TWSH District Wise Abstract</h3>
                    <div class="line-border"></div>
                    <html:form action="/districtWiseRegReport" >
                        <html:hidden property="mode"/>
                        <html:hidden property="dob"/>
                        <html:hidden property="bname"/>
                        <html:hidden property="gender"/>
                        <html:hidden property="caste"/>
                        <html:hidden property="fname"/>
                        <html:hidden property="grade"/>
                        <html:hidden property="hallticket"/>
                        <html:hidden property="aadhar"/>
                        <logic:present name="result1">
                            <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                            </logic:present>
                            <logic:present name="result">
                            <center> <font color="red" style="font-weight: bold">${result}</font></center>
                            </logic:present>
                            <logic:present  name="listData">
                             <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                <a href="./districtWiseRegReport.xls?mode=downloadExcel" ><img src="img/excel.png" style="width: 35px"/></a>
                            </div>
                            <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                
                                
                                <thead>
                                    <tr>
                                        <th rowspan="3">S.NO</th>
                                        <th rowspan="3">District Name</th>
                                        <th colspan="4">Candidates Registered Through Institutes</th>
                                        <th colspan="3" rowspan="2">Private Candidates</th>
                                        <th rowspan="3">Total</th>
                                        <th rowspan="3">Total Paid Applicants</th>
                                        <th rowspan="3">Total UnPaid Applicants</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">Total Institutes</th>
                                        <th colspan="3">Total Applicants</th>
                                    </tr>
                                    <tr>
                                        <th>Repeater</th>
                                        <th>Regular</th>
                                        <th>Total</th>
                                        <th>Repeater</th>
                                        <th>Regular</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <logic:iterate name="listData" id="list">
                                        <tr>
                                            <td style="text-align: center;"><%=++i%></td>
                                            <td style="text-align: left;">${list.districtName}</td>
                                            <td style="text-align: center;">${list.instCount}</td>  
                                            <td style="text-align: center;">${list.repAppCnt}</td>
                                            <td style="text-align: center;">${list.regAppCnt}</td>
                                            <td style="text-align: center;">${list.totalAppCnt}</td>
                                            <td style="text-align: center;">${list.pvtRepCnt}</td>
                                            <td style="text-align: center;">${list.pvtRegCnt}</td>
                                            <td style="text-align: center;">${list.pvttotal}</td>
                                            <td style="text-align: center;">${list.totalCnt}</td>
                                            <td style="text-align: center;">${list.completedPay}</td>
                                            <td style="text-align: center;">${list.pendingPay}</td>
                                        </tr>
                                    </logic:iterate>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">Total</td>
                                            <td style="text-align: center;">${list.totalInstCount}</td>
                                            <td style="text-align: center;">${list.totalrepAppCnt}</td>
                                            <td style="text-align: center;">${list.totalregAppCnt}</td>
                                            <td style="text-align: center;">${list.totaltotalAppCnt}</td>
                                            <td style="text-align: center;">${list.totalpvtRepCnt}</td>
                                            <td style="text-align: center;">${list.totalpvtRegCnt}</td>
                                            <td style="text-align: center;">${list.totalpvttotal}</td>
                                            <td style="text-align: center;">${list.totaltotalCnt}</td>
                                            <td style="text-align: center;">${list.totalcompletedPay}</td>
                                            <td style="text-align: center;">${list.totalpendingPay}</td>
                                        </tr>
                                        </tfoot>
                            </table>
                        </logic:present>
                            
                        
                    </html:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>                                       