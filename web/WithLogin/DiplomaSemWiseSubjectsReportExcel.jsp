<%-- 
    Document   : MalpracticeExcel
    Created on : Jul 29, 2021, 11:51:35 PM
    Author     : 1820530
--%>

<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            int i = 0;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <!--<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">-->

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/demo.js">
        </script>
        <script type="text/javascript" class="init">

            $(document).ready(function() {
                var table = $('#example').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    fixedColumns: true
                });
            });
        </script>
        <style type="text/css">
            th, td { white-space: nowrap; }
            div.dataTables_wrapper {
                margin: 0 auto;
            }
            /* 
               div.container {
                   width: 60%;
               }*/
            table.dataTable
            {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.dataTable thead th {
                padding: 10px 18px;
                border: 1px solid #115400 !important;
                color: #fff;
                background-color: #1d7806 !important;
            }
            table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #115400 !important;
            }
            .h3_head{
                background: #337ab7;
                color: #fff;
                font-size: 16px;
                padding: 10px !important;
                text-align: center;
                box-shadow: 5px 5px 5px #ccc;
            }
        </style>
        <style type="text/css">
            #example_wrapper
            {
                width: 70% !important;
            }
        </style>

    </head>
    <body>


        <h3 class="block-head-News">Subject Wise Report</h3>

        <logic:present  name="listData">

            <div id="dataDiv">
                <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>PIN NUMBER</th>
                            <th>Name</th>
                            <th>SUB1</th>
                            <th>SUB2</th>
                            <th>SUB3</th>
                            <th>SUB4</th>
                            <th>SUB5</th>
                            <th>SUB6</th>
                            <th>SUB7</th>
                            <th>SUB8</th>
                            <th>SUB9</th>
                            <th>SUB10</th>
                            <th>SUB11</th>
                        </tr>
                    </thead>
                    <tbody>
                        <logic:iterate name="listData" id="list">
                            <tr>
                                <td style="text-align: center;"><%=++i%></td>
                                <td style="text-align: center;">${list.rollNo}</td>
                                <td style="text-align: center;">${list.name}</td>     
                                <td style="text-align: center;">${list.sub1}</td>
                                <td style="text-align: center;">${list.sub2}</td>
                                <td style="text-align: center;">${list.sub3}</td>
                                <td style="text-align: center;">${list.sub4}</td>     
                                <td style="text-align: center;">${list.sub5}</td>
                                <td style="text-align: center;">${list.sub6}</td>
                                <td style="text-align: center;">${list.sub7}</td>
                                <td style="text-align: center;">${list.sub8}</td>
                                <td style="text-align: center;">${list.sub9}</td>
                                <td style="text-align: center;">${list.sub10}</td>
                                <td style="text-align: center;">${list.sub11}</td>
                            </tr>
                        </logic:iterate>
                    </tbody>
                    <td colspan="3" style="text-align: center;">Total</td>
                    <td style="text-align: center;">${list.totalsub1}</td>
                    <td style="text-align: center;">${list.totalsub2}</td>
                    <td style="text-align: center;">${list.totalsub3}</td>
                    <td style="text-align: center;">${list.totalsub4}</td>     
                    <td style="text-align: center;">${list.totalsub5}</td>
                    <td style="text-align: center;">${list.totalsub6}</td>
                    <td style="text-align: center;">${list.totalsub7}</td>
                    <td style="text-align: center;">${list.totalsub8}</td>
                    <td style="text-align: center;">${list.totalsub9}</td>
                    <td style="text-align: center;">${list.totalsub10}</td>
                    <td style="text-align: center;">${list.totalsub11}</td>
                </table>
            </div>
        </logic:present>
    </body>
</html>

