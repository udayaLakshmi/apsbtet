<%-- 
    Document   : CourseStatusReport
    Created on : Nov 23, 2020, 1:51:42 PM
    Author     : 1582792
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%int i = 0;%>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
//                    "scrollY": 300,
                    "scrollX": true
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .box {
                width: 40%;
                margin: 0 auto;
                background: rgba(255, 255, 255, 0.2);
                padding: 35px;
                border: 2px solid #fff;
                border-radius: 20px/50px;
                background-clip: padding-box;
                text-align: center;
            }

            span#customHTMLFooter {
                display: none;
            }

            .panel-default>.panel-heading {
                color: #fff;
                background-color: #f79400;
                border-color: #f79400;
            }

            .button {
                font-size: 1em;
                padding: 10px;
                color: #fff;
                border: 2px solid #06D85F;
                border-radius: 20px/50px;
                text-decoration: none;
                cursor: pointer;
                transition: all 0.3s ease-out;
            }

            .button:hover {
                background: #06D85F;
            }

            .overlay {
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                z-index: 111;
                background: rgba(0, 0, 0, 0.7);
                transition: opacity 500ms;
                visibility: hidden;
                opacity: 0;
            }

            .overlay:target {
                visibility: visible;
                opacity: 1;
            }

            .popup {
                margin: 20px auto;
                /* padding: 40px; */
                background: #fff;
                border-radius: 5px;
                width: 70%;
                position: relative;
                transition: all 5s ease-in-out;
            }


            .popup .content {
                width: 930px !important;
                overflow-x: scroll !important;
                height: 450px;
                overflow-y: scroll !important;
            }

            .popup h2 {
                margin-top: 0;
                color: #333;
                font-family: Tahoma, Arial, sans-serif;
            }

            .popup .close {
                position: absolute;
                top: 20px;
                right: 30px;
                transition: all 200ms;
                font-size: 30px;
                font-weight: bold;
                text-decoration: none;
                color: #333;
            }

            .popup .close:hover {
                color: #06D85F;
            }


            @media screen and (max-width: 700px) {
                .box {
                    width: 80%;
                }
                .popup {
                    width: 80%;
                }

            }

            .custexthidden{
                background: none;
                border: 0;
                outline: none;
            }
            .cusselecthidden{
                -webkit-appearance: none;
                -moz-appearance: none;
                text-indent: 1px;
                text-overflow: '';
                border: 0;
                background: none;

            }
            select {

            }
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>
    </head>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).ajaxStart(function() {
                $("body").addClass("loading");
            });
            $(document).ajaxComplete(function() {
                $("body").removeClass("loading");
            });
            var seconds = 5;
            setTimeout(function() {
                if ($(".msg") !== null)
                    $(".msg").hide();
            }, seconds * 1000);
            $("form").attr('autocomplete', 'off');
            $('input[type=text], textarea').bind("cut copy paste", function(e) {
                alert("Cut copy paste not allowed here");
                e.preventDefault();
            });
            $("#showDiv").hide();
            $("#showDiv1").hide();
            $("#buttonDiv").hide();
            $("#bundleDiv").hide();
            $("#bookletsDiv").hide();
            $("#brDiv").show();
            $("#brDiv1").hide();

            $('select[multiple] option').mousedown(function(e) {
                e.preventDefault();
                this.selected = !this.selected;
                return false;
            });
            $("form").attr('autocomplete', 'off');
        });
        var paperlist = [];

        function disableData(id) {
            $("#" + id).prop('readonly', true);
        }
        function findDuplicates() {
            if (($("#grade").val() == null)||($("#mobile").val() == "")) {
                $("#grade").val("");
                alert("Please select Grade");
            }
            else {
                var paraData = "barcode=" + $("#barcode").val();
                $.ajax({
                    type: "POST",
                    url: "bundleInvigPaper.xls?mode=getGradeStatus&mobile="+$("#mobile").val().trim()+"&grade=" + $("#grade").val().trim(),
                    data: paraData,
                    success: function(response) {
                        var sample = response.trim().split("_");
                        var res = sample[0];
                        if (res == 1) {
                            alert("One of the selected Grade Already Selected.Please Reselect Gades")
                             $("#grade").val(null)
                        }
                        else {
                        }
                    }
                });
            }
        }
        function SubmitForm() {
          if ($("#fname").val() === undefined || $("#fname").val() === "" || ($.trim($("#fname").val()).length === 0)) {
                alert("Please enter Examiner Name");
                $("#fname").focus().css({'border': '1px solid red'});
                return false;
            } else if ($("#mobile").val() === undefined || ($.trim($("#mobile").val()).length === 0 ||
                    $("#mobile").val().length < 10 || parseInt($("#mobile").val().trim()) < 6000000000)) {
                alert("Enter Mobile Number");
                $("#mobile").focus().css({'border': '1px solid red'});
                return false;
            }
//            else if ($("#email").val() === undefined || $("#email").val() === "" || ($.trim($("#email").val()).length) < 10) {
//                alert("Enter eMail");
//                $("#email").focus().css({'border': '1px solid red'});
//                return false;
//            }
            else if ($("#grade").val() === undefined || $("#grade").val() === "" || $("#grade").val() == "0" || $("#grade").val() === null) {
                alert("Select Grade");
                $("#grade").val(null)
                $("#grade").focus().css({'border': '1px solid red'});
                return false;
            }
            else {
        var paraData="";
        $.ajax({
                    type: "POST",
                    url: "bundleInvigPaper.xls?mode=getGradeStatus&mobile="+$("#mobile").val().trim()+"&grade=" + $("#grade").val(),
                    data: paraData,
                    success: function(response) {
                        var res = response;
                        if (res==1) {
                            alert("One of the selected Grade Already Registered.Please Reselect Grades")
                             $("#grade").val(null)
                        }
                        else {
                            submitData();
                        }
                    }
                });
            }
        }
        function submitData(){
                var x = confirm("Do you Want to Proceed?")
                if (x == false) {
                    return false
                } else {
                    var grade1 = $("#grade").val();
                    document.forms[0].gradename.value = grade1;
                    document.forms[0].mode.value = "submitDetails";
                    document.forms[0].submit();
                }
        }
        
        function space(evt, thisvalue)
        {
            var number = thisvalue.value;
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (number.length < 1) {
                if (evt.keyCode == 32) {
                    return false;
                }
            }
            return true;
        }
        function onlyNumbers(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Allows only Numerics");
                return false;
            }
            return true;
        }
        function inputLimiter(e, allow) {
            var AllowableCharacters = '';
            if (allow == 'Letters') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            }
            if (allow == 'Letters1') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
            }
            if (allow == 'Numbers') {
                AllowableCharacters = '1234567890';
            }
            if (allow == 'landline') {
                AllowableCharacters = '1234567890-';
            }
            if (allow == 'NameCharacters') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
            }
            if (allow == 'NameCharactersAndNumbers') {
                AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
            }
            if (allow == 'website') {
                AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
            }
            if (allow == 'HouseNo') {
                AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
            }
            if (allow == 'DistrictExp') {
                AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            }
            if (allow == 'ogpaExp') {
                AllowableCharacters = '1234567890.';
            }
            var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
            if (k != 13 && k != 8 && k != 0) {
                if ((e.ctrlKey == false) && (e.altKey == false)) {
                    return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
        function telephoneValidation() {
            var mobileNo = $("#mobile").val();
            var len = mobileNo.length;
            if (len < 10) {
                alert("Mobile Number Should be 10 digits");
                $("#mobile").val("");
                $("#mobile").focus().css({'border': '1px solid red'});
                return false;
            } else if (($("#mobile").val().trim()) == '9999999999'
                    || ($("#mobile").val().trim()) == '8888888888' ||
                    ($("#mobile").val().trim()) == '7777777777' ||
                    ($("#mobile").val().trim()) == '6666666666') {
                alert("Please Enter valid Mobile Number");
                $("#mobile").val("");
                $("#mobile").focus().css({'border': '1px solid red'});
                return false;
            }
            else if (($("#mobile").val().trim().charAt(0) != '9'
                    && $("#mobile").val().trim().charAt(0) != '8' &&
                    $("#mobile").val().trim().charAt(0) != '7' &&
                    $("#mobile").val().trim().charAt(0) != '6')) {
                alert("Please Enter valid Mobile Number");
                $("#mobile").val("");
                $("#mobile").focus().css({'border': '1px solid red'});
                return false;
            }
        }
        function isEmail() {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test($("#email").val())) {
                $("#email").val("");
                alert("Invalid eMail")
                return true;  //wrong mail
            } else {
                return false;  //correct mail

            }
        }
        function resetData() {
            bundlelist = [];
            paperlist = [];
            $("#epaper").val("0");
            $("#institute").val("0");
            $("#instaddress").val("0");
            $("#instcode").val("0");
            $("#instname").val("0");
            $("#bookletsDiv").hide();
        }
        function getPay(grade, mobile) {
            document.forms[0].gradename.value = grade;
            document.forms[0].coursename.value = mobile;
            document.forms[0].mode.value = "deleteRecord";
            document.forms[0].submit();
        }
    </script>
</head>
<body>
    <div class="row mainbodyrow">
        <div class="container" style="padding-top: 40px;">
            <div class="col-xs-12">
                <div class="maindodycnt">
                    <h3 class="block-head-News">Examiner Registration</h3>
                    <div class="line-border"></div>
                    <br>
                    <div class="row">
                        <div class=" col-md-12">
                            <!--<div class="login-form">-->
                            <div>
                                <html:form action="/bundleInvigPaper"  styleId="d" method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <html:hidden property="gradename" styleId="gradename"/>
                                    <html:hidden property="coursename" styleId="coursename"/>
                                    <logic:present name="result2">
                                        <span class="msg"><center> <font color="green" style="font-weight: bold">${result2}</font>  </center>  </span>
                                            </logic:present>
                                            <logic:present name="result">
                                        <span class="msg"><center> <font color="red" style="font-weight: bold">${result}</font></center></span>
                                    </logic:present><br>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examiner Name<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="fname" styleId="fname" maxlength="50" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Mobile No<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="mobile" styleId="mobile"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="telephoneValidation()" maxlength="10"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">eMail</label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="email" styleId="email"  onkeydown="return space(event, this);" maxlength="100" onchange='return isEmail(this)'/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination  Grade<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <select name="grade" id="grade" class="form-control" style="height:20%" multiple="multiple" onchange="return findDuplicates()">
                                                        <%--  <option value="0">--Select Grade--</option> 
                                                        <html:optionsCollection property="gradeslist" label="gname" value="gcode"/>--%>
                                                        <!--<option value="0">--Select Grade--</option>--> 
                                                        <option value="TEJ">Typewriting English Junior</option> 
                                                        <option value="TEL">Typewriting English Lower</option> 
                                                        <option value="TEH">Typewriting English Higher</option> 
                                                        <option value="TEHS">Typewriting English High Speed</option> 
                                                        <option value="TTL">Typewriting Telugu Lower</option> 
                                                        <option value="TTH">Typewriting Telugu Higher</option> 
                                                        <option value="TTHS">Typewriting Telugu High Speed</option> 
                                                        <option value="THL">TypeWriting Hindi Lower</option> 
                                                        <option value="THH">TypeWriting Hindi Higher</option> 
                                                        <option value="SEL">Shorthand English Lower</option> 
                                                        <option value="SEI">Shorthand English Intermediate</option> 
                                                        <option value="SEH">Shorthand English Higher</option> 
                                                        <option value="SEHS150">Shorthand English High Speed (150 WPM)</option> 
                                                        <option value="SEHS180">Shorthand English High Speed (180 WPM)</option> 
                                                        <option value="SEHS200">Shorthand English High Speed (200 WPM)</option> 
                                                        <option value="STL">Shorthand Telugu Lower</option> 
                                                        <option value="STH">Shorthand Telugu Higher</option> 
                                                        <option value="STHS80">Shorthand Telugu High Speed (80 WPM)</option> 
                                                        <option value="STHS100">Shorthand Telugu High Speed (100 WPM)</option> 
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <div class="col-sm-8">
                                                    <center><input type="button" onclick="return SubmitForm()" value="submit" class="btn btn-primary" style="max-width: 96px;"/>
                                                </div>
                                            </div>
                                        </div>
                                        <%--   <div class="col-md-4">
                                               <div class="form-group row">
                                                   <label for="name" class="col-sm-4 col-form-label">Examination  Paper<font color="red">*</font></label>
                                                   <div class="col-sm-8">
                                                       <html:select property="ecenter" styleId="ecenter" styleClass="form-control" onchange="resetData();addDetails();">
                                                           <option value="0">--Select Paper--</option> 
                                                           <option value="I">PAPER-I</option> 
                                                           <option value="II">PAPER-II</option> 
                                                       </html:select>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-md-4">
                                               <div class="form-group row">
                                                   <label for="name" class="col-sm-4 col-form-label">Examination Batch<font color="red">*</font></label>
                                                   <div class="col-sm-8">
                                                       <html:select property="examinationname" styleId="examinationname" styleClass="form-control" onchange="resetData();addDetails();">
                                                           <option value="0">--Select Batch--</option> 
                                                           <option value="B1">Batch-I</option> 
                                                           <option value="B2">Batch-II</option> 
                                                           <option value="B3">Batch-III</option> 
                                                           <option value="B4">Batch-IV</option> 
                                                       </html:select>
                                                   </div>
                                               </div>
                                           </div>--%>
                                    </div>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <div class="row">

                                    </div>
                                    <br/>
                                    <br/>
                                    <logic:present  name="invgList">
                                        <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Examiner Name</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    <th>Grade</th>
                                                    <th>InActive</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%int j = 0;%>
                                                <logic:iterate name="invgList" id="lists">
                                                    <tr>
                                                        <td><%=++j%></td>
                                                        <td>${lists.bname}</td>
                                                        <td>${lists.mobile}</td>
                                                        <td>${lists.email}</td>
                                                        <td>${lists.grade}</td>
                                                        <td><input type="button" onclick="getPay('${lists.grade}', '${lists.mobile}')" value="INACTIVE" class="btn btn-primary"/></td>

                                                    </tr>
                                                </logic:iterate>
                                            </tbody>
                                        </table>
                                    </logic:present>
                                </html:form>
                            </div>
                        </div>
                    </div>
                </div>
                </body>
                </html>                                       