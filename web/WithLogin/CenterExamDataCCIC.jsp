<%-- 
    Document   : PaymentReconsilationReport
    Created on : Jul 12, 2021, 7:09:24 PM
    Author     : 1820530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  


        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">


        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[13, -1], [13, "All"]]
                });
                $('#example1').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>

        <Script>

            function validFields() {
                if ($("#grade").val() === undefined || $("#grade").val() === "" || $("#grade").val() === "0") {
                    alert("Please Select Course");
                    $("#grade").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#subjectCode").val() === undefined || $("#subjectCode").val() === "" || $("#subjectCode").val() === "0") {
                    alert("Please Select Subject");
                    $("#subjectCode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#edate").val() === undefined || $("#edate").val() === "" || $("#edate").val() === "0") {
                    alert("Please Select Exam Date");
                    $("#edate").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    document.forms[0].mode.value = "getCentrBasicData";
                    //document.forms[0].mode.value = "getPdfData";
                    document.forms[0].submit();
                }
            }
        </Script>

        <script>
            $(function() {

                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);

                var array = ["31-07-2021", "01-08-2021", "07-08-2021", "08-08-2021"];

                $('#toDate').datepicker({
                    beforeShowDay: function(date) {
                        var string = jQuery.datepicker.formatDate('dd-mm-yy', date);
                        return [array.indexOf(string) == -1]
                    }
                });
                var today = new Date();
                yrRange = '2021' + ":" + '2021';
                $('#toDate').datepicker({
                    format: 'dd-mm-yyyy',
                    changeMonth: true,
                    minDate: new Date(2021, 06, 31),
                    autoclose: true,
                    changeYear: true,
                    yearRange: yrRange,
                    endDate: "today",
                    maxDate: new Date(2021, 07, 8)


                }).on('toDate', function(ev) {
                    $(this).datepicker('hide');
                });


                $('#toDate').keyup(function() {
                    if (this.value.match(/[^0-9]/g)) {
                        this.value = this.value.replace(/[^0-9^-]/g, '');
                    }
                });
            });

            function hideData() {
                $('.DataDiv').hide();
            }
            function getSubjectList(courseId) {
                var data = "courseId=" + $('#'+courseId).val();
                $.ajax({
                    type: "POST",
                    url: "./centerExamDataCCIC.do?mode=getSubject&courseId="+$('#'+courseId).val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        if (response.trim() == "0") {
                            alert("No Subject available with given Registration No");
                            $("#subjectCode").empty().append("<option value='0'>--Select Subject--</option>");
                        } else {
                            $("#subjectCode").empty().append(response);
                        }

                    },
                    error: function(e) {
                    }
                });
                $("#edate").empty();
                $("#edate").empty().append("<option value='0'>--Select--</option>");
            }
            function getExamDates() {
                var data = "subCode=" + $('#subjectCode').val();
                $.ajax({
                    type: "POST",
                    url: "./centerExamDataCCIC.do?mode=getExamDates&subCode="+$('#subjectCode').val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        if (response.trim() == "0") {
                            alert("No exam  date available");
                            $("#edate").empty().append("<option value='0'>--Select--</option>");
                        } else {
                            $("#edate").empty().append(response);
                        }

                    },
                    error: function(e) {
                    }
                });
            }
        </script>

    </head>
    <body>
        <html:form action="/centerExamDataCCIC">
            <html:hidden property="mode"/>  
            <div class="row mainbodyrow">
                <div class="container" style="padding-top: 15px;">
                    <div class="col-xs-12">
                        <div class="maindodycnt">
                            <h3 class="block-head-News">Exam Center Details
                                <logic:present name="result">
                                    <span id="msg" ><center><font color="red">${result}</font></center></span>
                                </logic:present>
                            </h3>
                            <div class="line-border"></div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Course<font color="red">*</font> </label>
                                        <div class="col-sm-8">
                                            <html:select property="grade" styleId="grade" styleClass="form-control" onchange="hideData(),getSubjectList('grade');">
                                                <html:option value="0">--Select Course--</html:option> 
                                                <html:optionsCollection property="gradeslist" label="gradename" value="gradecode"/>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Subject<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="subjectCode" styleId="subjectCode" styleClass="form-control" onchange="hideData(),getExamDates()">
                                                <html:option value="0">--Select Subject--</html:option> 
                                                <html:optionsCollection property="subjectList" label="subN" value="subC"/>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Exam Date<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="edate" styleId="edate" styleClass="form-control" onchange="hideData();">
                                                <html:option value="0">--Select--</html:option> 
                                               <html:optionsCollection property="stateList" label="examN" value="examC"/>
                                            <%-- <html:option value="2021-08-03">31/08/2021</html:option> 
                                                  <%-- <html:option value="2021-08-01">01/08/2021</html:option> 
                                                <html:option value="2021-08-07">07/08/2021</html:option> 
                                                <html:option value="2021-08-08">08/08/2021</html:option> --%>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <table align="center"  cellpadding="0" cellspacing="0" border="0" id="altrowstable1"  class="table"  style="margin: 0px auto;"> 
                                <tr>
                                    <td style="text-align:  center;padding: 5px !important;" colspan="4"  >
                                        <input type="Submit" value="Submit" style="width: auto" class="btn btn-primary" onclick="return validFields();"></td>
                                </tr>
                            </table>



                            <div class="DataDiv">    

                                <logic:present  name="listData">
                                    <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                        <a href="./centerExamDataCCIC.do?mode=getPdfData&subCode=<%=request.getAttribute("subCode")%>&examDate=<%=request.getAttribute("examDate")%>&courseCode=<%=request.getAttribute("courseCode")%>" >
                                            <img src="img/pdf.png" style="width: 35px"/></a>
                                    </div>
                                    <br>
                                    <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Center Code</th>
                                                <th>Name of the Center</th>
                                                <th>Course</th>
                                                <th>Subject</th>
                                                <th>Exam Date</th>
                                                <th>Time of Examination</th>
                                                <th>Total Candidate</th>
                                                <th>No. of Candidates Present with Hall Tickets</th>
                                                <th>No. of Candidates Absent</th>
                                                <th>MalPractices Count</th>
                                                <th>Buffer OMR(SGS) Count</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <logic:iterate name="listData" id="list">
                                                <tr>
                                                    <td style="text-align: left;">${list.ccode}</td>
                                                    <td style="text-align: left;">${list.cname}</td>     
                                                    <td style="text-align: left;">${list.gradecode}</td>
                                                    <td style="text-align: left;">${list.subject}</td>
                                                    <td style="text-align: left;">${list.eedate}</td>
                                                    <td style="text-align: left;">${list.etime}</td>
                                                    <td style="text-align: left;">${list.totalCount}</td>
                                                    <td style="text-align: center;">${list.attCount}</td>
                                                    <td style="text-align: center;">${list.abCount}</td>
                                                    <td style="text-align: center;">${list.malCount}</td>
                                                    <td style="text-align: center;">${list.bufferCount}</td>
                                                </tr>
                                            </logic:iterate>
                                        </tbody>

                                    </table>
                                </logic:present>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </html:form>
    </body>
    <script src="./js/datePicker.js"></script>
</html>
