<%-- 
    Document   : MalpracticeExcel
    Created on : Jul 29, 2021, 11:51:35 PM
    Author     : 1820530
--%>

<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>
        <%int i = 0;%>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/demo.js">
        </script>
        <style type="text/css">
            th, td { white-space: nowrap; }
            div.dataTables_wrapper {
                margin: 0 auto;
            }
            table.dataTable
            {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.dataTable thead th {
                padding: 10px 18px;
                border: 1px solid #115400 !important;
                color: #fff;
                background-color: #1d7806 !important;
            }
            table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #115400 !important;
            }
            .h3_head{
                background: #337ab7;
                color: #fff;
                font-size: 16px;
                padding: 10px !important;
                text-align: center;
                box-shadow: 5px 5px 5px #ccc;
            }
        </style>
        <style type="text/css">
            #example_wrapper
            {
                width: 70% !important;
            }
        </style>

    </head>
    <body>


        <h3 class="block-head-News">Date Wise Exam Report</h3>

        <logic:present  name="listData">

            <div id="dataDiv">
                <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="80%">
                    <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Center Code</th>
                            <th>Branch Code</th>
                            <th>Subject</th>
                            <th>PIN Number</th>
                            <th>Year/Semester</th>
                            <th>Exam Date</th>
                            <th>Attendance Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <logic:iterate name="listData" id="list">
                            <tr>
                                <td style="text-align: center;"><%=++i%></td>
                                <td style="text-align: center;">${list.centerC}</td>
                                <td style="text-align: center;">${list.branchC}</td>     
                                <td style="text-align: center;">${list.subName}</td>
                                <td style="text-align: center;">${list.rollNo}</td>
                                <td style="text-align: center;">${list.year}</td>
                                <td style="text-align: center;">${list.examDate}</td>     
                                <td style="text-align: center;">${list.status}</td>
                            </tr>
                        </logic:iterate>
                    </tbody>

                </table>

            </div>
        </logic:present>
    </body>
</html>

