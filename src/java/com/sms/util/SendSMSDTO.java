/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.util;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;
import java.util.HashMap;

/**
 *
 * @author 747577
 */
public class SendSMSDTO extends ActionForm {

    private String mode = null;
    private String sms = null;
    private String level = null;
    private String subject = null;
    private String body = null;
    private String count = null;
    private String systemIp = null;
    private String sendStatus = null;
     private String name = null;
      private String designation = null;
     private String mobileNumber;
    private String loginId = null;
    private String[] tahsildar = null;
    private String[] email = null;
    private String[] phoneNumber = null;
    private String[] dros = null;
    private String[] distoff = null;
    private String[] deptoff = null;
    private String department = null;
    private ArrayList tahasildarList = new ArrayList();
    private ArrayList districtofficersList = new ArrayList();
    private ArrayList depeofficersList = new ArrayList();
    private ArrayList departmentList = new ArrayList();
    private FormFile uploadFile1;
    private FormFile uploadFile2;
    private FormFile uploadFile3;
    private String officerType = null;
    private String officerPost = null;
    private String mandalDesignation = null;
    private String villageDesignation = null;
    private String radioButtonProperty = null;
    private HashMap<String, FormFile> hMap = new HashMap<String, FormFile>();

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getSystemIp() {
        return systemIp;
    }

    public void setSystemIp(String systemIp) {
        this.systemIp = systemIp;
    }

    public String getMandalDesignation() {
        return mandalDesignation;
    }

    public void setMandalDesignation(String mandalDesignation) {
        this.mandalDesignation = mandalDesignation;
    }

    public String getVillageDesignation() {
        return villageDesignation;
    }

    public void setVillageDesignation(String villageDesignation) {
        this.villageDesignation = villageDesignation;
    }

    public String getRadioButtonProperty() {
        return radioButtonProperty;
    }

    public void setRadioButtonProperty(String radioButtonProperty) {
        this.radioButtonProperty = radioButtonProperty;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public ArrayList getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(ArrayList departmentList) {
        this.departmentList = departmentList;
    }

    public String getOfficerPost() {
        return officerPost;
    }

    public void setOfficerPost(String officerPost) {
        this.officerPost = officerPost;
    }

    public String getOfficerType() {
        return officerType;
    }

    public void setOfficerType(String officerType) {
        this.officerType = officerType;
    }

    public String[] getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String[] phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String[] getEmail() {
        return email;
    }

    public void setEmail(String[] email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public FormFile getUploadFile1() {
        return uploadFile1;
    }

    public void setUploadFile1(FormFile uploadFile1) {
        this.uploadFile1 = uploadFile1;
    }

    public FormFile getUploadFile2() {
        return uploadFile2;
    }

    public void setUploadFile2(FormFile uploadFile2) {
        this.uploadFile2 = uploadFile2;
    }

    public FormFile getUploadFile3() {
        return uploadFile3;
    }

    public void setUploadFile3(FormFile uploadFile3) {
        this.uploadFile3 = uploadFile3;
    }

    public HashMap<String, FormFile> gethMap() {
        return hMap;
    }

    public void sethMap(HashMap<String, FormFile> hMap) {
        this.hMap = hMap;
    }

    public void setDynaProperty(String key, FormFile value) {
        this.hMap.put(key, value);
    }

    public FormFile getDynaProperty(String key) {
        return this.hMap.get(key);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public ArrayList getDepeofficersList() {
        return depeofficersList;
    }

    public void setDepeofficersList(ArrayList depeofficersList) {
        this.depeofficersList = depeofficersList;
    }

    public String[] getDeptoff() {
        return deptoff;
    }

    public void setDeptoff(String[] deptoff) {
        this.deptoff = deptoff;
    }

    public String[] getDistoff() {
        return distoff;
    }

    public void setDistoff(String[] distoff) {
        this.distoff = distoff;
    }

    public ArrayList getDistrictofficersList() {
        return districtofficersList;
    }

    public void setDistrictofficersList(ArrayList districtofficersList) {
        this.districtofficersList = districtofficersList;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public ArrayList getTahasildarList() {
        return tahasildarList;
    }

    public void setTahasildarList(ArrayList tahasildarList) {
        this.tahasildarList = tahasildarList;
    }

    public String[] getTahsildar() {
        return tahsildar;
    }

    public void setTahsildar(String[] tahsildar) {
        this.tahsildar = tahsildar;
    }

    public String[] getDros() {
        return dros;
    }

    public void setDros(String[] dros) {
        this.dros = dros;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
