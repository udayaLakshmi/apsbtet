/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author 1582792
 */
public class Sample {

    public static void main(String[] args) throws MalformedURLException, IOException {
        String responseString = "";
        String outputString = "";
        HttpURLConnection httpConn = null;
        try {
            String strURL = "https://apdept.meeseva.gov.in/MeesevaTransactions/MeesevaTransactions.asmx?wsdl";

            URL url = new URL(strURL);

            URLConnection connection = url.openConnection();
            connection.setRequestProperty("content-type", "text/xml");
            httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
//            String xmlInput = "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
//                    + "    <Body>\n"
//                    + "        <GetDetailsByAppNo xmlns=\"http://APTrans.org/\">\n"
//                    + "            <ApplicationNumber>CGC012038357744</ApplicationNumber>\n"
//                    + "            <userid>DCTADS</userid>\n"
//                    + "            <password>DCT@D$@XXXXXXXXs</password>\n"
//                    + "        </GetDetailsByAppNo>\n"
//                    + "    </Body>\n"
//                    + "</Envelope>";
            String xmlInput = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" + 
					"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:s=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://schemas.xmlsoap.org/wsdl/soap12/\" xmlns:http=\"http://schemas.xmlsoap.org/wsdl/http/\" xmlns:mime=\"http://schemas.xmlsoap.org/wsdl/mime/\" xmlns:tns=\"http://APTrans.org/\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:tm=\"http://microsoft.com/wsdl/mime/textMatching/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:wsdl=\"http://schemas.xmlsoap.org/wsdl/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + 
					"    <SOAP-ENV:Body>" + 
					"        <tns:GetDetailsByAppNo xmlns:tns=\"http://APTrans.org/\">" + 
					"            <tns:ApplicationNumber>CGC012038357744</tns:ApplicationNumber>" + 
					"            <tns:userid>DCTADS</tns:userid>" + 
					"            <tns:password>DCT@D$@XXXXXXXX</tns:password>" + 
					"        </tns:GetDetailsByAppNo>" + 
					"    </SOAP-ENV:Body>" + 
					"</SOAP-ENV:Envelope>";



            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes();
            bout.write(buffer);
            byte[] b = bout.toByteArray();

            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
                System.out.println("outputString=======================" + outputString);
            }
            httpConn.disconnect();

            //return "SENT";
        } catch (Exception e) {
            e.printStackTrace();
            //return e+"";
        }




    }
}
