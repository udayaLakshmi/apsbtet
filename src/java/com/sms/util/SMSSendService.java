/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import org.apache.log4j.Logger;

/**
 *
 * @author 1629028
 */
public class SMSSendService {

    static Logger loggers = Logger.getLogger(SMSSendService.class);
    private static SMSSendService serviceObj = null;

    public static SMSSendService getInstance() {
        if (serviceObj == null) {
            serviceObj = new SMSSendService();
        }
        return serviceObj;
    }

    public static String sendSMS(String message, String mobileNo) {
        String responseString = "";
        String outputString = "";
        HttpURLConnection httpConn = null;
        try {
            String strURL = "https://smsgateway.aptonline.in/APOLSMSGW/SMSservice.asmx?wsdl";

            URL url = new URL(strURL);
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setSSLSocketFactory(sslContext.getSocketFactory());
//			URLConnection connection = url.openConnection();
            connection.setRequestProperty("content-type", "text/xml");
            httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            String xmlInput = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
                    + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:s=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://schemas.xmlsoap.org/wsdl/soap12/\" xmlns:http=\"http://schemas.xmlsoap.org/wsdl/http/\" xmlns:mime=\"http://schemas.xmlsoap.org/wsdl/mime/\" xmlns:tns=\"http://tempuri.org/\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns:tm=\"http://microsoft.com/wsdl/mime/textMatching/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:wsdl=\"http://schemas.xmlsoap.org/wsdl/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
                    + "    <SOAP-ENV:Header>"
                    + "        <tns:AuthHeader xmlns:tns=\"http://tempuri.org/\">"
                    + "            <tns:Username>SBTET</tns:Username>"
                    + "            <tns:Password>SBTET@2021</tns:Password>"
                    + "        </tns:AuthHeader>"
                    + "    </SOAP-ENV:Header>"
                    + "    <SOAP-ENV:Body>"
                    + "        <tns:SendSMS xmlns:tns=\"http://tempuri.org/\">"
                    + "            <tns:message>" + message + "</tns:message>"
                    + "            <tns:phoneNo>" + mobileNo + "</tns:phoneNo>"
                    + "            <tns:Client_ID>SBTET</tns:Client_ID>"
                    + "        </tns:SendSMS>"
                    + "    </SOAP-ENV:Body>"
                    + "</SOAP-ENV:Envelope>";


            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes();
            bout.write(buffer);
            byte[] b = bout.toByteArray();

            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }
            httpConn.disconnect();

            return "SENT";
        } catch (Exception e) {
            return e + "";
        }

    }
}
