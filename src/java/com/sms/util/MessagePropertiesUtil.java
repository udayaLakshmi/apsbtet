/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.util;

import java.io.File;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author 1582792
 */
public class MessagePropertiesUtil {

    private static Logger log = Logger.getLogger(MessagePropertiesUtil.class);
    private static ResourceBundle resourceBundle = null;
    public static String FIlE_PATH = "E:\\SBTETFILES";
    public static String FIlE_PATH_CCIC = "E:\\CCICFILES";
    public static String FIlE_PATH2 = "F:\\RGUKT CET Applicants Data";
    private static final String FILE_NAME = "messages";
    //Key's in Properties file
    private static String scrollMessage1_key = "scrollMessage1";
    private static String scrollMessage2_key = "scrollMessage2";
    private static String scrollMessage3_key = "scrollMessage3";
    private static String scrollMessage4_key = "scrollMessage4";
    private static String scrollMessage5_key = "scrollMessage5";
    private static String smsMessage_key = "smsMessage";
    private static String downMessage_key = "downMessage";
    private static final String enableDashBoard = "enableDashBoard";
    public static String scrollMessage1 = null;
    public static String scrollMessage2 = null;
    public static String scrollMessage3 = null;
    public static String scrollMessage4 = null;
    public static String scrollMessage5 = null;
    public static String smsMessage = null;
    public static String downMessage = null;
    public static boolean enableDash_Board = false;
    private static File file = null;

//    private static ClassLoader loader=null;
    static {
        setAllMessageValues();
    }

    public static String getScrollMessage1() {
        scrollMessage1 = getValue(scrollMessage1_key, null);
        return scrollMessage1;
    }

    public static void setScrollMessage1(String scrollMessage1) {
        MessagePropertiesUtil.scrollMessage1 = scrollMessage1;
    }

    public static String getScrollMessage2() {
        scrollMessage2 = getValue(scrollMessage2_key, null);
        return scrollMessage2;
    }

    public static void setScrollMessage2(String scrollMessage2) {
        MessagePropertiesUtil.scrollMessage2 = scrollMessage2;
    }

    public static String getScrollMessage3() {
        scrollMessage3 = getValue(scrollMessage3_key, null);
        return scrollMessage3;
    }

    public static void setScrollMessage3(String scrollMessage3) {
        MessagePropertiesUtil.scrollMessage3 = scrollMessage3;
    }

    public static String getScrollMessage4() {
        scrollMessage4 = getValue(scrollMessage4_key, null);
        return scrollMessage4;
    }

    public static void setScrollMessage4(String scrollMessage4) {
        MessagePropertiesUtil.scrollMessage4 = scrollMessage4;
    }

    public static String getScrollMessage5() {
        scrollMessage5 = getValue(scrollMessage5_key, null);
        return scrollMessage5;
    }

    public static void setScrollMessage5(String scrollMessage5) {
        MessagePropertiesUtil.scrollMessage5 = scrollMessage5;
    }

    public static String getSmsMessage() {
        smsMessage = getValue(smsMessage_key, null);
        return smsMessage;
    }

    public static void setSmsMessage(String smsMessage) {
        MessagePropertiesUtil.smsMessage = smsMessage;
    }

    public static void setDownMessage(String downMessage) {
        MessagePropertiesUtil.downMessage = downMessage;
    }

    public static String getDownMessage() {
        downMessage = getValue(downMessage_key, null);
        return downMessage;
    }

    public static boolean enableDashBoard() {
        String enableDashBoardBoolean = getValue(enableDashBoard, "false");
        boolean isEnabled = Boolean.parseBoolean(enableDashBoardBoolean);
        return isEnabled;
    }

    public static boolean isDashBoardEnabled() {
        return enableDash_Board;
    }

    public void setSSCNREnabled(boolean enable) {
        this.enableDash_Board = enable;
    }

    public static void setAllMessageValues() {
        scrollMessage1 = getScrollMessage1();
        scrollMessage2 = getScrollMessage2();
        scrollMessage3 = getScrollMessage3();
        scrollMessage4 = getScrollMessage4();
        scrollMessage5 = getScrollMessage5();
        smsMessage = getSmsMessage();
        downMessage = getDownMessage();
        enableDash_Board = enableDashBoard();
    }

    /**
     *
     * @param key
     * @param value
     * @return
     */
    private static String getValue(String key, String value) {
        try {
              String systemIPAdress = InetAddress.getLocalHost().getHostAddress();
            if (systemIPAdress != null & systemIPAdress.equalsIgnoreCase("10.96.64.51")) {
                 FIlE_PATH = "E:\\SBTETFILES";
                FIlE_PATH_CCIC = "E:\\CCICFILES";

            } else if (systemIPAdress != null & systemIPAdress.equalsIgnoreCase("10.96.64.52")) {
                 FIlE_PATH = "E:\\SBTETFILES";
                FIlE_PATH_CCIC = "E:\\CCICFILES";

            } else if (systemIPAdress != null & systemIPAdress.equalsIgnoreCase("10.96.64.54")) {
                 FIlE_PATH = "E:\\SBTETFILES";
                FIlE_PATH_CCIC = "E:\\CCICFILES";

            }
            else {
                FIlE_PATH = "D:\\SBTETFILES";
                FIlE_PATH_CCIC = "D:\\CCICFILES";
            }
               
            
            if (file == null) {
                file = new File(FIlE_PATH);
            }
            URL[] urls = {file.toURI().toURL()};
            ClassLoader loader = new URLClassLoader(urls);
            String resultedValue;
//            resourceBundle = ResourceBundle.getBundle(fileName);
            resourceBundle = ResourceBundle.getBundle(FILE_NAME, Locale.getDefault(), loader);
            resultedValue = resourceBundle.getString(key);
            value = resultedValue != null ? resultedValue : value;
            System.out.println(key + ": " + resultedValue);
            log.info(" " + key + ": " + resultedValue);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return value;
    }

    public static void main(String[] args) throws Exception {
              String systemIPAdress = InetAddress.getLocalHost().getHostAddress();
            if (systemIPAdress != null & systemIPAdress.equalsIgnoreCase("10.96.64.51")) {
                 FIlE_PATH = "E:\\SBTETFILES";
                FIlE_PATH_CCIC = "E:\\CCICFILES";

            } else if (systemIPAdress != null & systemIPAdress.equalsIgnoreCase("10.96.64.52")) {
                 FIlE_PATH = "E:\\SBTETFILES";
                FIlE_PATH_CCIC = "E:\\CCICFILES";

            }  else if (systemIPAdress != null & systemIPAdress.equalsIgnoreCase("10.96.64.54")) {
                 FIlE_PATH = "E:\\SBTETFILES";
                FIlE_PATH_CCIC = "E:\\CCICFILES";

            }
            else {
                FIlE_PATH = "D:\\SBTETFILES";
                FIlE_PATH_CCIC = "D:\\CCICFILES";
            }
               
        if (file == null) {
            file = new File(FIlE_PATH);
        }
        URL[] urls = {file.toURI().toURL()};
        ClassLoader loader = new URLClassLoader(urls);
        try {
//            resourceBundle = ResourceBundle.getBundle(fileName);
            resourceBundle = ResourceBundle.getBundle("messages", Locale.getDefault(), loader);
        } catch (Exception ex) {
            resourceBundle = ResourceBundle.getBundle("messages");
        }
        Enumeration<String> keys = resourceBundle.getKeys();
        while (keys != null && keys.hasMoreElements()) {
            String key = keys.nextElement();
            String value = resourceBundle.getString(key);
            System.out.println(key + ": " + value);
        }
    }
}
