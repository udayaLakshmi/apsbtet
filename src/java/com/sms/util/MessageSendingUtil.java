/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.util;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.ps.isms.msgs.MessageOutboxDto;
//import com.ps.isms.dao.CommonDAO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.mail.internet.InternetAddress;
import org.apache.log4j.Logger;

/**
 *
 * @author APTOL301535
 */
public class MessageSendingUtil {

    static Logger loggers = Logger.getLogger(MessageSendingUtil.class);

    public static boolean sendSMS(String message, String mno) {
        String smsStatus = "";
        try {
            smsStatus = SMSSendService.sendSMS(message, mno);
            if (smsStatus.equals("SENT")) {
                //msg.setSmsStatus(smsStatus);	
                System.out.println("smsStatus  " + smsStatus);
            } else {
                System.out.println("smsStatus  " + smsStatus);
                //msg.setRemarks(smsStatus);
            }
//			msg.setNoOftries(msg.getNoOftries()+1);
//			msg.setUPDATED_TS(new Date());
//			updateMessages(msg);	
            return true;
        } catch (Exception e) {
            loggers.error(e);
            return false;
        }
    }

    public static boolean sendEmail(MessageOutboxDto messageOutboxDto) {
        try {
            ArrayList<InternetAddress> toMails = new ArrayList<InternetAddress>();
            ArrayList<InternetAddress> ccMails = new ArrayList<InternetAddress>();
            ArrayList<InternetAddress> bccMails = new ArrayList<InternetAddress>();


//            toMails.add(new InternetAddress(messageOutboxDto.getEmailId()));
//            ccMails.add(new InternetAddress("ts.isms.support@aptonline.in"));
//            bccMails.add(new InternetAddress("support-sed@telangana.gov.in"));
//            bccMails.add(new InternetAddress("support-sed@telangana.gov.in"));
//            if (EMailSentService.getInstance().SendEmail(toMails, ccMails, bccMails, "Private School Registration", "Login Details")) {
//                messageOutboxDto.setMailStatus("SENT");
//
//            } else {
//                messageOutboxDto.setMailStatus("DRAFT");
//            }
//            updateMailStatus(messageOutboxDto);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void sendMessagesInEvery2Hrs() {
        List<LinkedHashMap<String, MessageOutboxDto>> mesgList = new ArrayList<LinkedHashMap<String, MessageOutboxDto>>();
        try {
            //Update msg Track
//            mesgList = CommonDAO.getInstance().getMessageOutboxDetails("mail");
//
//            for (Map<String, MessageOutboxDto> messageOutboxdto : mesgList) {
//                for (Map.Entry<String, MessageOutboxDto> messageOutboxdto1 : messageOutboxdto.entrySet()) {
//                    MessageOutboxDto dto = new MessageOutboxDto();
//                    dto = messageOutboxdto1.getValue();
//                    sendEmail(dto);

//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
            loggers.error(e);
        }
    }

    public static void updateMailStatus(MessageOutboxDto messageOutboxDto) {
        try {
//            CommonDAO.getInstance().updateMailStatus(messageOutboxDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        sendMessagesInEvery2Hrs();
    }
}