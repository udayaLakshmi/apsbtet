/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Form;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author APTOL301655
 */
public class RegisterFormPhD extends ActionForm {
private String applicationNo;    
private String casteapp;
private String otp;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getApplicationNo() {
        return applicationNo;
    }

    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    public ArrayList getUniversityList() {
        return universityList;
    }

    public String getCasteapp() {
        return casteapp;
    }

    public void setCasteapp(String casteapp) {
        this.casteapp = casteapp;
    }

    public void setUniversityList(ArrayList universityList) {
        this.universityList = universityList;
    }
  private ArrayList universityList = new ArrayList();
    public FormFile getDegreefile1() {
        return degreefile1;
    }
            private ArrayList stateList = new ArrayList();

    public ArrayList getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList stateList) {
        this.stateList = stateList;
    }

    public void setDegreefile1(FormFile degreefile1) {
        this.degreefile1 = degreefile1;
    }

    public FormFile getDegreefile2() {
        return degreefile2;
    }

    public void setDegreefile2(FormFile degreefile2) {
        this.degreefile2 = degreefile2;
    }

    public FormFile getDegreefile3() {
        return degreefile3;
    }

    public void setDegreefile3(FormFile degreefile3) {
        this.degreefile3 = degreefile3;
    }

    public FormFile getDegreefile4() {
        return degreefile4;
    }

    public void setDegreefile4(FormFile degreefile4) {
        this.degreefile4 = degreefile4;
    }

    public FormFile getPgfile1() {
        return pgfile1;
    }

    public void setPgfile1(FormFile pgfile1) {
        this.pgfile1 = pgfile1;
    }

    public FormFile getPgfile2() {
        return pgfile2;
    }

    public void setPgfile2(FormFile pgfile2) {
        this.pgfile2 = pgfile2;
    }

    public FormFile getPgfile3() {
        return pgfile3;
    }

    public void setPgfile3(FormFile pgfile3) {
        this.pgfile3 = pgfile3;
    }

    public FormFile getPgfile4() {
        return pgfile4;
    }

    public void setPgfile4(FormFile pgfile4) {
        this.pgfile4 = pgfile4;
    }

    
        private FormFile degreefile1;
        private FormFile degreefile2;
        private FormFile degreefile3;
        private FormFile degreefile4;
        private FormFile pgfile1;
        private FormFile pgfile2;
        private FormFile pgfile3;
        private FormFile pgfile4;
    public FormFile getNccuniversityfile() {
        return nccuniversityfile;
    }

    public void setNccuniversityfile(FormFile nccuniversityfile) {
        this.nccuniversityfile = nccuniversityfile;
    }

    public FormFile getNssstatefile() {
        return nssstatefile;
    }

    public void setNssstatefile(FormFile nssstatefile) {
        this.nssstatefile = nssstatefile;
    }

    public FormFile getNssunivesityfile() {
        return nssunivesityfile;
    }

    public void setNssunivesityfile(FormFile nssunivesityfile) {
        this.nssunivesityfile = nssunivesityfile;
    }

    public FormFile getSpotsstatefile() {
        return spotsstatefile;
    }

    public void setSpotsstatefile(FormFile spotsstatefile) {
        this.spotsstatefile = spotsstatefile;
    }

    public FormFile getSportsuniversityfile() {
        return sportsuniversityfile;
    }

    public void setSportsuniversityfile(FormFile sportsuniversityfile) {
        this.sportsuniversityfile = sportsuniversityfile;
    }

    public FormFile getGamesstatefile() {
        return gamesstatefile;
    }

    public void setGamesstatefile(FormFile gamesstatefile) {
        this.gamesstatefile = gamesstatefile;
    }

    public FormFile getGamesuniversityfile() {
        return gamesuniversityfile;
    }

    public void setGamesuniversityfile(FormFile gamesuniversityfile) {
        this.gamesuniversityfile = gamesuniversityfile;
    }

    public FormFile getCulturalstatefile() {
        return culturalstatefile;
    }

    public void setCulturalstatefile(FormFile culturalstatefile) {
        this.culturalstatefile = culturalstatefile;
    }

    public FormFile getCulturaluniversityfile() {
        return culturaluniversityfile;
    }

    public void setCulturaluniversityfile(FormFile culturaluniversityfile) {
        this.culturaluniversityfile = culturaluniversityfile;
    }

    public FormFile getLiteralactivitystatefile() {
        return literalactivitystatefile;
    }

    public void setLiteralactivitystatefile(FormFile literalactivitystatefile) {
        this.literalactivitystatefile = literalactivitystatefile;
    }

    public FormFile getLiteralactivityuniversityfile() {
        return literalactivityuniversityfile;
    }

    public void setLiteralactivityuniversityfile(FormFile literalactivityuniversityfile) {
        this.literalactivityuniversityfile = literalactivityuniversityfile;
    }
  private FormFile nccuniversityfile;
        
        private FormFile nssstatefile;
        private FormFile nssunivesityfile;
        
        private FormFile spotsstatefile;
        private FormFile sportsuniversityfile;
        
        private FormFile gamesstatefile;
        private FormFile gamesuniversityfile;
        
        private FormFile culturalstatefile;
        private FormFile culturaluniversityfile;
        
        private FormFile literalactivitystatefile;
        private FormFile literalactivityuniversityfile;

    private String pgmarksbsc;

    public FormFile getRankfile() {
        return rankfile;
    }

    public void setRankfile(FormFile rankfile) {
        this.rankfile = rankfile;
    }
    private String localnon;
    private FormFile rankfile;

    public String getLocalnon() {
        return localnon;
    }

    public void setLocalnon(String localnon) {
        this.localnon = localnon;
    }

    public String getPgmarksbsc() {
        return pgmarksbsc;
    }

    public void setPgmarksbsc(String pgmarksbsc) {
        this.pgmarksbsc = pgmarksbsc;
    }
    private String thesis;
    private String isentrannceyn;
    private FormFile nrirelation;
    private String systemIp;

    public String getSystemIp() {
        return systemIp;
    }

    public void setSystemIp(String systemIp) {
        this.systemIp = systemIp;
    }
    
    public String getThesis() {
        return thesis;
    }

    public void setThesis(String thesis) {
        this.thesis = thesis;
    }
    
    public String getIsentrannceyn() {
        return isentrannceyn;
    }

    public void setIsentrannceyn(String isentrannceyn) {
        this.isentrannceyn = isentrannceyn;
    }

    public FormFile getNrirelation() {
        return nrirelation;
    }

    public void setNrirelation(FormFile nrirelation) {
        this.nrirelation = nrirelation;
    }

    public FormFile getNrisponser() {
        return nrisponser;
    }

    public void setNrisponser(FormFile nrisponser) {
        this.nrisponser = nrisponser;
    }

    public FormFile getNriresidence() {
        return nriresidence;
    }

    public void setNriresidence(FormFile nriresidence) {
        this.nriresidence = nriresidence;
    }

    public FormFile getNripassport() {
        return nripassport;
    }

    public void setNripassport(FormFile nripassport) {
        this.nripassport = nripassport;
    }

    public FormFile getIndustrypayslip() {
        return industrypayslip;
    }

    public void setIndustrypayslip(FormFile industrypayslip) {
        this.industrypayslip = industrypayslip;
    }

    public FormFile getIndustryemployee() {
        return industryemployee;
    }

    public void setIndustryemployee(FormFile industryemployee) {
        this.industryemployee = industryemployee;
    }

    public FormFile getIndustryincoprate() {
        return industryincoprate;
    }

    public void setIndustryincoprate(FormFile industryincoprate) {
        this.industryincoprate = industryincoprate;
    }

    public FormFile getIndustryaudit() {
        return industryaudit;
    }

    public void setIndustryaudit(FormFile industryaudit) {
        this.industryaudit = industryaudit;
    }

    public FormFile getIndustrycompany() {
        return industrycompany;
    }

    public void setIndustrycompany(FormFile industrycompany) {
        this.industrycompany = industrycompany;
    }

    public FormFile getIndustrycopay() {
        return industrycopay;
    }

    public void setIndustrycopay(FormFile industrycopay) {
        this.industrycopay = industrycopay;
    }

    public FormFile getIndustrytax() {
        return industrytax;
    }

    public void setIndustrytax(FormFile industrytax) {
        this.industrytax = industrytax;
    }

    public FormFile getIndustrysponser() {
        return industrysponser;
    }

    public void setIndustrysponser(FormFile industrysponser) {
        this.industrysponser = industrysponser;
    }
    private FormFile nrisponser;
    private FormFile nriresidence;
    private FormFile nripassport;
    private FormFile industrypayslip;
    private FormFile industryemployee;
    private FormFile industryincoprate;
    private FormFile industryaudit;
    private FormFile industrycompany;
    private FormFile industrycopay;
    private FormFile industrytax;
    private FormFile industrysponser;
    //omkaram start
    private String mscStateII;
    private String mscStateI;
    private String msclocationI;
    private String msclocationII;
    private String mscacyearI;
    private String mscacyearII;
    private String mscdistrict21;
    private String mscdistrict22;
    private String mscdistrict11;
    private String mscdistrict12;
    private FormFile mscStudyCI;
    private FormFile mscStudyCII;
    private String pGspecialization;
    private String pGpassoutyear;
    private String pGdegreetoyear;
    private String pGuniversity;
    private String pGbscinstitute;
    private String pGogpa;
    private String pGtotalpoints;
    private String pGpreAcdMaxMarks;
    private FormFile pGogpafile;
    private FormFile pGpreAcdMarksFile;
    private String pGpreAcdObtMarks;
    private String district1;
    private String mandal1;
    private String adddistrcit1;
    private String addmandal1;
    private String padddistrcit1;
    private String paddmandal1;
    private String IVdistrict1;
    private String IIIdistrict1;
    private String IIdistrict1;
    private String idistrict1;
    private String interseniordistrict1;
    private String interjuniordistrict1;
    private String xdistrict1;

    //omkaram End
    public String getDistrict1() {
        return district1;
    }

    public void setDistrict1(String district1) {
        this.district1 = district1;
    }

    public String getMandal1() {
        return mandal1;
    }

    public void setMandal1(String mandal1) {
        this.mandal1 = mandal1;
    }

    public String getAdddistrcit1() {
        return adddistrcit1;
    }

    public void setAdddistrcit1(String adddistrcit1) {
        this.adddistrcit1 = adddistrcit1;
    }

    public String getAddmandal1() {
        return addmandal1;
    }

    public void setAddmandal1(String addmandal1) {
        this.addmandal1 = addmandal1;
    }

    public String getPadddistrcit1() {
        return padddistrcit1;
    }

    public void setPadddistrcit1(String padddistrcit1) {
        this.padddistrcit1 = padddistrcit1;
    }

    public String getPaddmandal1() {
        return paddmandal1;
    }

    public void setPaddmandal1(String paddmandal1) {
        this.paddmandal1 = paddmandal1;
    }

    public String getIVdistrict1() {
        return IVdistrict1;
    }

    public void setIVdistrict1(String IVdistrict1) {
        this.IVdistrict1 = IVdistrict1;
    }

    public String getIIIdistrict1() {
        return IIIdistrict1;
    }

    public void setIIIdistrict1(String IIIdistrict1) {
        this.IIIdistrict1 = IIIdistrict1;
    }

    public String getIIdistrict1() {
        return IIdistrict1;
    }

    public void setIIdistrict1(String IIdistrict1) {
        this.IIdistrict1 = IIdistrict1;
    }

    public String getIdistrict1() {
        return idistrict1;
    }

    public void setIdistrict1(String idistrict1) {
        this.idistrict1 = idistrict1;
    }

    public String getInterseniordistrict1() {
        return interseniordistrict1;
    }

    public void setInterseniordistrict1(String interseniordistrict1) {
        this.interseniordistrict1 = interseniordistrict1;
    }

    public String getInterjuniordistrict1() {
        return interjuniordistrict1;
    }

    public void setInterjuniordistrict1(String interjuniordistrict1) {
        this.interjuniordistrict1 = interjuniordistrict1;
    }

    public String getXdistrict1() {
        return xdistrict1;
    }

    public void setXdistrict1(String xdistrict1) {
        this.xdistrict1 = xdistrict1;
    }

    public String getpGspecialization() {
        return pGspecialization;
    }

    public void setpGspecialization(String pGspecialization) {
        this.pGspecialization = pGspecialization;
    }

    public String getpGpassoutyear() {
        return pGpassoutyear;
    }

    public void setpGpassoutyear(String pGpassoutyear) {
        this.pGpassoutyear = pGpassoutyear;
    }

    public String getpGdegreetoyear() {
        return pGdegreetoyear;
    }

    public void setpGdegreetoyear(String pGdegreetoyear) {
        this.pGdegreetoyear = pGdegreetoyear;
    }

    public String getpGuniversity() {
        return pGuniversity;
    }

    public void setpGuniversity(String pGuniversity) {
        this.pGuniversity = pGuniversity;
    }

    public String getpGbscinstitute() {
        return pGbscinstitute;
    }

    public void setpGbscinstitute(String pGbscinstitute) {
        this.pGbscinstitute = pGbscinstitute;
    }

    public String getpGogpa() {
        return pGogpa;
    }

    public void setpGogpa(String pGogpa) {
        this.pGogpa = pGogpa;
    }

    public String getpGtotalpoints() {
        return pGtotalpoints;
    }

    public void setpGtotalpoints(String pGtotalpoints) {
        this.pGtotalpoints = pGtotalpoints;
    }

    public String getpGpreAcdMaxMarks() {
        return pGpreAcdMaxMarks;
    }

    public void setpGpreAcdMaxMarks(String pGpreAcdMaxMarks) {
        this.pGpreAcdMaxMarks = pGpreAcdMaxMarks;
    }

    public FormFile getpGogpafile() {
        return pGogpafile;
    }

    public void setpGogpafile(FormFile pGogpafile) {
        this.pGogpafile = pGogpafile;
    }

    public FormFile getpGpreAcdMarksFile() {
        return pGpreAcdMarksFile;
    }

    public void setpGpreAcdMarksFile(FormFile pGpreAcdMarksFile) {
        this.pGpreAcdMarksFile = pGpreAcdMarksFile;
    }

    public String getpGpreAcdObtMarks() {
        return pGpreAcdObtMarks;
    }

    public void setpGpreAcdObtMarks(String pGpreAcdObtMarks) {
        this.pGpreAcdObtMarks = pGpreAcdObtMarks;
    }

    public String getMscStateII() {
        return mscStateII;
    }

    public void setMscStateII(String mscStateII) {
        this.mscStateII = mscStateII;
    }

    public String getMscStateI() {
        return mscStateI;
    }

    public void setMscStateI(String mscStateI) {
        this.mscStateI = mscStateI;
    }

    public String getMsclocationI() {
        return msclocationI;
    }

    public void setMsclocationI(String msclocationI) {
        this.msclocationI = msclocationI;
    }

    public String getMsclocationII() {
        return msclocationII;
    }

    public void setMsclocationII(String msclocationII) {
        this.msclocationII = msclocationII;
    }

    public String getMscacyearI() {
        return mscacyearI;
    }

    public void setMscacyearI(String mscacyearI) {
        this.mscacyearI = mscacyearI;
    }

    public String getMscacyearII() {
        return mscacyearII;
    }

    public void setMscacyearII(String mscacyearII) {
        this.mscacyearII = mscacyearII;
    }

    public void setMscdistrict21(String mscdistrict21) {
        this.mscdistrict21 = mscdistrict21;
    }

    public void setMscdistrict22(String mscdistrict22) {
        this.mscdistrict22 = mscdistrict22;
    }

    public void setMscdistrict11(String mscdistrict11) {
        this.mscdistrict11 = mscdistrict11;
    }

    public String getMscdistrict21() {
        return mscdistrict21;
    }

    public String getMscdistrict22() {
        return mscdistrict22;
    }

    public String getMscdistrict11() {
        return mscdistrict11;
    }

    public String getMscdistrict12() {
        return mscdistrict12;
    }

    public void setMscdistrict12(String mscdistrict12) {
        this.mscdistrict12 = mscdistrict12;
    }

    public FormFile getMscStudyCI() {
        return mscStudyCI;
    }

    public void setMscStudyCI(FormFile mscStudyCI) {
        this.mscStudyCI = mscStudyCI;
    }

    public FormFile getMscStudyCII() {
        return mscStudyCII;
    }

    public void setMscStudyCII(FormFile mscStudyCII) {
        this.mscStudyCII = mscStudyCII;
    }

    public FormFile getCastefile() {
        return castefile;
    }

    public void setCastefile(FormFile castefile) {
        this.castefile = castefile;
    }
    private String preAcdMaxMarks;
    private String preAcdObtMarks;
    private String bscState;
    private String iacyear;
    private FormFile castefile;

    public String getIiibscState() {
        return iiibscState;
    }

    public void setIiibscState(String iiibscState) {
        this.iiibscState = iiibscState;
    }

    public String getIibscState() {
        return iibscState;
    }

    public void setIibscState(String iibscState) {
        this.iibscState = iibscState;
    }

    public String getIbscState() {
        return ibscState;
    }

    public void setIbscState(String ibscState) {
        this.ibscState = ibscState;
    }

    public String getInterjuniorState() {
        return interjuniorState;
    }

    public void setInterjuniorState(String interjuniorState) {
        this.interjuniorState = interjuniorState;
    }
    private String interState;
    private String xState;
    private FormFile preAcdMarksFile;
    private String pphouseno;
    private String pplocality;
    private String ppstate;
    private String ppadddistrcit;
    private String iiibscState;
    private String iibscState;
    private String ibscState;
    private String interjuniorState;

    public String getPphouseno() {
        return pphouseno;
    }

    public void setPphouseno(String pphouseno) {
        this.pphouseno = pphouseno;
    }

    public String getPplocality() {
        return pplocality;
    }

    public void setPplocality(String pplocality) {
        this.pplocality = pplocality;
    }

    public String getPpstate() {
        return ppstate;
    }

    public void setPpstate(String ppstate) {
        this.ppstate = ppstate;
    }

    public String getPpadddistrcit() {
        return ppadddistrcit;
    }

    public void setPpadddistrcit(String ppadddistrcit) {
        this.ppadddistrcit = ppadddistrcit;
    }

    public String getPpaddmandal() {
        return ppaddmandal;
    }

    public void setPpaddmandal(String ppaddmandal) {
        this.ppaddmandal = ppaddmandal;
    }

    public String getPppincode() {
        return pppincode;
    }

    public void setPppincode(String pppincode) {
        this.pppincode = pppincode;
    }
    private String ppaddmandal;
    private String pppincode;

    public String getBscState() {
        return bscState;
    }

    public void setBscState(String bscState) {
        this.bscState = bscState;
    }

    public String getInterState() {
        return interState;
    }

    public void setInterState(String interState) {
        this.interState = interState;
    }

    public String getxState() {
        return xState;
    }

    public void setxState(String xState) {
        this.xState = xState;
    }
    private FormFile photo;
    private FormFile dobfile;
    private FormFile socialstatusfile;
    private FormFile cwsnfile;
    private FormFile ogpafile;
    private FormFile iVBSE;
    private FormFile iIIBSE;
    private FormFile iIBSE;
    private FormFile iBSE;
    private FormFile intersenior;
    private FormFile interjunior;
    private FormFile nonresdentialgovfile;
    private FormFile xcertificate;
    private FormFile iXcertificate;
    private FormFile vIIIcertificate;
    private FormFile vIIcertificate;
    private FormFile vIcertificate;
    private FormFile nssfile;
    private FormFile gamesfile;

    public String getPreAcdMaxMarks() {
        return preAcdMaxMarks;
    }

    public void setPreAcdMaxMarks(String preAcdMaxMarks) {
        this.preAcdMaxMarks = preAcdMaxMarks;
    }

    public String getPreAcdObtMarks() {
        return preAcdObtMarks;
    }

    public void setPreAcdObtMarks(String preAcdObtMarks) {
        this.preAcdObtMarks = preAcdObtMarks;
    }

    public FormFile getPreAcdMarksFile() {
        return preAcdMarksFile;
    }

    public void setPreAcdMarksFile(FormFile preAcdMarksFile) {
        this.preAcdMarksFile = preAcdMarksFile;
    }
    private FormFile cultfile;
    private FormFile sportsfile;
    private FormFile literalfile;
    private FormFile nccfile;
    private String mode;

    public String getXtoyear() {
        return xtoyear;
    }

    public String getDegreetoyear() {
        return degreetoyear;
    }

    public void setDegreetoyear(String degreetoyear) {
        this.degreetoyear = degreetoyear;
    }

    public void setXtoyear(String xtoyear) {
        this.xtoyear = xtoyear;
    }

    public String getIntertoyear() {
        return intertoyear;
    }

    public void setIntertoyear(String intertoyear) {
        this.intertoyear = intertoyear;
    }
    private String degreetoyear;
    private String xtoyear;
    private String intertoyear;
    private String cwsn1;

    public String getCwsn1() {
        return cwsn1;
    }

    public void setCwsn1(String cwsn1) {
        this.cwsn1 = cwsn1;
    }

    public String getCwsn2() {
        return cwsn2;
    }

    public void setCwsn2(String cwsn2) {
        this.cwsn2 = cwsn2;
    }

    public String getCwsn3() {
        return cwsn3;
    }

    public void setCwsn3(String cwsn3) {
        this.cwsn3 = cwsn3;
    }
    private String cwsn2;
    private String cwsn3;
    private String aadhar;
    private String bname;
    private String parentdet;
    private String fname;
    private String mname;
    private String inservice;
    private String designation;
    private String office;
    private String gname;
    private String grelation;
    private String dob;
    private String age;
    private String gender;
    private String state;
    private String district;
    private String mandal;
    private String village;
    private String religion;
    private String nationality;
    private String caste;
    private String subcaste;
    private String socialstatus;
    private String cwsn;
    private String catcwsn;
    private String mobile;
    private String landLine;
    private String email;
    private String houseno;
    private String locality;
    private String addmandal;
    private String adddistrcit;
    private String addstate;
    private String pincode;
    private String phouseno;
    private String plocality;
    private String paddmandal;
    private String padddistrcit;
    private String pstate;
    private String ppincode;
    private String course;
    private String subjectgroup;
    private String specialization;
    private String passoutyear;
    private String university;
    private String bscinstitute;
    private String marksbsc;
    private String ogpa;
    private String totalpoints;
    private String inap;
    private String IVacyear;
    private String IVnonlocal;
    private String IVdistrict;
    private String IIIacyear;
    private String IIInonlocal;
    private String IIIdistrict;
    private String IIacyear;
    private String IInonlocal;
    private String IIdistrict;
    private String inonlocal;
    private String idistrict;
    private String intersenioracyear;
    private String interseniornonlocal;
    private String interseniordistrict;
    private String interjunioracyear;
    private String interjuniornonlocal;
    private String interjuniordistrict;
    private String xacyear;
    private String xnonlocal;
    private String xdistrict;
    private String VIIIacyear;
    private String VIIInonlocal;
    private String VIIIdistrict;
    private String VIIacyear;
    private String VIInonlocal;
    private String VIIdistrict;
    private String VIacyear;
    private String VInonlocal;
    private String VIdistrict;
    private String nonresidance;
    private String fromdate;
    private String todate;
    private String nonresdentialfile;
    private String residancerelationship;
    private String organization;
    private String exam;
    private String applicationno;
    private String rank;
    private String eeamarks;
    private String distanceeducation;
    private String nameofcourse;
    private String pfinstitute;
    private String ncc;
    private String nss;

    public String getNcc() {
        return ncc;
    }

    public void setNcc(String ncc) {
        this.ncc = ncc;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getGames() {
        return games;
    }

    public void setGames(String games) {
        this.games = games;
    }

    public String getCultural() {
        return cultural;
    }

    public void setCultural(String cultural) {
        this.cultural = cultural;
    }

    public String getLiteral() {
        return literal;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }
    private String sports;
    private String games;
    private String cultural;
    private String literal;

    public String getResidencerelationship() {
        return residencerelationship;
    }

    public void setResidencerelationship(String residencerelationship) {
        this.residencerelationship = residencerelationship;
    }
    private String pftime;
    private String pfyear;
    private String nccinternatiaonal;
    private String residencerelationship;

    public String getDualsame() {
        return dualsame;
    }

    public void setDualsame(String dualsame) {
        this.dualsame = dualsame;
    }
    private String nccinterstate;

    public String getNccintermnational() {
        return nccintermnational;
    }

    public void setNccintermnational(String nccintermnational) {
        this.nccintermnational = nccintermnational;
    }
    private String nccuniversity;
    private String nssintermnational;
    private String nccintermnational;
    private String nssinterstate;
    private String nssuniversity;
    private String sportsintermnational;
    private String sportsinterstate;
    private String sportsuniversity;
    private String gamesintermnational;
    private String gamesinterstate;
    private String gamesuniversity;
    private String dualsame;

    public String getEeatotalmarks() {
        return eeatotalmarks;
    }

    public void setEeatotalmarks(String eeatotalmarks) {
        this.eeatotalmarks = eeatotalmarks;
    }

    public String getInsevicedesignation() {
        return insevicedesignation;
    }

    public void setInsevicedesignation(String insevicedesignation) {
        this.insevicedesignation = insevicedesignation;
    }

    public FormFile getCwsnone() {
        return cwsnone;
    }

    public void setCwsnone(FormFile cwsnone) {
        this.cwsnone = cwsnone;
    }

    public FormFile getCwsntwo() {
        return cwsntwo;
    }

    public void setCwsntwo(FormFile cwsntwo) {
        this.cwsntwo = cwsntwo;
    }
    private FormFile cwsnone;
    private FormFile cwsntwo;
    private String cultintermnational;
    private String cultinterstate;
    private String cultuniversity;
    private String literalinterstate;
    private String literalintermnational;
    private String literaluniversity;
    private String eeatotalmarks;
    private String insevicedesignation;

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getParentdet() {
        return parentdet;
    }

    public void setParentdet(String parentdet) {
        this.parentdet = parentdet;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getInservice() {
        return inservice;
    }

    public void setInservice(String inservice) {
        this.inservice = inservice;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getGrelation() {
        return grelation;
    }

    public void setGrelation(String grelation) {
        this.grelation = grelation;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getMandal() {
        return mandal;
    }

    public void setMandal(String mandal) {
        this.mandal = mandal;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getSubcaste() {
        return subcaste;
    }

    public void setSubcaste(String subcaste) {
        this.subcaste = subcaste;
    }

    public String getSocialstatus() {
        return socialstatus;
    }

    public void setSocialstatus(String socialstatus) {
        this.socialstatus = socialstatus;
    }

    public String getCwsn() {
        return cwsn;
    }

    public void setCwsn(String cwsn) {
        this.cwsn = cwsn;
    }

    public String getCatcwsn() {
        return catcwsn;
    }

    public void setCatcwsn(String catcwsn) {
        this.catcwsn = catcwsn;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHouseno() {
        return houseno;
    }

    public void setHouseno(String houseno) {
        this.houseno = houseno;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getAddmandal() {
        return addmandal;
    }

    public void setAddmandal(String addmandal) {
        this.addmandal = addmandal;
    }

    public String getAdddistrcit() {
        return adddistrcit;
    }

    public void setAdddistrcit(String adddistrcit) {
        this.adddistrcit = adddistrcit;
    }

    public String getAddstate() {
        return addstate;
    }

    public void setAddstate(String addstate) {
        this.addstate = addstate;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPhouseno() {
        return phouseno;
    }

    public void setPhouseno(String phouseno) {
        this.phouseno = phouseno;
    }

    public String getPlocality() {
        return plocality;
    }

    public void setPlocality(String plocality) {
        this.plocality = plocality;
    }

    public String getPaddmandal() {
        return paddmandal;
    }

    public void setPaddmandal(String paddmandal) {
        this.paddmandal = paddmandal;
    }

    public String getPadddistrcit() {
        return padddistrcit;
    }

    public void setPadddistrcit(String padddistrcit) {
        this.padddistrcit = padddistrcit;
    }

    public String getPstate() {
        return pstate;
    }

    public void setPstate(String pstate) {
        this.pstate = pstate;
    }

    public String getPpincode() {
        return ppincode;
    }

    public void setPpincode(String ppincode) {
        this.ppincode = ppincode;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getSubjectgroup() {
        return subjectgroup;
    }

    public void setSubjectgroup(String subjectgroup) {
        this.subjectgroup = subjectgroup;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getPassoutyear() {
        return passoutyear;
    }

    public void setPassoutyear(String passoutyear) {
        this.passoutyear = passoutyear;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getBscinstitute() {
        return bscinstitute;
    }

    public void setBscinstitute(String bscinstitute) {
        this.bscinstitute = bscinstitute;
    }

    public String getMarksbsc() {
        return marksbsc;
    }

    public void setMarksbsc(String marksbsc) {
        this.marksbsc = marksbsc;
    }

    public String getOgpa() {
        return ogpa;
    }

    public void setOgpa(String ogpa) {
        this.ogpa = ogpa;
    }

    public String getTotalpoints() {
        return totalpoints;
    }

    public void setTotalpoints(String totalpoints) {
        this.totalpoints = totalpoints;
    }

    public String getInap() {
        return inap;
    }

    public void setInap(String inap) {
        this.inap = inap;
    }

    public String getIVacyear() {
        return IVacyear;
    }

    public void setIVacyear(String IVacyear) {
        this.IVacyear = IVacyear;
    }

    public String getIVnonlocal() {
        return IVnonlocal;
    }

    public void setIVnonlocal(String IVnonlocal) {
        this.IVnonlocal = IVnonlocal;
    }

    public String getIVdistrict() {
        return IVdistrict;
    }

    public void setIVdistrict(String IVdistrict) {
        this.IVdistrict = IVdistrict;
    }

    public String getIIIacyear() {
        return IIIacyear;
    }

    public void setIIIacyear(String IIIacyear) {
        this.IIIacyear = IIIacyear;
    }

    public String getIIInonlocal() {
        return IIInonlocal;
    }

    public void setIIInonlocal(String IIInonlocal) {
        this.IIInonlocal = IIInonlocal;
    }

    public String getIIIdistrict() {
        return IIIdistrict;
    }

    public void setIIIdistrict(String IIIdistrict) {
        this.IIIdistrict = IIIdistrict;
    }

    public String getIIacyear() {
        return IIacyear;
    }

    public void setIIacyear(String IIacyear) {
        this.IIacyear = IIacyear;
    }

    public String getIInonlocal() {
        return IInonlocal;
    }

    public void setIInonlocal(String IInonlocal) {
        this.IInonlocal = IInonlocal;
    }

    public String getIIdistrict() {
        return IIdistrict;
    }

    public void setIIdistrict(String IIdistrict) {
        this.IIdistrict = IIdistrict;
    }

    public String getIacyear() {
        return iacyear;
    }

    public void setIacyear(String iacyear) {
        this.iacyear = iacyear;
    }

    public String getInonlocal() {
        return inonlocal;
    }

    public void setInonlocal(String inonlocal) {
        this.inonlocal = inonlocal;
    }

    public String getIdistrict() {
        return idistrict;
    }

    public void setIdistrict(String idistrict) {
        this.idistrict = idistrict;
    }

    public String getIntersenioracyear() {
        return intersenioracyear;
    }

    public void setIntersenioracyear(String intersenioracyear) {
        this.intersenioracyear = intersenioracyear;
    }

    public String getInterseniornonlocal() {
        return interseniornonlocal;
    }

    public void setInterseniornonlocal(String interseniornonlocal) {
        this.interseniornonlocal = interseniornonlocal;
    }

    public String getInterseniordistrict() {
        return interseniordistrict;
    }

    public void setInterseniordistrict(String interseniordistrict) {
        this.interseniordistrict = interseniordistrict;
    }

    public String getInterjunioracyear() {
        return interjunioracyear;
    }

    public void setInterjunioracyear(String interjunioracyear) {
        this.interjunioracyear = interjunioracyear;
    }

    public String getInterjuniornonlocal() {
        return interjuniornonlocal;
    }

    public String getXacyear() {
        return xacyear;
    }

    public void setXacyear(String xacyear) {
        this.xacyear = xacyear;
    }

    public void setInterjuniornonlocal(String interjuniornonlocal) {
        this.interjuniornonlocal = interjuniornonlocal;
    }

    public String getInterjuniordistrict() {
        return interjuniordistrict;
    }

    public void setInterjuniordistrict(String interjuniordistrict) {
        this.interjuniordistrict = interjuniordistrict;
    }

    public String getXnonlocal() {
        return xnonlocal;
    }

    public void setXnonlocal(String xnonlocal) {
        this.xnonlocal = xnonlocal;
    }

    public String getXdistrict() {
        return xdistrict;
    }

    public void setXdistrict(String xdistrict) {
        this.xdistrict = xdistrict;
    }

    public String getVIIIacyear() {
        return VIIIacyear;
    }

    public void setVIIIacyear(String VIIIacyear) {
        this.VIIIacyear = VIIIacyear;
    }

    public String getVIIInonlocal() {
        return VIIInonlocal;
    }

    public void setVIIInonlocal(String VIIInonlocal) {
        this.VIIInonlocal = VIIInonlocal;
    }

    public String getVIIIdistrict() {
        return VIIIdistrict;
    }

    public void setVIIIdistrict(String VIIIdistrict) {
        this.VIIIdistrict = VIIIdistrict;
    }

    public String getVIIacyear() {
        return VIIacyear;
    }

    public void setVIIacyear(String VIIacyear) {
        this.VIIacyear = VIIacyear;
    }

    public String getVIInonlocal() {
        return VIInonlocal;
    }

    public void setVIInonlocal(String VIInonlocal) {
        this.VIInonlocal = VIInonlocal;
    }

    public String getVIIdistrict() {
        return VIIdistrict;
    }

    public void setVIIdistrict(String VIIdistrict) {
        this.VIIdistrict = VIIdistrict;
    }

    public String getVIacyear() {
        return VIacyear;
    }

    public void setVIacyear(String VIacyear) {
        this.VIacyear = VIacyear;
    }

    public String getVInonlocal() {
        return VInonlocal;
    }

    public void setVInonlocal(String VInonlocal) {
        this.VInonlocal = VInonlocal;
    }

    public String getVIdistrict() {
        return VIdistrict;
    }

    public void setVIdistrict(String VIdistrict) {
        this.VIdistrict = VIdistrict;
    }

    public String getNonresidance() {
        return nonresidance;
    }

    public void setNonresidance(String nonresidance) {
        this.nonresidance = nonresidance;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getNonresdentialfile() {
        return nonresdentialfile;
    }

    public void setNonresdentialfile(String nonresdentialfile) {
        this.nonresdentialfile = nonresdentialfile;
    }

    public String getResidancerelationship() {
        return residancerelationship;
    }

    public void setResidancerelationship(String residancerelationship) {
        this.residancerelationship = residancerelationship;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getApplicationno() {
        return applicationno;
    }

    public void setApplicationno(String applicationno) {
        this.applicationno = applicationno;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getEeamarks() {
        return eeamarks;
    }

    public void setEeamarks(String eeamarks) {
        this.eeamarks = eeamarks;
    }

    public String getDistanceeducation() {
        return distanceeducation;
    }

    public void setDistanceeducation(String distanceeducation) {
        this.distanceeducation = distanceeducation;
    }

    public String getNameofcourse() {
        return nameofcourse;
    }

    public void setNameofcourse(String nameofcourse) {
        this.nameofcourse = nameofcourse;
    }

    public String getPfinstitute() {
        return pfinstitute;
    }

    public void setPfinstitute(String pfinstitute) {
        this.pfinstitute = pfinstitute;
    }

    public String getPftime() {
        return pftime;
    }

    public void setPftime(String pftime) {
        this.pftime = pftime;
    }

    public String getPfyear() {
        return pfyear;
    }

    public void setPfyear(String pfyear) {
        this.pfyear = pfyear;
    }

    public String getNccinternatiaonal() {
        return nccinternatiaonal;
    }

    public void setNccinternatiaonal(String nccinternatiaonal) {
        this.nccinternatiaonal = nccinternatiaonal;
    }

    public String getNccinterstate() {
        return nccinterstate;
    }

    public void setNccinterstate(String nccinterstate) {
        this.nccinterstate = nccinterstate;
    }

    public String getNccuniversity() {
        return nccuniversity;
    }

    public void setNccuniversity(String nccuniversity) {
        this.nccuniversity = nccuniversity;
    }

    public String getNssintermnational() {
        return nssintermnational;
    }

    public void setNssintermnational(String nssintermnational) {
        this.nssintermnational = nssintermnational;
    }

    public String getNssinterstate() {
        return nssinterstate;
    }

    public void setNssinterstate(String nssinterstate) {
        this.nssinterstate = nssinterstate;
    }

    public String getNssuniversity() {
        return nssuniversity;
    }

    public void setNssuniversity(String nssuniversity) {
        this.nssuniversity = nssuniversity;
    }

    public String getSportsintermnational() {
        return sportsintermnational;
    }

    public void setSportsintermnational(String sportsintermnational) {
        this.sportsintermnational = sportsintermnational;
    }

    public String getSportsinterstate() {
        return sportsinterstate;
    }

    public void setSportsinterstate(String sportsinterstate) {
        this.sportsinterstate = sportsinterstate;
    }

    public String getSportsuniversity() {
        return sportsuniversity;
    }

    public void setSportsuniversity(String sportsuniversity) {
        this.sportsuniversity = sportsuniversity;
    }

    public String getGamesintermnational() {
        return gamesintermnational;
    }

    public void setGamesintermnational(String gamesintermnational) {
        this.gamesintermnational = gamesintermnational;
    }

    public String getGamesinterstate() {
        return gamesinterstate;
    }

    public void setGamesinterstate(String gamesinterstate) {
        this.gamesinterstate = gamesinterstate;
    }

    public String getGamesuniversity() {
        return gamesuniversity;
    }

    public void setGamesuniversity(String gamesuniversity) {
        this.gamesuniversity = gamesuniversity;
    }

    public String getCultintermnational() {
        return cultintermnational;
    }

    public void setCultintermnational(String cultintermnational) {
        this.cultintermnational = cultintermnational;
    }

    public String getCultinterstate() {
        return cultinterstate;
    }

    public void setCultinterstate(String cultinterstate) {
        this.cultinterstate = cultinterstate;
    }

    public String getCultuniversity() {
        return cultuniversity;
    }

    public void setCultuniversity(String cultuniversity) {
        this.cultuniversity = cultuniversity;
    }

    public String getLiteralinterstate() {
        return literalinterstate;
    }

    public void setLiteralinterstate(String literalinterstate) {
        this.literalinterstate = literalinterstate;
    }

    public String getLiteralintermnational() {
        return literalintermnational;
    }

    public void setLiteralintermnational(String literalintermnational) {
        this.literalintermnational = literalintermnational;
    }

    public String getLiteraluniversity() {
        return literaluniversity;
    }

    public void setLiteraluniversity(String literaluniversity) {
        this.literaluniversity = literaluniversity;
    }

    public FormFile getPhoto() {
        return photo;
    }

    public void setPhoto(FormFile photo) {
        this.photo = photo;
    }

    public FormFile getNonresdentialgovfile() {
        return nonresdentialgovfile;
    }

    public void setNonresdentialgovfile(FormFile nonresdentialgovfile) {
        this.nonresdentialgovfile = nonresdentialgovfile;
    }

    public FormFile getDobfile() {
        return dobfile;
    }

    public void setDobfile(FormFile dobfile) {
        this.dobfile = dobfile;
    }

    public FormFile getSocialstatusfile() {
        return socialstatusfile;
    }

    public void setSocialstatusfile(FormFile socialstatusfile) {
        this.socialstatusfile = socialstatusfile;
    }

    public FormFile getCwsnfile() {
        return cwsnfile;
    }

    public void setCwsnfile(FormFile cwsnfile) {
        this.cwsnfile = cwsnfile;
    }

    public FormFile getOgpafile() {
        return ogpafile;
    }

    public void setOgpafile(FormFile ogpafile) {
        this.ogpafile = ogpafile;
    }

    public FormFile getiVBSE() {
        return iVBSE;
    }

    public void setiVBSE(FormFile iVBSE) {
        this.iVBSE = iVBSE;
    }

    public FormFile getiIIBSE() {
        return iIIBSE;
    }

    public void setiIIBSE(FormFile iIIBSE) {
        this.iIIBSE = iIIBSE;
    }

    public FormFile getiIBSE() {
        return iIBSE;
    }

    public void setiIBSE(FormFile iIBSE) {
        this.iIBSE = iIBSE;
    }

    public FormFile getiBSE() {
        return iBSE;
    }

    public void setiBSE(FormFile iBSE) {
        this.iBSE = iBSE;
    }

    public FormFile getIntersenior() {
        return intersenior;
    }

    public void setIntersenior(FormFile intersenior) {
        this.intersenior = intersenior;
    }

    public FormFile getInterjunior() {
        return interjunior;
    }

    public void setInterjunior(FormFile interjunior) {
        this.interjunior = interjunior;
    }

    public FormFile getXcertificate() {
        return xcertificate;
    }

    public void setXcertificate(FormFile xcertificate) {
        this.xcertificate = xcertificate;
    }

    public FormFile getiXcertificate() {
        return iXcertificate;
    }

    public void setiXcertificate(FormFile iXcertificate) {
        this.iXcertificate = iXcertificate;
    }

    public FormFile getvIIIcertificate() {
        return vIIIcertificate;
    }

    public void setvIIIcertificate(FormFile vIIIcertificate) {
        this.vIIIcertificate = vIIIcertificate;
    }

    public FormFile getvIIcertificate() {
        return vIIcertificate;
    }

    public void setvIIcertificate(FormFile vIIcertificate) {
        this.vIIcertificate = vIIcertificate;
    }

    public FormFile getvIcertificate() {
        return vIcertificate;
    }

    public void setvIcertificate(FormFile vIcertificate) {
        this.vIcertificate = vIcertificate;
    }

    public FormFile getNssfile() {
        return nssfile;
    }

    public void setNssfile(FormFile nssfile) {
        this.nssfile = nssfile;
    }

    public FormFile getGamesfile() {
        return gamesfile;
    }

    public void setGamesfile(FormFile gamesfile) {
        this.gamesfile = gamesfile;
    }

    public FormFile getCultfile() {
        return cultfile;
    }

    public void setCultfile(FormFile cultfile) {
        this.cultfile = cultfile;
    }

    public FormFile getSportsfile() {
        return sportsfile;
    }

    public void setSportsfile(FormFile sportsfile) {
        this.sportsfile = sportsfile;
    }

    public FormFile getLiteralfile() {
        return literalfile;
    }

    public void setLiteralfile(FormFile literalfile) {
        this.literalfile = literalfile;
    }

    public FormFile getNccfile() {
        return nccfile;
    }

    public void setNccfile(FormFile nccfile) {
        this.nccfile = nccfile;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
      // Janakiramaiah Peddi
    private String paymentmode;
    private double amount;
    private String appNoPay;
    private String appNamePay;
    private String mobilePay;
    private String reqId;

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getAppNoPay() {
        return appNoPay;
    }

    public void setAppNoPay(String appNoPay) {
        this.appNoPay = appNoPay;
    }

    public String getAppNamePay() {
        return appNamePay;
    }

    public void setAppNamePay(String appNamePay) {
        this.appNamePay = appNamePay;
    }

    public String getMobilePay() {
        return mobilePay;
    }

    public void setMobilePay(String mobilePay) {
        this.mobilePay = mobilePay;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

}
