/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.DAO;

import com.ap.WithLoginDAO.*;
import com.ap.DAO.*;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64;
import com.sms.util.MessagePropertiesUtil;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import jxl.write.DateTime;
import net.apo.angrau.dao.GenerateQrAndBarCode;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author ci96067
 */
public class HallticketDownloadDiplomaDao {

    public ArrayList getApplicationStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = null;
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_DiplomaAplicationStatus_HallTicket (?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim()); ///Reg No
            cstmt.setString(2, rform.getGrade().trim()); ///Mobile No

            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                map = new HashMap<String, String>();
                map.put("key", rs.getString(1));
                map.put("keyValue", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
//        System.out.println("hhhhhhhhh"+list);
        return list;
    }

    public static ArrayList getSbjectsList(String regNO) throws Exception {
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_Diploma_HallTicketSubjectDetails_Get(?)}");
            cstmt.setString(1, regNO); ///Reg No

            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("sno", rs.getString(1));
                map.put("scheme", rs.getString(2));
                map.put("year", rs.getString(3));
                map.put("subCode", rs.getString(4));
                map.put("subName", rs.getString(5));

                list.add(map);
                map = null;
            }
        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public static ArrayList getTransactionList(String regNO) throws Exception {
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_Diploma_HallTicketPaymentDetails_Get(?)}");
            cstmt.setString(1, regNO); ///Reg No

            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("sno", rs.getString(1));
                map.put("pin", rs.getString(2));
                map.put("transanction", rs.getString(3));
                map.put("merchant", rs.getString(4));

                list.add(map);
                map = null;
            }
        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public ArrayList getData(RegisterForm rform) throws Exception {
        HashMap map = null;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {

            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_Diploma_HallTicketDetails_Get(?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim()); //Registration No
            cstmt.setString(2, rform.getGrade2().trim()); //systemIp
            // System.out.println("rform.getAadhar1().trim()==" + rform.getAadhar1().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                //  System.out.println("rform.getAadhar1().trim()==" + rform.getAadhar1().trim());
                map = new HashMap();
                map.put("monthYear", rs.getString(1));
                map.put("pin", rs.getString(2));
                map.put("exmCode", rs.getString(3));
                map.put("name", rs.getString(4));
                map.put("fname", rs.getString(5));
                map.put("branchName", rs.getString(6));
                map.put("scheme", rs.getString(7));
                //  map.put("photo", rs.getBytes(8));

                if (rs.getBytes(8) != null && rs.getBytes(8).length > 0) {
                    map.put("photo", DatatypeConverter.printBase64Binary(rs.getBytes(8)).trim());
                } else if (rs.getString(9) != null && rs.getString(9) != "") {
                    map.put("photo", getImageInBase64Format(rform.getAadhar1().trim(), rs.getString(9)));
                } else {
                    map.put("photo", "");
                }
//                String profilePic = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(36) + "\\" + rs.getString(24) + "\\" + rs.getString(25);
//                map.put("filepath", profilePic);
//
//                String signaturePath = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(36) + "\\" + rs.getString(24) + "\\" + rs.getString(33);
//                map.put("signature", signaturePath);

                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public void getPdfDataNew(String filepath, String filename, ArrayList list, String regNo) throws SQLException {

        Font header = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD, BaseColor.BLACK);
        Font italicNormal = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, new BaseColor(0, 0, 0)); //MAGENTA -Pink
        Font italicNormal1 = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, new BaseColor(0, 0, 0)); //MAGENTA -Pink
        Font subheading = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.BLUE);
        Font normalSizeBold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, new BaseColor(0, 0, 0));
        Font italic = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.DARK_GRAY);
        Font italicbold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
        Font headingUnderline = new Font(Font.FontFamily.HELVETICA, 10, Font.UNDERLINE, new BaseColor(0, 0, 0, 1));
        Font instrinctionsFont = new Font(Font.FontFamily.HELVETICA, 11, Font.NORMAL, BaseColor.BLACK);

        try {
            String Imgphoto = "E:\\Diploma\\SBTET\\SBTET LOGO.png";

            HashMap map = (HashMap) list.get(0);
            String hallticket = map.get("pin").toString(); ///IS6350721104
            String monthYr = map.get("monthYear").toString();
            String exmCode = map.get("exmCode").toString();

            String name = map.get("name").toString();
            String fname = map.get("fname").toString();
            String branch = map.get("branchName").toString();
            String scheme = map.get("scheme").toString();
            String photo = map.get("photo").toString();


            // Image photo = null;


            Document document = new Document(PageSize.A4, 35f, 30f, 45f, 25f);
            OutputStream file = new FileOutputStream(new File(filepath + filename));
            PdfWriter writer = PdfWriter.getInstance(document, file);

            document.open();


            PdfPTable table1 = new PdfPTable(2);
            table1.setWidths(new float[]{3, 1});
            table1.setWidthPercentage(100);
            table1.getDefaultCell().setBorderColor(BaseColor.BLACK);
            table1.getDefaultCell().setBorderWidth(1.2f);
            PdfPCell hcell1 = null;

            PdfPTable headerTable = new PdfPTable(2);
            headerTable.setWidthPercentage(60);
            headerTable.setWidths(new float[]{0.8f, 5});
            PdfPCell headerscell = null;

            File myFile1 = new File(Imgphoto);
            Image imgp = null;
            //  System.out.println("myFile1.exists()===="+myFile1.exists());
            if (myFile1.exists()) {
                imgp = Image.getInstance(Imgphoto);
                imgp.scaleToFit(55f, 55f);
                imgp.setAbsolutePosition(25f, 25f);
               // imgp.setBorder(Rectangle.BOX);
               // imgp.setBorderWidth(1);
                headerscell = new PdfPCell(imgp);
                headerscell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                headerscell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                headerscell.setFixedHeight(55);
             //   headerscell.setBorderWidthTop(0);
                headerscell.setBorderWidthBottom(0);
                headerscell.setBorderWidthRight(0);
                headerTable.addCell(headerscell);
            }
            Paragraph par = new Paragraph("\n");
            Chunk p = new Chunk("                 STATE BOARD OF TECHNICAL EDUCATION & TRAINING,AP \n "
                    + "                                              DIPLOMA EXAMINATIONS\n ", header);
            par.add(p);

            p = new Chunk("                                                      ORIGINAL HALL TICKET", normalSizeBold);
            par.add(p);
            par.setSpacingBefore(20f);
//            p = new Chunk("\n\n ");
//            par.add(p); 

            headerscell = new PdfPCell(par);
            headerscell.setColspan(2);
           // headerscell.setHorizontalAlignment(Element.ALIGN_CENTER);
           //  headerscell.setBorderWidthRight(0);
          //   headerscell.setBorderWidthTop(0);
              headerscell.setBorderWidthLeft(0);
            headerscell.setBorderWidthBottom(0);
            headerTable.addCell(headerscell);

            hcell1 = new PdfPCell(headerTable);
            // hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setColspan(2);
            table1.addCell(hcell1);
// Ender of Header Section
            
            hcell1 = new PdfPCell(new Paragraph("\n"));
            hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0);
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderWidthBottom(0);
            table1.addCell(hcell1);

            // File myFile1 = new File(Imgphoto);
            //  System.out.println("myFile1.exists()===="+myFile1.exists());
            if (photo != null && photo != "") {
//                byte[] bytes = Base64.decode(photo);
                byte[] bytes = Base64.decode(photo);

                imgp = Image.getInstance(bytes);

                imgp.scaleToFit(80f, 70f);
                imgp.setAbsolutePosition(15f, 15f);
                imgp.setBorder(Rectangle.BOX);
                imgp.setBorderWidth(1);
                hcell1 = new PdfPCell(imgp);
                hcell1.setRowspan(4);
                hcell1.setBorderWidthTop(0);
                hcell1.setBorderWidthBottom(0);
                hcell1.setBorderWidthLeft(0);
                hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(hcell1);

            } else {
                //  System.out.println("else");
                Paragraph p1 = new Paragraph("\n\n\n  \n\n\n\n\n\n", normalSizeBold);
                PdfPCell cell1p = new PdfPCell(p1);
                cell1p.setRowspan(4);
                cell1p.setBorderWidthTop(0);
                cell1p.setBorderWidthBottom(0);
                cell1p.setBorderWidthLeft(0);
                cell1p.setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(cell1p);
            }



            par = new Paragraph("");
            p = new Chunk("MONTH & YEAR OF EXAM:        ", italic);
            par.add(p);
            p = new Chunk(monthYr, italicbold);
            par.add(p);
            hcell1 = new PdfPCell(par);
            hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0);
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);

            table1.addCell(hcell1);




            par = new Paragraph("");
            p = new Chunk("PIN  NO :             ", italic);
            par.add(p);
            p = new Chunk(hallticket, italicbold);
            par.add(p);
            hcell1 = new PdfPCell(par);
            hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0);
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(hcell1);

            par = new Paragraph("");
            p = new Chunk("Examination Centre :   ", italic);
            par.add(p);
            p = new Chunk(exmCode, italicbold);
            par.add(p);
            hcell1 = new PdfPCell(par);
            hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0);
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            // hcell1.setColspan(2);
            table1.addCell(hcell1);

            par = new Paragraph("");
            p = new Chunk("Certified that Sri/Kum/Smt ", italicNormal);
            par.add(p);
            p = new Chunk(name, italicbold);
            par.add(p);
            p = new Chunk(" S/oD/oSri ", italicNormal);
            par.add(p);
            p = new Chunk(fname, italicbold);
            par.add(p);
            p = new Chunk(" is a candidate for the under mentioned examination.", italicNormal);
            par.add(p);
            hcell1 = new PdfPCell(par);
            hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setColspan(2);
            table1.addCell(hcell1);

            par = new Paragraph("\n");
            p = new Chunk("   Branch:  ", italic);
            par.add(p);
            p = new Chunk(branch, italicbold);
            par.add(p);
            hcell1 = new PdfPCell(par);
            hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthRight(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell(hcell1);

            par = new Paragraph("\n");
            p = new Chunk("   Scheme:     ", italic);
            par.add(p);
            p = new Chunk(scheme, italicbold);
            par.add(p);
            hcell1 = new PdfPCell(par);
            hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthLeft(0);
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.addCell(hcell1);

//            par = new Paragraph("");
//            p = new Chunk("             For Secretary, SBTET, AP.     ", italicNormal);
//            par.add(p);
//
//            hcell1 = new PdfPCell(par);
//            hcell1.setColspan(2);
//            hcell1.setBorderWidthTop(0);
//            hcell1.setBorderWidthBottom(0);
//
//            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
//            table1.addCell(hcell1);



            //Transanction Table
//            ArrayList translist = getTransactionList(regNo);
//            if (translist != null && translist.size() > 0) {
//                PdfPTable innerTransactionTable = new PdfPTable(3);
//                innerTransactionTable.setWidthPercentage(100);
//                innerTransactionTable.setWidths(new int[]{1, 3, 3});
//
//                PdfPCell intranscell = new PdfPCell(new Paragraph("S.No", normalSizeBold));
//                intranscell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                // intranscell.setBorder(Rectangle.NO_BORDER);
//                //intranscell.setFixedHeight(13f);
//                innerTransactionTable.addCell(intranscell);
//
//                intranscell = new PdfPCell(new Paragraph("Transaction Ref No.", normalSizeBold));
//                intranscell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                // intranscell.setBorder(Rectangle.NO_BORDER);
//                // intranscell.setFixedHeight(13f);
//                innerTransactionTable.addCell(intranscell);
//
//                intranscell = new PdfPCell(new Paragraph("Merchant Ref No.", normalSizeBold));
//                intranscell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                // intranscell.setBorder(Rectangle.NO_BORDER);
//                //intranscell.setFixedHeight(13f);
//                innerTransactionTable.addCell(intranscell);
//                HashMap tansMap = null;
//                for (int i = 0; i < translist.size(); i++) {
//                    tansMap = (HashMap) translist.get(i);
//
//                    intranscell = new PdfPCell(new Paragraph("" + tansMap.get("sno"), italicNormal));
//                    intranscell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    // intranscell.setBorder(Rectangle.NO_BORDER);
//                    //intranscell.setFixedHeight(13f);
//                    innerTransactionTable.addCell(intranscell);
//
//                    intranscell = new PdfPCell(new Paragraph("" + tansMap.get("transanction"), italicNormal));
//                    intranscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                    // intranscell.setBorder(Rectangle.NO_BORDER);
//                    // intranscell.setFixedHeight(13f);
//                    innerTransactionTable.addCell(intranscell);
//
//                    intranscell = new PdfPCell(new Paragraph("" + tansMap.get("merchant"), italicNormal));
//                    intranscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                    // intranscell.setBorder(Rectangle.NO_BORDER);
//                    //intranscell.setFixedHeight(13f);
//                    innerTransactionTable.addCell(intranscell);
//                }
//                hcell1 = new PdfPCell(innerTransactionTable);
//                hcell1.setColspan(2);
//                table1.addCell(hcell1);
//            }
            hcell1 = new PdfPCell(new Paragraph("\n"));
            hcell1.setBorderWidthTop(0);
            hcell1.setBorderWidthBottom(0);
            hcell1.setColspan(2);
            table1.addCell(hcell1);

            //Subject Details Table
            ArrayList sublist = getSbjectsList(regNo);
            if (sublist != null && sublist.size() > 0) {
                PdfPTable innersubTable = new PdfPTable(6);
                innersubTable.setWidthPercentage(100);
                innersubTable.setWidths(new float[]{0.5f, 0.8f, 1, 1, 3, 2});

                PdfPCell insubcell = new PdfPCell(new Paragraph("S.No", normalSizeBold));
                insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                // intranscell.setBorder(Rectangle.NO_BORDER);
                //intranscell.setFixedHeight(13f);
                innersubTable.addCell(insubcell);

                insubcell = new PdfPCell(new Paragraph("Scheme", normalSizeBold));
                insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                // intranscell.setBorder(Rectangle.NO_BORDER);
                // intranscell.setFixedHeight(13f);
                innersubTable.addCell(insubcell);

                insubcell = new PdfPCell(new Paragraph("Year / Sem", normalSizeBold));
                insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                // intranscell.setBorder(Rectangle.NO_BORDER);
                //intranscell.setFixedHeight(13f);
                innersubTable.addCell(insubcell);

                insubcell = new PdfPCell(new Paragraph("Subject Code", normalSizeBold));
                insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                // intranscell.setBorder(Rectangle.NO_BORDER);
                //intranscell.setFixedHeight(13f);
                innersubTable.addCell(insubcell);

                insubcell = new PdfPCell(new Paragraph("Name of Subject", normalSizeBold));
                insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                // intranscell.setBorder(Rectangle.NO_BORDER);
                //intranscell.setFixedHeight(13f);
                innersubTable.addCell(insubcell);

                insubcell = new PdfPCell(new Paragraph("Signature of Invigilator", normalSizeBold));
                insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                // intranscell.setBorder(Rectangle.NO_BORDER);
                //intranscell.setFixedHeight(13f);
                innersubTable.addCell(insubcell);

                HashMap subMap = null;
                for (int i = 0; i < sublist.size(); i++) {
                    subMap = (HashMap) sublist.get(i);

                    insubcell = new PdfPCell(new Paragraph("" + subMap.get("sno"), italicNormal));
                    insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    // intranscell.setBorder(Rectangle.NO_BORDER);
                    //intranscell.setFixedHeight(13f);
                    innersubTable.addCell(insubcell);

                    insubcell = new PdfPCell(new Paragraph("" + subMap.get("scheme"), italicNormal));
                    insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    // intranscell.setBorder(Rectangle.NO_BORDER);
                    // intranscell.setFixedHeight(13f);
                    innersubTable.addCell(insubcell);

                    insubcell = new PdfPCell(new Paragraph("" + subMap.get("year"), italicNormal));
                    insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    // intranscell.setBorder(Rectangle.NO_BORDER);
                    //intranscell.setFixedHeight(13f);
                    innersubTable.addCell(insubcell);

                    insubcell = new PdfPCell(new Paragraph("" + subMap.get("subCode"), italicNormal));
                    insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    // intranscell.setBorder(Rectangle.NO_BORDER);
                    //intranscell.setFixedHeight(13f);
                    innersubTable.addCell(insubcell);

                    insubcell = new PdfPCell(new Paragraph("" + subMap.get("subName"), italicNormal1));
                    insubcell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    // intranscell.setBorder(Rectangle.NO_BORDER);
                    //intranscell.setFixedHeight(13f);
                    innersubTable.addCell(insubcell);

                    insubcell = new PdfPCell(new Paragraph("  ", italicNormal));
                    insubcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    // intranscell.setBorder(Rectangle.NO_BORDER);
                    //intranscell.setFixedHeight(13f);
                    innersubTable.addCell(insubcell);


                }
                hcell1 = new PdfPCell(innersubTable);
                hcell1.setColspan(2);
                table1.addCell(hcell1);
            }

            hcell1 = new PdfPCell(new Paragraph("\n"));
            hcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            hcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            hcell1.setColspan(2);
            table1.addCell(hcell1);
            //signature Part
            PdfPTable inssigtable = new PdfPTable(2);
            inssigtable.setWidthPercentage(100);

            inssigtable.setWidths(new float[]{3f, 5f});

            PdfPCell inssigcell = new PdfPCell(new Paragraph("   Marks of Identification:(As per SSC)", italicNormal));
            inssigcell.setBorderWidthBottom(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthTop(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthRight(Rectangle.NO_BORDER);
            inssigcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            inssigcell.setRowspan(2);
            inssigtable.addCell(inssigcell);

            inssigcell = new PdfPCell(new Paragraph("1)", italicNormal));
            inssigcell.setBorderWidthBottom(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthTop(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthLeft(Rectangle.NO_BORDER);
            inssigcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            inssigtable.addCell(inssigcell);

            inssigcell = new PdfPCell(new Paragraph("\n 2)", italicNormal));
            inssigcell.setBorderWidthBottom(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthTop(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthLeft(Rectangle.NO_BORDER);
            inssigcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            inssigtable.addCell(inssigcell);

            inssigcell = new PdfPCell(new Paragraph("\n"));
            inssigcell.setBorderWidthBottom(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthTop(Rectangle.NO_BORDER);
            inssigcell.setColspan(2);
            inssigcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            inssigtable.addCell(inssigcell);

            inssigcell = new PdfPCell(new Paragraph("\n Signature of Candidate", italicNormal));
            inssigcell.setBorderWidthBottom(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthTop(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthRight(Rectangle.NO_BORDER);
            inssigcell.setHorizontalAlignment(Element.ALIGN_LEFT);
            inssigtable.addCell(inssigcell);


            inssigcell = new PdfPCell(new Paragraph("\n  Signature of Head of Section", italicNormal));
            inssigcell.setBorderWidthBottom(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthTop(Rectangle.NO_BORDER);
            inssigcell.setBorderWidthLeft(Rectangle.NO_BORDER);
            inssigcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            inssigtable.addCell(inssigcell);

            hcell1 = new PdfPCell(inssigtable);
            hcell1.setColspan(2);
            hcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            hcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            table1.addCell(hcell1);

            document.add(table1);
            //For Instructions start
            PdfPTable instable1 = new PdfPTable(2);
            instable1.setWidthPercentage(100);
            float[] instcolumnWidthsFOrTable1 = {0.6f, 10f};
            instable1.setWidths(instcolumnWidthsFOrTable1);

            PdfPCell insthcell1 = null;

            insthcell1 = new PdfPCell(new Paragraph(" \n", normalSizeBold));
            insthcell1.setColspan(2);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" INSTRUCTIONS", normalSizeBold));
            insthcell1.setColspan(2);
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
//   insthcell1.setFixedHeight(13f);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 1.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);

            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("No candidate will be allowed to enter the examination hall without proper hall-ticket.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 2.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Candidate shall arrive at the examination center at least 30 minutes before the commencement of the examination. While entering the examination hall every student shall deposit all books, personal belongings etc at counter provided.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 3.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("No printed/written material, in any form, shall be taken inside the exam hall, other than hall ticket.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 4.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Every student shall cooperate while pockets are being checked or frisked.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 5.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Candidates will not be allowed to enter the examination hall, after half an hour of the commencement of examination.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 6.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Candidates will not be allowed to leave the examination hall before one hour from the commencement of examination. After one hour if any candidate wishes to leave or wants to attend any emergency needs, the candidate must return his / her answer script along with question paper to the Invigilator.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 7.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Candidates are advised to go through the instructions given on Answer Booklet or OMR Bar Code Sheet before starting answering.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 8.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Candidates should not write any matter inside the Answer Booklet which may lead to the identification of the candidate or institution. If he/she does so, he/she will be booked under malpractice. No color sketch pens are to be used unless specified question.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 9.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Candidate should carry their own Scientific Calculators, Pens and Pencils, required drawing instruments and shall answer the questions with blue or black pen only.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 10.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Candidates will not be allowed with Cell Phones, Pagers, Organizers, PDA’s and palm tops or any other Electronic Gadgets, etc.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 11.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Every student shall follow the regulations during examinations, failing which he/she will be booked under malpractice case. ", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 12.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("Candidates are advised to check all the pages in the ’32-page Answer Booklet’ supplied to him/her. All answers are to be written within the given booklet only.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" 13.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            //insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthRight(Rectangle.NO_BORDER);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph(" No additional sheet/sheets will be supplied under any circumstances.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorderWidthTop(Rectangle.NO_BORDER);
            // insthcell1.setBorderWidthBottom(Rectangle.NO_BORDER);
            insthcell1.setBorderWidthLeft(Rectangle.NO_BORDER);
            // insthcell1.setColspan(6);
            instable1.addCell(insthcell1);


//            hcell1 = new PdfPCell(instable1);
//            hcell1.setColspan(2);
//            table1.addCell(hcell1);

            document.add(instable1);
            //Instructions End
            Paragraph dateP = new Paragraph("\n  Generated Date : " + new Date());
            dateP.setAlignment(Element.ALIGN_RIGHT);
            document.add(dateP);
            document.close();

        } catch (Exception ex) {
            //  logger.info("1======================"+ex.getMessage());
            ex.printStackTrace();
        } finally {
            // con.close(); 
        }
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "D://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getImageInBase64Format(String pin, String filename) throws Exception {

        //   String path = "\\\\10.3.37.204\\E$\\ISMS\\UDISE\\";
        //   String path = "\\\\10.138.133.8\\D$\\Diploma\\";
        String path = "\\\\10.96.64.51\\E$\\Diploma\\";

        String filepath = null;
        String b64 = "";
        ByteArrayOutputStream baos = null;
        BufferedImage image = null;


        try {
            filepath = path + pin + "\\" + filename;
            //  System.out.println("filepath===="+filepath);
            File file = new File(filepath);
            if (!file.exists()) {
                return "";
            }
            image = ImageIO.read(file);
            baos = new ByteArrayOutputStream();
            int lastIndex = file.getName().lastIndexOf('.');

            String extenstion = file.getName().substring(lastIndex);
            if (extenstion != null && extenstion.equalsIgnoreCase(".jpg")) {
                ImageIO.write(image, "jpg", baos);
            } else {
                ImageIO.write(image, "png", baos);
            }

            baos.flush();
            byte[] imageInByteArray = baos.toByteArray();
            b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                baos.close();
            }

            if (image != null) {
            }
        }
        //  System.out.println("b64===="+b64);
        return b64;
    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.trim().split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {

            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1).toLowerCase()).append(" ");
        }
        //   System.out.println("sb.toString()==="+sb.toString());
        return sb.toString().trim();
    }
}
