/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.DAO;

import com.ap.WithLoginDAO.*;
import com.ap.DAO.*;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.MessagePropertiesUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.GenerateQrAndBarCode;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author ci96067
 */
public class HallticketDownloadLoginDao {

    public ArrayList getApplicationStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map =null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_AplicationStatus_HallTicket (?,?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim()); ///Reg No
            cstmt.setString(2, rform.getGrade().trim()); ///Mobile No
            cstmt.setString(3, rform.getUserName().trim()); ///Institute Code
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                map = new HashMap<String, String>();
                map.put("key", rs.getString(1));
                map.put("keyValue", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select Grade,GRADE_DESC from TWSH_GRADE_MASTER with(nolock) order by RowID asc";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }
    
    public ArrayList getData(RegisterForm rform) throws Exception {
        HashMap map = null;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_HallTicket_Institute(?,?,?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim()); //Registration No
            cstmt.setString(2, rform.getGrade1().trim()); //Mobile No
            cstmt.setString(3, rform.getGrade2().trim()); //SystemIp
            cstmt.setString(4, rform.getUserName().trim()); //userName
            System.out.println(" "+rform.getAadhar1().trim());
            System.out.println(" "+rform.getGrade1().trim());
            System.out.println(" "+rform.getGrade2().trim());
            System.out.println(" "+rform.getUserName().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                map = new HashMap();
                map.put("name", rs.getString(1));
                //map.put("name", "SAILAJA");
                map.put("fname", rs.getString(2));
                map.put("gender", rs.getString(4));
                map.put("caste", rs.getString(5));

                map.put("visImp", rs.getString(6));
                map.put("institution", rs.getString(7));
                map.put("examinationAppearing", rs.getString(8));
                map.put("language", rs.getString(9));
                map.put("grade", rs.getString(10));
                map.put("examinationDistrict", rs.getString(11));

                map.put("examinationCenter", rs.getString(12));
                map.put("examinationDate", rs.getString(13));
                //map.put("examinationDate", "27-08-2021");
                map.put("examinationBatch", rs.getString(14));
                map.put("hNo", rs.getString(15));
                map.put("street", rs.getString(16));

                map.put("village", rs.getString(17));
                map.put("state", rs.getString(18));
                map.put("district", rs.getString(19));
                map.put("mandal", rs.getString(20));
                map.put("pincode", rs.getString(21));

                map.put("mobile", rs.getString(22));
                map.put("email", rs.getString(23));
                map.put("aadhar", rs.getString(24));

                map.put("RegNo", rs.getString(26));
                map.put("paymentRefNo", rs.getString(28));
                map.put("paymentAmount", rs.getString(29));
                map.put("paymentDate", rs.getString(30));
                map.put("dob", rs.getString(3));

                map.put("institutionCode", rs.getString(32));
                map.put("codeForGenerateQR", rs.getString(34));
                map.put("codeForGenerateBar", rs.getString(35));

                map.put("gradeCode", rform.getGrade1().trim());
                
                map.put("part2", rs.getString(37));
                map.put("part1", rs.getString(38));


                String profilePic = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(36) + "\\" + rs.getString(24) + "\\" + rs.getString(25);
                map.put("filepath", profilePic);

                String signaturePath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(36) + "\\" + rs.getString(24) + "\\" + rs.getString(33);
                map.put("signature", signaturePath);

                String qrUploadFileLoadingpath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(36) + "\\" + rs.getString(24) + "\\" + rs.getString(24) + "_QRCODE.PNG";
                
                String barUploadFileLoadingpath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(36) + "\\" + rs.getString(24) + "\\" + rs.getString(24) + "_BARCODE.PNG";
                
               // String controlarSig = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(36) + "\\" + rs.getString(24) + "\\" + "ControlarSig.jpg";
             //   String controlarSig = rform.getGradename()+"//img//ControlarSig.jpg";
                  String controlarSig = "E:\\SBTETFILES\\ControlarSig.jpg";
                map.put("controlarSig", controlarSig);
                map.put("barUploadFileLoadingpath", barUploadFileLoadingpath);
                map.put("qrUploadFileLoadingpath", qrUploadFileLoadingpath);
                GenerateQrAndBarCode generateQrAndBarCode = new GenerateQrAndBarCode();

                String qrSucess = generateQrAndBarCode.writeQRCode(rs.getString(34), qrUploadFileLoadingpath);
                String barSucess = generateQrAndBarCode.writeBarCode(rs.getString(35), barUploadFileLoadingpath);
               
                if (qrSucess.toString().equalsIgnoreCase("1")) {
                    map.put("qrSucess", qrSucess);
                } else {
                    map.put("qrSucess", qrSucess);
                }
                
                if (barSucess.toString().equalsIgnoreCase("1")) {
                    map.put("barSucess", barSucess);
                } else {
                    map.put("barSucess", barSucess);
                }

                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public void getPdfDataNew(String filepath, String filename, ArrayList list) throws SQLException {

        Font f1 = new Font(Font.FontFamily.COURIER, 8, Font.BOLD, BaseColor.BLACK);
        //Font f3 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL, BaseColor.BLACK);
        Font hallTicket = new Font(Font.FontFamily.COURIER, 9, Font.BOLD, new BaseColor(225, 51, 153));
        Font header = new Font(Font.FontFamily.COURIER, 8, Font.BOLD, new BaseColor(225, 51, 153));
        Font f3 = new Font(Font.FontFamily.COURIER, 7, Font.BOLD, new BaseColor(225, 51, 153));
        Font underLine = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.UNDERLINE, BaseColor.BLACK);
        Font dottedLine = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL, BaseColor.BLACK);
        Font instrinctionsFont = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL, BaseColor.BLACK);

        try {

            HashMap map = (HashMap) list.get(0);
            String hallticket = map.get("RegNo").toString();
            String name = map.get("name").toString();
            String fname = map.get("fname").toString();
            String dob = map.get("dob").toString();
            String grade = map.get("grade").toString();
            String examinationdate = map.get("examinationDate").toString();
            String examinationAppearing = map.get("examinationAppearing").toString(); 
            String institution = map.get("institution").toString();
            String examinationcenter = map.get("examinationCenter").toString();
            String qrUploadFileLoadingpath = map.get("qrUploadFileLoadingpath").toString();
            String barUploadFileLoadingpath = map.get("barUploadFileLoadingpath").toString();
            String controlarSig = map.get("controlarSig").toString();

            String photopath = map.get("filepath").toString();//profilePic
            String photopath2 = map.get("signature").toString();
            
            String part1 = map.get("part1").toString();
            String part2 = map.get("part2").toString();
            String examinationbatch = map.get("examinationBatch").toString();

            Image photo = null;
            Image photo1 = null; //QR CODE
            Image photo2 = null; //
            Image photo3 = null; //
            Image barCodeImgObj = null;

            try {
                photo = Image.getInstance(photopath);//profilePic
                photo2 = Image.getInstance(photopath2);//signature
                photo3 = Image.getInstance(controlarSig);//signature
                    photo1 = Image.getInstance(qrUploadFileLoadingpath);
                    barCodeImgObj = Image.getInstance(barUploadFileLoadingpath);
            } catch (FileNotFoundException ex) {
            }

            //profilePic
            Image image = Image.getInstance(photo);
            image.scaleToFit(70f, 52f);
            image.setAlignment(Element.ALIGN_CENTER);
            image.setBorder(Rectangle.BOX);
            image.setBorderWidth(0);
            image.setAbsolutePosition(100, 100);
            image.setWidthPercentage(100f);

            //QR Code
            Image image1 = Image.getInstance(photo1);
            image1.setAlignment(Element.ALIGN_CENTER);
            image1.setBorder(Rectangle.BOX);
            image1.setBorderWidth(0);
            image1.setBorderColorTop(new BaseColor(255, 153, 255));
            image1.setBorderColorBottom(new BaseColor(255, 153, 255));
            image1.setAbsolutePosition(90, 50);
            image1.setWidthPercentage(100f);
            
            //Bar Code
            Image barCodeImg = Image.getInstance(barCodeImgObj);
            barCodeImg.setAlignment(Element.ALIGN_CENTER);
            //barCodeImg.setBorder(Rectangle.BOX);
            //barCodeImg.setBorderColorTop(new BaseColor(255, 153, 255));
            barCodeImg.setAbsolutePosition(90, 50);
            barCodeImg.setWidthPercentage(100f);

            //signature
            Image image2 = Image.getInstance(photo2);
            image2.scaleToFit(70f, 52f);
            image2.setAlignment(Element.ALIGN_CENTER);
            //image2.setBorder(Rectangle.BOX);
            image2.setBorderWidth(0);
            image2.setAbsolutePosition(100, 100);
            image2.setWidthPercentage(100f);
            
            Image image3 = Image.getInstance(photo3);
            image3.scaleToFit(70f, 52f);
            image3.setAlignment(Element.ALIGN_CENTER);
            image3.setBorder(0);
            image3.setBorderWidth(0);
            image3.setWidthPercentage(100f);


            Document document = new Document();
            OutputStream file = new FileOutputStream(new File(filepath + filename));
            PdfWriter writer = PdfWriter.getInstance(document, file);

            document.open();

            //border table
            PdfPTable bordertable = new PdfPTable(1);
            bordertable.setWidthPercentage(100);
            bordertable.getDefaultCell().setBorderColor(new BaseColor(255, 153, 255));
            bordertable.getDefaultCell().setBorderWidth(1.2f);

            PdfPTable tableo = new PdfPTable(1);
            tableo.setWidthPercentage(99);
            tableo.getDefaultCell().setBorder(Rectangle.NO_BORDER);

            PdfPTable table1 = new PdfPTable(6);
            table1.setWidthPercentage(99);
            table1.getDefaultCell().setBorderColor(new BaseColor(255, 153, 255));
            float[] columnWidthsFOrTable1 = {2.f, 4.1f, 1f, 0.1f, 1f, 1.8f};
            table1.setWidths(columnWidthsFOrTable1);

            PdfPCell hcell1 = null;
//            
//            hcell1 = new PdfPCell(new Paragraph("STATE BOARD OF TECHNICAL EDUCATION & TRAINING", header));
//            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//            hcell1.setBorderColor(new BaseColor(255, 153, 255));
//            hcell1.setColspan(6);
//            hcell1.setBorderWidthLeft(0.1f);
//            hcell1.setBorderWidthRight(0.1f);
//            hcell1.setFixedHeight(13f);
//            table1.addCell(hcell1);
//            
//            hcell1 = new PdfPCell(new Paragraph("ANDHRA PRADESH - VIJAYAWADA", header));
//            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//            hcell1.setBorderColor(new BaseColor(255, 153, 255));
//            hcell1.setColspan(6);
//            hcell1.setBorderWidthLeft(0.1f);
//            hcell1.setBorderWidthRight(0.1f);
//            hcell1.setFixedHeight(13f);
//            table1.addCell(hcell1);
//            
//            hcell1 = new PdfPCell(new Paragraph("TYPEWRITING AND SHORTHAND EXAMINATIONS, JULY/AUGUST - 2021", header));
//            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//            hcell1.setBorderColor(new BaseColor(255, 153, 255));
//            hcell1.setColspan(6);
//            hcell1.setBorderWidthLeft(0.1f);
//            hcell1.setBorderWidthRight(0.1f);
//            hcell1.setFixedHeight(13f);
//            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph(
                     "STATE BOARD OF TECHNICAL EDUCATION & TRAINING "
                    + "                       "
                    + "                       "
                    + "                       "
                    + " ANDHRA PRADESH - VIJAYAWADA"
                    + "                       "
                    + "                       "
                    + "                       "
                    + " TYPEWRITING AND SHORTHAND EXAMINATIONS, JULY/AUGUST - 2021"
                    + "                       "
                    + "                      "
                    + "HALL TICKET", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(6);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(40f);
            table1.addCell(hcell1);
            
            
            hcell1 = new PdfPCell(new Paragraph("Hall Ticket No", f3));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(""+hallticket, f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setBorderWidthBottom(0.0f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            //Profile Pic
            hcell1 = new PdfPCell(image, true);
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setRowspan(4);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            hcell1.setPadding(2);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(barCodeImg, true);
            hcell1.setRowspan(2);
            hcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            hcell1.setPaddingTop(5);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("Name of the Candidate", f3));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(""+name, f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setColspan(2);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.0f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Father Name", f3));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph(""+fname, f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            //QR Code
            hcell1 = new PdfPCell(image1, true);
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setRowspan(4);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            hcell1.setPaddingTop(2);
            hcell1.setPaddingBottom(2);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Date of Birth", f3));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(""+dob, f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Grade", f3));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("" + grade, f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(2);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(image2, true); //signature
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setColspan(1);
            hcell1.setRowspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            hcell1.setPadding(2);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("Date of Examination", f3));
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(examinationdate+" ("+examinationbatch+")", f1));
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setColspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("Examination Timings :", f3));
            hcell1.setColspan(3);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.0f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("", f1)); 
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(2);
            hcell1.setRowspan(5);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("Paper-I", f3));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("" + part1, f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Paper-II", f3));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(""+part2, f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("Name of the Institution", f3));
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("" + institution, f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setColspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            
            hcell1 = new PdfPCell(new Paragraph("Examination Venue", f3));
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("" + examinationcenter+"", f1));
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setColspan(2);
            hcell1.setBorderWidthTop(0.1f);
            hcell1.setBorderWidthLeft(0.1f);
            hcell1.setBorderWidthBottom(0.1f);
            hcell1.setBorderWidthRight(0.1f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("", f1));
            hcell1.setBorderColor(new BaseColor(255, 153, 255));
            hcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            hcell1.setBorder(Rectangle.NO_BORDER);
            hcell1.setFixedHeight(13f);
            table1.addCell(hcell1);
            

            //inner table adding to main table
            tableo.addCell(table1);
            bordertable.addCell(tableo);
            document.add(bordertable);

            //For dotted Lines
            PdfPTable dotedLinesTable1 = new PdfPTable(5);
            dotedLinesTable1.setWidthPercentage(99);
            float[] columnWidthsFOrdotedLinesTable1 = {1.6f, 3f, 1f, 3f, 1f};
            dotedLinesTable1.setWidths(columnWidthsFOrdotedLinesTable1);

            PdfPCell dotedLinesTableCell11 = null;
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setColspan(5);
            dotedLinesTableCell11.setBorder(Rectangle.NO_BORDER);
            dotedLinesTableCell11.setFixedHeight(20f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("Exam", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setFixedHeight(13f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("Signature of Candidate", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setColspan(2);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("Signature of Invigilator", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setColspan(2);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("Paper-1", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setFixedHeight(20f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setColspan(2);
            dotedLinesTableCell11.setFixedHeight(20f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setColspan(2);
            dotedLinesTableCell11.setFixedHeight(20f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("Paper-2", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setFixedHeight(20f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setColspan(2);
            dotedLinesTableCell11.setFixedHeight(20f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setColspan(2);
            dotedLinesTableCell11.setFixedHeight(20f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            

            dotedLinesTableCell11 = new PdfPCell(new Paragraph("", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setBorder(Rectangle.NO_BORDER);
            dotedLinesTableCell11.setColspan(5);
            dotedLinesTableCell11.setFixedHeight(13f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            
            
            dotedLinesTableCell11 = new PdfPCell(new Paragraph("", f1));
            dotedLinesTableCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell11.setColspan(5);
            dotedLinesTableCell11.setBorder(Rectangle.NO_BORDER);
            dotedLinesTableCell11.setFixedHeight(13f);
            dotedLinesTable1.addCell(dotedLinesTableCell11);
            document.add(dotedLinesTable1);
            
            
            //For dotted Lines
            PdfPTable dotedLinesTable = new PdfPTable(6);
            dotedLinesTable.setWidthPercentage(99);
            float[] columnWidthsFOrdotedLinesTable = {1.6f, 4.5f, 1f, 0.1f, 1f, 1.8f};
            dotedLinesTable.setWidths(columnWidthsFOrdotedLinesTable);

            PdfPCell dotedLinesTableCell1 = null;
            
            
            dotedLinesTableCell1 = new PdfPCell(new Paragraph("", f1));
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTableCell1.setColspan(4);
            dotedLinesTable.addCell(dotedLinesTableCell1);
            
            
            dotedLinesTableCell1 = new PdfPCell(image3, true);
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTableCell1.setColspan(2);  
            dotedLinesTable.addCell(dotedLinesTableCell1);
            
            
            
            dotedLinesTableCell1 = new PdfPCell(new Paragraph("Signature & Seal Of Attesting Officer", f1));
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell1.setFixedHeight(13f);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTableCell1.setColspan(4);
            dotedLinesTable.addCell(dotedLinesTableCell1);
              
            dotedLinesTableCell1 = new PdfPCell(new Paragraph("Controller of Examinations", f1));
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell1.setFixedHeight(13f);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTableCell1.setColspan(2);
            dotedLinesTable.addCell(dotedLinesTableCell1);
            
            
            dotedLinesTableCell1 = new PdfPCell(new Paragraph("(Gazetted Officer for Private Candidate", f1));
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell1.setFixedHeight(13f);
            dotedLinesTableCell1.setColspan(4);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTable.addCell(dotedLinesTableCell1);
            
            dotedLinesTableCell1 = new PdfPCell(new Paragraph(" State Board of Technical Eduction ", f1));
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell1.setFixedHeight(13f);
            dotedLinesTableCell1.setColspan(2);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTable.addCell(dotedLinesTableCell1);
            
            
            dotedLinesTableCell1 = new PdfPCell(new Paragraph("And Typewriting Institution Principal for Regular Candidates)", f1));
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell1.setFixedHeight(13f);
            dotedLinesTableCell1.setColspan(4);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTable.addCell(dotedLinesTableCell1);
            
            dotedLinesTableCell1 = new PdfPCell(new Paragraph(" &Training,A.P Vijayawada", f1));
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            dotedLinesTableCell1.setFixedHeight(13f);
            dotedLinesTableCell1.setColspan(2);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTable.addCell(dotedLinesTableCell1);
            
            
            
            
            dotedLinesTableCell1 = new PdfPCell(new Paragraph("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------", dottedLine));
            dotedLinesTableCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            dotedLinesTableCell1.setColspan(6);
            dotedLinesTableCell1.setBorder(Rectangle.NO_BORDER);
            dotedLinesTableCell1.setFixedHeight(13f);
            dotedLinesTable.addCell(dotedLinesTableCell1);
            document.add(dotedLinesTable);

            //For Instructions start

            //border table
            PdfPTable insbordertable = new PdfPTable(1);
            insbordertable.setWidthPercentage(100);
            insbordertable.getDefaultCell().setBorderColor(new BaseColor(255, 153, 255));
            //insbordertable.getDefaultCell().setBorderColor(BaseColor.BLACK);
            insbordertable.getDefaultCell().setBorderWidth(1.2f);

            PdfPTable instableo = new PdfPTable(1);
            instableo.setWidthPercentage(99);
            instableo.getDefaultCell().setBorder(Rectangle.NO_BORDER);

            PdfPTable instable1 = new PdfPTable(6);
            instable1.setWidthPercentage(99);
            float[] instcolumnWidthsFOrTable1 = {1.6f, 4.5f, 1f, 0.1f, 1f, 1.8f};
            instable1.setWidths(instcolumnWidthsFOrTable1);

            PdfPCell insthcell1 = null;

            insthcell1 = new PdfPCell(new Paragraph("INSTRUCTIONS", underLine));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            insthcell1.setFixedHeight(13f);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("1. Hall-Ticket issued to you is an important document to be preserved carefully.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("2. No candidate will be allowed to enter the examination hall without proper hall-ticket.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("3. Candidate shall arrive at the examination centre at least 15 minutes before the commencement of the examination and shall not leave the hall after commencement of exam.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("4. No printed/written material in any form shall be allowed into the examination hall, except hall ticket.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("5. Candidate should maintain absolute silence.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("6. Candidates are advised to check OMR Bar Code Sheet before starting Examination.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("7. Candidates should not write any matter inside the Answer Booklet which may lead to the identification of the candidate or institution. If he/she does so, he/she will be booked under malpractice.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("8. Candidates will not be allowed into the examination hall with cell Phone (CDMA & GSM), Pagers, Organizers, PDA’s and Palmtops or any other Electronic Gadgets, etc.,", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("9. Every student shall follow the regulations during examinations, failing which he/she will be booked under malpractice case.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("10. Batch change will not be permitted under any Circumstances.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("11. No candidate will be allowed into the examination hall once the Paper-II examination is commenced.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("12. Using different type machines in the examination hall without prior permission of the Chief Superintendent/Joint Chief leads to Cancellation of results without any intimation to the candidate.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("13. Unauthorised exchange, replacement, insertion of answer sheets, removal of unused answer sheets will be viewed seriously and  render ineligible for valuation.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("14. No candidate is allowed to take away any used or unused answer sheets and renders cancellation of examination without any intimation to the candidate.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            insthcell1 = new PdfPCell(new Paragraph("15. The Examination in Paper-II (Manuscript) of Typewriting of all grades will be conducted first and the Paper-I (Speed) will be conducted after completion of the Paper-II (Manuscript) Examination.", instrinctionsFont));
            insthcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            insthcell1.setBorder(Rectangle.NO_BORDER);
            insthcell1.setColspan(6);
            instable1.addCell(insthcell1);

            instableo.addCell(instable1);
            insbordertable.addCell(instableo);
            document.add(insbordertable);
            //Instructions End

            document.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // con.close(); 
        }
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "D://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
