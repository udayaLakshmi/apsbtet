/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.DAO;

import com.ap.Form.RegisterForm;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ci96067
 */
public class MSCRegistrationPrintDAO {
    
    public List getMScDocumentVerficationDetails() throws Exception {
        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_ApplicantListForDocumentVerification](?)}");
            cstmt.setString(1, "1");
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicationId", rs.getString(1)); 
                map.put("aadharnumber", rs.getString(2));
                map.put("name", rs.getString(3));
                map.put("father", rs.getString(4)); 
                map.put("DOB", rs.getString(5));
                map.put("gender", rs.getString(6));
                map.put("mobilenumber", rs.getString(7));
             
                lstDetails.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return lstDetails;
    }

    public List<HashMap> getDocumentDetails(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
       
        try {
            con = DatabaseConnection.getConnection();
//            cstmt = con.prepareCall("{Call MSc_Registration_Educational_AchivementsDetails_Verification(?)}");
            cstmt = con.prepareCall("{Call MSc_Registration_Educational_AchivementsDetails_Print(?)}");

            cstmt.setString(1, rform.getAadhar());
            System.out.println("DAO"+rform.getAadhar());
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                req.setAttribute("applicantid", rs.getString(1));
                req.setAttribute("aadharnumber", rs.getString(2));
                 String aadhar=rs.getString(2);
                String adh="";
                if(aadhar!=null && aadhar!=""){
                adh="xxxxxxxx"+aadhar.substring(8,12);
                }else{
                 adh="xxxxxxxxxxxx";
                }
                 
                 req.setAttribute("aadharmaskingnumber", adh);
                req.setAttribute("applicantname", rs.getString(3));
                req.setAttribute("dependencytype", rs.getString(4));
                req.setAttribute("fathername", rs.getString(5));
                req.setAttribute("monthername", rs.getString(6));
                req.setAttribute("gaurdianname", rs.getString(7));
                req.setAttribute("relationship", rs.getString(8));
                req.setAttribute("dob", rs.getString(9));
                req.setAttribute("age", rs.getString(10));

                req.setAttribute("gender", rs.getString(11));
                req.setAttribute("pdstate", rs.getString(12));
                req.setAttribute("pddistrict", rs.getString(13));
                req.setAttribute("pdmandal", rs.getString(14));
                req.setAttribute("birthplace", rs.getString(15));
                req.setAttribute("religion", rs.getString(16));
                req.setAttribute("national", rs.getString(17));
                req.setAttribute("caste", rs.getString(18));
                req.setAttribute("socialstatus", rs.getString(19));
                req.setAttribute("mobile", rs.getString(20));

                req.setAttribute("landline", rs.getString(21));
                req.setAttribute("email", rs.getString(22));
                req.setAttribute("cwsn", rs.getString(23));
                req.setAttribute("hno", rs.getString(24));
                req.setAttribute("locality", rs.getString(25));
                req.setAttribute("pastate", rs.getString(26));

                System.out.println("===pastate===" + rs.getString(26));
                req.setAttribute("padistrict", rs.getString(27));
                System.out.println("===padistrict===" + rs.getString(27));

                req.setAttribute("pamandal", rs.getString(28));
                System.out.println("===pamandal===" + rs.getString(28));

                req.setAttribute("papincode", rs.getString(29));
                req.setAttribute("addresssame", rs.getString(30));

                req.setAttribute("perhno", rs.getString(31));
                req.setAttribute("perlocality", rs.getString(32));
                req.setAttribute("perstate", rs.getString(33));
                req.setAttribute("perdistrict", rs.getString(34));
                req.setAttribute("permandal", rs.getString(35));
                req.setAttribute("perpincode", rs.getString(36));
                req.setAttribute("course", rs.getString(37));
                req.setAttribute("subgroup", rs.getString(38));
                req.setAttribute("qualification", rs.getString(39));
                req.setAttribute("qualfrom", rs.getString(40));

                req.setAttribute("qualto", rs.getString(41));
                req.setAttribute("university", rs.getString(42));
                req.setAttribute("institute", rs.getString(43));
                req.setAttribute("ogpapoint", rs.getString(44));
                req.setAttribute("ogpatotal", rs.getString(45));
                req.setAttribute("ogpamarks", rs.getString(46));
                req.setAttribute("degreemarks", rs.getString(47));
                req.setAttribute("degreetotalmarks", rs.getString(48));

                req.setAttribute("icaapplicationno", rs.getString(49));
                req.setAttribute("icarank", rs.getString(50));
                req.setAttribute("icamarksobtained", rs.getString(51));
                req.setAttribute("icatotalmarks", rs.getString(52));

                req.setAttribute("distinctedu", rs.getString(53));
                req.setAttribute("distinctcourse", rs.getString(54));
                req.setAttribute("distinctinstitue", rs.getString(55));
                req.setAttribute("distincttype", rs.getString(56));
                req.setAttribute("distinctyear", rs.getString(57));

                req.setAttribute("uploadphoto", rs.getString(58));
                // req.setAttribute("distinctyear", rs.getString(58));
                req.setAttribute("xcourse", rs.getString(81));
                req.setAttribute("xyear", rs.getString(82));
                req.setAttribute("xstate", rs.getString(83));
                req.setAttribute("xdistrict", rs.getString(84));
                req.setAttribute("xinstitute", rs.getString(85));
                req.setAttribute("xfile", rs.getString(86));
                req.setAttribute("intersrcourse", rs.getString(87));

                req.setAttribute("intersryear", rs.getString(88));
                req.setAttribute("intersrstate", rs.getString(89));
                req.setAttribute("intersrdistrict", rs.getString(90));

                req.setAttribute("intersrinstitute", rs.getString(91));
                req.setAttribute("intersrfile", rs.getString(92));

                req.setAttribute("interjrcourse", rs.getString(93));
                req.setAttribute("interjryear", rs.getString(94));
                req.setAttribute("interjrstate", rs.getString(95));
                req.setAttribute("interjrdistrict", rs.getString(96));
                req.setAttribute("interjrinstitute", rs.getString(97));
                req.setAttribute("interjrfile", rs.getString(98));
                req.setAttribute("IBSCcourse", rs.getString(99));
                req.setAttribute("IBSCyear", rs.getString(100));

                req.setAttribute("IBSCstate", rs.getString(101));
                req.setAttribute("IBSCdistrict", rs.getString(102));
                req.setAttribute("IBSCinstitute", rs.getString(103));
                req.setAttribute("IBSCfile", rs.getString(104));
                req.setAttribute("IIBSCcourse", rs.getString(105));
                req.setAttribute("IIBSCyear", rs.getString(106));
                req.setAttribute("IIBSCstate", rs.getString(107));
                req.setAttribute("IIBSCdistrict", rs.getString(108));
                req.setAttribute("IIBSCinstitute", rs.getString(109));
                req.setAttribute("IIBSCfile", rs.getString(110));

                req.setAttribute("IIIBSCcourse", rs.getString(111));
                req.setAttribute("IIIBSCyear", rs.getString(112));
                req.setAttribute("IIIBSCstate", rs.getString(113));
                req.setAttribute("IIIBSCdistrict", rs.getString(114));
                req.setAttribute("IIIBSCinstitute", rs.getString(115));
                req.setAttribute("IIIBSCfile", rs.getString(116));
                req.setAttribute("IVBSCcourse", rs.getString(117));
                req.setAttribute("IVBSCyear", rs.getString(118));
                req.setAttribute("IVBSCstate", rs.getString(119));
                req.setAttribute("IVBSCdistrict", rs.getString(120));

                req.setAttribute("IVBSCinstitute", rs.getString(121));
                req.setAttribute("IVBSCfile", rs.getString(122));

                req.setAttribute("casteFile", rs.getString(66));
                req.setAttribute("socialstatusfile", rs.getString(60));
                req.setAttribute("disabilityfile", rs.getString(61));
                req.setAttribute("ogpafile", rs.getString(64));
                req.setAttribute("academiccertificationfile", rs.getString(65));
                req.setAttribute("Xcerfile", rs.getString(86));
                req.setAttribute("intercerfile", rs.getString(92));
                req.setAttribute("degreecerfile", rs.getString(122));
                req.setAttribute("isenterance", rs.getString(67));
                req.setAttribute("nrisponser", rs.getString(68));
                req.setAttribute("nripassport", rs.getString(69));
                req.setAttribute("nriresidence", rs.getString(70));
                req.setAttribute("nrirelation", rs.getString(71));
                req.setAttribute("industrysponser", rs.getString(72));
                req.setAttribute("industryTax", rs.getString(73));
                req.setAttribute("industrycopay", rs.getString(74));
                req.setAttribute("industrycompany", rs.getString(75));
                req.setAttribute("industryaudit", rs.getString(76));
                req.setAttribute("industryincorporate", rs.getString(77));
                req.setAttribute("industryemployee", rs.getString(78));
                req.setAttribute("industrypayslip", rs.getString(79));

                req.setAttribute("nccinternational", rs.getString(124));
                req.setAttribute("nccstate", rs.getString(125));
                req.setAttribute("nccuniversity", rs.getString(126));
                req.setAttribute("nccinternationalfile", rs.getString(127));
                req.setAttribute("nccstatefile", rs.getString(128));
                req.setAttribute("nccuniversityfile", rs.getString(129));
                // req.setAttribute("nccstatus", rs.getString(130));
                req.setAttribute("nccstatus", "N");

                req.setAttribute("nssinternational", rs.getString(132));
                req.setAttribute("nssstate", rs.getString(133));
                req.setAttribute("nssuniversity", rs.getString(134));
                req.setAttribute("nssinternationalfile", rs.getString(135));
                req.setAttribute("nssstatefile", rs.getString(136));
                req.setAttribute("nssuniversityfile", rs.getString(137));
                req.setAttribute("nssstatus", rs.getString(138));


                req.setAttribute("sportsinternational", rs.getString(140));
                req.setAttribute("sportsstate", rs.getString(141));
                req.setAttribute("sportsuniversity", rs.getString(142));
                req.setAttribute("sportsinternationalfile", rs.getString(143));
                req.setAttribute("sportsstatefile", rs.getString(144));
                req.setAttribute("sportsuniversityfile", rs.getString(145));
                req.setAttribute("sportsstatus", rs.getString(146));


                req.setAttribute("cultinternational", rs.getString(148));
                req.setAttribute("cultstate", rs.getString(149));
                req.setAttribute("cultuniversity", rs.getString(150));
                req.setAttribute("cultinternationalfile", rs.getString(151));
                req.setAttribute("cultstatefile", rs.getString(152));
                req.setAttribute("cultuniversityfile", rs.getString(153));
                req.setAttribute("cultstatus", rs.getString(154));

                req.setAttribute("gameinternational", rs.getString(156));
                req.setAttribute("gamestate", rs.getString(157));
                req.setAttribute("gameuniversity", rs.getString(158));
                req.setAttribute("gameinternationalfile", rs.getString(159));
                req.setAttribute("gamestatefile", rs.getString(160));
                req.setAttribute("gameuniversityfile", rs.getString(161));
                req.setAttribute("gamestatus", rs.getString(162));

                req.setAttribute("literalinternational", rs.getString(164));
                req.setAttribute("literalstate", rs.getString(165));
                req.setAttribute("literaluniversity", rs.getString(166));
                req.setAttribute("literalinternationalfile", rs.getString(167));
                req.setAttribute("literalstatefile", rs.getString(168));
                req.setAttribute("literaluniversityfile", rs.getString(169));
                req.setAttribute("literalstatus", rs.getString(170));

                req.setAttribute("rankfile", rs.getString(171));
                req.setAttribute("dgreefile1", rs.getString(172));
                req.setAttribute("dgreefile2", rs.getString(173));
                req.setAttribute("dgreefile3", rs.getString(174));
                req.setAttribute("dgreefile4", rs.getString(175));
                
                  req.setAttribute("pgref", rs.getString(176));
                  req.setAttribute("amount", rs.getString(177));
                  req.setAttribute("date", rs.getString(178));
                  req.setAttribute("deptype", rs.getString(179));
                  req.setAttribute("localnonlocal", rs.getString(180));
                  
               req.setAttribute("dattt", new Date());
               
                map.put("district_Name", "1234");

                String final_vCard = String.format("%s%n%s%n%s%n%s%n%s", rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(9), rs.getString(11));
//                BarcodeQRCode my_code = new BarcodeQRCode(final_vCard, 1, 1, null);
//                com.itextpdf.text.Document vCard_QR_Code = new com.itextpdf.text.Document();
//                System.out.println("my_code" + my_code);
//                map.put("mycode", final_vCard);
//               String final_vCard = String.format("%s%n%s%n%s%n%s%n%s", Academicyear, UDISEcode, Fathername, StudentID, Class);
//            //Create Contact event QR Code
//            BarcodeQRCode my_code = new BarcodeQRCode(final_vCard, 1, 1, null);
//            com.itextpdf.text.Document vCard_QR_Code = new com.itextpdf.text.Document();
////            Image symbol = Image.getInstance("D:\\TransferCertificate\\satyamevjayate.png");
//                Image symbol = Image.getInstance("C:\\TransferCertificate\\satyamevjayate.png");
//            symbol.scaleAbsolute(90f, 90f);
//            symbol.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
////            String FILE = "d:/TransferCertificate/";
////            String FILE = path;
////                String FILE = "c:/TransferCertificate/";
////            PdfWriter writer = PdfWriter.getInstance(vCard_QR_Code, new FileOutputStream(FILE + TCUniqueNo + ".pdf"));
//            PdfWriter writer = PdfWriter.getInstance(vCard_QR_Code, new FileOutputStream(TCUniqueNo + ".pdf"));
//            vCard_QR_Code.open();
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return lstDetails;
    }

    public String submitDocumentStatusDetails(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        PreparedStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call MSc_Certificate_VerificationStatus_Insert(?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?)}");
            cstmt.setString(1, req.getParameter("applicationno"));
            cstmt.setString(2, req.getParameter("aadhar"));
            cstmt.setString(3, req.getParameter("photoConfrm"));
            cstmt.setString(4, req.getParameter("casteConfirm"));
            cstmt.setString(5, req.getParameter("cwsnconfirm"));
            cstmt.setString(6, req.getParameter("socialstatusConfirm"));
            cstmt.setString(7, req.getParameter("ogpaConfirm"));
            cstmt.setString(8, req.getParameter("academicConfirm"));
            cstmt.setString(9, req.getParameter("bscConfirm"));
            cstmt.setString(10, req.getParameter("interConfirm"));
            cstmt.setString(11, req.getParameter("xConfirm"));
            // nccConfirm nssStateConfirm nssUniversityConfirm sportsInternationalConfirm sportsStateConfirm sportsunivConfirm
            //gameinternationalConfirm gamestateConfirm gameunivConfirm cultinternationalConfirm cultstateConfirm cultunivConfirm
            //literalinternationalConfirm literalstateConfirm literalunivConfirm
            cstmt.setString(12, req.getParameter("nccConfirm"));
            
            cstmt.setString(13, req.getParameter("nssStateConfirm"));
            cstmt.setString(14, req.getParameter("nssUniversityConfirm"));
            
            cstmt.setString(15, req.getParameter("sportsInternationalConfirm"));
            cstmt.setString(16, req.getParameter("sportsStateConfirm"));
            cstmt.setString(17, req.getParameter("sportsunivConfirm"));
            
            cstmt.setString(18, req.getParameter("gameinternationalConfirm"));
            cstmt.setString(19, req.getParameter("gamestateConfirm"));
            cstmt.setString(20, req.getParameter("gameunivConfirm"));
            
            cstmt.setString(21, req.getParameter("cultinternationalConfirm"));
            cstmt.setString(22, req.getParameter("cultstateConfirm"));
            cstmt.setString(23, req.getParameter("cultunivConfirm"));
            
            cstmt.setString(24, req.getParameter("literalinternationalConfirm"));
            cstmt.setString(25, req.getParameter("literalstateConfirm"));
            cstmt.setString(26, req.getParameter("literalunivConfirm"));
            
            
            cstmt.setString(27, req.getParameter("nrisponserConfirm"));
            cstmt.setString(28, req.getParameter("nripassportConfirm"));
            cstmt.setString(29, req.getParameter("nriresidenceConfirm"));
            
            cstmt.setString(30, req.getParameter("nrirelationConfirm"));
            cstmt.setString(31, req.getParameter("industrysponserConfirm"));
            cstmt.setString(32, req.getParameter("industrytaxConfirm"));
            cstmt.setString(33, req.getParameter("industrycopayConfirm"));
            cstmt.setString(34, req.getParameter("industrycompanyConfirm"));
            cstmt.setString(35, req.getParameter("industryauditConfirm"));
            cstmt.setString(36, req.getParameter("industryincoprateConfirm"));
            cstmt.setString(37, req.getParameter("industryemployeeConfirm"));
            cstmt.setString(38, req.getParameter("industrypayslip"));
            cstmt.setString(39, req.getParameter("rankconfirm"));
            cstmt.setString(40, req.getParameter("degree1"));
            cstmt.setString(41, req.getParameter("degree2"));
            cstmt.setString(42, req.getParameter("degree3"));
            cstmt.setString(43, req.getParameter("degree4"));
            cstmt.setString(44, "PGCET");
             cstmt.setString(45,req.getParameter("eligibleConfirm"));


            rs = cstmt.executeQuery();

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
    public String getvalidatingAadharNumWithApplication(String aadharNum,String appNO,String dob) throws Exception {
        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String mb ="";
        String mm ="";
        try {
            String[] mb1= dob.split("/");
             mm=mb1[2].toString()+"-"+mb1[1].toUpperCase()+"-"+mb1[0].toString();
            con = DatabaseConnection.getConnection();
            query = "select Mobile from MSC_Registration  where Aadhaar='"+aadharNum+"' and ApplicationID='"+appNO+"' and DOB='"+mm+"'";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    mb = rs.getString(1);
                }
            }else{
                mb ="";
            }
        } catch (Exception e) {
            mb ="";
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mb;
    }
     public int SaveOTP(String aadharNum,String mobile,String otp,String systemIp) throws Exception {
        int count=0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call proc_OTP_Details_Insert(?,?,?,?)}");
            cstmt.setString(1, aadharNum);
            cstmt.setString(2, mobile);
            cstmt.setString(3, otp);
            cstmt.setString(4, systemIp);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }
    
    public String getvalidatingOTP(String aadharNum,String mobile) throws Exception {

        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String otp = "";

        try {
            con = DatabaseConnection.getConnection();
            query = "select OTP  from OTP_Details where Mobile_Num='"+mobile+"' and Aadhar_Id='" + aadharNum + "'";
            System.out.println("query "+query);
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    otp = rs.getString(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return otp;
    }
     public int getvalidatingApplicationNum(String application) throws Exception {

        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;

        try {
            con = DatabaseConnection.getConnection();
            query = "select count(*)count from MSC_Registration (nolock)  where  ApplicationID='" + application + "'";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }
       public List getPhdRegistractionDetails(String aadharNum) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_Phd_Registration_Educational_AchivementsDetails_Get(?)}");
            cstmt.setString(1, aadharNum);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    HashMap map = new HashMap();

                    map.put("applicationNO", rs.getString(1));
                    map.put("aadhar", rs.getString(2));
                    map.put("dob", rs.getString(3));
                    map.put("age", rs.getString(4));
                    map.put("sscOrBirthFile", rs.getString(5));
                    map.put("photoFile", rs.getString(6));
                    map.put("firstName", rs.getString(7));
                    map.put("gender", rs.getString(8));

                    //Dependency Details
                    map.put("typeGuardian", rs.getString(9));
                    map.put("relationship", rs.getString(10));
                    map.put("fatherName", rs.getString(11));
                    map.put("motherName", rs.getString(12));
                    map.put("guardianName", rs.getString(13));


                    map.put("placeOfBirth", rs.getString(14));
                    map.put("personalState", rs.getString(15));
                    map.put("personalDistrict", rs.getString(16));
                    map.put("personalMandal", rs.getString(17));
                    map.put("religion", rs.getString(18));
                    map.put("nationality", rs.getString(19));
                    map.put("caste", rs.getString(20));
                    map.put("castefile", rs.getString(21));
                    map.put("socialStatus", rs.getString(22));
                    map.put("socialStatusFile", rs.getString(23));
                    map.put("mobile", rs.getString(24));
                    map.put("phone", rs.getString(25));
                    map.put("email", rs.getString(26));
                    map.put("stateDiffrentAbled", rs.getString(27));
                    map.put("stateDiffrentAbledFile", rs.getString(28));

                    //PRESENT ADDRESS
                    map.put("presentAddressHNO", rs.getString(29));
                    map.put("presentAddressLocality", rs.getString(30));
                    map.put("presentAddressState", rs.getString(31));
                    map.put("presentAddressDistrict", rs.getString(32));
                    map.put("presentAddressMandal", rs.getString(33));
                    map.put("presentAddressPincode", rs.getString(34));
                    map.put("presentAddressSameWithPermeent", rs.getString(35));/////missed

                    //PERMANENT ADDRESS
                    map.put("permaentAddressHNO", rs.getString(36));
                    map.put("permaentAddressLocality", rs.getString(37));
                    map.put("permaentAddressState", rs.getString(38));
                    map.put("permaentAddressDistrict", rs.getString(39));
                    map.put("permaentAddressMandal", rs.getString(40));
                    map.put("permaentAddressPincode", rs.getString(41));

                    //Course Details
                    map.put("nameOfTheCourse", rs.getString(42));
                    map.put("majorSubjectGroup", rs.getString(43));
                    //PARTICULARS OF PREVIOUS ACADEMIC RECORD
                    //PG
                    map.put("pgExaminationPassed", rs.getString(44));
                    map.put("pgPeriodOfStudyFromYear", rs.getString(45));
                    map.put("pgperiodOfStudyToYear", rs.getString(46));
                    map.put("pgNameOfUniversity", rs.getString(47));
                    map.put("pgNameOfInstitute", rs.getString(48));
                    map.put("pgOgpa", rs.getString(49));
                    map.put("pgTotalPoints", rs.getString(50));
                    map.put("pgOgpaFile", rs.getString(51));
                    map.put("pgAcademicMaxMarks", rs.getString(52));
                    map.put("pgAcademicObtainedMarks", rs.getString(53));
                    map.put("pgdegreeFile", rs.getString(54));
                    //PARTICULARS OF PREVIOUS ACADEMIC RECORD
                    //Degree
                    map.put("degreeExaminationPassed", rs.getString(55));
                    map.put("degreePeriodOfStudyFromYear", rs.getString(56));
                    map.put("degreeperiodOfStudyToYear", rs.getString(57));
                    map.put("degreeNameOfUniversity", rs.getString(58));
                    map.put("degreeNameOfInstitute", rs.getString(59));
                    map.put("degreeOgpa", rs.getString(60));
                    map.put("degreeTotalPoints", rs.getString(61));
                    map.put("degreeOgpaFIle", rs.getString(62));
                    map.put("degreeAcademicMaxMarks", rs.getString(63));
                    map.put("degreeAcademicObtainedMarks", rs.getString(64));
                    map.put("degreeFIle", rs.getString(65));

                    //Title of Thesis
                    map.put("titleOfThesis", rs.getString(66));

                    //ICA R-AIEEA (PG),2020 Exam Details
                    map.put("entranceAppNo", rs.getString(67));
                    map.put("rankNo", rs.getString(68));
                    map.put("obtainedMarks", rs.getString(69));
                    map.put("totalMarks", rs.getString(70));

                    //State whether you have joined Ph.D or any other degree programme on part-time / full-time basis in this or any other institution*
                    map.put("anyOtherDegreeProgramme", rs.getString(71));
                    map.put("otherNameOfCourse", rs.getString(72));
                    map.put("otherNameOfInstitute", rs.getString(73));
                    map.put("otherNameOfCoursePartime", rs.getString(74));
                    map.put("otherNameOfCourseInternshipYear", rs.getString(75));

                    ///PARTICULARS OF EDUCATION
                    map.put("mscTwoAcaYear", rs.getString(82));
                    map.put("mscTwoAcaState", rs.getString(83));
                    map.put("mscTwoAcaDistrict", rs.getString(84));
                    map.put("mscTwoAcaNameOfInstitution", rs.getString(85));
                    map.put("mscTwoFile", rs.getString(86));

                    map.put("mscOneAcaYear", rs.getString(88));
                    map.put("mscOneAcaState", rs.getString(89));
                    map.put("mscOneAcaDistrict", rs.getString(90));
                    map.put("mscOneAcaNameOfInstitution", rs.getString(91));
                    map.put("mscOneFile", rs.getString(92));


                    map.put("degreeFourAcaYear", rs.getString(94));
                    map.put("degreeFourAcaState", rs.getString(95));
                    map.put("degreeFourAcaDistrict", rs.getString(96));
                    map.put("degreeFourAcaNameOfInstitution", rs.getString(97));
                    map.put("degreeFourFile", rs.getString(98));


                    map.put("degreeThreeAcaYear", rs.getString(100));
                    map.put("degreeThreeAcaState", rs.getString(101));
                    map.put("degreeThreeAcaDistrict", rs.getString(102));
                    map.put("degreeThreeAcaNameOfInstitution", rs.getString(103));
                    map.put("degreeThreeFile", rs.getString(104));

                    map.put("degreeTwoAcaYear", rs.getString(106));
                    map.put("degreeTwoAcaState", rs.getString(107));
                    map.put("degreeTwoAcaDistrict", rs.getString(108));
                    map.put("degreeTwoAcaNameOfInstitution", rs.getString(109));
                    map.put("degreeTwoFile", rs.getString(110));

                    map.put("degreeOneAcaYear", rs.getString(112));
                    map.put("degreeOneAcaState", rs.getString(113));
                    map.put("degreeOneAcaDistrict", rs.getString(114));
                    map.put("degreeOneAcaNameOfInstitution", rs.getString(115));
                    map.put("degreeOneFile", rs.getString(116));

                    map.put("SInterAcaYear", rs.getString(118));
                    map.put("SInterAcaState", rs.getString(119));
                    map.put("SInterAcaDistrict", rs.getString(120));
                    map.put("SInterAcaNameOfInstitution", rs.getString(121));
                    map.put("SInterAcaFile", rs.getString(122));

                    map.put("JInterAcaYear", rs.getString(124));
                    map.put("JInterAcaState", rs.getString(125));
                    map.put("JInterAcaDistrict", rs.getString(126));
                    map.put("JInterAcaNameOfInstitution", rs.getString(127));
                    map.put("JInterAcaFile", rs.getString(128));

                    map.put("xAcaYear", rs.getString(130));
                    map.put("xAcaState", rs.getString(131));
                    map.put("xAcaDistrict", rs.getString(132));
                    map.put("xAcaNameOfInstitution", rs.getString(133));
                    map.put("xAcaFile", rs.getString(134));

                    //PARTICULARS OF EXTRA CURRICULAR
                    if (rs.getString(135) == null) {
                        map.put("nccAchievementType", "N");
                        map.put("nccInternational", "N");
                        map.put("nccState", "N");
                        map.put("nccInterUniversity", "N");
                    } else {
                        map.put("nccAchievementType", rs.getString(135));
                        map.put("nccInternational", rs.getString(136));
                        map.put("nccState", rs.getString(137));
                        map.put("nccInterUniversity", rs.getString(138));
                    }
                    map.put("nccFilePath", rs.getString(139));
                    map.put("nccStateFilePath", rs.getString(140));
                    map.put("nccInternationalFilePath", rs.getString(141));

                    if (rs.getString(142) == null) {
                        map.put("nssAchievementType", "N");
                        map.put("nssInternational", "N");
                        map.put("nssState", "N");
                        map.put("nssInterUniversity", "N");
                    } else {
                        map.put("nssAchievementType", rs.getString(142));
                        map.put("nssInternational", rs.getString(143));
                        map.put("nssState", rs.getString(144));
                        map.put("nssInterUniversity", rs.getString(145));
                    }
                    map.put("nssFilePath", rs.getString(146));
                    map.put("nssStateFilePath", rs.getString(147));
                    map.put("nssInternationalFilePath", rs.getString(148));

                    if (rs.getString(149) == null) {
                        map.put("sportsAchievementType", "N");
                        map.put("sportsInternational", "N");
                        map.put("sportsState", "N");
                        map.put("sportsInterUniversity", "N");
                    } else {
                        map.put("sportsAchievementType", rs.getString(149));
                        map.put("sportsInternational", rs.getString(150));
                        map.put("sportsState", rs.getString(151));
                        map.put("sportsInterUniversity", rs.getString(152));
                    }
                    map.put("sportsFilePath", rs.getString(153));
                    map.put("sportsStateFilePath", rs.getString(154));
                    map.put("sportsInternationalFilePath", rs.getString(155));

                    if (rs.getString(156) == null) {
                        map.put("gamesAchievementType", "N");
                        map.put("gamesInternational", "N");
                        map.put("gamesState", "N");
                        map.put("gamesInterUniversity", "N");
                    } else {
                        map.put("gamesAchievementType", rs.getString(156));
                        map.put("gamesInternational", rs.getString(157));
                        map.put("gamesState", rs.getString(158));
                        map.put("gamesInterUniversity", rs.getString(159));
                    }
                    map.put("gamesFilePath", rs.getString(160));
                    map.put("gamesStateFilePath", rs.getString(161));
                    map.put("gamesInternationalFilePath", rs.getString(162));

                    if (rs.getString(163) == null) {
                        map.put("culturalAchievementType", "N");
                        map.put("culturalInternational", "N");
                        map.put("culturalState", "N");
                        map.put("culturalInterUniversity", "N");
                    } else {
                        map.put("culturalAchievementType", rs.getString(163));
                        map.put("culturalInternational", rs.getString(164));
                        map.put("culturalState", rs.getString(165));
                        map.put("culturalInterUniversity", rs.getString(166));
                    }
                    map.put("culturalFilePath", rs.getString(167));
                    map.put("culturalStateFilePath", rs.getString(168));
                    map.put("culturalInternationalFilePath", rs.getString(169));

                    if (rs.getString(170) == null) {
                        map.put("literalAchievementType", "N");
                        map.put("literalInternational", "N");
                        map.put("literalState", "N");
                        map.put("literalInterUniversity", "N");
                    } else {
                        map.put("literalAchievementType", rs.getString(170));
                        map.put("literalInternational", rs.getString(171));
                        map.put("literalState", rs.getString(172));
                        map.put("literalInterUniversity", rs.getString(173));
                    }
                    map.put("literalFilePath", rs.getString(174));
                    map.put("literalStateFilePath", rs.getString(175));
                    map.put("literalInternationalFilePath", rs.getString(176));

                    ///
                    map.put("examDetails", rs.getString(8));


                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
          public int getstatusvalidate(String aadharNum,String applicationnumber,String flag) throws Exception {
        int statuscount=0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_PaymentStatus_validation(?,?,?)}");
            cstmt.setString(1, aadharNum);
            cstmt.setString(2, applicationnumber);
             cstmt.setString(3, flag);
           
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    statuscount = rs.getInt(1);
                    System.out.println("statuscount---"+statuscount);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return statuscount;
    }

}
