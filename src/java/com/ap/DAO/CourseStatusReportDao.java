/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.DAO;

import com.ap.WithLoginDAO.*;
import com.ap.DAO.*;
import com.ap.Form.RegisterForm;
import com.ap.Form.RegisterFormPhD;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.MessagePropertiesUtil;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author ci96067
 */
public class CourseStatusReportDao {

    public List<HashMap> getCourseWiseReport() throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_Dashboard_Rpt()}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("programme", rs.getString(4));
                    map.put("totalAplications", rs.getString(1));
                    map.put("submitted", rs.getString(2));
                    map.put("paymentMade", rs.getString(3));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getCandidateReport(String flag) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CandidateReport(?)}");
            cstmt.setString(1, flag);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("hall", rs.getString(1));
                    map.put("name", rs.getString(2));
                    map.put("fname", rs.getString(3));
                    map.put("gender", rs.getString(4));
                    map.put("dob", rs.getString(5));
                    map.put("caste", rs.getString(6));
                    map.put("ews", rs.getString(7));
                    map.put("areastu", rs.getString(8));
                    map.put("mobile", rs.getString(9));
                    map.put("flag", flag);
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getLoginDashBoard() throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TotalApplicantAbstractReport_Get()}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("pgTotalApplied", rs.getString(1));
                    map.put("pgNoOfCandiatesSub", rs.getString(2));
                    map.put("pgNoOfCandiatesPayMade", rs.getString(3));
                    map.put("phdTotalApplied", rs.getString(4));
                    map.put("phdNoOfCandiatesSub", rs.getString(5));
                    map.put("phdNoOfCandiatesPayMade", rs.getString(6));
                    lstDetails.add(map);
                    map = null;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "D://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }

    public void getPdfData(String type, String filepath, String filename, String flag) throws SQLException {
        if (flag.equalsIgnoreCase("0")) {
            createPDF(type, filepath, filename, flag);
        } else {
            createPDFFlag(type, filepath, filename, flag);
        }
    }

    private void createPDF(String type, String filepath, String filename, String flag) {

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list = getCourseWiseReport();
            //special font sizes
            Font bfBold12 = new Font(FontFamily.TIMES_ROMAN, 8, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD, new BaseColor(119, 41, 4)); //MAGENTA -Pink
            Font bf12 = new Font(FontFamily.TIMES_ROMAN, 8);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            //document header attributes
            doc.addAuthor("betterThanZero");
            doc.addCreationDate();
            doc.addProducer();
            doc.addCreator("MySampleCode.com");
            doc.addTitle("Report with Column Headings");
            doc.setPageSize(PageSize.A4);

            //open document
            doc.open();

            //create a paragraph
            Paragraph paragraph = new Paragraph("        Report : Status of Applications");


            //specify column widths
            float[] columnWidths = {3f, 5f, 3f, 5f};
            //create PDF table with the given widths
            PdfPTable table = new PdfPTable(columnWidths);
            // set table width a percentage of the page width
            table.setWidthPercentage(90f);

            //insert column headings
            insertCell(table, "S.NO", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Name Of the Stream", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "No. of Candidates Submitted", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "No. of Candidates Edited", Element.ALIGN_CENTER, 1, bfBold12);
//            insertCell(table, "No. of Candidates Payment Made", Element.ALIGN_CENTER, 1, bfBold12);
            table.setHeaderRows(1);

            int totalAplications = 0, submitted = 0, submittedv = 0, paymentMade = 0, total = 0;
            int sno = 0;
            //just some random data to fill 
            for (int x = 0; x < list.size(); x++) {
                HashMap map = list.get(x);
                sno++;
                insertCell(table, sno + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("programme") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("totalAplications") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("submitted") + "", Element.ALIGN_CENTER, 1, bf12);
//                insertCell(table, map.get("paymentMade") + "", Element.ALIGN_CENTER, 1, bf12);
            }

            //add the PDF table to the paragraph 
            paragraph.add(table);
            // add the paragraph to the document
            doc.add(paragraph);

        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void createPDFFlag(String type, String filepath, String filename, String flag) {

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list = getCandidateReport(flag);
            //special font sizes
            Font bfBold12 = new Font(FontFamily.TIMES_ROMAN, 7, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD, new BaseColor(119, 41, 4)); //MAGENTA -Pink
            Font bf12 = new Font(FontFamily.TIMES_ROMAN, 7);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            //document header attributes
            doc.addAuthor("betterThanZero");
            doc.addCreationDate();
            doc.addProducer();
            doc.addCreator("MySampleCode.com");
            doc.addTitle("Report with Column Headings");
            doc.setPageSize(PageSize.A4);

            //open document
            doc.open();

            //create a paragraph
            Paragraph paragraph = new Paragraph("        Report : Status of Applications");


            //specify column widths
            float[] columnWidths = {2f, 4f, 6f, 6f, 3.5f, 2.5f, 3.8f, 2f, 2f, 3f};
            //create PDF table with the given widths
            PdfPTable table = new PdfPTable(columnWidths);
            // set table width a percentage of the page width
            table.setWidthPercentage(90f);

            //insert column headings
            insertCell(table, "SNO", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Hall Ticket", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Name Of the Candidate", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Father Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Date of Birth", Element.ALIGN_CENTER, 1, bfBold12);

            insertCell(table, "Gender", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Mobile", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Caste", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "EWS", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Area Studied", Element.ALIGN_CENTER, 1, bfBold12);

            table.setHeaderRows(1);

            int sno = 0;
            for (int x = 0; x < list.size(); x++) {
                HashMap map = list.get(x);
                sno++;
                insertCell(table, sno + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("hall") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("name") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("fname") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("dob") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("gender") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("mobile") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("caste") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("ews") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("areastu") + "", Element.ALIGN_LEFT, 1, bf12);
                map = null;
            }
            //add the PDF table to the paragraph 
            paragraph.add(table);
            // add the paragraph to the document
            doc.add(paragraph);

        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

// public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> getData(RegisterFormPhD rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        RegisterForm regform = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {

            Date todate1 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate1);

            con = DatabaseConnection.getConnection();

            cstmt = con.prepareCall("{Call USP_Candidate_Profile(?,?,?)}");
            cstmt.setString(1, rform.getCourse());///hallticket
            cstmt.setString(2, dob);
            cstmt.setString(3, rform.getPgmarksbsc());//flag


            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("hall", rs.getString(1));
                request.setAttribute("bname", rs.getString(2));
                request.setAttribute("fname", rs.getString(4));
                request.setAttribute("dob", rform.getDob());
                request.setAttribute("mothername", rs.getString(5));
                request.setAttribute("gender", rs.getString(6));
                request.setAttribute("caste", rs.getString(7));
                request.setAttribute("ews", rs.getString(8));
                request.setAttribute("fatheroccu", rs.getString(9));
                request.setAttribute("income", rs.getString(10));
                request.setAttribute("mobile", rs.getString(11));
                request.setAttribute("email", rs.getString(12));
                request.setAttribute("areaStudied", rs.getString(13));
                request.setAttribute("address", rs.getString(14) + "," + rs.getString(15) + "," + rs.getString(82) + "," + rs.getString(16) + "," + rs.getString(17) + "," + rs.getString(18) + "," + rs.getString(19));

                request.setAttribute("cap", rs.getString(20));
                request.setAttribute("ph", rs.getString(21));
                request.setAttribute("ncc", rs.getString(22));
                request.setAttribute("sports", rs.getString(23));

                request.setAttribute("marks", rs.getString(26));

                request.setAttribute("tenClass", rs.getString(29));
                request.setAttribute("tenYear", rs.getString(30));
                request.setAttribute("tenState", rs.getString(31));
                request.setAttribute("tenDistrict", rs.getString(32));
                request.setAttribute("tenArea", rs.getString(33));

                request.setAttribute("nineClass", rs.getString(34));
                request.setAttribute("nineYear", rs.getString(35));
                request.setAttribute("nineState", rs.getString(36));
                request.setAttribute("nineDistrict", rs.getString(37));
                request.setAttribute("nineArea", rs.getString(38));

                request.setAttribute("eightClass", rs.getString(39));
                request.setAttribute("eightYear", rs.getString(40));
                request.setAttribute("eightState", rs.getString(41));
                request.setAttribute("eightDistrict", rs.getString(42));
                request.setAttribute("eightArea", rs.getString(43));

                request.setAttribute("sevenClass", rs.getString(44));
                request.setAttribute("sevenYear", rs.getString(45));
                request.setAttribute("sevenState", rs.getString(46));
                request.setAttribute("sevenDistrict", rs.getString(47));
                request.setAttribute("sevenArea", rs.getString(48));

                request.setAttribute("sixClass", rs.getString(49));
                request.setAttribute("sixYear", rs.getString(50));
                request.setAttribute("sixState", rs.getString(51));
                request.setAttribute("sixDistrict", rs.getString(52));
                request.setAttribute("sixArea", rs.getString(53));

                request.setAttribute("fiveClass", rs.getString(54));
                request.setAttribute("fiveYear", rs.getString(55));
                request.setAttribute("fiveState", rs.getString(56));
                request.setAttribute("fiveDistrict", rs.getString(57));
                request.setAttribute("fiveArea", rs.getString(58));

                request.setAttribute("fourClass", rs.getString(59));
                request.setAttribute("fourYear", rs.getString(60));
                request.setAttribute("fourState", rs.getString(61));
                request.setAttribute("fourDistrict", rs.getString(62));
                request.setAttribute("fourArea", rs.getString(63));

                request.setAttribute("thirdClass", rs.getString(64));
                request.setAttribute("thirdYear", rs.getString(65));
                request.setAttribute("thirdState", rs.getString(66));
                request.setAttribute("thirdDistrict", rs.getString(67));
                request.setAttribute("thirdArea", rs.getString(68));

                request.setAttribute("secondClass", rs.getString(69));
                request.setAttribute("secondYear", rs.getString(70));
                request.setAttribute("secondState", rs.getString(71));
                request.setAttribute("secondDistrict", rs.getString(72));
                request.setAttribute("secondArea", rs.getString(73));

                request.setAttribute("firstClass", rs.getString(74));
                request.setAttribute("firstYear", rs.getString(75));
                request.setAttribute("firstState", rs.getString(76));
                request.setAttribute("firstDistrict", rs.getString(77));
                request.setAttribute("firstArea", rs.getString(78));


                request.setAttribute("sportsinternational", rs.getString(79));
                request.setAttribute("sportsnational", rs.getString(80));
                request.setAttribute("sportsstate", rs.getString(81));


                request.setAttribute("village", rs.getString(82));
                request.setAttribute("localStatus", rs.getString(83));

                request.setAttribute("lastUpdatedDate", rs.getString(84));
                request.setAttribute("paymentRefNo", rs.getString(85));
                request.setAttribute("paymentAmount", rs.getString(86));
                request.setAttribute("paymentDate", rs.getString(87));
                request.setAttribute("mat", rs.getString(88));
                request.setAttribute("phy", rs.getString(89));
                request.setAttribute("bio", rs.getString(90));
                try{
               String filepath = MessagePropertiesUtil.FIlE_PATH2 + "\\"+ rs.getString(91)+"\\"+rs.getString(91)+"_Photo.jpg";
File file = new File(filepath);
if (file.isFile()) {
                BufferedImage image = ImageIO.read(file);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "jpg", baos);
                baos.flush();
                byte[] imageInByteArray = baos.toByteArray();
                String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                request.setAttribute("photo", b64);
                }else{
    
                   String filepath1 = MessagePropertiesUtil.FIlE_PATH+ "\\"+ rs.getString(1)+"\\"+rs.getString(1)+"_PHOTO.jpg";
                 File file1 = new File(filepath1);
                BufferedImage image = ImageIO.read(file1);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "jpg", baos);
                baos.flush();
                byte[] imageInByteArray = baos.toByteArray();
                String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                request.setAttribute("photo", b64);  
                }
                 }catch(Exception e){
                    String   filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\"+ rs.getString(1)+"\\"+rs.getString(1)+"_PHOTO.jpg";
               File  file1 = new File(filepath1);
                BufferedImage image = ImageIO.read(file1);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "jpg", baos);
                baos.flush();
                byte[] imageInByteArray = baos.toByteArray();
                String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                request.setAttribute("photo", b64);   
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }
}
