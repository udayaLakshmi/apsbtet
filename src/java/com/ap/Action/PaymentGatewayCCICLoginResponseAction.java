/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Action;

import com.ap.DAO.PaymentGatewayCCICLoginDAO;
import com.ap.payment.PaymentWSAccess;
import com.sms.util.SMSSendService;
import com.sms.util.SendEmail;
import com.sms.util.SendSMSDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.RegisterDao;
import net.apo.angrau.db.DatabaseConnection;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1582792
 */
public class PaymentGatewayCCICLoginResponseAction extends DispatchAction {

    final static String SUCCESS = "success";

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String res = "";
        String decryptData = "";
        ArrayList list = new ArrayList();
        Map map = new HashMap();
        String reqId = "";
        String appId = "";
        String PgRefNo = "";
        String baseAmt = "";
        String Conv_Charges = "";
        String gst = "";
        String serviceId = "";
        String checksum = "";
        String customerName = "";
        String serviceCharges = "";
        String status = "";
        HttpSession session = request.getSession();
        PaymentGatewayCCICLoginDAO dao = new PaymentGatewayCCICLoginDAO();
        ArrayList paymentDoneList = new ArrayList();
        String resultStatus = null;
        SendSMSDTO sendSMSDTO = new SendSMSDTO();
        //  ArrayList list = new ArrayList();
        ArrayList<InternetAddress> ToMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> CCMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> BCCMailsList = new ArrayList<InternetAddress>();
        InternetAddress temailId = null;
        InternetAddress ccmailId = null;
        InternetAddress bccmailId = null;
        Connection con = null;
        String query =  null;
        PreparedStatement pstmt = null;
        try {
            res = request.getParameter("resp").toString().replace(" ", "+");
            if (res != null) {
                // dao.encryptedRes(res, userName);
                //decryptData = decrypt(res);

                decryptData = PaymentWSAccess.decrypt(res);

//                 decryptData = "924820210713160835287|EBS1307AEF9C666B|200|1.89|1001|6527|2070516212|wq";//decrypt("");
                String[] arrData = decryptData.split("\\|");
//                System.out.println("Decrypted Array Dataa-->" + arrData);
                for (int i = 0; i < arrData.length; i++) {
                    if (i == 0) {
                        map.put("reqId", arrData[i]);
                        reqId = arrData[i];
//                        System.out.println("reqId reqId -->" + reqId);
                    } else if (i == 1) {
                        map.put("PgRefNo", arrData[i]);
                        PgRefNo = arrData[i];
//                        System.out.println("PgRefNo" + PgRefNo);
                    } else if (i == 2) {
                        map.put("baseAmt", arrData[i]);
                        baseAmt = arrData[i];
//                        System.out.println("baseAmt" + baseAmt);
                    } else if (i == 3) {
                        map.put("Conv_Charges", arrData[i]);
                        Conv_Charges = arrData[i];
                    } else if (i == 4) {
                        map.put("gst", arrData[i]); // gst as DeptID
                        gst = arrData[i];
                    } else if (i == 5) {
                        map.put("serviceId", arrData[i]);
                        serviceId = arrData[i];
                    } else if (i == 6) {
                        map.put("checksum", arrData[i]);
                        checksum = arrData[i];
                    } else if (i == 7) {
                        map.put("customerName", arrData[i]);
                        customerName = arrData[i];
                    }
                }
                status = dao.paymentSuccess(reqId, appId, PgRefNo, baseAmt, Conv_Charges, gst, serviceId, checksum, customerName, serviceCharges, appId);
//                list.add(map);
//                request.setAttribute("paymentres", list);

                if (status != null || status.equalsIgnoreCase("Inserted Successfully")) {

                    paymentDoneList = dao.getDataAfterPayment(reqId);
                    request.setAttribute("paymentres", paymentDoneList);

                    //   target="success";
                    // Added on 22-Nov-2020 for SMS and Email Integration
                /*    try {
                        RegisterDao smsdaoInsert = new RegisterDao();
                        if (paymentDoneList.size() > 0) {
                            HashMap paymap = (HashMap) paymentDoneList.get(0);
                            String mobilesms = paymap.get("mobile").toString();
                            String email = paymap.get("email").toString();

                            String sms = "Dear Applicant,Your application for admission into Polytechnic Courses has been registered successfully. Please visit Admissions page on (www.angrau.ac.in) for further and latest updates. Registrar,ANGRAU   ";
                            resultStatus = SMSSendService.sendSMS(sms, mobilesms);
                            sendSMSDTO.setMobileNumber(mobilesms);
                            sendSMSDTO.setSystemIp(request.getRemoteAddr());
                            sendSMSDTO.setSms(sms);
                            sendSMSDTO.setLoginId("");
                            if (resultStatus.equalsIgnoreCase("OK") || resultStatus.equalsIgnoreCase("Message Sent Successfully") || resultStatus.equalsIgnoreCase("SENT")) {
                                sendSMSDTO.setSendStatus("Sent");
                            } else {
                                sendSMSDTO.setSendStatus("Not Send");
                            }
                            smsdaoInsert.insertSmsLogDetails(sendSMSDTO);
                            //  Email Configuration 
                            if (email != null) {
                                String emails = email;
                                temailId = new InternetAddress(emails, "");
                                String bccemails = "janakiramaiah.peddi@aptonline.in";
                        String ccemailId = "ugadmissionsangrau@gmail.com";
                                bccmailId = new InternetAddress(bccemails, "");
                        ccmailId = new InternetAddress(ccemailId, "");
                                ToMailsList.add(temailId);
                                BCCMailsList.add(bccmailId);
                        CCMailsList.add(ccmailId);
//                                boolean emailResult = SendEmail.sendEmailANGRANGA(ToMailsList, CCMailsList, BCCMailsList, "UG Admission Registration", "Dear Applicant,<br>Your application for admission into BiPC Stream UG Courses has been registered successfully.\n"
//                                        + "<br>Please visit Admissions page on (www.angrau.ac.in) for further and latest updates.  <br>Registrar,<br>ANGRAU<br><br><br>Note: This is auto generated e-mail, please do not reply.");
                            }


                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } */

 try {
//                        System.out.println("qqqqqqqqqqqqqqqqq");
                        //  Email Configuration 
//                        RegisterDao smsdaoInsert = new RegisterDao();
                        if (paymentDoneList.size() > 0) {
                            HashMap paymap = (HashMap) paymentDoneList.get(0);
                            String mobilesms = paymap.get("mobile").toString();
                            String email = paymap.get("email").toString();
                            String reqNO = paymap.get("reqId").toString();
                            String Exam = paymap.get("grade").toString();
// SMS 
                            RegisterDao smsdaoInsert = new RegisterDao();
                            String sms = "Dear Applicant, Your application for enrolling "+Exam+" examination has been registered successfully.  Please visit www.sbtetap.gov.in for further and latest updates.Secretary, SBTET AP";
                            resultStatus = SMSSendService.sendSMS(sms, mobilesms);
                            sendSMSDTO.setMobileNumber(mobilesms);
                            sendSMSDTO.setSystemIp(request.getRemoteAddr());
                            sendSMSDTO.setSms(sms);
                            sendSMSDTO.setLoginId("");
                            if (resultStatus.equalsIgnoreCase("OK") || resultStatus.equalsIgnoreCase("Message Sent Successfully") || resultStatus.equalsIgnoreCase("SENT")) {
                                sendSMSDTO.setSendStatus("Sent");
                            } else {
                                sendSMSDTO.setSendStatus("Not Send");
                            }
                            smsdaoInsert.insertSmsLogDetails(sendSMSDTO);   
                            
                            
                            // SMS
                            if (email != null) {
                                String emails = email;
                                temailId = new InternetAddress(emails, "");
                                String bccemails = "janakiramaiah.peddi@aptonline.in";
                                String ccemailId = "";
                                bccmailId = new InternetAddress(bccemails, "");
                                ccmailId = new InternetAddress(ccemailId, "");
                                ToMailsList.add(temailId);
                                BCCMailsList.add(bccmailId);
                                CCMailsList.add(ccmailId);
                                boolean emailResult = SendEmail.sendEmailANGRANGA(ToMailsList, CCMailsList, BCCMailsList, "CCIC Payment", "Dear Applicant,<br>Your payment for registered Application No." + reqNO + " for enrolling " + Exam + " examination has been received. Please visit www.sbtetap.gov.in  for further and latest updates.  <br>Secretary,<br>SBTET AP<br><br><br>Note: This is auto generated e-mail, please do not reply.");
                                try {
                                    int i = 0;
                                    con = DatabaseConnection.getConnection();
                                    query = "insert into  CCIC_Email_Log (EmailID,status,Subject,created_date)  values ('" + email + "','" + emailResult + "','Payment',getdate())\n";
                                    pstmt = con.prepareStatement(query);
                                    i = pstmt.executeUpdate();
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                } finally {
                                    if (pstmt != null) {
                                        pstmt.close();
                                    }
                                    if (con != null) {
                                        con.close();
                                    }
                                }
                            }
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

        } catch (Exception e) {
            request.setAttribute("failed", "Your transaction was declained due to Server Failure. Money will be credited your bank account within 7 working days");
            e.printStackTrace();
        }
        return mapping.findForward(SUCCESS);

    }
}
    

