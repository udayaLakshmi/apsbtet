/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Action;

import com.ap.DAO.PaymentGatewayDiplomaCTwentyDAO;
import com.ap.DAO.PaymentGatewayDiplomaDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.ap.payment.PaymentWSAccess;
import com.sms.util.SendSMSDTO;
import javax.mail.internet.InternetAddress;
/**
 *
 * @author 1582792
 */
public class PaymentGatewayDiplomaResponseCTwenty extends DispatchAction {

    final static String SUCCESS = "success";

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String res = "";
        String decryptData = "";
        ArrayList list = new ArrayList();
        Map map = new HashMap();
        String reqId = "";
        String appId = "";
        String PgRefNo = "";
        String baseAmt = "";
        String Conv_Charges = "";
        String gst = "";
        String serviceId = "";
        String checksum = "";
        String customerName = "";
        String serviceCharges = "";
        String status = "";
        HttpSession session = request.getSession();
        PaymentGatewayDiplomaCTwentyDAO dao = new PaymentGatewayDiplomaCTwentyDAO();
        ArrayList paymentDoneList = new ArrayList();
        String resultStatus = null;
        SendSMSDTO sendSMSDTO = new SendSMSDTO();
        //  ArrayList list = new ArrayList();
        ArrayList<InternetAddress> ToMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> CCMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> BCCMailsList = new ArrayList<InternetAddress>();
        InternetAddress temailId = null;
        InternetAddress ccmailId = null;
        InternetAddress bccmailId = null;
//        Connection con = null;
        String query = null;
//        PreparedStatement pstmt = null;
        try {
            res = request.getParameter("resp").toString().replace(" ", "+");
            if (res != null) {
                // dao.encryptedRes(res, userName);
                //decryptData = decrypt(res);

                decryptData = PaymentWSAccess.decrypt(res);

//                 decryptData = "13012AEI21120210911155722900|EBS11092E9CEBAF3|550|5.19|1001|6527|1539728591|GANDROTHU VEERENDRA KALYAN KUMAR";//decrypt("");
                String[] arrData = decryptData.split("\\|");
//                System.out.println("Decrypted Array Dataa-->" + arrData);
                for (int i = 0; i < arrData.length; i++) {
                    if (i == 0) {
                        map.put("reqId", arrData[i]);
                        reqId = arrData[i];
//                        System.out.println("reqId reqId -->" + reqId);
                    } else if (i == 1) {
                        map.put("PgRefNo", arrData[i]);
                        PgRefNo = arrData[i];
//                        System.out.println("PgRefNo" + PgRefNo);
                    } else if (i == 2) {
                        map.put("baseAmt", arrData[i]);
                        baseAmt = arrData[i];
//                        System.out.println("baseAmt" + baseAmt);
                    } else if (i == 3) {
                        map.put("Conv_Charges", arrData[i]);
                        Conv_Charges = arrData[i];
                    } else if (i == 4) {
                        map.put("gst", arrData[i]); // gst as DeptID
                        gst = arrData[i];
                    } else if (i == 5) {
                        map.put("serviceId", arrData[i]);
                        serviceId = arrData[i];
                    } else if (i == 6) {
                        map.put("checksum", arrData[i]);
                        checksum = arrData[i];
                    } else if (i == 7) {
                        map.put("customerName", arrData[i]);
                        customerName = arrData[i];
                    }
                }
                status = dao.paymentSuccess(reqId, appId, PgRefNo, baseAmt, Conv_Charges, gst, serviceId, checksum, customerName, serviceCharges, appId);
//                list.add(map);
//                request.setAttribute("paymentres", list);

                if (status != null || status.equalsIgnoreCase("Inserted Successfully")) {

                    paymentDoneList = dao.getDataAfterPayment(reqId);
                    request.setAttribute("paymentres", paymentDoneList);


                }
            }
        } catch (Exception e) {
            request.setAttribute("failed", "Your transaction was declained due to Server Failure. Money will be credited your bank account within 7 working days");
            e.printStackTrace();
        }
        return mapping.findForward(SUCCESS);

    }
}
