/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.payment;

/**
 *
 * @author 1629028
 */
public class PaymentResponseDto1 {
    
    private String requestNo;
    private String pgTransNo;
    private String baseAmount;
    private String charges;
    private String userName;
    private String status;
    private String createdBy;
   

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getPgTransNo() {
        return pgTransNo;
    }

    public void setPgTransNo(String pgTransNo) {
        this.pgTransNo = pgTransNo;
    }

    public String getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(String baseAmount) {
        this.baseAmount = baseAmount;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public double getTransactionAmount() {
        
        try{
          return Double.parseDouble(baseAmount) + Double.parseDouble(charges);  
        }catch(Exception e){
            return 2000.0;
        }
        
    }

   
    
    

    @Override
    public String toString() {
        return "PaymentResponseDto{" + "requestNo=" + requestNo + ", pgTransNo=" + pgTransNo + ", baseAmount=" + baseAmount + ", charges=" + charges + ", userName=" + userName + ", status=" + status + '}';
    }
    
    
         
    
    
}
