/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.payment;

/**
 *
 * @author 1582792
 */
public class PaymentGateWayDto {
	private String requestID;
        private String clientid;       
        private String deptID;
        private String serviceID;
        private String paymentMode;
        private double baseAmount;
        private double convienceCharges;
        private double transactionAmount;
        private String returnURL;
        private String customerName;
        private String address;
        private String city;
        private String state;
        private String pincode;
        private String emailAddress;
        private String phoneNo;
        private String checksum;
        private String failureURL;
        private String additionlInfo1;
        private String additionlInfo2;
        private String additionlInfo3;
        private String additionlInfo4;
        private String additionlInfo5;
        private String payType;

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

        
        
        
        
    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getDeptID() {
        return deptID;
    }

    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public double getConvienceCharges() {
        return convienceCharges;
    }

    public void setConvienceCharges(double convienceCharges) {
        this.convienceCharges = convienceCharges;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getReturnURL() {
        return returnURL;
    }

    public void setReturnURL(String returnURL) {
        this.returnURL = returnURL;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getFailureURL() {
        return failureURL;
    }

    public void setFailureURL(String failureURL) {
        this.failureURL = failureURL;
    }

    public String getAdditionlInfo1() {
        return additionlInfo1;
    }

    public void setAdditionlInfo1(String additionlInfo1) {
        this.additionlInfo1 = additionlInfo1;
    }

    public String getAdditionlInfo2() {
        return additionlInfo2;
    }

    public void setAdditionlInfo2(String additionlInfo2) {
        this.additionlInfo2 = additionlInfo2;
    }

    public String getAdditionlInfo3() {
        return additionlInfo3;
    }

    public void setAdditionlInfo3(String additionlInfo3) {
        this.additionlInfo3 = additionlInfo3;
    }

    public String getAdditionlInfo4() {
        return additionlInfo4;
    }

    public void setAdditionlInfo4(String additionlInfo4) {
        this.additionlInfo4 = additionlInfo4;
    }

    public String getAdditionlInfo5() {
        return additionlInfo5;
    }

    public void setAdditionlInfo5(String additionlInfo5) {
        this.additionlInfo5 = additionlInfo5;
    }
       
        
}
