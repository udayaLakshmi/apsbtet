package com.ap.payment;



import com.ap.payment.DecryptEcrypte;
import com.ap.payment.PaymentGateWayDto;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
//import loadonstartUp.ProjectConfig;
import org.apache.log4j.Logger;
import ws.pg.org.tempuri.EncryptDEcrypt;
import ws.pg.org.tempuri.EncryptDEcryptSoap;

public class PaymentGatewayRequestHelper {
static Logger LOG = Logger.getLogger(PaymentGatewayRequestHelper.class.getName());
//    private final String PSURL = PayConfig.PGRESPURL();
    private final String CLIENT_ID;
    private final String SERVICE_ID;
    private final String DEPT_ID;
    private final String CHECKSUM_KEY;
    private final String RETURN_SC_URL;
    private final String RETURN_FL_URL;
    private final String APP_REQ_CODE;
    private final PaymentGateWayDto DTO;

    public PaymentGatewayRequestHelper(PaymentGateWayDto dto) {
        this.APP_REQ_CODE = dto.getRequestID();
        this.CLIENT_ID = dto.getClientid();
        this.SERVICE_ID = dto.getServiceID();
        this.DEPT_ID = dto.getDeptID();
        this.CHECKSUM_KEY = dto.getChecksum();
        this.RETURN_SC_URL =  dto.getReturnURL();
        this.RETURN_FL_URL =  dto.getFailureURL();
        this.DTO = dto;
    }

    
//parameters: 1234567899999|100|1001|6512|NB|0.0|5.0|2005.9|||address|AP|AndhraPradesh|500072|||YESCHCKERDA||NA|NA|NA|NA|NA
 //   public String[] pgRequest() {
    
    public String[] pgRequest (String applicationServiceNumber,String payMode,double amount,String grade){
        try {
            String ACCESS = "UAT";
            String[] sArry = new String[3];
            String checkSumData ="YESCHCKERDA";
            StringBuilder requestSB = new StringBuilder();
            DecryptEcrypte encDec = new DecryptEcrypte();
            Connection con = null;
              double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String requestId = null;
        String mobile = null;
        String name = null;
            System.out.println("dddd"+applicationServiceNumber);
            try{
                con = DatabaseConnection.getConnection();
                stmt = con.prepareCall("{call proc_PaymentMode(?,?,?,?)}");
                stmt.setDouble(1, amount);
                stmt.setString(2, payMode);
                stmt.setString(3, applicationServiceNumber);
                stmt.setString(4, grade);
                System.out.println("oooooooo"+payMode);
                System.out.println("yyyyyyyyy"+applicationServiceNumber);
                rs = stmt.executeQuery();
                if (rs != null & rs.next()) {

                    ConvenienceCharges = rs.getDouble(2);
                    System.out.println("ConvenienceCharges"+ConvenienceCharges);
                    GST_Charges = rs.getDouble(3);
                    totConvGst = rs.getDouble(4);
                    totalFinalAmount = rs.getDouble(5);
                    requestId = rs.getString(6);
                    name = rs.getString(7);
                    System.out.println("name"+name);
                    mobile = rs.getString(8);
                    System.out.println("mobile"+mobile);
                    // name mobi
                    //
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
            if(payMode.equals("EBS")){
                 payMode = "DC";
             }else  if(payMode.equals("PTM")){
                 payMode = "NB";
             }else{
                 payMode = payMode;
             }
            
            
            
//            String parameters = requestSB.append(requestId).append("|").append("100").append("|").append("1001")
//                    .append("|").append("6527").append("|").append(payMode).append("|").append(amount)
//                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("http://sbtet.ap.gov.in/SBTET/paymentSuccess.do")
//                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
//                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
//                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("http://sbtet.ap.gov.in/SBTET/paymentFailurePage.do")
//                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
//                    .append("|").append("NA").append("|").append("NA").toString();

             String parameters = requestSB.append(requestId).append("|").append("128").append("|").append("1135")
                    .append("|").append("6563").append("|").append(payMode).append("|").append(amount)
                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentSuccess.do")
                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentFailurePage.do")
                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
                    .append("|").append("NA").append("|").append("NA").toString();

            
           // String check = APP_REQ_CODE+"#"+CLIENT_ID+"#"+SERVICE_ID+"#"+DTO.getPaymentMode()+"#"+DTO.getBaseAmount()+"#"+CHECKSUM_KEY+"#"+ ProjectConfig.ACCESS+"#"+DTO.getPayType();
//             String check = requestId+"#"+100+"#"+6527+"#"+payMode+"#"+amount+"#"+741852963+"#"+ACCESS+"#"+payMode;
             
              String check = requestId+"#"+128+"#"+6563+"#"+payMode+"#"+amount+"#"+1131882120+"#"+ACCESS+"#"+payMode;
           
            System.out.println("parameters: "+parameters);
            System.out.println("check: "+check);
            
            sArry[0] = encDec.encrypt(parameters);
            sArry[1] = encDec.encrypt(check);
            return (sArry);
        } catch (Exception e) {
            LOG.error("e ",e);
            e.printStackTrace();
            return null;
        }
    }
public String[] pgRequestMpc (String applicationServiceNumber,String payMode,double amount,String grade){
        try {
            String ACCESS = "UAT";
            String[] sArry = new String[3];
            String checkSumData ="YESCHCKERDA";
            StringBuilder requestSB = new StringBuilder();
            DecryptEcrypte encDec = new DecryptEcrypte();
            Connection con = null;
              double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String requestId = null;
        String mobile = null;
        String name = null;
            System.out.println("dddd"+applicationServiceNumber);
            try{
                con = DatabaseConnection.getConnection();
                stmt = con.prepareCall("{call proc_PaymentMode_ENG(?,?,?,?)}");
                stmt.setDouble(1, amount);
                stmt.setString(2, payMode);
                stmt.setString(3, applicationServiceNumber);
                stmt.setString(4, grade);
                System.out.println("oooooooo"+payMode);
                System.out.println("yyyyyyyyy"+applicationServiceNumber);
                rs = stmt.executeQuery();
                if (rs != null & rs.next()) {

                    ConvenienceCharges = rs.getDouble(2);
                    System.out.println("ConvenienceCharges"+ConvenienceCharges);
                    GST_Charges = rs.getDouble(3);
                    totConvGst = rs.getDouble(4);
                    totalFinalAmount = rs.getDouble(5);
                    requestId = rs.getString(6);
                    name = rs.getString(7);
                    System.out.println("name"+name);
                    mobile = rs.getString(8);
                    System.out.println("mobile"+mobile);
                    // name mobi
                    //
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
            if(payMode.equals("EBS")){
                 payMode = "DC";
             }else  if(payMode.equals("PTM")){
                 payMode = "NB";
             }else{
                 payMode = payMode;
             }
            
            
//            String parameters = requestSB.append(requestId).append("|").append("100").append("|").append("1001")
//                    .append("|").append("6527").append("|").append(payMode).append("|").append(amount)
//                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("http://sbtet.ap.gov.in/SBTET/paymentSuccessLogin.do")
//                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
//                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
//                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("http://sbtet.ap.gov.in/SBTET/paymentFailurePageLogin.do")
//                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
//                    .append("|").append("NA").append("|").append("NA").toString();

                   String parameters = requestSB.append(requestId).append("|").append("128").append("|").append("1135")
                    .append("|").append("6563").append("|").append(payMode).append("|").append(amount)
                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://sbtet.ap.gov.in/APSBTET1/paymentSuccessLogin.do")
                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://sbtet.ap.gov.in/APSBTET1/paymentFailurePageLogin.do")
                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
                    .append("|").append("NA").append("|").append("NA").toString();

            
           // String check = APP_REQ_CODE+"#"+CLIENT_ID+"#"+SERVICE_ID+"#"+DTO.getPaymentMode()+"#"+DTO.getBaseAmount()+"#"+CHECKSUM_KEY+"#"+ ProjectConfig.ACCESS+"#"+DTO.getPayType();
           
//             String check = requestId+"#"+100+"#"+6527+"#"+payMode+"#"+amount+"#"+741852963+"#"+ACCESS+"#"+payMode;
            String check = requestId+"#"+128+"#"+6563+"#"+payMode+"#"+amount+"#"+1131882120+"#"+ACCESS+"#"+payMode;
           
            System.out.println("parameters: "+parameters);
            System.out.println("check: "+check);
            
            sArry[0] = encDec.encrypt(parameters);
            sArry[1] = encDec.encrypt(check);
            return (sArry);
        } catch (Exception e) {
            LOG.error("e ",e);
            e.printStackTrace();
            return null;
        }
    }

   /* private String checkSumValue() throws Exception {
        EncryptDEcrypt service = new EncryptDEcrypt();
        EncryptDEcryptSoap port = service.getEncryptDEcryptSoap();
        return port.checkSumforRequest(APP_REQ_CODE, CLIENT_ID, SERVICE_ID, DTO.getPaymentMode(), DTO.getBaseAmount(), CHECKSUM_KEY);
    }

    private static String encrypt(String request) throws Exception {
        EncryptDEcrypt service = new EncryptDEcrypt();
        EncryptDEcryptSoap port = service.getEncryptDEcryptSoap();
        return port.encrypt(request);
    }*/

   
//        public static void main(String... ar) throws Exception{
//            String parameters ="1099|105|1103|6330|CC|1000.0|1.2|1001.2|http://localhost:8084/PrivateSchool/paymentResponse.do|jjj|jjj|Na|Na|500072|aaa@asa.in|9526536565|3390918437|http://localhost:8084/PrivateSchool/paymentResponseFailed.do|NA|NA|NA|NA|NA";
//            System.out.println("enc "+encrypt(parameters));
//    }



 public String[] pgRequestBeforeCCIC (String applicationServiceNumber,String payMode,double amount,String grade){
        try {
            String ACCESS = "UAT";
            String[] sArry = new String[3];
            String checkSumData ="YESCHCKERDA";
            StringBuilder requestSB = new StringBuilder();
            DecryptEcrypte encDec = new DecryptEcrypte();
            Connection con = null;
              double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String requestId = null;
        String mobile = null;
        String name = null;
            System.out.println("dddd"+applicationServiceNumber);
            try{
                con = DatabaseConnection.getConnection();
                stmt = con.prepareCall("{call proc_PaymentMode_CCIC_Before(?,?,?,?)}");
                stmt.setDouble(1, amount);
                stmt.setString(2, payMode);
                stmt.setString(3, applicationServiceNumber);
                stmt.setString(4, grade);
                System.out.println("oooooooo"+payMode);
                System.out.println("yyyyyyyyy"+applicationServiceNumber);
                 System.out.println("grade"+grade);
                   System.out.println("amount"+amount);
                rs = stmt.executeQuery();
                if (rs != null & rs.next()) {

                    ConvenienceCharges = rs.getDouble(2);
                    System.out.println("ConvenienceCharges"+ConvenienceCharges);
                    GST_Charges = rs.getDouble(3);
                    totConvGst = rs.getDouble(4);
                    totalFinalAmount = rs.getDouble(5);
                    requestId = rs.getString(6);
                    name = rs.getString(7);
                    System.out.println("name"+name);
                    mobile = rs.getString(8);
                    System.out.println("totConvGst"+totConvGst);
                       System.out.println("requestId"+requestId);
                          System.out.println("mobile"+mobile);
                    // name mobi
                    //
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
            if(payMode.equals("EBS")){
                 payMode = "DC";
             }else  if(payMode.equals("PTM")){
                 payMode = "NB";
             }else{
                 payMode = payMode;
             }
            
            
            
//            String parameters = requestSB.append(requestId).append("|").append("100").append("|").append("1001")
//                    .append("|").append("6527").append("|").append(payMode).append("|").append(amount)
//                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://sbtetuat.ap.gov.in/CCIC/paymentSuccessBeforeCCIC.do")
//                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
//                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
//                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://sbtetuat.ap.gov.in/CCIC/paymentFailurePageBeforeCCIC.do")
//                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
//                    .append("|").append("NA").append("|").append("NA").toString();

             String parameters = requestSB.append(requestId).append("|").append("128").append("|").append("1135")
                    .append("|").append("6563").append("|").append(payMode).append("|").append(amount)
                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentSuccessBeforeCCIC.do")
                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentFailurePageBeforeCCIC.do")
                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
                    .append("|").append("NA").append("|").append("NA").toString();

            
           // String check = APP_REQ_CODE+"#"+CLIENT_ID+"#"+SERVICE_ID+"#"+DTO.getPaymentMode()+"#"+DTO.getBaseAmount()+"#"+CHECKSUM_KEY+"#"+ ProjectConfig.ACCESS+"#"+DTO.getPayType();
//             String check = requestId+"#"+100+"#"+6527+"#"+payMode+"#"+amount+"#"+741852963+"#"+ACCESS+"#"+payMode;
             
              String check = requestId+"#"+128+"#"+6563+"#"+payMode+"#"+amount+"#"+1131882120+"#"+ACCESS+"#"+payMode;
           
            System.out.println("parameters: "+parameters);
            System.out.println("check: "+check);
            
            sArry[0] = encDec.encrypt(parameters);
            sArry[1] = encDec.encrypt(check);
            return (sArry);
        } catch (Exception e) {
            LOG.error("e ",e);
            e.printStackTrace();
            return null;
        }
    }
 
    
    public String[] pgRequestCCIC (String applicationServiceNumber,String payMode,double amount,String grade){
        try {
            String ACCESS = "UAT";
            String[] sArry = new String[3];
            String checkSumData ="YESCHCKERDA";
            StringBuilder requestSB = new StringBuilder();
            DecryptEcrypte encDec = new DecryptEcrypte();
            Connection con = null;
              double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String requestId = null;
        String mobile = null;
        String name = null;
            System.out.println("dddd"+applicationServiceNumber);
            try{
                con = DatabaseConnection.getConnection();
                stmt = con.prepareCall("{call proc_PaymentMode_CCIC_Login(?,?,?,?)}");
                stmt.setDouble(1, amount);
                stmt.setString(2, payMode);
                stmt.setString(3, applicationServiceNumber);
                stmt.setString(4, grade);
                System.out.println("oooooooo"+payMode);
                System.out.println("yyyyyyyyy"+applicationServiceNumber);
                rs = stmt.executeQuery();
                if (rs != null & rs.next()) {

                    ConvenienceCharges = rs.getDouble(2);
                    System.out.println("ConvenienceCharges"+ConvenienceCharges);
                    GST_Charges = rs.getDouble(3);
                    totConvGst = rs.getDouble(4);
                    totalFinalAmount = rs.getDouble(5);
                    requestId = rs.getString(6);
                    name = rs.getString(7);
                    System.out.println("name"+name);
                    mobile = rs.getString(8);
                    System.out.println("mobile"+mobile);
                    // name mobi
                    //
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
            if(payMode.equals("EBS")){
                 payMode = "DC";
             }else  if(payMode.equals("PTM")){
                 payMode = "NB";
             }else{
                 payMode = payMode;
             }
            
            
            
//            String parameters = requestSB.append(requestId).append("|").append("100").append("|").append("1001")
//                    .append("|").append("6527").append("|").append(payMode).append("|").append(amount)
//                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://sbtetuat.ap.gov.in/CCIC/paymentSuccessCCICLogin.do")
//                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
//                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
//                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://sbtetuat.ap.gov.in/CCIC/paymentFailurePageCCICLogin.do")
//                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
//                    .append("|").append("NA").append("|").append("NA").toString();

             String parameters = requestSB.append(requestId).append("|").append("128").append("|").append("1135")
                    .append("|").append("6563").append("|").append(payMode).append("|").append(amount)
                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentSuccessCCICLogin.do")
                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentFailurePageCCICLogin.do")
                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
                    .append("|").append("NA").append("|").append("NA").toString();

            
           // String check = APP_REQ_CODE+"#"+CLIENT_ID+"#"+SERVICE_ID+"#"+DTO.getPaymentMode()+"#"+DTO.getBaseAmount()+"#"+CHECKSUM_KEY+"#"+ ProjectConfig.ACCESS+"#"+DTO.getPayType();
//             String check = requestId+"#"+100+"#"+6527+"#"+payMode+"#"+amount+"#"+741852963+"#"+ACCESS+"#"+payMode;
             
              String check = requestId+"#"+128+"#"+6563+"#"+payMode+"#"+amount+"#"+1131882120+"#"+ACCESS+"#"+payMode;
           
            System.out.println("parameters: "+parameters);
            System.out.println("check: "+check);
            
            sArry[0] = encDec.encrypt(parameters);
            sArry[1] = encDec.encrypt(check);
            return (sArry);
        } catch (Exception e) {
            LOG.error("e ",e);
            e.printStackTrace();
            return null;
        }
    }
        public String[] pgRequestDiploma (String applicationServiceNumber,String payMode,double amount,String grade){
        try {
            String ACCESS = "UAT";
            String[] sArry = new String[3];
            String checkSumData ="YESCHCKERDA";
            StringBuilder requestSB = new StringBuilder();
            DecryptEcrypte encDec = new DecryptEcrypte();
            Connection con = null;
              double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String requestId = null;
        String mobile = null;
        String name = null;
            System.out.println("dddd"+applicationServiceNumber);
            try{
                con = DatabaseConnection.getConnectionDiploma();
                stmt = con.prepareCall("{call proc_PaymentMode_Diploma(?,?,?,?)}");
                stmt.setDouble(1, amount);
                stmt.setString(2, payMode);
                stmt.setString(3, applicationServiceNumber);
                stmt.setString(4, grade);
//                System.out.println("oooooooo"+payMode);
//                System.out.println("yyyyyyyyy"+applicationServiceNumber);
                rs = stmt.executeQuery();
                if (rs != null & rs.next()) {

                    ConvenienceCharges = rs.getDouble(2);
//                    System.out.println("ConvenienceCharges"+ConvenienceCharges);
                    GST_Charges = rs.getDouble(3);
                    totConvGst = rs.getDouble(4);
                    totalFinalAmount = rs.getDouble(5);
                    requestId = rs.getString(6);
                    name = rs.getString(7);
//                    System.out.println("name"+name);
                    mobile = rs.getString(8);
//                    System.out.println("mobile"+mobile);
                    // name mobi
                    //
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
            if(payMode.equals("EBS")){
                 payMode = "DC";
             }else  if(payMode.equals("PTM")){
                 payMode = "NB";
             }else{
                 payMode = payMode;
             }
            
            
            
//            String parameters = requestSB.append(requestId).append("|").append("100").append("|").append("1001")
//                    .append("|").append("6527").append("|").append(payMode).append("|").append(amount)
//                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("http://sbtetuat.ap.gov.in/SBTET/paymentSuccessDiploma.do")
//                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
//                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
//                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("http://sbtetuat.ap.gov.in/SBTET/paymentFailurePageDiploma.do")
//                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
//                    .append("|").append("NA").append("|").append("NA").toString();

             String parameters = requestSB.append(requestId).append("|").append("128").append("|").append("1135")
                    .append("|").append("6563").append("|").append(payMode).append("|").append(amount)
                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentSuccessDiploma.do")
                    .append("|").append(name).append("|").append("Address").append("|").append(applicationServiceNumber)
                    .append("|").append(grade).append("|").append("500000").append("|").append("test@gmail.com")
                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentFailurePageDiploma.do")
                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
                    .append("|").append("NA").append("|").append("NA").toString();

            
           // String check = APP_REQ_CODE+"#"+CLIENT_ID+"#"+SERVICE_ID+"#"+DTO.getPaymentMode()+"#"+DTO.getBaseAmount()+"#"+CHECKSUM_KEY+"#"+ ProjectConfig.ACCESS+"#"+DTO.getPayType();
//             String check = requestId+"#"+100+"#"+6527+"#"+payMode+"#"+amount+"#"+741852963+"#"+ACCESS+"#"+payMode;
             
              String check = requestId+"#"+128+"#"+6563+"#"+payMode+"#"+amount+"#"+1131882120+"#"+ACCESS+"#"+payMode;
           
            System.out.println("parameters: "+parameters);
            System.out.println("check: "+check);
            
            sArry[0] = encDec.encrypt(parameters);
            sArry[1] = encDec.encrypt(check);
            return (sArry);
        } catch (Exception e) {
            LOG.error("e ",e);
            e.printStackTrace();
            return null;
        }
    }
        

public String[] pgRequestDiplomaCTwenty (String applicationServiceNumber,String payMode,double amount,String grade){
        try {
            String ACCESS = "UAT";
            String[] sArry = new String[3];
            String checkSumData ="YESCHCKERDA";
            StringBuilder requestSB = new StringBuilder();
            DecryptEcrypte encDec = new DecryptEcrypte();
            Connection con = null;
              double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String requestId = null;
        String mobile = null;
        String name = null;
            System.out.println("dddd"+applicationServiceNumber);
            try{
                con = DatabaseConnection.getConnectionDiplomaC20();
                stmt = con.prepareCall("{call proc_PaymentMode_Diploma(?,?,?,?)}");
                stmt.setDouble(1, amount);
                stmt.setString(2, payMode);
                stmt.setString(3, applicationServiceNumber);
                stmt.setString(4, grade);
//                System.out.println("oooooooo"+payMode);
//                System.out.println("yyyyyyyyy"+applicationServiceNumber);
                rs = stmt.executeQuery();
                if (rs != null & rs.next()) {

                    ConvenienceCharges = rs.getDouble(2);
//                    System.out.println("ConvenienceCharges"+ConvenienceCharges);
                    GST_Charges = rs.getDouble(3);
                    totConvGst = rs.getDouble(4);
                    totalFinalAmount = rs.getDouble(5);
                    requestId = rs.getString(6);
                    name = rs.getString(7);
//                    System.out.println("name"+name);
                    mobile = rs.getString(8);
//                    System.out.println("mobile"+mobile);
                    // name mobi
                    //
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
            if(payMode.equals("EBS")){
                 payMode = "DC";
             }else  if(payMode.equals("PTM")){
                 payMode = "NB";
             }else{
                 payMode = payMode;
             }
            
            
            
//            String parameters = requestSB.append(requestId).append("|").append("100").append("|").append("1001")
//                    .append("|").append("6527").append("|").append(payMode).append("|").append(amount)
//                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("http://sbtet.ap.gov.in/APSBTET/paymentSuccessDiplomaCTwenty.do")
//                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
//                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
//                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("http://sbtet.ap.gov.in/APSBTET/paymentFailurePageCTwenty.do")
//                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
//                    .append("|").append("NA").append("|").append("NA").toString();

             String parameters = requestSB.append(requestId).append("|").append("128").append("|").append("1135")
                    .append("|").append("6563").append("|").append(payMode).append("|").append(amount)
                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentSuccessDiplomaCTwenty.do")
                    .append("|").append(name).append("|").append("Address").append("|").append(applicationServiceNumber)
                    .append("|").append(grade).append("|").append("500000").append("|").append("test@gmail.com")
                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://sbtet.ap.gov.in/APSBTET/paymentFailurePageCTwenty.do")
                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
                    .append("|").append("NA").append("|").append("NA").toString();

            
           // String check = APP_REQ_CODE+"#"+CLIENT_ID+"#"+SERVICE_ID+"#"+DTO.getPaymentMode()+"#"+DTO.getBaseAmount()+"#"+CHECKSUM_KEY+"#"+ ProjectConfig.ACCESS+"#"+DTO.getPayType();
//             String check = requestId+"#"+100+"#"+6527+"#"+payMode+"#"+amount+"#"+741852963+"#"+ACCESS+"#"+payMode;
             
              String check = requestId+"#"+128+"#"+6563+"#"+payMode+"#"+amount+"#"+1131882120+"#"+ACCESS+"#"+payMode;
           
            System.out.println("parameters: "+parameters);
            System.out.println("check: "+check);
            
            sArry[0] = encDec.encrypt(parameters);
            sArry[1] = encDec.encrypt(check);
            return (sArry);
        } catch (Exception e) {
            LOG.error("e ",e);
            e.printStackTrace();
            return null;
        }
    }
}
