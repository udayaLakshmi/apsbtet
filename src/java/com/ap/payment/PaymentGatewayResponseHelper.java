/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.payment;
import com.ap.payment.DecryptEcrypte;
import com.ap.payment.PaymentResponseDto;
import java.util.logging.Level;
import java.util.logging.Logger;

import ws.pg.org.tempuri.EncryptDEcrypt;
import ws.pg.org.tempuri.EncryptDEcryptSoap;

/**
 *
 * @author APTOL301535
 */
public class PaymentGatewayResponseHelper {
    public static PaymentResponseDto pgResponse(String response) {
        PaymentResponseDto dto = null;
        try {
            String decryptData;
            
                decryptData = decrypt(response);
            
            String[] arrData = decryptData.split("\\|");
            
            
            
//            if (res != null) {
//                // dao.encryptedRes(res, userName);
//                decryptData = decrypt(res);
//                String[] arrData = decryptData.split("\\|");
//                System.out.println("Decrypted Array Dataa-->"+arrData);
//                for(int i=0;i<arrData.length;i++){
//                   if(i==0){
//                       map.put("reqId", arrData[i]);
//                       reqId = arrData[i];
//                       System.out.println("reqId reqId -->"+reqId);
//                   }else if(i==1){
//                       map.put("PgRefNo", arrData[i]);
//                       PgRefNo = arrData[i];
//                       System.out.println("PgRefNo"+PgRefNo);
//                   }else if(i==2){
//                       map.put("baseAmt", arrData[i]);
//                       baseAmt = arrData[i];
//                       System.out.println("baseAmt"+baseAmt);
//                   }else if(i==3){
//                       map.put("Conv_Charges", arrData[i]);
//                       Conv_Charges = arrData[i];
//                   }else if(i==4){
//                       map.put("gst", arrData[i]); // gst as DeptID
//                       gst = arrData[i];
//                   }else if(i==5){
//                       map.put("serviceId", arrData[i]);
//                       serviceId = arrData[i];
//                   }else if(i==6){
//                       map.put("checksum", arrData[i]);
//                       checksum = arrData[i];
//                   }else if(i==7){
//                       map.put("customerName", arrData[i]);
//                       customerName = arrData[i];
//                   } 
//                }
//                status = dao.paymentSuccess(reqId, appId, PgRefNo, baseAmt, Conv_Charges, gst, serviceId, checksum, customerName, serviceCharges, appId);
////                list.add(map);
////                request.setAttribute("paymentres", list);
//                
//                if(status!=null || status.equalsIgnoreCase("Inserted Successfully")){
//                     
//                     paymentDoneList =  dao.getDataAfterPayment(reqId);
//                      request.setAttribute("paymentres", paymentDoneList);
//                    //   target="success";
//                     
//                 }
//            

            dto = new PaymentResponseDto();
            for (int i = 0; i < arrData.length; i++) {
                if (i == 0) {

                    String reqId = arrData[i];
                    if(reqId.contains("PSP"))
                    dto.setRequestNo(reqId.split("PSP")[0]);
                    else
                        dto.setRequestNo(reqId);
                    System.out.println("VR FINE REQUEST "+reqId);
                } else if (i == 1) {

                    String PgRefNo = arrData[i];
                    System.out.println("pgref=======+pg"+PgRefNo);
                    dto.setPgTransNo(PgRefNo);
                    dto.setStatus("Success");

                } else if (i == 2) {

                    String baseAmt = arrData[i];
                    dto.setBaseAmount(baseAmt);
                } else if (i == 3) {

                    String Conv_Charges = arrData[i];
                    dto.setCharges(Conv_Charges);
                }/*else if(i==4){
                   
                 String gst = arrData[i];
                 }else if(i==5){
                   
                 String serviceId = arrData[i];
                 }else if(i==6){
                   
                 String checksum = arrData[i];
                 }*/ else if (i == 7) {

                    String customerName = arrData[i];
                    System.out.println("");
                    dto.setUserName(customerName);
                }
            }



        } catch (Exception e) {
            // PG response save in error log

            e.printStackTrace();
        }

        return dto;
    }
    
    
     private static String decrypt(String request) throws Exception {
         System.out.println("yyyyyyy");
      DecryptEcrypte encDec = new DecryptEcrypte();
        return encDec.decrypt(request);
    }
     
     public static void main(String[] args) {
        try {
            String decryptData = decrypt("gxbjtD69QvfxXnOALTRc5PhTa1YK5tRuu3LOfzXCfym9YWLOvKf1fzhhssG3qlui413nDXY40c+c27wAQifeymyS0dQli+0FlR5H2BMHoPo=");
            System.out.println("decryptData=="+decryptData);
        } catch (Exception ex) {
            Logger.getLogger(PaymentGatewayResponseHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }
             
}
