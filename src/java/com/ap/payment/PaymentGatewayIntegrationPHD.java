/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.payment;

import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DecimalFormat;

/**
 *
 * @author 1582792
 */
public class PaymentGatewayIntegrationPHD {

    public String payRequestFlowPHD(String applicationServiceNumber, String payMode, double amount) {
        PaymentGateWayDto dto = getOnlinePaymentDetails(applicationServiceNumber, payMode, amount);
//        if (dto != null) {
        // String reqNum = onlinePaymentInsert(dto, createdBy);
        String reqNum = "";
        if (!reqNum.equals("NA")) {
//                dto.setRequestID(reqNum);
            PaymentGatewayRequestHelperPHD helper = new PaymentGatewayRequestHelperPHD(dto);
            String req[] = helper.pgRequestPHD(applicationServiceNumber, payMode, amount);
            System.out.println("req[1]====" + req[1]);
            System.out.println("9999999999999999999999999");
            return PGURL(req[0], req[1]);
        } else {
            return "PG_ERROR";
        }

//        } else {
//            return "PG_ERROR";
//        }



    }

    public String PGURL(String payLoad, String sum) {
        System.out.println("u r hereeeeeeeeeeeeeeeeeeeeeeeeee");
        return "https://pgadmissions2020angrau.aptonline.in/PayInteract/redirect?payLoad=" + payLoad + "&sum=" + sum;
    }

    public static void main(String[] args) {

        PaymentGatewayIntegration pi = new PaymentGatewayIntegration();
        //pi.payRequestFlow("CSN2020072704", "PTM", "10.0");

    }

//    public PaymentGateWayDto getOnlinePaymentDetails(String appNumber, String payMode) {
//        Connection con = null;
//        ResultSet rs = null;
//        CallableStatement cstmt = null;
//
//        PaymentGateWayDto dto = new PaymentGateWayDto();
//
//
//        try {
//            con = DatabaseConnection.getConnection();
//            cstmt = con.prepareCall("{Call Pvt_Online_Payment_Req_Get(?)}");
//            cstmt.setString(1, appNumber);
//            rs = cstmt.executeQuery();
//            if (rs != null) {
//                while (rs.next()) {
//                    //rs.getString("ApplicationNumber")
//
//
//                    dto.setRequestID(rs.getString("ApplicationServiceNumber"));
//                    dto.setCustomerName(rs.getString("FullName"));
//                    dto.setEmailAddress(rs.getString("MailID"));
//                    dto.setPhoneNo(rs.getString("MobileNo"));
//
//                    dto.setAddress(rs.getString("societyAddress"));
//                    dto.setCity(rs.getString("societyCity"));
//                    dto.setState(rs.getString("societyState"));
//                    dto.setPincode(rs.getString("societyPinCode"));
//
//                    dto.setClientid(rs.getString("ClientId"));
//                    dto.setDeptID(rs.getString("DeptID"));
//                    dto.setServiceID(rs.getString("ServiceID"));
//                    dto.setChecksum(rs.getString("ChecksumKey"));
//                    dto.setReturnURL(rs.getString("ReturnURL"));
//                    dto.setFailureURL(rs.getString("FailureURL"));
//
//                    dto.setAdditionlInfo1(rs.getString("ainfo1"));
//                    dto.setAdditionlInfo2(rs.getString("ainfo2"));
//                    dto.setAdditionlInfo3(rs.getString("ainfo3"));
//                    dto.setAdditionlInfo4(rs.getString("ainfo4"));
//                    dto.setAdditionlInfo5(rs.getString("ainfo5"));
//
//
//                    dto.setBaseAmount(rs.getDouble("baseAmount"));
//
//                    dto.setPayType(payMode);
//                    dto.setPaymentMode(payMode);
//                    //Payment charge based payment mode
//                    if (payMode.equalsIgnoreCase("EBS")) {
//                        dto.setPaymentMode("NB");
//                        dto.setConvienceCharges(rs.getDouble("ChargeNBEBS"));
//                    } else if (payMode.equalsIgnoreCase("PTM")) {
//                        dto.setConvienceCharges(rs.getDouble("ChargeNBPaytm"));
//                        dto.setPaymentMode("NB");
//                    } else if (payMode.equalsIgnoreCase("DC")) {
//                        double charge = paymentCharges(dto.getBaseAmount(), rs.getDouble("ChargeDC"), rs.getDouble("TaxDC"));
//                        dto.setConvienceCharges(charge);
//                    } else if (payMode.equalsIgnoreCase("CC")) {
//                        double charge = paymentCharges(dto.getBaseAmount(), rs.getDouble("ChargeCC"), rs.getDouble("TaxCC"));
//                        dto.setConvienceCharges(charge);
//
//                    } else if (payMode.equalsIgnoreCase("NBS") || payMode.equalsIgnoreCase("NBI")) {
//
//                        dto.setConvienceCharges(11.8);
//
//                    } else if (payMode.equalsIgnoreCase("NBH")) {
//
//                        dto.setConvienceCharges(15.7);
//
//                    }
//
//                    dto.setTransactionAmount(dto.getBaseAmount() + dto.getConvienceCharges());
//
//
//
//                }
//            }
//        } catch (Exception e) {
//            System.out.println("ex: " + e);
////            log.error("Ex 163: ", e);
//            dto = null;
//        } finally {
////            closeConnection(con);
////            closeResultSet(rs);
////            closeCallableSt(cstmt);
//        }
//
//
//
//        return dto;
//    }
    public String onlinePaymentInsert(PaymentGateWayDto dto, String createdBy) {
        Connection con = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;
        CallableStatement rowscstmt = null;
        String flag = "NA";

        try {
            con = DatabaseConnection.getConnection();


            cstmt = con.prepareCall("{ call Pvt_Online_Payment_Req_Insert(?,?,?,?,?,?)}");

            cstmt.setString(1, dto.getRequestID());//2
            cstmt.setDouble(2, dto.getBaseAmount());
            cstmt.setDouble(3, dto.getConvienceCharges());
            cstmt.setDouble(4, dto.getTransactionAmount());
            cstmt.setString(5, dto.getPaymentMode());
            cstmt.setString(6, createdBy);

            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    flag = dto.getRequestID() + "PSP" + rs.getString("TRIES");

                }
            }



        } catch (Exception e) {
            System.out.println("e " + e);
//            log.error("Ex 68: ", e);
        } finally {
        }
        return flag;
    }

    public String onlinePaymentUpdate(PaymentResponseDto dto) {
        Connection con = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;
        CallableStatement rowscstmt = null;

        String flag = "";


        try {
            con = DatabaseConnection.getConnection();



            cstmt = con.prepareCall("{ call Pvt_Online_Payment_Res_Update(?,?,?,?,?)}");
            cstmt.setString(1, dto.getRequestNo());
            cstmt.setString(2, dto.getPgTransNo());//2
            cstmt.setString(3, dto.getStatus());
            cstmt.setDouble(4, dto.getTransactionAmount());
            cstmt.setString(5, dto.getCreatedBy());



            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    flag = rs.getString("ENCNUM");

                }
            }


        } catch (Exception e) {
            flag = dto.getRequestNo() + "_" + dto.getRequestNo();
            System.out.println("e " + e);
//            log.error("Ex 68: ", e);
        } finally {
//            closeConnection(con);
//            closeResultSet(rs);
//            closeCallableSt(cstmt);
//            closeCallableSt(rowscstmt);
        }
        return flag;



    }

    public static double paymentCharges(Double amount, double chargePer, double taxPer) {
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            String chrg = "0.00";

            double charge = ((chargePer * amount) / 100);
            double tax = ((taxPer * charge) / 100);
            chrg = df.format(charge + tax);

            return Double.parseDouble(chrg);
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }

    public PaymentGateWayDto getOnlinePaymentDetails(String appNumber, String payMode, double amount) {
        Connection con = null;
        ResultSet rs = null;
        CallableStatement stmt = null;
        PaymentGateWayDto dto = new PaymentGateWayDto();
//        String path = re;
//        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
//        String returnUrl = basePath + "paymentSuccess.do";
//        String failureUrl = basePath + "paymentFailurePage.do";
        try {
//            con = DatabaseConnection.getConnection();
//            stmt = con.prepareCall("{call proc_PaymentMode_PHD(?,?,?)}");
//            System.out.println("in dto");
//            stmt.setDouble(1, amount);
//            stmt.setString(2, payMode);
//            stmt.setString(3, appNumber);
//            System.out.println("rfform.getAmount()" + amount);
//            System.out.println("rfform.getAmount()" + payMode);
//            rs = stmt.executeQuery();
//            if (rs != null) {
//                while (rs.next()) {
                    //rs.getString("ApplicationNumber")
                    System.out.println("result  hhhhh " + rs);
                    dto.setRequestID(appNumber);
                    dto.setCustomerName("");
                    dto.setEmailAddress("");
                    dto.setPhoneNo("");
                    dto.setAddress("address");
                    dto.setCity("AP");
                    dto.setState("AndhraPradesh");
                    dto.setPincode("500072");
                    dto.setClientid("100");
                    dto.setDeptID("1001");
                    dto.setServiceID("6527");
                    dto.setChecksum("741852963");
                    dto.setReturnURL("");
                    dto.setFailureURL("");
                    dto.setAdditionlInfo1("NA");
                    dto.setAdditionlInfo2("NA");
                    dto.setAdditionlInfo3("NA");
                    dto.setAdditionlInfo4("NA");
                    dto.setAdditionlInfo5("NA");
                    dto.setBaseAmount(amount);
                    dto.setPayType(payMode);
                    dto.setPaymentMode(payMode);
                    dto.setConvienceCharges(0);
                    dto.setTransactionAmount(0);
//                }
//            }
        } catch (Exception e) {
            dto = null;
        } finally {
        }
        return dto;
    }
}
