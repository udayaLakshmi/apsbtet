/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.payment;

//import com.ap.payment.De;
import net.apo.angrau.db.DatabaseConnection;

/**
 *
 * @author chandu
 */
public class PayFlow {

    public static String initPayRequest(String payLoad, String checkSum) {
        DecryptEcrypte encDcpt = new DecryptEcrypte();
        String target;
        String load = encDcpt.decrypt(payLoad);
        String chkSum = encDcpt.decrypt(checkSum);
        String[] arryChkSum = chkSum.split("#");
        String remarks = "OK";
        try {
            String encChkSum = PaymentWSAccess.checkSum(arryChkSum[0], arryChkSum[1], arryChkSum[2], arryChkSum[3], arryChkSum[4], arryChkSum[5]);
            load = load.replace("YESCHCKERDA", encChkSum);
            System.out.println("load " + load);
            String redirectData = PaymentWSAccess.encryptPayData(load);

            target = PGURL(arryChkSum[6], redirectData, arryChkSum[7]);
        } catch (Exception e) {
            remarks = e.getMessage();
            target = "PG_ERROR";
        } finally {
            // DatabaseConnection.insertReqTrack(arryChkSum[0], remarks, "PSP_"+arryChkSum[7]);
        }


        return target;
    }

    public static String initPayResponse(String response, String access) {
        DecryptEcrypte encDcpt = new DecryptEcrypte();
        String target;


        try {
            String decResponse = PaymentWSAccess.decryptPayData(response.replaceAll("\\s", "+"));
            String[] arryRespond = decResponse.split("\\|");
            target = APPURL(access, encDcpt.encrypt(decResponse));

//            DataBasePlugin.updateResTrack(arryRespond[0], "OK", "Success");
        } catch (Exception e) {
//            DataBasePlugin.insertReqTrack("DECRYPT_FAIL", response, access);
            target = "PG_ERROR";
        } finally {
        }


        return target;
    }

//    private static String PGURL(String access, String data, String payType) {
////        if (access.equals("UAT") || access.equals("LOCAL")) {
//        if (payType.equals("NBS")) {
//            return "https://uat.aponline.gov.in:9443/ThirdPartyPaymentPG/PayTM/PayTMPGRequest.aspx?CheckSumValue=" + data;
//        } else {
//            return "https://uat.aponline.gov.in:9443/ThirdPartyPaymentPG/EBS/PaymentRequest.aspx?CheckSumValue=" + data;
//        }
//    }
    
  private static String PGURL(String access, String data, String payType) {
        if (payType.equals("NBS") || payType.equals("NBI") || payType.equals("NBH")) {
            return "https://aptonline.in/ThirdPartyPaymentPG/EBS_HDFC_SBI/EBS_HDFC_SBI_PaymentRequest.aspx?CheckSumValue=" + data;
        } else if (payType.equals("PTM")) {
            return "https://aptonline.in/ThirdPartyPaymentPG/PayTM/PayTMPGRequest.aspx?CheckSumValue=" + data;
        } else {
            return "https://aptonline.in/ThirdPartyPaymentPG/EBS/PaymentRequest.aspx?CheckSumValue=" + data;
        }
    }

    private static String APPURL(String access, String data) {
//        if (access.equals("PSP")) {
//            return "http://cce.telangana.gov.in/PSP/pgSuccessResponse.do?resp=" + data;
//        } else if (access.equals("PSP_UAT")) {
//            return "http://164.100.189.12/PrivateSchool/pgSuccessResponse.do?resp=" + data;
//        }else if (access.equals("PSP_LOCAL")) {
//            return "http://localhost:8086/PrivateSchool/pgSuccessResponse.do?resp=" + data;
//        }else {
//            //Live
//            return "";
//        }
        return "";
    }
}
