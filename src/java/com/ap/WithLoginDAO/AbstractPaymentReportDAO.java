/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginDAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import net.apo.angrau.db.DatabaseConnection;

/**
 *
 * @author 1820530
 */
public class AbstractPaymentReportDAO {
    
    private static AbstractPaymentReportDAO paymentObj = null;

    private AbstractPaymentReportDAO() {
    }

    public static AbstractPaymentReportDAO getInstance() {

        if (paymentObj == null) {
            paymentObj = new AbstractPaymentReportDAO();
        }
        return paymentObj;
    }
    
    public ArrayList getPaymentAbstractReport() {
       Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        ArrayList paymentList = new ArrayList();
        Map map = null; 
        
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_CCIC_CourseWisePaymentAbstractDetailsReport_Get }");            
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("courseCode", rs.getString(1));
                    map.put("courseName", rs.getString(2));
                    map.put("limit", rs.getString(3));
                    map.put("paymentDone", rs.getString(4));
                    map.put("paymentNotDone", rs.getString(5));
                    map.put("total", rs.getString(6));
                                    
                    paymentList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
        return paymentList;
    }

    

    public ArrayList getPaymentAbstractSubReport(String statusCode,String courseCode) {
      
        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        ArrayList paymentSubList = new ArrayList();
        Map map = null; 
        
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_CCIC_PaymentAbstractDetailsSubReport_Get(?,?)}");
            cstmt.setString(1, statusCode); 
             cstmt.setString(2, courseCode); 
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("name", rs.getString(1));
                    map.put("fatherName", rs.getString(2));
                    map.put("dob", rs.getString(3));
                    map.put("gender", rs.getString(4));
                    map.put("mobileNum", rs.getString(5));
                    map.put("courseName", rs.getString(6)); 
                    map.put("category", rs.getString(7)); 
                    map.put("institution", rs.getString(8)); 
                    map.put("studiedDist", rs.getString(9)); 
                    map.put("examCenterCode", rs.getString(10));                                     
                    paymentSubList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
        return paymentSubList;
    }
    
}
