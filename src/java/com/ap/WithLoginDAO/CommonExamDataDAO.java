/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginDAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import net.apo.angrau.db.DatabaseConnection;

/**
 *
 * @author 1820530
 */
public class CommonExamDataDAO {
    
    private static CommonExamDataDAO commonObj = null;  

   
    

    private CommonExamDataDAO() {
    }

    public static CommonExamDataDAO getInstance() {

        if (commonObj == null) {
            commonObj = new CommonExamDataDAO();
        }
        return commonObj;
    }
    
    
     public static ArrayList getExamDataShortHandList() {
        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        ArrayList shExamCenterList = new ArrayList();
        Map map = null; 
        
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_SHExamCenterWiseAbstractReport_Get }");            
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("centerCode", rs.getString(1));
                    map.put("centerName", rs.getString(2));
                    map.put("sel", rs.getString(3));
                    map.put("sei", rs.getString(4));
                    map.put("seh", rs.getString(5));
                    map.put("sehs150", rs.getString(6));
                    map.put("sehs180", rs.getString(7));
                    map.put("sehs200", rs.getString(8));
                    map.put("sth", rs.getString(9));
                    map.put("sths80", rs.getString(10));
                    map.put("sths100", rs.getString(11));
                    map.put("stl", rs.getString(12));
                                    
                    shExamCenterList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
        return shExamCenterList;
    }
     
     public static ArrayList getInstituteDataShortHandList() {
       Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        ArrayList shInstituteList = new ArrayList();
        Map map = null; 
        
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_SHInstitutionWiseAbstractReport_Get }");            
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("instituteCode", rs.getString(1));
                    map.put("instituteName", rs.getString(2));
                    map.put("sel", rs.getString(3));
                    map.put("sei", rs.getString(4));
                    map.put("seh", rs.getString(5));
                    map.put("sehs150", rs.getString(6));
                    map.put("sehs180", rs.getString(7));
                    map.put("sehs200", rs.getString(8));
                    map.put("sth", rs.getString(9));
                    map.put("sths80", rs.getString(10));
                    map.put("sths100", rs.getString(11));
                    map.put("stl", rs.getString(12));
                                    
                    shInstituteList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
        return shInstituteList;
    }
     
     public static ArrayList getExamDataTWList() {
       Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        ArrayList twExamCenterList = new ArrayList();
        Map map = null; 
        
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_TWCenterWiseAbstractReport_Get }");            
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("centerCode", rs.getString(1));
                    map.put("centerName", rs.getString(2));
                    map.put("typeWritingEngLowerB1", rs.getString(3));
                    map.put("typeWritingEngLowerB2", rs.getString(4));
                    map.put("typeWritingEngLowerB3", rs.getString(5));
                    map.put("typeWritingEngLowerB4", rs.getString(6));
                    map.put("typeWritingEngHigherB1", rs.getString(7));
                    map.put("typeWritingEngHigherB2", rs.getString(8));
                    map.put("typeWritingEngHigherB3", rs.getString(9));
                    map.put("typeWritingEngHigherB4", rs.getString(10));
                    map.put("typeWritingteluguLowerB1", rs.getString(11));
                    map.put("typeWritingteluguLowerB2", rs.getString(12));
                   map.put("typeWritingteluguLowerB3", rs.getString(13));
                    map.put("typeWritingteluguLowerB4", rs.getString(14));
                    map.put("typeWritingteluguHigherB1", rs.getString(15));
                    map.put("typeWritingteluguHigherB2", rs.getString(16));
                    map.put("typeWritingteluguHigherB3", rs.getString(17));
                    map.put("typeWritingteluguHigherB4", rs.getString(18));
                    map.put("typeWritingEngJnrB1", rs.getString(19));
                    map.put("typeWritingEngJnrB2", rs.getString(20));
                    map.put("typeWritingHindiLowerB1", rs.getString(21));
                    map.put("typeWritingHindiHigherB1", rs.getString(22));
                    map.put("typeWritingEngHighSpeedB1", rs.getString(23));
                    map.put("typeWritingTeluguHighSpeedB1", rs.getString(24));
                    map.put("totalStrength", rs.getString(25));
                                    
                    twExamCenterList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
        return twExamCenterList;
    }
     
     
      public static ArrayList getInstituteTWList() {
          Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        ArrayList twInstituteList = new ArrayList();
        Map map = null; 
        
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_TWInstitutionWiseAbstractReport_Get }");            
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("instituteCode", rs.getString(1));
                    map.put("instituteName", rs.getString(2));
                    map.put("typeWritingEngLowerB1", rs.getString(3));
                    map.put("typeWritingEngLowerB2", rs.getString(4));
                    map.put("typeWritingEngLowerB3", rs.getString(5));
                    map.put("typeWritingEngLowerB4", rs.getString(6));
                    map.put("typeWritingEngHigherB1", rs.getString(7));
                    map.put("typeWritingEngHigherB2", rs.getString(8));
                    map.put("typeWritingEngHigherB3", rs.getString(9));
                    map.put("typeWritingEngHigherB4", rs.getString(10));
                    map.put("typeWritingteluguLowerB1", rs.getString(11));
                    map.put("typeWritingteluguLowerB2", rs.getString(12));
                   map.put("typeWritingteluguLowerB3", rs.getString(13));
                    map.put("typeWritingteluguLowerB4", rs.getString(14));
                    map.put("typeWritingteluguHigherB1", rs.getString(15));
                    map.put("typeWritingteluguHigherB2", rs.getString(16));
                    map.put("typeWritingteluguHigherB3", rs.getString(17));
                    map.put("typeWritingteluguHigherB4", rs.getString(18));
                    map.put("typeWritingEngJnrB1", rs.getString(19));
                    map.put("typeWritingEngJnrB2", rs.getString(20));
                    map.put("typeWritingHindiLowerB1", rs.getString(21));
                    map.put("typeWritingHindiHigherB1", rs.getString(22));
                    map.put("typeWritingEngHighSpeedB1", rs.getString(23));
                    map.put("typeWritingTeluguHighSpeedB1", rs.getString(24));
                    map.put("totalStrength", rs.getString(25));
                                    
                    twInstituteList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
        return twInstituteList;
    }

    
}
