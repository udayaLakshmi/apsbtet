/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginForm;


import org.apache.struts.action.ActionForm;

/**
 *
 * @author 484898
 */
public class LoginForm extends ActionForm {

    
    
    private String roleId;
    private String userName;
    private String password;
    private String transporter;
    private String color;
    private String mode;
    private String loginStatus;
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;
    private String loginRoles;
    private String captchaCode;
    private String newNormPassword;
    private String districtId;
    private String schcode;
    private String schname;
    private String schmgmt;
    private String schcat;
    private String districtname;
    private String mandalname;
    private String villagename;
    private String lowclass;
    private String highclass;
    private String serviceid;
    private String parentid;
    private String targeturl;
    private String servicename;
    private String priority;
    private String encryptpassword;
    private String status;
    private String schoolcode;
    private String passwordNew;

    public String getPasswordNew() {
        return passwordNew;
    }

    public void setPasswordNew(String passwordNew) {
        this.passwordNew = passwordNew;
    }
    public String getSchoolcode() {
        return schoolcode;
    }

    public void setSchoolcode(String schoolcode) {
        this.schoolcode = schoolcode;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
    
    public String getEncryptpassword() {
        return encryptpassword;
    }

    public void setEncryptpassword(String encryptpassword) {
        this.encryptpassword = encryptpassword;
    }
    
    
    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getNewNormPassword() {
        return newNormPassword;
    }

    public void setNewNormPassword(String newNormPassword) {
        this.newNormPassword = newNormPassword;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getLoginRoles() {
        return loginRoles;
    }

    public void setLoginRoles(String loginRoles) {
        this.loginRoles = loginRoles;
    }

    public String getCaptchaCode() {
        return captchaCode;
    }

    public void setCaptchaCode(String captchaCode) {
        this.captchaCode = captchaCode;
    }

    public String getSchcode() {
        return schcode;
    }

    public void setSchcode(String schcode) {
        this.schcode = schcode;
    }

    public String getSchname() {
        return schname;
    }

    public void setSchname(String schname) {
        this.schname = schname;
    }

    public String getSchmgmt() {
        return schmgmt;
    }

    public void setSchmgmt(String schmgmt) {
        this.schmgmt = schmgmt;
    }

    public String getSchcat() {
        return schcat;
    }

    public void setSchcat(String schcat) {
        this.schcat = schcat;
    }

    public String getDistrictname() {
        return districtname;
    }

    public void setDistrictname(String districtname) {
        this.districtname = districtname;
    }

    public String getMandalname() {
        return mandalname;
    }

    public void setMandalname(String mandalname) {
        this.mandalname = mandalname;
    }

    public String getVillagename() {
        return villagename;
    }

    public void setVillagename(String villagename) {
        this.villagename = villagename;
    }

    public String getLowclass() {
        return lowclass;
    }

    public void setLowclass(String lowclass) {
        this.lowclass = lowclass;
    }

    public String getHighclass() {
        return highclass;
    }

    public void setHighclass(String highclass) {
        this.highclass = highclass;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getTargeturl() {
        return targeturl;
    }

    public void setTargeturl(String targeturl) {
        this.targeturl = targeturl;
    }

    public String getServicename() {
        return servicename;
    }

    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
    
    
}
