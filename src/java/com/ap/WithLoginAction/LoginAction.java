/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginAction;

import com.ap.WithLoginDAO.LoginDAO;
import com.ap.WithLoginForm.LoginForm;
import net.apo.angrau.db.DatabaseConnection;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301461
 */
public class LoginAction extends DispatchAction {

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            HttpSession session = null;

            String code = "";
            String codeGen = "";
            String randomInt = null;
            Random randomGenerator = new Random();
            session = request.getSession(true);
            for (int idx = 1; idx <= 5; ++idx) {
                randomInt = Integer.toHexString(randomGenerator.nextInt(6));
                code = code + "" + randomInt;
                codeGen = codeGen + " " + randomInt;
            }
            if (code != null) {
                request.setAttribute("captchaCode", codeGen);
                session.setAttribute("captcha", code);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }

    public ActionForward checkDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        System.out.println("LoginAction-===");
        LoginForm loginForm = (LoginForm) form;
        Vector resultList = null;
        String loginDetails = "";
        String target = "success";
        HttpSession session = request.getSession();
        String flag = null;
        try {

            if (loginForm.getUserName() != null && loginForm.getPassword() != null
                    && loginForm.getUserName().length() > 0
                    && loginForm.getPassword().length() > 0) {
                if (request.getParameter("txtInput") != null && session.getAttribute("captcha") != null && session.getAttribute("captcha").equals(request.getParameter("txtInput"))) { // Captcha Check

                    loginDetails = LoginDAO.getInstance().getLoginFlag(loginForm);
                    if (loginDetails != null && loginDetails.equalsIgnoreCase("N")) {
                        target = "changePassword";
                        request.setAttribute("passwordFlag", "passwordFlag");
                        request.setAttribute("Username", loginForm.getUserName());
                    } else if (loginDetails != null && loginDetails.equalsIgnoreCase("Y")) {
                        loginForm.setLoginStatus("Success");

                        ArrayList userInfoList = new ArrayList();

                        userInfoList = LoginDAO.getInstance().getUserInfoBasedOnLoginUser(loginForm, request);
                        if (userInfoList.size() > 0) {
                            Vector servicesList = new Vector();
                            servicesList = LoginDAO.getInstance().getUserServices(loginForm, request);
                            if (session.getAttribute("RoleId").toString().equalsIgnoreCase("1")) {
                                LoginDAO.getInstance().getDetails(loginForm, request);
                            } else if (session.getAttribute("RoleId").toString().equalsIgnoreCase("4")) {
                                LoginDAO.getInstance().getInstituteDetails(loginForm, request);
                            }
                            if (servicesList.size() > 0) {
                                // If services found
                                session.setAttribute("services", servicesList);
                                request.setAttribute("msg", "Welcome to Student Information System");

                                //new code start  here
                                if (servicesList != null) {
                                    ArrayList mainList = new ArrayList();
                                    ArrayList menuList = new ArrayList();
                                    ArrayList subMenuList = new ArrayList();
                                    ArrayList subMenuParentIdsList = new ArrayList();
                                    String subMenuParentIds = "";
                                    ArrayList subMenuKeyValues = new ArrayList();
                                    for (int i = 0; i < servicesList.size(); i++) {
                                        String servicedesc[] = (String[]) servicesList.elementAt(i);

                                        HashMap map = new HashMap();
                                        map.put("serviceId", servicedesc[0]);
                                        map.put("parentId", servicedesc[1]);
                                        map.put("target_url", servicedesc[2]);
                                        map.put("service_name", servicedesc[3]);

                                        if (servicedesc[1].equalsIgnoreCase("0")) {
                                            menuList.add(map);
                                        } else {
                                            HashMap subMenuKeyValues1 = new HashMap();
                                            subMenuKeyValues1.put(map.get("serviceId"),map.get("service_name")+"-"+map.get("target_url"));
                                            subMenuKeyValues.add(subMenuKeyValues1);
                                            subMenuParentIdsList.add(map.get("parentId"));
                                            subMenuParentIds = subMenuParentIds + map.get("parentId") + "-";
                                            subMenuList.add(map);
                                            subMenuKeyValues1=null;
                                        }
                                        mainList.add(map);
                                        map = null;

                                    }

                                    System.out.println("menuList " + menuList);
                                    System.out.println("subMenuList " + subMenuList);

                                    if (subMenuParentIds != null && subMenuParentIds.length() > 0 && subMenuParentIds.charAt(subMenuParentIds.length() - 1) == '-') {
                                        subMenuParentIds = subMenuParentIds.substring(0, subMenuParentIds.length() - 1);
                                    }

                                    System.out.println("subMenuParentIds " + subMenuParentIds);
                                    session.setAttribute("subMenuParentIdsList", subMenuParentIdsList);
                                    session.setAttribute("subMenuParentIds", subMenuParentIds);
                                    session.setAttribute("mainList", mainList);
                                    session.setAttribute("menuList", menuList);
                                    session.setAttribute("subMenuList", subMenuList);
                                    session.setAttribute("subMenuKeyValues", subMenuKeyValues);
                                } else {
                                    response.sendRedirect("./unAuthorised.do");
                                }
                                //new code End  here

                                try {
                                } catch (Exception e) {
                                    log.error(e.getMessage(), e);
                                }
                                target = "welcomePage";
                            } else {
                                // If services not-found
                                request.setAttribute("msg", "Welcome Commissioner and Director of School Education");
                                String servicedesc[] = new String[5];
                                servicedesc[0] = "1";
                                servicedesc[1] = "0";
                                servicedesc[2] = "home.do";
                                servicedesc[3] = "Home";
                                servicesList.addElement(servicedesc);
                                String servicedesc1[] = new String[5];
                                servicedesc1[0] = "5";
                                servicedesc1[1] = "0";
                                servicedesc1[2] = "logout.do";
                                servicedesc1[3] = "Logout";
                                servicesList.addElement(servicedesc1);
                                session.setAttribute("services", servicesList);
                                target = "welcomePage";
                            }
                        } else {
                            request.setAttribute("msg", "Invalid UserName / Password");
                            loginForm.setLoginStatus("Fail");
                            loginForm.setUserName("");
                            loginForm.setPassword("");
                            target = "failure";
                        }
                    } else {
                        request.setAttribute("msg", "Invalid UserName / Password");
                        loginForm.setLoginStatus("Fail");
                        loginForm.setUserName("");
                        loginForm.setPassword("");
                        target = "failure";
                    }
                } else {
                    request.setAttribute("msg", "Entered Captcha Code not Match. Please Enter Correct Code");
                    target = "failure";
                }
            } else {
                request.setAttribute("msg", "Invalid UserName / Password");
                target = "failure";
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
//        System.out.println("sasasasasasaas"+target);
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }

    public ActionForward getDashBoardData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PreparedStatement pst = null;
        Connection con = null;
        ResultSet rs = null;
        String namesList = "";
        CallableStatement stmt = null;
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        try {
            String uname = session.getAttribute("userName").toString();
            String roleId = session.getAttribute("RoleId").toString();
            con = DatabaseConnection.getConnection();
            stmt = con.prepareCall("{call USP_Dashboard_Rpt(?,?)}");
            stmt.setString(1, uname);
            stmt.setString(2, roleId);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    namesList = rs.getString(1) + "$" + rs.getString(2) + "$" + rs.getString(3) + "$" + rs.getString(4)
                            + "$" + rs.getString(5) + "$" + rs.getString(6) + "$" + rs.getString(7) + "$"
                            + rs.getString(8) + "$" + rs.getString(9) + "$" + rs.getString(10) + "$" + rs.getString(11) + "$" + rs.getString(12) + "$"
                            + rs.getString(14) + "$" + rs.getString(15) + "$" + rs.getString(16) + "$" + rs.getString(17) + "$"
                            + rs.getString(18) + "$" + rs.getString(19) + "$" + rs.getString(20) + "$"
                            //DIPLOMA
                            + rs.getString(21) + "$" + rs.getString(22) + "$" + rs.getString(23) + "$" + rs.getString(24) + "$" + rs.getString(25)+ "$"
                            + rs.getString(26) + "$" + rs.getString(27) + "$" + rs.getString(28) + "$" + rs.getString(29) + "$" + rs.getString(30)+ "$"
                            + rs.getString(31) + "$" + rs.getString(32);
                }
            }
//            System.out.println("namesList "+namesList);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pst != null) {
                pst.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward changePasswordBeforeLogin(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "changePassword";
        try {

            LoginForm loginForm = (LoginForm) form;
            HttpSession httpSession = request.getSession();
            String userName = request.getParameter("userName").toString();
            loginForm.setUserName(userName);
            request.setAttribute("Username", loginForm.getUserName());
            int loginChecking = LoginDAO.getInstance().getLoginCount(loginForm);
            System.out.println("loginChecking " + loginChecking);
            if (loginChecking > 0) {
                loginChecking = LoginDAO.getInstance().updatePasswordBeforeLogin(loginForm);
                System.out.println("loginChecking----1 " + loginChecking);
                if (loginChecking > 0) {
                    target = "changePassword";
                    loginForm.setOldPassword("");
                    loginForm.setNewPassword("");
                    loginForm.setConfirmPassword("");
                    request.setAttribute("result1", "Password Changed successfully login with new cridentials");
                } else {
                    loginForm.setOldPassword("");
                    loginForm.setNewPassword("");
                    loginForm.setConfirmPassword("");
                    request.setAttribute("passwordFlag", "passwordFlag");
                    request.setAttribute("result", "Password Failed to update please re-try");
                }
            } else {
                loginForm.setOldPassword("");
                loginForm.setNewPassword("");
                loginForm.setConfirmPassword("");
                request.setAttribute("passwordFlag", "passwordFlag");
                request.setAttribute("result", "Please Enter Valid Cridentials");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return mapping.findForward(target);
    }
}