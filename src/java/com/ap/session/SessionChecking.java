package com.ap.session;

import java.io.IOException;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @author 525485
 */
public class SessionChecking implements Filter {

    public void init(FilterConfig arg0) throws ServletException {
    }

    public void doFilter(ServletRequest servletrequest, ServletResponse servletresponse, FilterChain filterChain) throws IOException, ServletException {

        String contextPath = ((HttpServletRequest) servletrequest).getServerName() + ((HttpServletRequest) servletrequest).getContextPath();
        HttpServletResponse response = (HttpServletResponse) servletresponse;
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache,no-store, must-revalidate");
        response.addHeader("Cache-Control", "pre-check=0, post-check=0");
        response.setHeader("Expires", "0");
        response.setDateHeader("Expires", 0);
        response.addHeader("Referrer-Policy", "no-referrer");
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        response.addHeader("Strict-Transport-Security", " max-age=63072000; includeSubDomains; preload");
//    response.addHeader("Content-Security-Policy", "default-src https: 'self' 'unsafe-eval' 'unsafe-inline'  image-src 'self' data:; object-src 'none' ");

        String path = ((HttpServletRequest) servletrequest).getRequestURI();
        int p = path.lastIndexOf("/");
        String action = path.substring(p + 1, path.length() - 3);
        if (action != null
                && (action.equalsIgnoreCase("login")
                || action.equalsIgnoreCase("logout")
                || action.equalsIgnoreCase("officialLogin")
                || action.equalsIgnoreCase("register")
                || action.equalsIgnoreCase("mbaCaptF")
                || action.equalsIgnoreCase("Welcome")
                || action.equalsIgnoreCase("phdregister")
                || action.equalsIgnoreCase("verfication")
                || action.equalsIgnoreCase("paymentSuccess")
                || action.equalsIgnoreCase("paymentSuccessPHD")
                || action.equalsIgnoreCase("mscedit")
                || action.equalsIgnoreCase("editPhdForm")
                || action.equalsIgnoreCase("verficationPHDPrint")
                || action.equalsIgnoreCase("adimissionpdfs")
                || action.equalsIgnoreCase("paymentFailurePage")
                || action.equalsIgnoreCase("editDegree")
                || action.equalsIgnoreCase("editDegreeEng")
                || action.equalsIgnoreCase("paymentFailurePagePHD")
                || action.equalsIgnoreCase("mpcregister")
                || action.equalsIgnoreCase("printBipc")
                || action.equalsIgnoreCase("printMpc")
                || action.equalsIgnoreCase("verficationF")
                || action.equalsIgnoreCase("resend")
                || action.equalsIgnoreCase("diplomaReupload")
                || action.equalsIgnoreCase("diplomaReuploadNcc")
                || action.equalsIgnoreCase("diplomaReuploadCap")
                || action.equalsIgnoreCase("regPay")
                || action.equalsIgnoreCase("diplomaedit")
                || action.equalsIgnoreCase("registeredFormPrint")
                || action.equalsIgnoreCase("contactus")
                || action.equalsIgnoreCase("weboptions")
                || action.equalsIgnoreCase("weboptionsprint")
                || action.equalsIgnoreCase("downloadHallticket")
                || action.equalsIgnoreCase("formPrint")
                || action.equalsIgnoreCase("resetPwd")
                || action.equalsIgnoreCase("dtoregister")
                || action.equalsIgnoreCase("paymentSuccessBeforeCCIC")
                || action.equalsIgnoreCase("paymentFailurePageBeforeCCIC")
                || action.equalsIgnoreCase("instituteMaster")
                || action.equalsIgnoreCase("DiplomaAllotmentLetter")
                || action.equalsIgnoreCase("paymentSuccessLogin")
                || action.equalsIgnoreCase("paymentFailurePageLogin")
                 || action.equalsIgnoreCase("formPrintCCIC")
                 || action.equalsIgnoreCase("downloadHallticket")
                || action.equalsIgnoreCase("resendCCIC")
                || action.equalsIgnoreCase("downloadBeforeLoginHallticketCCIC")
                  || action.equalsIgnoreCase("paymentSuccessDiploma")
                || action.equalsIgnoreCase("paymentFailurePageDiploma")
                    || action.equalsIgnoreCase("onlineExaminationPayment")
                || action.equalsIgnoreCase("oep")
                || action.equalsIgnoreCase("downloadDiplomaHallticket")
                  || action.equalsIgnoreCase("registerCTwenty")
                || action.equalsIgnoreCase("paymentSuccessDiplomaCTwenty")
                || action.equalsIgnoreCase("paymentFailurePageCTwenty")

                
                
                || action.equalsIgnoreCase("verficationPrint"))) {

            filterChain.doFilter(servletrequest, servletresponse);

        } else {
            try {
                HttpSession session = ((HttpServletRequest) servletrequest).getSession(true);
                //  List service = (List) session.getAttribute("services");

                if (session.getAttribute("userName") == null) {
                    session.invalidate();
                    RequestDispatcher rd = ((HttpServletRequest) servletrequest).getRequestDispatcher("unAuthorised.do");
                    rd.forward(((HttpServletRequest) servletrequest), ((HttpServletResponse) servletresponse));
                } else {

                    filterChain.doFilter(servletrequest, servletresponse);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  filterChain.doFilter(servletrequest, servletresponse);
        }
    }

    public void destroy() {
    }
}
