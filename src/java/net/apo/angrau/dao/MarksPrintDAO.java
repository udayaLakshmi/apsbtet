/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author APTOL301655
 */
public class MarksPrintDAO {

    public ArrayList<HashMap> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        String query = null;

        ArrayList<HashMap> list = new ArrayList<HashMap>();
        HashMap<String, String> map = new HashMap<String, String>();
//        CallableStatement cstmt = null;
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        int totalcount = 0;
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_Diploma_Bundlewise_BookletMarks_web(?)}");
            cstmt.setString(1, rform.getBundlecode().trim());
            rs = cstmt.executeQuery();
            while (rs.next() == true) {
             request.setAttribute("Marks1", rs.getString(3)==null?"":rs.getString(3));
               request.setAttribute("Marks2", rs.getString(4)==null?"":rs.getString(4));
               request.setAttribute("Marks3", rs.getString(5)==null?"":rs.getString(5));
               request.setAttribute("Marks4", rs.getString(6)==null?"":rs.getString(6));
               request.setAttribute("Marks5", rs.getString(7)==null?"":rs.getString(7));
               request.setAttribute("Marks6", rs.getString(8)==null?"":rs.getString(8));
               request.setAttribute("Marks7", rs.getString(9)==null?"":rs.getString(9));
               request.setAttribute("Marks8", rs.getString(10)==null?"":rs.getString(10));
               request.setAttribute("Marks9", rs.getString(11)==null?"":rs.getString(11));
               request.setAttribute("Marks10", rs.getString(12)==null?"":rs.getString(12));
                
                
               request.setAttribute("Marks11", rs.getString(13)==null?"":rs.getString(13));
               request.setAttribute("Marks12", rs.getString(14)==null?"":rs.getString(14));
               request.setAttribute("Marks13", rs.getString(15)==null?"":rs.getString(15));
               request.setAttribute("Marks14", rs.getString(16)==null?"":rs.getString(16));
               request.setAttribute("Marks15", rs.getString(17)==null?"":rs.getString(17));
               request.setAttribute("Marks16", rs.getString(18)==null?"":rs.getString(18));
               request.setAttribute("Marks17", rs.getString(19)==null?"":rs.getString(19));
               request.setAttribute("Marks18", rs.getString(20)==null?"":rs.getString(20));
               request.setAttribute("Marks19", rs.getString(21)==null?"":rs.getString(21));
               request.setAttribute("Marks20", rs.getString(22)==null?"":rs.getString(22));
                
               request.setAttribute("Marks21", rs.getString(23)==null?"":rs.getString(23));
               request.setAttribute("Marks22", rs.getString(24)==null?"":rs.getString(24));
               request.setAttribute("Marks23", rs.getString(25)==null?"":rs.getString(25));
               request.setAttribute("Marks24", rs.getString(26)==null?"":rs.getString(26));
               request.setAttribute("Marks25", rs.getString(27)==null?"":rs.getString(27));
               request.setAttribute("Marks26", rs.getString(28)==null?"":rs.getString(28));
               request.setAttribute("Marks27", rs.getString(29)==null?"":rs.getString(29));
               request.setAttribute("Marks28", rs.getString(30)==null?"":rs.getString(30));
               request.setAttribute("Marks29", rs.getString(31)==null?"":rs.getString(31));
               request.setAttribute("Marks30", rs.getString(32)==null?"":rs.getString(32));
                
               request.setAttribute("Marks31", rs.getString(33)==null?"":rs.getString(33));
               request.setAttribute("Marks32", rs.getString(34)==null?"":rs.getString(34));
               request.setAttribute("Marks33", rs.getString(35)==null?"":rs.getString(35));
               request.setAttribute("Marks34", rs.getString(36)==null?"":rs.getString(36));
               request.setAttribute("Marks35", rs.getString(37)==null?"":rs.getString(37));
               request.setAttribute("Marks36", rs.getString(38)==null?"":rs.getString(38));
               request.setAttribute("Marks37", rs.getString(39)==null?"":rs.getString(39));
               request.setAttribute("Marks38", rs.getString(40)==null?"":rs.getString(40));
               request.setAttribute("Marks39", rs.getString(41)==null?"":rs.getString(41));
               request.setAttribute("Marks40", rs.getString(42)==null?"":rs.getString(42));
               request.setAttribute("TotalMarks", rs.getString(2));
               request.setAttribute("TotalBooklets", rs.getString(1));
               request.setAttribute("Booklet",rform.getBundlecode());
               map.put("child","123456");
                list.add(map);
                map = null;
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public List<HashMap> getDistrictWiseReport() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_Diploma_Bundlewise_Count_Web}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("bCode", rs.getString(1));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
}
