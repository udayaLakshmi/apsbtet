/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;
import com.ap.WithLoginDAO.*;
import com.ap.Action.*;
import com.sms.util.MessagePropertiesUtil;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author APTOL301655
 */
public class VerificationDAO {
     public List getDocumentVerficationDetails(String username, String course, String subgroup) throws Exception {
        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_TSWH_DocumentVerificationList](?)}");
            cstmt.setString(1, username);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("regno", rs.getString(1));
                map.put("name", rs.getString(2));
                map.put("father", rs.getString(3));
                map.put("DOB", rs.getString(4));
                map.put("gender", rs.getString(5));
                map.put("mobile", rs.getString(6));
                map.put("status", rs.getString(7));
                  map.put("grade", rs.getString(8));

                lstDetails.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return lstDetails;
    }

// added on 11 Nov 2020
    public List getDegreeDocumentSubmitedDetails(String username) throws Exception {
         Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_TSWH_DocumentVerificationList_Completed](?,?)}");
            cstmt.setString(1, "1");
            cstmt.setString(2, username);

            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicationId", rs.getString(1));
                map.put("name", rs.getString(3));
                map.put("father", rs.getString(4));
                map.put("DOB", rs.getString(5));
                map.put("gender", rs.getString(6));
                map.put("mobilenumber", rs.getString(7));
                map.put("status", rs.getString(8));
                 map.put("grade", rs.getString(9));

                lstDetails.add(map);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return lstDetails;
    }
    // added on 11 Nov 2020 ends

    public List<HashMap> getDocumentDetails(RegisterForm rform, HttpServletRequest request) throws SQLException {


        ResultSet rs = null;
        Connection con = null;
        RegisterForm regform = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            Date todate1 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate1);

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Print(?,?)}");
            cstmt.setString(1, rform.getHallticket());
            cstmt.setString(2, dob);

            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("hall", rs.getString(1));
                request.setAttribute("bname", rs.getString(2));
                request.setAttribute("fname", rs.getString(4));
                request.setAttribute("dob", rform.getDob());
                request.setAttribute("mothername", rs.getString(5));
                request.setAttribute("gender", rs.getString(6));
                request.setAttribute("caste", rs.getString(7));
                request.setAttribute("ews", rs.getString(8));
                request.setAttribute("fatheroccu", rs.getString(9));
                request.setAttribute("income", rs.getString(10));
                request.setAttribute("mobile", rs.getString(11));
                request.setAttribute("email", rs.getString(12));
                request.setAttribute("areaStudied", rs.getString(13));
                request.setAttribute("address", rs.getString(14) + "," + rs.getString(15) + "," + rs.getString(82) + "," + rs.getString(16) + "," + rs.getString(17) + "," + rs.getString(18) + "," + rs.getString(19));
                //request.setAttribute("address1",rs.getString(17)+","+rs.getString(18)+","+rs.getString(19));

                request.setAttribute("cap", rs.getString(20));
                request.setAttribute("ph", rs.getString(21));
                request.setAttribute("ncc", rs.getString(22));
                request.setAttribute("sports", rs.getString(23));

                request.setAttribute("marks", rs.getString(26));

                request.setAttribute("tenClass", rs.getString(29));
                request.setAttribute("tenYear", rs.getString(30));
                request.setAttribute("tenState", rs.getString(31));
                request.setAttribute("tenDistrict", rs.getString(32));
                request.setAttribute("tenArea", rs.getString(33));

                request.setAttribute("nineClass", rs.getString(34));
                request.setAttribute("nineYear", rs.getString(35));
                request.setAttribute("nineState", rs.getString(36));
                request.setAttribute("nineDistrict", rs.getString(37));
                request.setAttribute("nineArea", rs.getString(38));

                request.setAttribute("eightClass", rs.getString(39));
                request.setAttribute("eightYear", rs.getString(40));
                request.setAttribute("eightState", rs.getString(41));
                request.setAttribute("eightDistrict", rs.getString(42));
                request.setAttribute("eightArea", rs.getString(43));

                request.setAttribute("sevenClass", rs.getString(44));
                request.setAttribute("sevenYear", rs.getString(45));
                request.setAttribute("sevenState", rs.getString(46));
                request.setAttribute("sevenDistrict", rs.getString(47));
                request.setAttribute("sevenArea", rs.getString(48));

                request.setAttribute("sixClass", rs.getString(49));
                request.setAttribute("sixYear", rs.getString(50));
                request.setAttribute("sixState", rs.getString(51));
                request.setAttribute("sixDistrict", rs.getString(52));
                request.setAttribute("sixArea", rs.getString(53));

                request.setAttribute("fiveClass", rs.getString(54));
                request.setAttribute("fiveYear", rs.getString(55));
                request.setAttribute("fiveState", rs.getString(56));
                request.setAttribute("fiveDistrict", rs.getString(57));
                request.setAttribute("fiveArea", rs.getString(58));

                request.setAttribute("fourClass", rs.getString(59));
                request.setAttribute("fourYear", rs.getString(60));
                request.setAttribute("fourState", rs.getString(61));
                request.setAttribute("fourDistrict", rs.getString(62));
                request.setAttribute("fourArea", rs.getString(63));

                request.setAttribute("thirdClass", rs.getString(64));
                request.setAttribute("thirdYear", rs.getString(65));
                request.setAttribute("thirdState", rs.getString(66));
                request.setAttribute("thirdDistrict", rs.getString(67));
                request.setAttribute("thirdArea", rs.getString(68));

                request.setAttribute("secondClass", rs.getString(69));
                request.setAttribute("secondYear", rs.getString(70));
                request.setAttribute("secondState", rs.getString(71));
                request.setAttribute("secondDistrict", rs.getString(72));
                request.setAttribute("secondArea", rs.getString(73));

                request.setAttribute("firstClass", rs.getString(74));
                request.setAttribute("firstYear", rs.getString(75));
                request.setAttribute("firstState", rs.getString(76));
                request.setAttribute("firstDistrict", rs.getString(77));
                request.setAttribute("firstArea", rs.getString(78));


                request.setAttribute("sportsinternational", rs.getString(79));
                request.setAttribute("sportsnational", rs.getString(80));
                request.setAttribute("sportsstate", rs.getString(81));


                request.setAttribute("village", rs.getString(82));
                request.setAttribute("localStatus", rs.getString(83));

                request.setAttribute("lastUpdatedDate", rs.getString(84));
                request.setAttribute("paymentRefNo", rs.getString(85));
                request.setAttribute("paymentAmount", rs.getString(86));
                request.setAttribute("paymentDate", rs.getString(87));

//                String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rform.getHallticket() + "\\" + rs.getString(27);
//       
//        File file = new File(filepath);
//                BufferedImage image = ImageIO.read(file);
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                ImageIO.write(image, "jpg", baos);
//                baos.flush();
//                byte[] imageInByteArray = baos.toByteArray();
//                String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
//                request.setAttribute("photo", b64);
                String filepath = MessagePropertiesUtil.FIlE_PATH2 + "\\" + rs.getString(27) + "\\" + rs.getString(27) + "_Photo.jpg";
                File file = new File(filepath);
                if (file.isFile()) {
                    BufferedImage image = ImageIO.read(file);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageIO.write(image, "jpg", baos);
                    baos.flush();
                    byte[] imageInByteArray = baos.toByteArray();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    request.setAttribute("photo", b64);
                }
                map.put("dd", "dd");
            }

            list.add(map);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public List<HashMap> getAcademicsDocumentDetails(RegisterForm rform, HttpServletRequest request) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_TSWH_DocumentVerificationList_Candidate](?)}");
            cstmt.setString(1, rform.getHallticket());
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();

                request.setAttribute("regno", rs.getString(1));
                request.setAttribute("bname", rs.getString(2));
                request.setAttribute("fname", rs.getString(3));
                request.setAttribute("dob", rs.getString(4));
                request.setAttribute("gender", rs.getString(5));
                request.setAttribute("caste", rs.getString(6));
                request.setAttribute("blind", rs.getString(7));
                request.setAttribute("Institute", rs.getString(8));
                request.setAttribute("examination", rs.getString(9));
                request.setAttribute("language", rs.getString(10));
                request.setAttribute("grade", rs.getString(11));
                request.setAttribute("mobile", rs.getString(23));
                request.setAttribute("email", rs.getString(24));
                request.setAttribute("aadhar", rs.getString(25));
                request.setAttribute("oldgregistrationno", rs.getString(27));
               
                request.setAttribute("photofile", rs.getString(33));
                request.setAttribute("signaturefile", rs.getString(34));
                request.setAttribute("upload1file", rs.getString(35));
                request.setAttribute("upload2file", rs.getString(36));
                request.setAttribute("upload3file", rs.getString(37));
                request.setAttribute("upload4file", rs.getString(38));
                request.setAttribute("upload5file", rs.getString(39));
                
                request.setAttribute("photocfrm", rs.getString(40));
                request.setAttribute("signaturecfrm", rs.getString(41));
                request.setAttribute("upload11cfrm", rs.getString(42));
                request.setAttribute("upload12cfrm", rs.getString(43));
                request.setAttribute("upload13cfrm", rs.getString(44));
                request.setAttribute("upload14cfrm", rs.getString(45));
                request.setAttribute("oldregistrationcfrm", rs.getString(46));
                
                request.setAttribute("blindfile", rs.getString(47));
                 request.setAttribute("blindcfrm", rs.getString(48));
                request.setAttribute("finalstatus", rs.getString(49));
                request.setAttribute("gradetype", rs.getString(50)==null?"":rs.getString(50));
                
                   request.setAttribute("upload1rmks", rs.getString(53));
                   request.setAttribute("upload2rmks", rs.getString(54));
                   request.setAttribute("upload3rmks", rs.getString(55));
                   request.setAttribute("upload4rmks", rs.getString(56));
                   request.setAttribute("upload5rmks", rs.getString(57));
                   request.setAttribute("blindrmks", rs.getString(58));
                   request.setAttribute("grade", rs.getString(59));
                   request.setAttribute("sscflag", rs.getString(60));
                   request.setAttribute("sschallticket", rs.getString(61));
                   request.setAttribute("lowegradeflag", rs.getString(62));
                   request.setAttribute("lowegradehallticket", rs.getString(63));
                   request.setAttribute("interflag", rs.getString(64));
                   request.setAttribute("interhallticket", rs.getString(65));
                   if((rs.getString(59).equalsIgnoreCase("TEH")||rs.getString(59).equalsIgnoreCase("TTH")||rs.getString(59).equalsIgnoreCase("THH"))){
                       if(rs.getString(60).equalsIgnoreCase("Y")&&rs.getString(62).equalsIgnoreCase("N")){
                           request.setAttribute("msg","SSC Certificate Autoverified");
                       }else if(rs.getString(60).equalsIgnoreCase("N")&&rs.getString(62).equalsIgnoreCase("Y")){
                              request.setAttribute("msg","LowerGrade Certificate Autoverified");
                       }else if(rs.getString(64).equalsIgnoreCase("Y")){
                              request.setAttribute("msg","Intermediate Certificate Autoverified");
                       }else{
                            request.setAttribute("msg","");
                       }
                   }else{
                        request.setAttribute("msg","");
                   }
                if(rform.getVstatus().equals("New Application")){
                  request.setAttribute("vstatus", "0");
                }else if(rform.getVstatus().equals("ReUploaded Application")){
                     request.setAttribute("vstatus", "1");
                }
                request.setAttribute("address", rs.getString(16)+","+rs.getString(17)+","+rs.getString(18)+","+rs.getString(21)+","+","+rs.getString(20)+","+","+rs.getString(22));

                try {
                    String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(50) + "\\" + rs.getString(25)+ "\\" + rs.getString(33);
                    File file = new File(filepath);
                    if (file.isFile()) {
//                        BufferedImage image = ImageIO.read(file);
//                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                        ImageIO.write(image, "jpg", baos);
//                        baos.flush();
//                        byte[] imageInByteArray = baos.toByteArray();
//                        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                        byte[] inFileName = FileUtils.readFileToByteArray(file);
   String b64 = DatatypeConverter.printBase64Binary(inFileName);
                        request.setAttribute("photo", b64);
                    } 
                } catch (Exception e) {
                    e.printStackTrace();
                }
                map.put("district_Name", "1234");
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return lstDetails;
    }

    public List<HashMap> getNCCDocumentDetails(RegisterForm rform, HttpServletRequest request,String roleid) {
        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_SportsDetails_Verfication](?,?)}");
            cstmt.setString(1, rform.getHallticket());
            cstmt.setString(2, roleid);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();

                request.setAttribute("aadharnumber", rs.getString(1));

                request.setAttribute("bname", rs.getString(2));
                request.setAttribute("dob", rs.getString(3));
                request.setAttribute("fname", rs.getString(4));

                request.setAttribute("mothername", rs.getString(5));
                request.setAttribute("gender", rs.getString(6));
                request.setAttribute("caste", rs.getString(7));
                request.setAttribute("ews", rs.getString(8));
                request.setAttribute("fatheroccu", rs.getString(9));
                request.setAttribute("income", rs.getString(10));
                request.setAttribute("mobile", rs.getString(11));
                request.setAttribute("email", rs.getString(12));
                request.setAttribute("areaStudied", rs.getString(13));
                request.setAttribute("address", rs.getString(14) + "," + rs.getString(15) + "," + rs.getString(82) + "," + rs.getString(16) + "," + rs.getString(17) + "," + rs.getString(18) + "," + rs.getString(19));
                //request.setAttribute("address1",rs.getString(17)+","+rs.getString(18)+","+rs.getString(19));

                request.setAttribute("cap", rs.getString(20));
                request.setAttribute("ph", rs.getString(21));
                request.setAttribute("ncc", rs.getString(22));
                request.setAttribute("sports", rs.getString(23));

                request.setAttribute("marks", rs.getString(26));

                request.setAttribute("tenClass", rs.getString(29));
                request.setAttribute("tenYear", rs.getString(30));
                request.setAttribute("tenState", rs.getString(31));
                request.setAttribute("tenDistrict", rs.getString(32));
                request.setAttribute("tenArea", rs.getString(33));

                request.setAttribute("nineClass", rs.getString(34));
                request.setAttribute("nineYear", rs.getString(35));
                request.setAttribute("nineState", rs.getString(36));
                request.setAttribute("nineDistrict", rs.getString(37));
                request.setAttribute("nineArea", rs.getString(38));

                request.setAttribute("eightClass", rs.getString(39));
                request.setAttribute("eightYear", rs.getString(40));
                request.setAttribute("eightState", rs.getString(41));
                request.setAttribute("eightDistrict", rs.getString(42));
                request.setAttribute("eightArea", rs.getString(43));

                request.setAttribute("sevenClass", rs.getString(44));
                request.setAttribute("sevenYear", rs.getString(45));
                request.setAttribute("sevenState", rs.getString(46));
                request.setAttribute("sevenDistrict", rs.getString(47));
                request.setAttribute("sevenArea", rs.getString(48));

                request.setAttribute("sixClass", rs.getString(49));
                request.setAttribute("sixYear", rs.getString(50));
                request.setAttribute("sixState", rs.getString(51));
                request.setAttribute("sixDistrict", rs.getString(52));
                request.setAttribute("sixArea", rs.getString(53));

                request.setAttribute("fiveClass", rs.getString(54));
                request.setAttribute("fiveYear", rs.getString(55));
                request.setAttribute("fiveState", rs.getString(56));
                request.setAttribute("fiveDistrict", rs.getString(57));
                request.setAttribute("fiveArea", rs.getString(58));

                request.setAttribute("fourClass", rs.getString(59));
                request.setAttribute("fourYear", rs.getString(60));
                request.setAttribute("fourState", rs.getString(61));
                request.setAttribute("fourDistrict", rs.getString(62));
                request.setAttribute("fourArea", rs.getString(63));

                request.setAttribute("thirdClass", rs.getString(64));
                request.setAttribute("thirdYear", rs.getString(65));
                request.setAttribute("thirdState", rs.getString(66));
                request.setAttribute("thirdDistrict", rs.getString(67));
                request.setAttribute("thirdArea", rs.getString(68));

                request.setAttribute("secondClass", rs.getString(69));
                request.setAttribute("secondYear", rs.getString(70));
                request.setAttribute("secondState", rs.getString(71));
                request.setAttribute("secondDistrict", rs.getString(72));
                request.setAttribute("secondArea", rs.getString(73));

                request.setAttribute("firstClass", rs.getString(74));
                request.setAttribute("firstYear", rs.getString(75));
                request.setAttribute("firstState", rs.getString(76));
                request.setAttribute("firstDistrict", rs.getString(77));
                request.setAttribute("firstArea", rs.getString(78));


                request.setAttribute("sportsinternational", rs.getString(79));
                request.setAttribute("sportsnational", rs.getString(80));
                request.setAttribute("sportsstate", rs.getString(81));


                request.setAttribute("village", rs.getString(82));
                request.setAttribute("localStatus", rs.getString(83));

                request.setAttribute("lastUpdatedDate", rs.getString(84));
                request.setAttribute("paymentRefNo", rs.getString(85));
                request.setAttribute("paymentAmount", rs.getString(86));
                request.setAttribute("paymentDate", rs.getString(87));



                request.setAttribute("capfile", rs.getString(91));
                request.setAttribute("phfile", rs.getString(92));
                request.setAttribute("nccfile", rs.getString(93));
                request.setAttribute("sportsinternationalfile", rs.getString(94));
                request.setAttribute("sportsnationalfile", rs.getString(95));
                request.setAttribute("sportsstatefile", rs.getString(96));

                request.setAttribute("capcfrm", rs.getString(97));
                request.setAttribute("phcfrm", rs.getString(98));
                request.setAttribute("ncccfrm", rs.getString(99));
                request.setAttribute("sportsinternationalcfrm", rs.getString(100));
                request.setAttribute("sportsnationalcfrm", rs.getString(101));
                request.setAttribute("sportsstatecfrm", rs.getString(102));

                request.setAttribute("finalstatus", rs.getString(103));
                // System.out.println("rs.getString(126)=="+rs.getString(126));

//                String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rform.getHallticket() + "\\" + rs.getString(27);
//                File file = new File(filepath);
//                if(file.exists()){
//                BufferedImage image = ImageIO.read(file);
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                ImageIO.write(image, "jpg", baos);
//                baos.flush();
//                byte[] imageInByteArray = baos.toByteArray();
//                String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
//                // System.out.println("b64=="+b64);
//                request.setAttribute("phtimage", b64);
//                }
//                  String filepath = MessagePropertiesUtil.FIlE_PATH2 + "\\" + rs.getString(27) + "\\" + rs.getString(27) + "_Photo.jpg";
//                    File file = new File(filepath);
//                    if (file.isFile()) {
//                        BufferedImage image = ImageIO.read(file);
//                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                        ImageIO.write(image, "jpg", baos);
//                        baos.flush();
//                        byte[] imageInByteArray = baos.toByteArray();
//                        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
//                        request.setAttribute("phtimage", b64);
//                    }
//                          request.setAttribute("phtimage", "");
                try {
                    String filepath = MessagePropertiesUtil.FIlE_PATH2 + "\\" + rs.getString(27) + "\\" + rs.getString(27) + "_Photo.jpg";
                    File file = new File(filepath);
                    if (file.isFile()) {
                        BufferedImage image = ImageIO.read(file);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ImageIO.write(image, "jpg", baos);
                        baos.flush();
                        byte[] imageInByteArray = baos.toByteArray();
                        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                        request.setAttribute("phtimage", b64);
                    } else {
                        String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(1) + "\\" + rs.getString(1) + "_PHOTO.jpg";
                        File file1 = new File(filepath1);
                        BufferedImage image = ImageIO.read(file1);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ImageIO.write(image, "jpg", baos);
                        baos.flush();
                        byte[] imageInByteArray = baos.toByteArray();
                        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                        request.setAttribute("phtimage", b64);
                    }
                } catch (Exception e) {
                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(1) + "\\" + rs.getString(1) + "_PHOTO.jpg";
                    File file1 = new File(filepath1);
                    BufferedImage image = ImageIO.read(file1);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageIO.write(image, "jpg", baos);
                    baos.flush();
                    byte[] imageInByteArray = baos.toByteArray();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    request.setAttribute("phtimage", b64);
                }
                map.put("district_Name", "1234");
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return lstDetails;
    }

    public int submitAcademicsDocumentStatusDetails(RegisterForm rform, HttpServletRequest req,String uname) {

        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int res = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_TSWH_DocumentVerification_Update] (?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, req.getParameter("applicationno"));
            cstmt.setString(2, uname);
            cstmt.setString(3, "3");
            cstmt.setString(4, "3");
            cstmt.setString(5, req.getParameter("blindConfirm"));
            cstmt.setString(6, req.getParameter("upload1Confirm"));
            cstmt.setString(7, req.getParameter("upload2Confirm"));
            cstmt.setString(8, req.getParameter("upload3Confirm"));
            cstmt.setString(9, req.getParameter("upload4Confirm"));
            cstmt.setString(10, req.getParameter("upload5Confirm"));
            cstmt.setString(11, req.getParameter("eligibleConfirm"));
            cstmt.setString(12, "None");
            cstmt.setString(13, "None");
              if (req.getParameter("blindConfirm").equalsIgnoreCase("2")) {
                cstmt.setString(14, req.getParameter("blindRemarksU"));
            } else {
                cstmt.setString(14, req.getParameter("blindRemarks"));
            }
         
            if (req.getParameter("upload1Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(15, req.getParameter("upload1RemarksU"));
            } else {
                cstmt.setString(15, req.getParameter("upload1Remarks"));
            }
            
            if (req.getParameter("upload2Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(16, req.getParameter("upload2RemarksU"));
            } else {
                cstmt.setString(16, req.getParameter("upload2Remarks"));
            }
            
            if (req.getParameter("upload3Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(17, req.getParameter("upload3RemarksU"));
            } else {
                cstmt.setString(17, req.getParameter("upload3Remarks"));
            }
            
            if (req.getParameter("upload4Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(18, req.getParameter("upload4RemarksU"));
            } else {
                cstmt.setString(18, req.getParameter("upload4Remarks"));
            }
            if (req.getParameter("upload5Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(19, req.getParameter("upload5RemarksU"));
            } else {
                cstmt.setString(19, req.getParameter("upload5Remarks"));
            }
            cstmt.setString(20, req.getParameter("finaleligibleRemarks"));
            res = cstmt.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public int submitNCCDocumentStatusDetails(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int res = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_Sports_Details_Update] (?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?)}");

//  capclass capConfrm capRemarks caprmks
//phclass  phConfirm phRemarks  phrmks
//nccclass nccConfirm nccRemarks nccrmks
//  sportsinterclass  sportsinterConfirm     sportsinterRemarks   sportsinterRemarks
//                    sportsnationalclass sportsnationalConfirm sportsnationalRemarks sportsnationalrmks
//                    sportsstateclass sportsstateConfirm sportsstateRemarks sportsstatermks
//            
            //   System.out.println("req.getParameter(\"applicationno\")=="+req.getParameter("applicationno"));   
            cstmt.setString(1, req.getParameter("applicationno"));
            cstmt.setString(2, "NCCNRI");

            cstmt.setString(3, req.getParameter("capConfrm"));

            cstmt.setString(4, req.getParameter("phConfirm"));
            cstmt.setString(5, req.getParameter("nccConfirm"));
            cstmt.setString(6, req.getParameter("sportsinterConfirm"));
            cstmt.setString(7, req.getParameter("sportsnationalConfirm"));
            cstmt.setString(8, req.getParameter("sportsstateConfirm"));

            cstmt.setString(9, "0".equals(req.getParameter("eligibleConfirm")) ? "1" : req.getParameter("eligibleConfirm"));
            //  System.out.println("nccRemarks====="+req.getParameter("nccRemarks"));
            cstmt.setString(10, req.getParameter("capRemarks"));
            cstmt.setString(11, req.getParameter("phRemarks"));
            cstmt.setString(12, req.getParameter("nccRemarks"));
            cstmt.setString(13, req.getParameter("sportsinterRemarks"));
            cstmt.setString(14, req.getParameter("sportsnationalRemarks"));
            cstmt.setString(15, req.getParameter("sportsstateRemarks"));
            cstmt.setString(16, req.getParameter("finaleligibleRemarks"));

            res = cstmt.executeUpdate();
            // System.out.println("res DAO ===="+res);

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }
      public int submitCAPDocumentStatusDetails(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int res = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_SportsCAP_Details_Update] (?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?)}");

//  capclass capConfrm capRemarks caprmks
//phclass  phConfirm phRemarks  phrmks
//nccclass nccConfirm nccRemarks nccrmks
//  sportsinterclass  sportsinterConfirm     sportsinterRemarks   sportsinterRemarks
//                    sportsnationalclass sportsnationalConfirm sportsnationalRemarks sportsnationalrmks
//                    sportsstateclass sportsstateConfirm sportsstateRemarks sportsstatermks
//            
            //   System.out.println("req.getParameter(\"applicationno\")=="+req.getParameter("applicationno"));   
            cstmt.setString(1, req.getParameter("applicationno"));
            cstmt.setString(2, "NCCNRI");

            cstmt.setString(3, req.getParameter("capConfrm"));

            cstmt.setString(4, req.getParameter("phConfirm"));
            cstmt.setString(5, req.getParameter("nccConfirm"));
            cstmt.setString(6, req.getParameter("sportsinterConfirm"));
            cstmt.setString(7, req.getParameter("sportsnationalConfirm"));
            cstmt.setString(8, req.getParameter("sportsstateConfirm"));

            cstmt.setString(9, "0".equals(req.getParameter("eligibleConfirm")) ? "1" : req.getParameter("eligibleConfirm"));
            //  System.out.println("nccRemarks====="+req.getParameter("nccRemarks"));
            cstmt.setString(10, req.getParameter("capRemarks"));
            cstmt.setString(11, req.getParameter("phRemarks"));
            cstmt.setString(12, req.getParameter("nccRemarks"));
            cstmt.setString(13, req.getParameter("sportsinterRemarks"));
            cstmt.setString(14, req.getParameter("sportsnationalRemarks"));
            cstmt.setString(15, req.getParameter("sportsstateRemarks"));
            cstmt.setString(16, req.getParameter("finaleligibleRemarks"));

            res = cstmt.executeUpdate();
            // System.out.println("res DAO ===="+res);

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }
      
        public int submitOnlyNCCDocumentStatusDetails(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int res = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_SportsNCC_Details_Update] (?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?)}");

//  capclass capConfrm capRemarks caprmks
//phclass  phConfirm phRemarks  phrmks
//nccclass nccConfirm nccRemarks nccrmks
//  sportsinterclass  sportsinterConfirm     sportsinterRemarks   sportsinterRemarks
//                    sportsnationalclass sportsnationalConfirm sportsnationalRemarks sportsnationalrmks
//                    sportsstateclass sportsstateConfirm sportsstateRemarks sportsstatermks
//            
            //   System.out.println("req.getParameter(\"applicationno\")=="+req.getParameter("applicationno"));   
            cstmt.setString(1, req.getParameter("applicationno"));
            cstmt.setString(2, "NCCNRI");

            cstmt.setString(3, req.getParameter("capConfrm"));

            cstmt.setString(4, req.getParameter("phConfirm"));
            cstmt.setString(5, req.getParameter("nccConfirm"));
            cstmt.setString(6, req.getParameter("sportsinterConfirm"));
            cstmt.setString(7, req.getParameter("sportsnationalConfirm"));
            cstmt.setString(8, req.getParameter("sportsstateConfirm"));

            cstmt.setString(9, "0".equals(req.getParameter("eligibleConfirm")) ? "1" : req.getParameter("eligibleConfirm"));
            //  System.out.println("nccRemarks====="+req.getParameter("nccRemarks"));
            cstmt.setString(10, req.getParameter("capRemarks"));
            cstmt.setString(11, req.getParameter("phRemarks"));
            cstmt.setString(12, req.getParameter("nccRemarks"));
            cstmt.setString(13, req.getParameter("sportsinterRemarks"));
            cstmt.setString(14, req.getParameter("sportsnationalRemarks"));
            cstmt.setString(15, req.getParameter("sportsstateRemarks"));
            cstmt.setString(16, req.getParameter("finaleligibleRemarks"));

            res = cstmt.executeUpdate();
            // System.out.println("res DAO ===="+res);

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }
}
