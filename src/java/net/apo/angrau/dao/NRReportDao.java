/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.MessagePropertiesUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.forms.RegisterForm;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author APTOL301294
 */
public class NRReportDao {
    
    public String getCentercode(String userName) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement cstmt = null;
        CallableStatement st = null;
        String Centercode="";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareStatement("select Examination_CenterCode from TWSH_Centers_CS_JCS_Mapping with(nolock) where USER_NAME=?");
            cstmt.setString(1,userName);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                 Centercode=rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return Centercode;
    }
    
    public ArrayList getGradeList(RegisterForm rForm) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_GradeDetails_Get(?)}");
         
            cstmt.setString(1,rForm.getBlind());
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("gradecode", rs.getString(1));
                    map.put("gradename", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    public ArrayList getCenterList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_CenterDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("ccode", rs.getString(1));
                    map.put("cname", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    public ArrayList getBatchList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_ExamBatchTypeDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("batchcode1", rs.getString(1));
                    map.put("batchname", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getTWReport(String centerCode,String gradeCode,String batchCode) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_NRDATA_TWDetailsReport_Get(?,?,?)}");
            cstmt.setString(1, centerCode);
            cstmt.setString(2, gradeCode);
            cstmt.setString(3, batchCode);
//               System.out.println("centerCode : "+centerCode);
//            System.out.println("gradeCode : "+gradeCode);
//            System.out.println("batchCode : "+batchCode);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("instName", rs.getString(2));
                    map.put("address", rs.getString(3));
                    map.put("totalAppli", rs.getString(4));
                    map.put("totalPaid", rs.getString(5));
                    map.put("totalUnPaid", rs.getString(6));

                    map.put("add", rs.getString(7));
                    map.put("centerC", rs.getString(8));
                    map.put("centerN", rs.getString(9));
                    map.put("grade", rs.getString(10));
                    map.put("batch", rs.getString(11));
                    map.put("mobile", rs.getString(12));

                    map.put("instcode", rs.getString(13));

                    String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(10) + "\\" + rs.getString(16) + "\\" + rs.getString(14);
                    map.put("photopath", filepath);
                    map.put("sigpath", filepath);
                    File file = new File(filepath);
                    String b64 = "No Image";
                    if (file.exists() && !file.isDirectory()) {
                        byte[] inFileName = FileUtils.readFileToByteArray(file);
                        b64 = DatatypeConverter.printBase64Binary(inFileName);
                    }
                    map.put("photo", b64);

                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(10) + "\\" + rs.getString(16) + "\\" + rs.getString(15);
                    map.put("sigpath", filepath1);
                    String sigb64 = "No Image";
                    File file1 = new File(filepath1);
                    if (file1.exists() && !file1.isDirectory()) {
                        byte[] inFileName1 = FileUtils.readFileToByteArray(file1);
                        sigb64 = DatatypeConverter.printBase64Binary(inFileName1);
                    }
                    map.put("sig", sigb64);

                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getSHReport(String centerCode,String gradeCode,String batchCode) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_NRDATA_SHDetailsReport_Get(?,?,?)}");
            cstmt.setString(1, centerCode);
            cstmt.setString(2, gradeCode);
            cstmt.setString(3, batchCode);
//            System.out.println("centerCode : "+centerCode);
//            System.out.println("gradeCode : "+gradeCode);
//            System.out.println("batchCode : "+batchCode);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("instName", rs.getString(2));
                    map.put("address", rs.getString(3));
                    map.put("totalAppli", rs.getString(4));
                    map.put("totalPaid", rs.getString(5));
                    map.put("totalUnPaid", rs.getString(6));

                    map.put("add", rs.getString(7));
                    map.put("centerC", rs.getString(8));
                    map.put("centerN", rs.getString(9));
                    map.put("grade", rs.getString(10));
                    map.put("batch", rs.getString(11));
                    map.put("mobile", rs.getString(12));

                    map.put("instcode", rs.getString(13));
                    map.put("phhot", rs.getString(14));
                    map.put("sig", rs.getString(15));
                    String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(10) + "\\" + rs.getString(16) + "\\" + rs.getString(14);
//                    String filepath = MessagePropertiesUtil.FIlE_PATH + "\\SEH"+"\\449585831514"+"\\449585831514_PHOTO.jpg";
                    File file = new File(filepath);
                    map.put("photopath", filepath);
                    String b64 = "";
                    if (file.exists() && !file.isDirectory()) {
                        try {
                            byte[] inFileName = FileUtils.readFileToByteArray(file);
                            b64 = DatatypeConverter.printBase64Binary(inFileName);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("photo", b64);

                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(10) + "\\" + rs.getString(16) + "\\" + rs.getString(15);
//                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\SEH" + "\\449585831514" + "\\449585831514_SIGNATURE.jpg";
                    map.put("sigpath", filepath1);
                    File file1 = new File(filepath1);
                    String sigb64 = "";
                    if (file1.exists() && !file1.isDirectory()) {
                        try {
                            byte[] inFileName1 = FileUtils.readFileToByteArray(file1);
                            sigb64 = DatatypeConverter.printBase64Binary(inFileName1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("sig", sigb64);

                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public void getPdfData(String filepath, String filename, String userName,String gradeCode,String batchCode) throws SQLException {
        createPDF(filepath, filename, userName, gradeCode, batchCode);
    }

    private void createPDF(String filepath, String filename, String userName,String gradeCode,String batchCode) {

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list = getSHReport(userName,gradeCode,batchCode);
            //special font sizes
            Font heading = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
            Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD, new BaseColor(119, 41, 4)); //MAGENTA -Pink
            Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 9.5f);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            doc.setPageSize(PageSize.A4.rotate());

            //open document
            doc.open();

            //create a paragraph
            Paragraph paragraph = new Paragraph(""
                    + "                    "
                    + "STATE BOARD OF TECHNICAL EDUCTION & TRAINING           "
                    + "                                      "
                    + "                                      "
                    + "                                      "
                    + "                          "
                    + " ANDHRA PRADESH - VIJAYAWADA"
                    + "                          "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "TYPEWRITING AND SHORTHAND EXAMINATIONS, JULY/AUGUST - 2021"
                    + "                          "
                    + "                                        "
                    + "                                        "
                    + "SHORTHAND NR REPORT", heading);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            

            //specify column widths
            float[] columnWidths = {1f, 6f, 6f, 4f, 4f, 3f, 4f, 4f, 2f, 5f, 3f, 3f, 3f, 7f, 6f};
            //create PDF table with the given widths
            PdfPTable table = new PdfPTable(columnWidths);
            // set table width a percentage of the page width
            table.setWidthPercentage(100f);

            //insert column headings
            insertCell(table, "SNO", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Reg.No/ \n Hall ticket No", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Name of the Candidate", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Father Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Date of Birth", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Institute Code/ \n Private", Element.ALIGN_CENTER, 1, bfBold12);

            insertCell(table, "Institute Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Institute Address", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Center Code", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Center Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Grade", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Batch", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Phone.No", Element.ALIGN_CENTER, 1, bfBold12);

            insertCell(table, "Photo", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Candidate Signature", Element.ALIGN_CENTER, 1, bfBold12);

            table.setHeaderRows(1);


            for (int x = 0; x < list.size(); x++) {
                HashMap map = list.get(x);

                insertCell(table, map.get("sno") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("instName") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("address") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("totalAppli") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("totalPaid") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("instcode") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("totalUnPaid") + "", Element.ALIGN_LEFT, 1, bf12);

                insertCell(table, map.get("add") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("centerC") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("centerN") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("grade") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("batch") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("mobile") + "", Element.ALIGN_LEFT, 1, bf12);

                float[] columnWidths1 = {2f};
                PdfPTable photostable = new PdfPTable(columnWidths1);
                photostable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);

                photostable.getRowHeight(150);
                String Photo = map.get("photopath").toString();
                String sign = map.get("sigpath").toString();;
                File myFile = new File(Photo);
                if (myFile.exists() && !myFile.isDirectory()) {
                    Image img = Image.getInstance(Photo);
                    img.scaleToFit(80f, 80f);
                    img.setAlignment(Element.ALIGN_CENTER);
                    PdfPCell pcell1 = new PdfPCell(img);
                    pcell1.setFixedHeight(80f);
                    photostable.addCell(pcell1);
                } else {
                    Paragraph p1 = new Paragraph("            No image       ", bf12);
                    photostable.addCell(p1);
                }
                File myFile1 = new File(sign);
                if (myFile1.exists() && !myFile1.isDirectory()) {
                    Image img1 = Image.getInstance(sign);
                    img1.scaleToFit(65f, 40f);
                    img1.setAlignment(Element.ALIGN_CENTER);
                    PdfPCell pcell2 = new PdfPCell(img1);

                    photostable.addCell(pcell2);
                } else {
                    Paragraph p1 = new Paragraph("             No image            ", bf12);
                    photostable.addCell(p1);
                }
                table.addCell(photostable);

                insertCell(table, "", Element.ALIGN_LEFT, 1, bf12);

                map = null;

            }
            //add the PDF table to the paragraph 
            paragraph.add(table);
            // add the paragraph to the document
            doc.add(paragraph);

        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    public void getPdfDataTW(String filepath, String filename, String userName,String gradeCode,String batchCode) throws SQLException {
        createPDFTW(filepath, filename, userName, gradeCode, batchCode);
    }

    private void createPDFTW(String filepath, String filename,  String userName,String gradeCode,String batchCode){

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list = getTWReport( userName, gradeCode, batchCode);
            //special font sizes
            Font heading = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
            Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD, new BaseColor(119, 41, 4)); //MAGENTA -Pink
            Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 9.5f);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            doc.setPageSize(PageSize.A4.rotate());

            //open document
            doc.open();

            //create a paragraph
            
            Paragraph paragraph = new Paragraph(""
                    + "                    "
                    + "STATE BOARD OF TECHNICAL EDUCTION & TRAINING           "
                    + "                                      "
                    + "                                      "
                    + "                                      "
                    + "                          "
                    + " ANDHRA PRADESH - VIJAYAWADA"
                    + "                          "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "TYPEWRITING AND SHORTHAND EXAMINATIONS, JULY/AUGUST - 2021"
                    + "                          "
                    + "                                        "
                    + "                                        "
                    + "TYPEWRITING NR REPORT", heading);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            

            //specify column widths
            float[] columnWidths = {1f, 6f, 6f, 4f, 4f, 3f, 4f, 4f, 2f, 5f, 3f, 3f, 3f, 7f, 6f};
            //create PDF table with the given widths
            PdfPTable table = new PdfPTable(columnWidths);
            // set table width a percentage of the page width
            table.setWidthPercentage(100f);

            //insert column headings
            insertCell(table, "SNO", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Reg.No/ \n Hall ticket No", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Name of the Candidate", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Father Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Date of Birth", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Institute Code/ \n Private", Element.ALIGN_CENTER, 1, bfBold12);

            insertCell(table, "Institute Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Institute Address", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Center Code", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Center Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Grade", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Batch", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Phone.No", Element.ALIGN_CENTER, 1, bfBold12);

            insertCell(table, "Photo", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Candidate Signature", Element.ALIGN_CENTER, 1, bfBold12);

            table.setHeaderRows(1);


            for (int x = 0; x < list.size(); x++) {
                HashMap map = list.get(x);

                insertCell(table, map.get("sno") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("instName") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("address") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("totalAppli") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("totalPaid") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("instcode") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("totalUnPaid") + "", Element.ALIGN_LEFT, 1, bf12);

                insertCell(table, map.get("add") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("centerC") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("centerN") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("grade") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("batch") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("mobile") + "", Element.ALIGN_LEFT, 1, bf12);

                float[] columnWidths1 = {2f};
                PdfPTable photostable = new PdfPTable(columnWidths1);
                photostable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);

                photostable.getRowHeight(150);
                String Photo = map.get("photopath").toString();
                String sign = map.get("sigpath").toString();;
                File myFile = new File(Photo);
                if (myFile.exists() && !myFile.isDirectory()) {
                    Image img = Image.getInstance(Photo);
                    img.scaleToFit(80f, 80f);
                    img.setAlignment(Element.ALIGN_CENTER);
                    PdfPCell pcell1 = new PdfPCell(img);
                    pcell1.setFixedHeight(80f);
                    photostable.addCell(pcell1);
                } else {
                    Paragraph p1 = new Paragraph("            No image       ", bf12);
                    photostable.addCell(p1);
                }
                File myFile1 = new File(sign);
                if (myFile1.exists() && !myFile1.isDirectory()) {
                    Image img1 = Image.getInstance(sign);
                    img1.scaleToFit(65f, 40f);
                    img1.setAlignment(Element.ALIGN_CENTER);
                    PdfPCell pcell2 = new PdfPCell(img1);

                    photostable.addCell(pcell2);
                } else {
                    Paragraph p1 = new Paragraph("             No image            ", bf12);
                    photostable.addCell(p1);
                }
                table.addCell(photostable);

                insertCell(table, "", Element.ALIGN_LEFT, 1, bf12);

                map = null;

            }
            //add the PDF table to the paragraph 
            paragraph.add(table);
            // add the paragraph to the document
            doc.add(paragraph);

        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
