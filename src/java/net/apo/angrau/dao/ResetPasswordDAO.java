/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author 1582792
 */
public class ResetPasswordDAO {
      public List<HashMap> getLoginDetailsReport() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
//            st = con.prepareStatement("select INST_CODE,INSTITUTION_NAME,ADDRESS,TOWN\n" +
//" from logindetails (nolock) a join TWSH_INSTITUTE_MASTER (nolock) b \n" +
//"on a.UserName=b.INST_CODE\n" +
//"where RoleId='1'");
//            
                cstmt = con.prepareCall("{Call ResetPassword_Get()}");
            
            rs = cstmt.executeQuery();
//            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("instCode", rs.getString(1));
//                    map.put("regCode", rs.getString(3));
                    map.put("instName", rs.getString(2));
                    map.put("address", rs.getString(3));
                    
                    if (rs.getString(4) != null) {
                        map.put("town", rs.getString(4));
                    } else {
                        map.put("town", "");
                    }
                   
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    public String submitData(RegisterForm rform,String uname) throws Exception {
        String result = null;

        CallableStatement cstmt = null;
//        PreparedStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        try {
            con = DatabaseConnection.getConnection();
//            cstmt = con.prepareStatement("select rgukt_rollno,name,fathername,category,sc_name,school_type,managment from Studentinfo where rgukt_rollno=? and dob=?");
            cstmt = con.prepareCall("{Call Usp_LoginDetails_update (?,?)}");
            cstmt.setString(1, rform.getInstcode().trim());
          
            cstmt.setString(2, uname);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result=rs.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return result;
    }
}
