/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.sms.util.MessagePropertiesUtil;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.forms.RegisterForm;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author APTOL301294
 */
public class RegisterBeforeLoginCCICPrintDao {

    public ArrayList getApplicationStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_AplicationStatus_PrintApplication (?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim()); ///Reg No
            cstmt.setString(2, rform.getGrade1().trim()); ///Mobile No
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                map = new HashMap<String, String>();
                map.put("key", rs.getString(1));
                map.put("keyValue", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public String getStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        String result = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_HallticketStatus (?,?,?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim());
            cstmt.setString(2, rform.getGrade().trim());
            cstmt.setString(3, rform.getExamination().trim());
            cstmt.setString(4, rform.getLanguage().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return result;
    }

    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_Student_Print_BeforeLogin(?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim());//RegNo
            cstmt.setString(2, rform.getGrade1().trim());//Mobile
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                request.setAttribute("name", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
                request.setAttribute("dob", rs.getString(3));
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("instID", rs.getString(5));

                request.setAttribute("institution", rs.getString(6));
                request.setAttribute("cat", rs.getString(7));
                request.setAttribute("courseId", rs.getString(8));
                request.setAttribute("courseName", rs.getString(9));
                request.setAttribute("examinationDistrict", rs.getString(10));

                request.setAttribute("examinationCenter", rs.getString(11));
                request.setAttribute("hNo", rs.getString(12));
                request.setAttribute("street", rs.getString(13));

                request.setAttribute("village", rs.getString(14));
                request.setAttribute("state", rs.getString(15));
                request.setAttribute("district", rs.getString(16));
                request.setAttribute("mandal", rs.getString(17));
                request.setAttribute("pincode", rs.getString(18));

                request.setAttribute("mobile", rs.getString(19));
                request.setAttribute("email", rs.getString(20));
                request.setAttribute("aadhar", rs.getString(21));


                request.setAttribute("paymentRefNo", rs.getString(25));
                request.setAttribute("paymentAmount", rs.getString(26));
                request.setAttribute("paymentDate", rs.getString(27));

                request.setAttribute("date", new Date());

                request.setAttribute("attendance", rs.getString(29).toString());
                request.setAttribute("sscHallTicket", rs.getString(30).toString());
                request.setAttribute("RegNo", rs.getString(31));

                String filepath = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(8) + "\\" + rs.getString(21) + "\\" + rs.getString(22);
                File file = new File(filepath);
                byte[] inFileName = FileUtils.readFileToByteArray(file);
                String b64 = DatatypeConverter.printBase64Binary(inFileName);
                request.setAttribute("photo", b64);

                String filepath1 = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(8) + "\\" + rs.getString(21) + "\\" + rs.getString(23);
                File file1 = new File(filepath1);
                byte[] inFileName1 = FileUtils.readFileToByteArray(file1);
                String sigb64 = DatatypeConverter.printBase64Binary(inFileName1);
                request.setAttribute("signature", sigb64);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }

    public String getvalidatingAadharNumWithApplication(String appNO, String dob) throws Exception {
        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String mb = "";
        String mm = "";
        try {
            String[] mb1 = dob.split("/");
            mm = mb1[2].toString() + "-" + mb1[1].toUpperCase() + "-" + mb1[0].toString();
            con = DatabaseConnection.getConnection();
            query = "select mobile from Diploma_Registration with(nolock)  where HallTicket='" + appNO + "' and DOB='" + mm + "'";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    mb = rs.getString(1);
                }
            } else {
                mb = "";
            }
        } catch (Exception e) {
            mb = "";
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mb;
    }

    public int SaveOTP(String Appno, String mobile, String otp, String systemIp) throws Exception {
        int count = 0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call proc_OTP_Details_Insert(?,?,?,?)}");
            cstmt.setString(1, Appno);
            cstmt.setString(2, mobile);
            cstmt.setString(3, otp);
            cstmt.setString(4, systemIp);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public String getvalidatingOTP(String Appno, String mobile) throws Exception {
        String otp = "";
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select OTP  from OTP_Details where Mobile_Num='" + mobile + "' and Aadhar_Id='" + Appno + "'";
            //System.out.println("query "+query);
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    otp = rs.getString(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return otp;
    }

    public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select Grade,GRADE_DESC from TWSH_GRADE_MASTER with(nolock) order by RowID asc";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }
}
