/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.MessagePropertiesUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author APTOL301294
 */
public class RegisterCCICPrintPDFDao {

    public List<HashMap> getCandidateList(String uname) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_CollegeWise_CandidateList_For_PDFPrint(?)}");
            cstmt.setString(1, uname);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("name", rs.getString(1));
                    Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(2));
                    String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                    map.put("dob", dob);
                    map.put("mobile", rs.getString(3));
                    map.put("examination", rs.getString(4));
                    map.put("grade", rs.getString(5));
                    map.put("regno", rs.getString(6));
                    map.put("aadhar", rs.getString(7));
                    map.put("sdate", rs.getString(8));
                    map.put("sstatus", rs.getString(9));
                    map.put("category", rs.getString(10));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPdfData(String filepath, String filename, RegisterForm rform, HttpServletRequest request) throws SQLException {
        createPDF(filepath, filename, rform, request);
    }

    private void createPDF(String filepath, String filename, RegisterForm rform, HttpServletRequest request) {

        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {
            // fname gender caste region cname university deciplane   
            HashMap map = getData1(rform);
            String name = map.get("name").toString();
            String fname = map.get("fname").toString();
            String dob = map.get("dob").toString();
            String gender = map.get("gender").toString();

            String institution = map.get("institution").toString();
            String institutionId = map.get("institutionId").toString();

            String examinationType = map.get("examinationAppearing").toString();
            String grade = map.get("courseName").toString();
            String examinationdistrict = map.get("examinationDistrict").toString();
            String examinationcenter = map.get("examinationCenter").toString();
            String attendance = map.get("attendance").toString();
            

            String houseno = map.get("hNo").toString();
            String street = map.get("street").toString();
            String village = map.get("village").toString();
            String state = map.get("state").toString();
            String district = map.get("district").toString();
            String mandal = map.get("mandal").toString();
            String pincode = map.get("pincode").toString();
            String mobile = map.get("mobile").toString();
            String email = map.get("email").toString();
            String aadhar = map.get("aadhar").toString();
            String date = map.get("date").toString();

            String registrationno = map.get("RegNo").toString();
            String paymentrefno = map.get("paymentRefNo").toString();
            String paymentamount = map.get("paymentAmount").toString();
            String paymentdate = map.get("paymentDate").toString();
            String filepath1 = map.get("filepath").toString();
            //special font sizes
            Font heading = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD, BaseColor.BLACK);
            Font f5 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, new BaseColor(0, 0, 0)); //MAGENTA -Pink
            Font subheading = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE);
            Font subheading1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLUE);
            Font normalSizeBold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, new BaseColor(0, 0, 0));
            Font italic = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLDITALIC, BaseColor.DARK_GRAY);
            Font italicbold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLDITALIC, BaseColor.BLACK);
            Font italicblue = new Font(Font.FontFamily.HELVETICA, 10, Font.UNDERLINE, BaseColor.BLUE);
            //file path
            String path = filepath + "/" + filename;

            String path2 = request.getServletContext().getRealPath("//").toString() + "//assets//img//logo.png";

            //doc.setPageSize(PageSize.A4);
            Rectangle layout = new Rectangle(PageSize.A4);
//            layout.setBackgroundColor(new BaseColor(100, 200, 180)); //Background color
//            layout.setBorderColor(BaseColor.DARK_GRAY);  //Border color
            layout.setBorderWidth(3);      //Border width  
            layout.setBorder(Rectangle.BOX);  //Border on 4 sides
            Document doc = new Document(layout);
            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));
            //open document
            doc.open();

            Image logo = null;
            Paragraph ImgPh = new Paragraph("");
            try {
                logo = Image.getInstance(path2);
            } catch (FileNotFoundException e) {
            }


            PdfPTable tableheading = new PdfPTable(1);
            Image image = Image.getInstance(logo);
            image.scaleToFit(10f, 10f);
            image.setAlignment(Element.ALIGN_CENTER);
            image.setBorder(Rectangle.BOX);
            image.setBorderWidth(1);
            image.setAbsolutePosition(5, 5);
            tableheading.addCell(new PdfPCell(image, true));
            // Adding image to the document       
            doc.add(tableheading);


            doc.add(new Paragraph("\n"));
            //Chunk underline = new Chunk("CCIC REGISTRATION FORM", heading);
            Chunk underline = new Chunk("CCIC, CRAFT & OTHER CERTIFICATE COURSES REGISTRATION FORM", heading);
            underline.setUnderline(0.1f, -2f); //0.1 thick, -2 y-location
            Phrase phrase = new Phrase();
            phrase.add(underline);
            Paragraph param = new Paragraph();
            param.add(phrase);
            param.setAlignment(Element.ALIGN_CENTER);
            doc.add(param);
            doc.add(new Paragraph("\n"));
            Paragraph para = new Paragraph("");
            para.setAlignment(Element.ALIGN_LEFT);
            Image image1 = Image.getInstance(filepath1);
            image1.scaleToFit(70f, 40f);
            image1.setBorder(Rectangle.BOX);
            image1.setAlignment(2);
            image1.setBorderWidth(3);
            doc.add(para);
            doc.add(image1);
//            doc.add(new Paragraph("\n"));
            Chunk provision = new Chunk("PERSONAL DETAILS ", subheading);
            Paragraph provision1 = new Paragraph();
            provision1.add(new Phrase(provision));
            doc.add(provision1);
            doc.add(new Paragraph("\n"));

            PdfPTable tableper = new PdfPTable(4);
            tableper.setLockedWidth(true);
            tableper.setTotalWidth(530f);
            tableper.setWidths(new int[]{1, 1, 1, 1});
            PdfPCell cell1 = new PdfPCell();
            cell1.addElement(new Phrase("Name ", normalSizeBold));
            PdfPCell cell2 = new PdfPCell();
            cell2.addElement(new Phrase("" + name, italic));
            tableper.addCell(cell1);
            tableper.addCell(cell2);
// Image image1 = Image.getInstance(filepath1);
//            cell1 = new PdfPCell();
//            cell2 = new PdfPCell();
//            cell1.addElement(new Phrase("Photo ", normalSizeBold));
//            tableper.addCell(cell1);
//            tableper.addCell(image1);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Father Name ", normalSizeBold));
            cell2.addElement(new Phrase("" + fname, italic));
            tableper.addCell(cell1);
            tableper.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Date Of Birth", normalSizeBold));
            cell2.addElement(new Phrase("" + dob, italic));
            tableper.addCell(cell1);
            tableper.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Gender", normalSizeBold));
            cell2.addElement(new Phrase("" + gender, italic));
            tableper.addCell(cell1);
            tableper.addCell(cell2);

            doc.add(tableper);

            doc.add(new Paragraph("\n"));

            Chunk provision2 = new Chunk("EXAMINATION DETAILS ", subheading);
            Paragraph provision21 = new Paragraph();
            provision21.add(new Phrase(provision2));
            doc.add(provision21);
            doc.add(new Paragraph("\n"));


            PdfPTable tableper2 = new PdfPTable(4);
            tableper2.setLockedWidth(true);
            tableper2.setTotalWidth(530f);
            tableper2.setWidths(new int[]{1, 1, 1, 1});
            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Institution", normalSizeBold));
            cell2.addElement(new Phrase("" + institution, italic));
            tableper2.addCell(cell1);
            tableper2.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Category", normalSizeBold));
            cell2.addElement(new Phrase("" + examinationType, italic));
            tableper2.addCell(cell1);
            tableper2.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Course", normalSizeBold));
            cell2.addElement(new Phrase("" + grade, italic));
            tableper2.addCell(cell1);
            tableper2.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Studied District", normalSizeBold));
            cell2.addElement(new Phrase("" + examinationdistrict, italic));
            tableper2.addCell(cell1);
            tableper2.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            if (!institutionId.toString().equalsIgnoreCase("9999")) {
                cell2.setColspan(3);
            }
            cell1.addElement(new Phrase("Examination Center", normalSizeBold));
            cell2.addElement(new Phrase("" + examinationcenter, italic));
            tableper2.addCell(cell1);
            tableper2.addCell(cell2);

            if (institutionId.toString().equalsIgnoreCase("9999")) {
                cell1 = new PdfPCell();
                cell2 = new PdfPCell();
                cell1.addElement(new Phrase("Attendance", normalSizeBold));
                cell2.addElement(new Phrase("" + attendance, italic));
                tableper2.addCell(cell1);
                tableper2.addCell(cell2);
            }



            doc.add(tableper2);

            doc.add(new Paragraph("\n"));

            Chunk provision3 = new Chunk("COMMUNICATION DETAILS ", subheading);
            Paragraph provision31 = new Paragraph();
            provision31.add(new Phrase(provision3));
            doc.add(provision31);
            doc.add(new Paragraph("\n"));
            PdfPTable tableper3 = new PdfPTable(4);
            tableper3.setLockedWidth(true);
            tableper3.setTotalWidth(530f);
            tableper3.setWidths(new int[]{1, 1, 1, 1});
            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("House No", normalSizeBold));
            cell2.addElement(new Phrase("" + houseno, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Street", normalSizeBold));
            cell2.addElement(new Phrase("" + street, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Village/Town", normalSizeBold));
            cell2.addElement(new Phrase("" + village, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("State", normalSizeBold));
            cell2.addElement(new Phrase("" + state, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);


            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("District", normalSizeBold));
            cell2.addElement(new Phrase("" + district, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Mandal", normalSizeBold));
            cell2.addElement(new Phrase("" + mandal, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Pincode", normalSizeBold));
            cell2.addElement(new Phrase("" + pincode, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Mobile", normalSizeBold));
            cell2.addElement(new Phrase("" + mobile, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("eMail", normalSizeBold));
            cell2.addElement(new Phrase("" + email, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Aadhar", normalSizeBold));
            cell2.addElement(new Phrase("" + aadhar, italic));
            tableper3.addCell(cell1);
            tableper3.addCell(cell2);
            doc.add(tableper3);

            doc.add(new Paragraph("\n"));

            Chunk provision4 = new Chunk("PAYMENT DETAILS ", subheading);
            Paragraph provision41 = new Paragraph();
            provision41.add(new Phrase(provision4));
            doc.add(provision41);
            doc.add(new Paragraph("\n"));
            PdfPTable tableper4 = new PdfPTable(4);
            tableper4.setLockedWidth(true);
            tableper4.setTotalWidth(530f);
            tableper4.setWidths(new int[]{1, 1, 1, 1});
            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Registration No", normalSizeBold));
            cell2.addElement(new Phrase("" + registrationno, italic));
            tableper4.addCell(cell1);
            tableper4.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Payment RefNo", normalSizeBold));
            cell2.addElement(new Phrase("" + paymentrefno, italic));
            tableper4.addCell(cell1);
            tableper4.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Payment Amount", normalSizeBold));
            cell2.addElement(new Phrase("" + paymentamount, italic));
            tableper4.addCell(cell1);
            tableper4.addCell(cell2);

            cell1 = new PdfPCell();
            cell2 = new PdfPCell();
            cell1.addElement(new Phrase("Paymetnt Date", normalSizeBold));
            cell2.addElement(new Phrase("" + paymentdate, italic));
            tableper4.addCell(cell1);
            tableper4.addCell(cell2);
            doc.add(tableper4);

            doc.add(new Paragraph("\n"));
            Chunk provision5 = new Chunk("Print Taken Date: ", subheading1);
            Paragraph provision51 = new Paragraph();
            provision51.add(new Phrase(provision5));
            provision51.setAlignment(Element.ALIGN_RIGHT);
            provision51.add(new Phrase("" + date, italic));
            doc.add(provision51);

            if (doc != null) {
                //close the document
                doc.close();
            }

        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    public HashMap<String, String> getData1(RegisterForm rform) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_Student_Print_PDF(?,?,?)}");
            cstmt.setString(1, rform.getHallticket());
            cstmt.setString(2, rform.getAadhar());
            cstmt.setString(3, rform.getGrade());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                map.put("name", rs.getString(1));
                map.put("fname", rs.getString(2));
                map.put("dob", rs.getString(3));
                map.put("gender", rs.getString(4));
                map.put("institutionId", rs.getString(5));

                map.put("institution", rs.getString(6));
                map.put("examinationAppearing", rs.getString(7));
                map.put("courseCode", rs.getString(8));
                map.put("courseName", rs.getString(9));
                map.put("examinationDistrict", rs.getString(10));

                map.put("examinationCenter", rs.getString(11));
                map.put("hNo", rs.getString(12));
                map.put("street", rs.getString(13));
                map.put("state", rs.getString(15));
                map.put("village", rs.getString(14));

                map.put("district", rs.getString(16));
                map.put("mandal", rs.getString(17));
                map.put("pincode", rs.getString(18));
                map.put("mobile", rs.getString(19));
                map.put("email", rs.getString(20));

                map.put("aadhar", rs.getString(21));

                
                map.put("paymentRefNo", rs.getString(25));
                map.put("paymentAmount", rs.getString(26));
                map.put("paymentDate", rs.getString(27));
                map.put("attendance", rs.getString(29));
                map.put("RegNo", rs.getString(30));
                
                map.put("filepath", MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rform.getGrade().trim() + "\\" + rs.getString(21) + "\\" + rs.getString(22));
                map.put("sigfilepath", MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rform.getGrade().trim() + "\\" + rs.getString(21) + "\\" + rs.getString(23));
                map.put("date", new Date().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }

    public HashMap<String, String> getDataForPaymentDob(RegisterForm rform, HttpServletRequest request, String uname) throws Exception {
        String query = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        String dob = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Get_CCIC(?,?,?)}");
            cstmt.setString(1, rform.getAadhar().trim());
            cstmt.setString(2, rform.getCourse().trim());
            cstmt.setString(3, uname);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("gender", rs.getString(2));
                request.setAttribute("caste", rs.getString(3));
                request.setAttribute("aadhaar", "XXXXXXXX" + rs.getString(6).substring(8, 12));
                request.setAttribute("mobile", rs.getString(3));
                request.setAttribute("email", rs.getString(4));
                request.setAttribute("amount", rs.getDouble(5));
                request.setAttribute("aadhar", rs.getString(6));
                request.setAttribute("course", rs.getString(7));
                map.put("child", "9866747477");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }
}
