/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.log4j.Logger;

/**
 *
 * @author APTOL301655
 */
public class BundleInvigilatorDAO {

    private static final Logger logger = Logger.getLogger(BundleInvigilatorDAO.class);

   
    public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select Grade,GRADE_DESC from TWSH_GRADE_MASTER with(nolock) where rowid not in('22','23','10','11') order by RowID asc";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }
    public ArrayList getInvgList(String uname) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select * from TWSH_Invigilator_Registration with(nolock) where username=? order by Created_date desc";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,uname);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("bname", rs.getString(1));
                map.put("mobile", rs.getString(2));
                map.put("email", rs.getString(3));
                map.put("grade", rs.getString(4));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public String submitDetails(HttpServletRequest req, String username, RegisterForm rForm) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap<String, String> map = new HashMap<String, String>();
        String result="";
        try {
            String sytemIp = req.getRemoteAddr();
            String grade=rForm.getGradename();
            String grade1=grade.replace(",", "~");
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Invigilator_Registration(?,?,?,?,?,?)}");
            cstmt.setString(1, rForm.getFname());
            cstmt.setString(2, rForm.getMobile());
            cstmt.setString(3, rForm.getEmail());
            cstmt.setString(4, grade1);
            cstmt.setString(5, username);
            cstmt.setString(6, sytemIp);
            rs = cstmt.executeQuery();
            if (rs != null && rs.next()) {
               result=rs.getString(1);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                logger.info("e=====================" + e.getMessage());
                e.printStackTrace();
            }
        }
        return result;
    }
    public String deleteDetails(RegisterForm rForm,HttpServletRequest req, String username ) {

        Connection con = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        HashMap<String, String> map = new HashMap<String, String>();
        String result="";
        try {
            String sytemIp = req.getRemoteAddr();
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareStatement("{Call Usp_Invigilator_delete(?,?)}");
            cstmt.setString(1, rForm.getCoursename());
            cstmt.setString(2, rForm.getGradename());
            rs = cstmt.executeQuery();
            if (rs != null && rs.next()) {
               result=rs.getString(1);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                logger.info("e=====================" + e.getMessage());
                e.printStackTrace();
            }
        }
        return result;
    }
     public int  getDetailsStatus(String grade,String  mobile) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        PreparedStatement cstmt = null;
        HashMap map = null;
        int count=0;
        try {
            con = DatabaseConnection.getConnection();
            String[] gradeslist=grade.split(",");
            StringBuilder sb=new StringBuilder();
            for(int i=0;i<gradeslist.length;i++){
                sb.append("'"+gradeslist[i]+"',");
            }
            String finalgrade=sb.toString();
            System.out.println("select count(*) from TWSH_Invigilator_Registration with(nolock) where invgilatormobile='"+mobile+"' and grade in("+finalgrade.substring(0,finalgrade.length()-1)+")");
            cstmt = con.prepareStatement("select count(*) from TWSH_Invigilator_Registration with(nolock) where invgilatormobile=? and grade in("+finalgrade.substring(0,finalgrade.length()-1)+")");
            cstmt.setString(1, mobile);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                   count=rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("count==============================="+count);
        return count;
    }

}
