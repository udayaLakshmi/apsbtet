/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.util.Map;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.text.SimpleDateFormat;
import net.apo.angrau.forms.RegisterForm;
import net.apo.angrau.db.DatabaseConnection;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.action.RegisterFileUpload;

/**
 *
 * @author APTOL301294
 */
public class AdminEditDao {

    public List<HashMap> getCandidateList() throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_Admin_Edit_Report}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("name", rs.getString(1));
                    Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(2));
                    String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                    map.put("dob", dob);
                    map.put("name", rs.getString(1));
                    map.put("mobile", rs.getString(3));
                    map.put("caste", rs.getString(4));
                    map.put("examination", rs.getString(5));
                    map.put("grade", rs.getString(6));
                    map.put("regno", rs.getString(7));
                    map.put("aadhar", rs.getString(8));
                    map.put("gender", rs.getString(11));
                    map.put("fname", rs.getString(12));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    public List<HashMap> getCandidatePendingList() throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_EditConfirmation_Status_Report}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(2));
                    String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                    map.put("dob", dob);
                    map.put("name", rs.getString(1));
                    map.put("mobile", rs.getString(3));
                    map.put("caste", rs.getString(4));
                    map.put("examination", rs.getString(5));
                    map.put("grade", rs.getString(6));
                    map.put("regno", rs.getString(7));
                    map.put("aadhar", rs.getString(8));
                    map.put("gender", rs.getString(9));
                    map.put("fname", rs.getString(10));
                    map.put("payStatus", rs.getString(11));
                    map.put("EditStatus", rs.getString(12));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }


    public ArrayList getcenterDistDetails(String examination) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            if (examination.equals("TW")) {
                query = "select DISTCD,DISTNAME from  STEPS_DISTRICT with(nolock) where DISTCD in(select DISTCD from  TWSH_CENTER_MASTER with(nolock) where TW_COURSE='Y') order by DISTCD";
            } else if (examination.equals("SH")) {
                query = "select DISTCD,DISTNAME from  STEPS_DISTRICT with(nolock) where DISTCD in(select DISTCD from  TWSH_CENTER_MASTER with(nolock) where  SH_COURSE='Y') order by DISTCD";
            }
            pstmt = con.prepareStatement(query);
//          pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public ArrayList getStateDetails() throws Exception {

        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select * from StateMaster order by StateName";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("scode", rs.getString(2));
                    map.put("sname", rs.getString(2));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_Admin_Edit_Get_Data(?,?,?)}");
            cstmt.setString(1, rform.getHallticket().trim());
            cstmt.setString(2, rform.getAadhar().trim());
            cstmt.setString(3, rform.getGrade().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));

                request.setAttribute("blind", rs.getString(6));
                request.setAttribute("institution", rs.getString(7));
                request.setAttribute("examinationname", rs.getString(8));
                request.setAttribute("language", rs.getString(9));
                request.setAttribute("gradename", rs.getString(10));
                request.setAttribute("apdistrict1", rs.getString(11));

                request.setAttribute("ecenter1", rs.getString(12));
                request.setAttribute("edate1", rs.getString(13));
                request.setAttribute("ebatch1", rs.getString(14));
                request.setAttribute("houseno", rs.getString(15));
                request.setAttribute("street", rs.getString(16));

                request.setAttribute("village", rs.getString(17));
                request.setAttribute("state", rs.getString(18));
                request.setAttribute("district1", rs.getString(19));
                request.setAttribute("mandal1", rs.getString(20));
                request.setAttribute("pincode", rs.getString(21));

                request.setAttribute("mobile", rs.getString(22));
                request.setAttribute("email", rs.getString(23));
                request.setAttribute("aadhar", rs.getString(24));
                request.setAttribute("regNo", rs.getString(26));
                request.setAttribute("grade", rs.getString(32));
                request.setAttribute("examination", rs.getString(33));
                request.setAttribute("district", rs.getString(34));
                request.setAttribute("mandal", rs.getString(35));
                request.setAttribute("apdistrict", rs.getString(36));
                request.setAttribute("ecenter", rs.getString(37));
                request.setAttribute("edate", rs.getString(38));
                request.setAttribute("ebatch", rs.getString(39));

                Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(31));
                String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                request.setAttribute("dob", dob);
                request.setAttribute("date", new Date());

                if (!rs.getString(25).equalsIgnoreCase("")) {
                    request.setAttribute("photoDB", rs.getString(25));
                }

                if (!rs.getString(40).equalsIgnoreCase("")) {
                    request.setAttribute("signatureDB", rs.getString(40));
                }

                if (!rs.getString(41).equalsIgnoreCase("")) {
                    request.setAttribute("upload1DB", rs.getString(41));
                }

                if (!rs.getString(42).equalsIgnoreCase("")) {
                    request.setAttribute("upload2DB", rs.getString(42));
                }

                if (!rs.getString(43).equalsIgnoreCase("")) {
                    request.setAttribute("upload3DB", rs.getString(43));
                }

                if (!rs.getString(44).equalsIgnoreCase("")) {
                    request.setAttribute("upload4DB", rs.getString(44));
                }

                if (!rs.getString(45).equalsIgnoreCase("")) {
                    request.setAttribute("blindfileDB", rs.getString(45));
                }

                if (!rs.getString(45).equalsIgnoreCase("")) {
                    request.setAttribute("blindfileDB", rs.getString(45));
                }

                request.setAttribute("sadaramNo", rs.getString(46));
                request.setAttribute("oldRegNo", rs.getString(47));
                request.setAttribute("hallticketFile", rs.getString(48));
                //New 
                request.setAttribute("sschallticket", rs.getString(49));
                request.setAttribute("sscflag", rs.getString(50));
                request.setAttribute("interhallticket", rs.getString(51));
                request.setAttribute("intermonth", rs.getString(52));
                request.setAttribute("interyear", rs.getString(53));
                request.setAttribute("interflag", rs.getString(54));
                request.setAttribute("blindflag", rs.getString(55));
                request.setAttribute("lowergradehallticket", rs.getString(56));
                request.setAttribute("lowergradeflag", rs.getString(57));
                
                
              

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }

    public String getUpdateCandidateInfo(RegisterForm rform, String remoteAddress, String uname) {
        Connection con = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        String result = "";

        String fileName1 = "";
        String fileName2 = "";
        String fileName3 = "";
        String fileName4 = "";
        String fileName5 = "";
        String fileName6 = "";
        String fileName7 = "";
        String fileName8 = "";

        RegisterFileUpload rf = new RegisterFileUpload();
        boolean flag = false;


        String hallticket = rform.getAadhar();
        String grade = rform.getGrade();
        if (rform.getPhotoNew().equalsIgnoreCase("Y")) {
            if (rform.getPhoto() != null && rform.getPhoto().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getPhoto(), hallticket + "_PHOTO_EDIT_SA.jpg", hallticket, grade);
                if (flag == true) {
                    fileName1 = hallticket + "_PHOTO_EDIT_SA.jpg";
                }
            } else {
                fileName1 = rform.getPhotoNew1();
            }
        } else {
            fileName1 = rform.getPhotoNew1();
        }
        if (rform.getSignatureNew().equalsIgnoreCase("Y")) {
            if (rform.getSignature() != null && rform.getSignature().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getSignature(), hallticket + "_SIGNATURE_EDIT_SA.jpg", hallticket, grade);
                if (flag == true) {
                    fileName3 = hallticket + "_SIGNATURE_EDIT_SA.jpg";
                }
            } else {
                fileName3 = rform.getSignatureNew1();
            }
        } else {
            fileName3 = rform.getSignatureNew1();
        }
        if (rform.getBlind().equalsIgnoreCase("YES")) {
            if (rform.getBlindfileNew().equalsIgnoreCase("Y")) {
                if (rform.getBlindfile() != null && rform.getBlindfile().toString().length() > 0) {
                    flag = rf.uploadDocs(rform.getBlindfile(), hallticket + "_BLIND_EDIT_SA.jpg", hallticket, grade);
                    if (flag == true) {
                        fileName2 = hallticket +"_BLIND_EDIT_SA.jpg";
                    }
                }
            } else {
                fileName2 = rform.getBlindfileNew1();
            }
        }
        if (rform.getUpload1New().equalsIgnoreCase("Y")) {

            if (rform.getUpload1() != null && rform.getUpload1().toString().length() > 0) {

                if (rform.getGrade().equalsIgnoreCase("TTL") || rform.getGrade().equalsIgnoreCase("TEL")
                        || rform.getGrade().equalsIgnoreCase("THL")
                        || rform.getGrade().equalsIgnoreCase("TUL")
                        || rform.getGrade().equalsIgnoreCase("STL")
                        || rform.getGrade().equalsIgnoreCase("SEL")
                        || rform.getGrade().equalsIgnoreCase("SUL")) {
                    fileName4 = hallticket + "_SSC_EDIT_SA.jpg";
                } else if (rform.getGrade().equalsIgnoreCase("TTHS")
                        || rform.getGrade().equalsIgnoreCase("TEHS")
                        || rform.getGrade().equalsIgnoreCase("SEHS150")
                        || rform.getGrade().equalsIgnoreCase("SEHS180")
                        || rform.getGrade().equalsIgnoreCase("SEHS200")
                        || rform.getGrade().equalsIgnoreCase("STHS80")
                        || rform.getGrade().equalsIgnoreCase("STHS100")) {
                    fileName4 = hallticket + "_HIGHERGRADE_EDIT_SA.jpg";
                } else if (rform.getGrade().equalsIgnoreCase("TEJ")) {
                    fileName4 = hallticket + "_VII_EDIT_SA.jpg";
                } else {
                    fileName4 = hallticket + "_UPLOAD1_EDIT_SA.jpg";
                }
                flag = rf.uploadDocs(rform.getUpload1(), fileName4, hallticket, grade);
                if (flag == false) {
                    fileName4 = rform.getUpload1New1();
                }
            } else {
                fileName4 = rform.getUpload1New1();
            }
        } else {
            fileName4 = rform.getUpload1New1();
        }

        if (rform.getUpload2New().equalsIgnoreCase("Y")) {
            if (rform.getUpload2() != null && rform.getUpload2().toString().length() > 0) {

                if (rform.getGrade().equalsIgnoreCase("TTH")
                        || rform.getGrade().equalsIgnoreCase("TEH")
                        || rform.getGrade().equalsIgnoreCase("TUH")
                        || rform.getGrade().equalsIgnoreCase("THH")
                        || rform.getGrade().equalsIgnoreCase("STH")
                        || rform.getGrade().equalsIgnoreCase("SUH")
                        || rform.getGrade().equalsIgnoreCase("SEI")
                        || rform.getGrade().equalsIgnoreCase("SEH")) {
                    fileName5 = hallticket + "_LOWERGRADE_EDIT_SA.jpg";
                } else {
                    fileName5 = hallticket + "_UPLOAD2_EDIT_SA.jpg";
                }

                flag = rf.uploadDocs(rform.getUpload2(), fileName5, hallticket, grade);
                if (flag == false) {
                    fileName5 = rform.getUpload2New1();
                }
            } else {
                fileName5 = rform.getUpload2New1();
            }
        } else {
            fileName5 = rform.getUpload2New1();
        }
        if (rform.getUpload3New().equalsIgnoreCase("Y")) {
            if (rform.getUpload3() != null && rform.getUpload3().toString().length() > 0) {

                if (rform.getGrade().equalsIgnoreCase("TTH")
                        || rform.getGrade().equalsIgnoreCase("TEH")
                        || rform.getGrade().equalsIgnoreCase("TUH")
                        || rform.getGrade().equalsIgnoreCase("THH")) {
                    fileName6 = hallticket + "_SSC_EDIT_SA.jpg";
                } else if (rform.getGrade().equalsIgnoreCase("STH")
                        || rform.getGrade().equalsIgnoreCase("SUH")
                        || rform.getGrade().equalsIgnoreCase("SEI")
                        || rform.getGrade().equalsIgnoreCase("SEH")) {
                    fileName6 = hallticket + "_GRADUATION_EDIT_SA.jpg";

                } else {
                    fileName6 = hallticket + "_UPLOAD3_EDIT_SA.jpg";
                }
                flag = rf.uploadDocs(rform.getUpload3(), fileName6, hallticket, grade);
                if (flag == false) {
                    fileName6 = rform.getUpload3New1();
                }
            } else {
                fileName6 = rform.getUpload3New1();
            }
        } else {
            fileName6 = rform.getUpload3New1();
        }
        if (rform.getUpload4New().equalsIgnoreCase("Y")) {

            if (rform.getUpload4() != null && rform.getUpload4().toString().length() > 0) {
                if (rform.getGrade().equalsIgnoreCase("TTH")
                        || rform.getGrade().equalsIgnoreCase("TEH")
                        || rform.getGrade().equalsIgnoreCase("TUH")
                        || rform.getGrade().equalsIgnoreCase("THH")) {
                    fileName7 = hallticket + "_INTERMEDIATE_EDIT_SA.jpg";
                } else if (rform.getGrade().equalsIgnoreCase("SEH")) {
                    fileName7 = hallticket + "_VOCATIONAL_EDIT_SA.jpg";
                } else {
                    fileName7 = hallticket + "_UPLOAD4_EDIT_SA.jpg";
                }
                flag = rf.uploadDocs(rform.getUpload4(), fileName7, hallticket, grade);
                if (flag == false) {
                    fileName7 = rform.getUpload4New1();
                }
            } else {
                fileName7 = rform.getUpload4New1();
            }
        } else {
            fileName7 = rform.getUpload4New1();
        }
//        if (rform.getUpload5New().equalsIgnoreCase("Y")) {
//            if (rform.getUpload5() != null && rform.getUpload5().toString().length() > 0) {
//                flag = rf.uploadDocs(rform.getUpload5(), hallticket + "_Hallticket1.jpg", hallticket, grade);
//                if (flag == true) {
//                    fileName8 = hallticket + "_Hallticket1.jpg";
//                }
//            }
//        }else {
//            fileName8 = rform.getUpload5New1();
//        }
        fileName8 = rform.getUpload5New1();
        try {
            Date todate2 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate2);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_TWSH_Registration_Edit_Admin(?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?)}");
            cstmt.setString(1, rform.getBname());
            cstmt.setString(2, rform.getFname());
            cstmt.setString(3, dob);
            cstmt.setString(4, rform.getGender());
            cstmt.setString(5, rform.getCaste());
            cstmt.setString(6, rform.getBlind());
            cstmt.setString(7, fileName2);
            cstmt.setString(8, rform.getInstitute());
            cstmt.setString(9, rform.getExamination());
            cstmt.setString(10, rform.getLanguage());
            cstmt.setString(11, rform.getGrade());
            cstmt.setString(12, rform.getApdistrict());
            cstmt.setString(13, rform.getEcenter());
            cstmt.setString(14, rform.getEdate());
            cstmt.setString(15, rform.getEbatch());
            cstmt.setString(16, rform.getHouseno());
            cstmt.setString(17, rform.getLocality());
            cstmt.setString(18, rform.getVillage());
            cstmt.setString(19, rform.getState());
            if ((rform.getState().equalsIgnoreCase("Andra Pradesh")) || (rform.getState().equalsIgnoreCase("Telagana"))) {
                cstmt.setString(20, rform.getDistrict());
                cstmt.setString(21, rform.getMandal());
            } else {
                cstmt.setString(20, rform.getDistrict1());
                cstmt.setString(21, rform.getMandal1());
            }
            cstmt.setString(22, rform.getPincode());
            cstmt.setString(23, rform.getMobile());
            cstmt.setString(24, rform.getEmail());
            cstmt.setString(25, rform.getAadhar());
            cstmt.setString(26, fileName1);
            cstmt.setString(27, remoteAddress);
            cstmt.setString(28, uname);
            cstmt.setString(29, fileName3);
            cstmt.setString(30, fileName4);
            cstmt.setString(31, fileName5);
            cstmt.setString(32, fileName6);
            cstmt.setString(33, fileName7);
            cstmt.setString(34, rform.getRank());//Old Reg
            cstmt.setString(35, fileName8);
            cstmt.setString(36, rform.getSmgt()); //sadaramNo
            //new fields
            cstmt.setString(37, rform.getSschallticket());
            cstmt.setString(38, rform.getSscflag());
            cstmt.setString(39, rform.getInterhallticket());
            cstmt.setString(40, rform.getIntermonth());
            cstmt.setString(41, rform.getInteryear());
            cstmt.setString(42, rform.getInterflag());
            cstmt.setString(43, rform.getBlindflag());
            cstmt.setString(44, rform.getSports());//Reg No
            cstmt.setString(45, rform.getNcc());//lowergradehallticket
            cstmt.setString(46, rform.getCwsn());//lowergradeflag
            cstmt.setString(47, "editProfile.do");
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
}