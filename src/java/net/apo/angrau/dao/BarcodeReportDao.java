/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;
import com.ap.DAO.*;
import com.ap.Form.RegisterForm;
import com.ap.Form.RegisterFormPhD;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author APTOL301655
 */
public class BarcodeReportDao {
   public List<HashMap> getCandidateReport(String type, String click) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int totalbookletstotal = 0, scannedtotal = 0, mismatchtotal = 0,  tobescantotal = 0;
        try {
//            Date date = new Date();
//            String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_BookletScanningStatusReport}");
//            cstmt.setString(1, modifiedDate);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("grade", rs.getString(1));
                    map.put("batch", rs.getString(2));
                    map.put("paper", rs.getString(3));
                    map.put("totalbooklets", rs.getString(4));
                    map.put("scanned", rs.getString(5));
                    map.put("mismatch", rs.getString(6));
                    map.put("tobescan", rs.getString(7));
                    totalbookletstotal = totalbookletstotal + Integer.parseInt(rs.getString(4));
                    scannedtotal = scannedtotal + Integer.parseInt(rs.getString(5));
                    mismatchtotal = mismatchtotal + Integer.parseInt(rs.getString(6));
                    tobescantotal = tobescantotal + Integer.parseInt(rs.getString(7));
                    map.put("totalbookletstotal", totalbookletstotal);
                    map.put("scannedtotal", scannedtotal);
                    map.put("mismatchtotal", mismatchtotal);
                    map.put("tobescantotal", tobescantotal);
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
}
