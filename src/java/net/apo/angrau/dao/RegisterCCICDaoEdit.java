/*
 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.sms.util.MessagePropertiesUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;
import net.apo.angrau.action.RegisterFileUpload;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author APTOL301294
 */
public class RegisterCCICDaoEdit {

    private static final Logger logger = Logger.getLogger(RegisterCCICDaoEdit.class);
public List<HashMap> getCandidateList(String uname) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_CollegeWise_CandidateList_For_Edit(?)}");
            cstmt.setString(1, uname);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("name", rs.getString(2));
                    Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(3));
                    String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                    map.put("dob", dob);
                    map.put("mobile", rs.getString(4));
                    map.put("examination", rs.getString(5));
                    map.put("grade", rs.getString(6));
                    map.put("regno", rs.getString(7));
                    map.put("aadhar", rs.getString(8));
                    map.put("sdate", rs.getString(9));
                    map.put("sstatus", rs.getString(10));
                    map.put("category", rs.getString(11));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getMasterData(String course, String hallticket, String instCode) throws Exception {
        String query = null;
        HashMap map = null;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement st = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            query = "select * from CCIC_ApplicantHallTicketDetails where  CourseCode=? and HallTicketNO=? and Institutioncode=? ";
            st = con.prepareStatement(query);
            st.setString(1, course);
            st.setString(2, hallticket);
            st.setString(3, instCode);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("instiCode", rs.getString(3));
                    map.put("hallticket", rs.getString(4));
                    map.put("appNo", rs.getString(5));
                    map.put("name", rs.getString(6));
                    map.put("fname", rs.getString(7));
                    map.put("mobile", rs.getString(9));
                    map.put("courseCode", rs.getString(8));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public HashMap getCheckCourseStatus(String courseId, String aadhar, String loginInstituteCode) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_CheckCourse_Status(?,?,?)}");
            cstmt.setString(1, courseId);
            cstmt.setString(2, aadhar);
            cstmt.setString(3, loginInstituteCode);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("key", rs.getString(1));
                map.put("value", rs.getString(2));
                map.put("fileFlag", rs.getString(3));
                map.put("fileFlagDesc", rs.getString(4));
//                list.add(map);
//                map = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }

    public String checkCourseInBacklogList(String courseId, String aadhar, String InstiCode,String oldHallticket) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        String status="";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_CheckCourse_InBackLog(?,?,?,?)}");
            cstmt.setString(1, courseId);
            cstmt.setString(2, aadhar);
            cstmt.setString(3, InstiCode);
            cstmt.setString(4, oldHallticket);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
              status=rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return status;
    }

    public ArrayList getSubjectsByCourses(String distCode, String instCode, String backlogCourseId, String oldHallTicketNo) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_BacklogSubjectDetails_Get(?,?,?,?)}");
            cstmt.setString(1, distCode);
            cstmt.setString(2, instCode);
            cstmt.setString(3, backlogCourseId);
            cstmt.setString(4, oldHallTicketNo);
            //System.out.println("EXEC CCIC_BacklogSubjectDetails_Get '"+distCode+"','"+instCode+"','"+backlogCourseId+"','"+oldHallTicketNo+"'");
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("sub_Id", rs.getString(1));
                map.put("sub_Name", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public ArrayList getInstituteByDistrict(String distCode) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_DistrictInstitutionDetails_Get (?)}");
            cstmt.setString(1, distCode);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("inst_Id", rs.getString(1));
                map.put("inst_Name", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public ArrayList getCoursesByInstCodeAndHallticket(String distCode, String instCode, String hallticket) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_BacklogCoursesDetails_Get(?,?,?)}");
            cstmt.setString(1, distCode);
            cstmt.setString(2, instCode);
            cstmt.setString(3, hallticket);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("course_Id", rs.getString(1));
                map.put("course_Name", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public ArrayList getCoursesByInstCode(String distCode, String instCode) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_DistrictInstitutionCourseDetails_Get(?,?)}");
            cstmt.setString(1, distCode);
            cstmt.setString(2, instCode);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("course_Id", rs.getString(2));
                map.put("course_Name", rs.getString(3));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public ArrayList getCoursesByInstCodeForBacklog(String distCode, String instCode) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_DistrictInstitutionCourseDetails_Get(?,?)}");
            cstmt.setString(1, distCode);
            cstmt.setString(2, instCode);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("course_Id", rs.getString(2));
                map.put("course_Name", rs.getString(3));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        RegisterForm regform = null;
        CallableStatement cstmt = null;
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Edit_TWSH (?)}");
            cstmt.setString(1, rform.getHallticket().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1).trim());
                request.setAttribute("fname", rs.getString(2).trim());
                request.setAttribute("dob", rs.getString(3).trim());
                request.setAttribute("gender", rs.getString(6).trim());
                request.setAttribute("mobile", rs.getString(5).trim());
                request.setAttribute("email", rs.getString(4).trim());
                request.setAttribute("grade", rs.getString(7).trim());
                request.setAttribute("examination", rs.getString(8).trim());
                request.setAttribute("language", rs.getString(9).trim());
                request.setAttribute("examinationname", rs.getString(10).trim());
                request.setAttribute("gradename", rs.getString(11).trim());
                request.setAttribute("hallticket", rform.getHallticket().trim());
                request.setAttribute("aadhar", rform.getAadhar2().trim());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }

    public HashMap<String, String> getDataForPayment(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            Date todate1 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate1);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_student_Get (?,?)}");
            cstmt.setString(1, rform.getHallticket().trim());
            cstmt.setString(2, dob);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));
                request.setAttribute("aadhaar", "XXXXXXXX" + rs.getString(6).substring(8, 12));
                request.setAttribute("mobile", rs.getString(9));
                request.setAttribute("ph", rs.getString(55));
                request.setAttribute("amount", rs.getDouble(52));
                request.setAttribute("sschallticket", rs.getString(16));
                request.setAttribute("hallticket", rs.getString(16));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }

    public int getStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        int result = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_HallticketStatus (?,?,?,?)}");
            cstmt.setString(1, rform.getAadhar().trim());
            cstmt.setString(2, rform.getGrade().trim());
            cstmt.setString(3, rform.getExamination().trim());
            cstmt.setString(4, rform.getLanguage().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return result;
    }

    public String getvalidatingOTP(String aadharNum, String mobile) throws Exception {

        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String otp = "";

        try {
            con = DatabaseConnection.getConnection();
            query = "select OTP  from OTP_Details where Mobile_Num='" + mobile + "' and Aadhar_Id='" + aadharNum + "'";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    otp = rs.getString(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return otp;
    }

    public int SaveOTP(String aadharNum, String mobile, String otp, String systemIp) throws Exception {
        int count = 0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call proc_OTP_Details_Insert(?,?,?,?)}");
            cstmt.setString(1, aadharNum);
            cstmt.setString(2, mobile);
            cstmt.setString(3, otp);
            cstmt.setString(4, systemIp);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

  
    public String updateData(RegisterForm rform, String remoteAddress) {
        Connection con = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        String result = "";

        String fileName1 = "";
        String fileName2 = "";
        String fileName3 = "";
        String fileName4 = "";
        String fileName5= "";
        String fileName6= "";
        String fileName7= "";
        String fileName8= "";
        String subjectCode = "0";
        boolean flag = false;
        String hallticket = rform.getAadhar();
        String course = rform.getCourse(); //Course Code

        if (rform.getCategory().equalsIgnoreCase("Backlog")) {
            subjectCode = rform.getBlind();
        } else {
            subjectCode = "0";
        }

        if (rform.getPhoto() != null && rform.getPhoto().toString().length() > 0) {
            flag = uploadDocs(rform.getPhoto(), hallticket + "_PHOTO.jpg", hallticket, course);
            if (flag == true) {
                fileName3 = hallticket + "_PHOTO.jpg";
            }
        }
        if (rform.getSignature() != null && rform.getSignature().toString().length() > 0) {
            flag = uploadDocs(rform.getSignature(), hallticket + "_SIGNATURE.jpg", hallticket, course);
            if (flag == true) {
                fileName2 = hallticket + "_SIGNATURE.jpg";
            }
        }
        if (rform.getUpload1() != null && rform.getUpload1().toString().length() > 0) {
            if (rform.getFileFlag().equals("2")) {
                fileName5 = hallticket + "_SSC_PASS_FAIL_EQUAL.jpg";
            }else if (rform.getFileFlag().equals("3")) {
                fileName5 = hallticket + "_SSC.jpg";
            } else if (rform.getFileFlag().equals("4") && course.equals("AC")) {
                fileName5 = hallticket + "_INTER_OR_EQUAL.jpg";
            } else if (rform.getFileFlag().equals("4")) {
                fileName5 = hallticket + "_INTER.jpg";
            } else if (rform.getFileFlag().equals("5")) {
                fileName5 = hallticket + "_DEGREE_OR_DIPLOMA_OR_ENGINEERING.jpg";
            }else if (rform.getFileFlag().equals("6")) {
                fileName5 = hallticket + "_YOGA.jpg";
            } else {
                fileName5 = hallticket + "_UPLOAD1.jpg";
            }
            flag = uploadDocs(rform.getUpload1(), fileName5, hallticket, course);
            if (flag == true) {
                fileName1 = fileName5;
            }
        }
        String isqualification="";
        if (course.equals("IS")) {
            isqualification = rform.getQualification();
            if (rform.getUpload2() != null && rform.getUpload2().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload2(), hallticket + "_EXPERIENCE1.jpg", hallticket, course);
                if (flag == true) {
                    fileName4 = hallticket + "_EXPERIENCE1.jpg";
                }
            }
            if (rform.getUpload3() != null && rform.getUpload3().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload3(), hallticket + "_EXPERIENCE2.jpg", hallticket, course);
                if (flag == true) {
                    fileName6 = hallticket + "_EXPERIENCE2.jpg";
                }
            }
            if (rform.getUpload4() != null && rform.getUpload4().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload4(), hallticket + "_EXPERIENCE3.jpg", hallticket, course);
                if (flag == true) {
                    fileName7 = hallticket + "_EXPERIENCE3.jpg";
                }
            }
            if (rform.getUpload5() != null && rform.getUpload5().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload5(), hallticket + "_EXPERIENCE4.jpg", hallticket, course);
                if (flag == true) {
                    fileName8 = hallticket + "_EXPERIENCE4.jpg";
                }
            }
        }

        try {
            Date todate2 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate2);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_ApplicantRegistrationDetails_Insert(?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, rform.getBname());
            cstmt.setString(2, rform.getFname());
            cstmt.setString(3, dob);
            cstmt.setString(4, rform.getGender());
            cstmt.setString(5, rform.getAadhar());
            cstmt.setString(6, rform.getCategory());
            cstmt.setString(7, rform.getApdistrict());
            cstmt.setString(8, rform.getInstitutionCode());
            cstmt.setString(9, course);
            cstmt.setString(10, subjectCode);
            cstmt.setString(11, rform.getEcenter());
            cstmt.setString(12, fileName1);
            cstmt.setString(13, rform.getHouseno());
            cstmt.setString(14, rform.getLocality());
            cstmt.setString(15, rform.getVillage());
            cstmt.setString(16, rform.getState());
            if ((rform.getState().equalsIgnoreCase("Andra Pradesh")) || (rform.getState().equalsIgnoreCase("Telagana"))) {
                cstmt.setString(17, rform.getDistrict());
                cstmt.setString(18, rform.getMandal());
            } else {
                cstmt.setString(17, rform.getDistrict1());
                cstmt.setString(18, rform.getMandal1());
            }
            cstmt.setString(19, rform.getPincode());
            cstmt.setString(20, rform.getMobile());
            cstmt.setString(21, rform.getEmail());
            cstmt.setString(22, fileName2);
            cstmt.setString(23, fileName3);
            cstmt.setString(24, rform.getGrade2());//Old hallticket
            cstmt.setString(25, remoteAddress);
            cstmt.setString(26, rform.getUserName());
            cstmt.setString(27, fileName4);
            cstmt.setString(28, rform.getSschallticket());
            cstmt.setString(29, rform.getSscyear());
            cstmt.setString(30, rform.getSscflag());
            cstmt.setString(31, rform.getInterhallticket());
            cstmt.setString(32, rform.getIntermonth());
            cstmt.setString(33, rform.getInteryear());
            cstmt.setString(34, rform.getInterflag());
            cstmt.setString(35, rform.getAttendance());
            
            cstmt.setString(36, isqualification);
            cstmt.setString(37, fileName6);
            cstmt.setString(38, fileName7);
            cstmt.setString(39, fileName8);
            
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getString(1);
            }
        } catch (Exception ex) {
            logger.info("Error================" + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public ArrayList getDataForPaymentSuccess(RegisterForm rform, String uname) {

        Connection con = null;
        PreparedStatement pstmt = null;
        String query = "";
        ArrayList list = new ArrayList();
        ResultSet rs = null;
        Map map = null;
        CallableStatement stmt = null;
        try {
            con = DatabaseConnection.getConnection();
//            query = "select  Request_Id,App_Id,PGRefNo,Base_amount,Convience_Charges,GST,Customer_Name from MSc_PaymentDetails (nolock) where Request_Id = '"+reqId+"' ";
//                System.out.println("query - query"+query);
//            pstmt = con.prepareStatement(query);
//           
//             rs = pstmt.executeQuery();

            stmt = con.prepareCall("{call Usp_PaymentStatus_CCIC(?,?,?,?)}");
            stmt.setString(1, "2");
            stmt.setString(2, rform.getAadhar());
            stmt.setString(3, rform.getCourse());
            stmt.setString(4, uname);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {

                    map = new HashMap<String, String>();
                    map.put("customerName", rs.getString(1));
                    map.put("gender", rs.getString(2));
                    map.put("caste", rs.getString(3));
                    map.put("dob", rs.getString(4));
                    map.put("mobile", rs.getString(5));
                    map.put("ph", rs.getString(6));
                    map.put("serviceCharges", rs.getString(7));
                    map.put("PgRefNo", rs.getString(8));
                    map.put("gst", rs.getString(9));
                    map.put("baseAmt", rs.getString(10));
                    map.put("reqId", rs.getString(11));
                    map.put("aadhar", "XXXXXXXX" + rs.getString(12).substring(8, 12));
                    list.add(map);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("list=============" + list);
        return list;
    }
//      public int getvalidatingAadharNum(String aadharNum) throws Exception {
//
//        String query = null;
//        PreparedStatement st = null;
//        Connection con = null;
//        ResultSet rs = null;
//        int count = 0;
//        CallableStatement stmt = null;
//        try {
//            con = DatabaseConnection.getConnection();
//            stmt = con.prepareCall("{call Usp_AadhaarStatusCheck(?)}");
//            stmt.setString(1, aadharNum);
//            rs = stmt.executeQuery();
//            if (rs != null) {
//                while (rs.next()) {
//                    count = rs.getInt(1);
//                }
//            }
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        } finally {
//            try {
//                if (rs != null) {
//                    rs.close();
//                }
//                if (stmt != null) {
//                    stmt.close();
//                }
//                if (con != null) {
//                    con.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return count;
//    }

    public ArrayList getDistrictDetails() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_DISTRICT with(nolock)";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public ArrayList getMandalDetails(String district) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_BLOCK with(nolock) where LEFT(BLKCD,4)=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("mandal_ID", rs.getString(1));
                map.put("mandal_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public ArrayList getcenterDetails(String district) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_MasterExaminationCenters_Get(?)}");
            cstmt.setString(1, district);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("ccode", rs.getString(1));
                map.put("cname", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public ArrayList getcenterDistDetails(String examination) throws Exception {
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
//            if (examination.equals("TW")) {
//                query = "select DISTCD,DISTNAME from  STEPS_DISTRICT with(nolock) where DISTCD in(select DISTCD from  TWSH_CENTER_MASTER with(nolock) where TW_COURSE='Y') order by DISTCD";
//            } else if (examination.equals("SH")) {
//                query = "select DISTCD,DISTNAME from  STEPS_DISTRICT with(nolock) where DISTCD in(select DISTCD from  TWSH_CENTER_MASTER with(nolock) where  SH_COURSE='Y') order by DISTCD";
//            }
            query = "select DISTCD,DISTNAME from  STEPS_DISTRICT with(nolock)";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public ArrayList getStateDetails() throws Exception {

        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select * from StateMaster with(nolock) order by StateName";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("scode", rs.getString(2));
                    map.put("sname", rs.getString(2));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {


                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;

    }

    public ArrayList getvillageDetails(String district) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM steps_village with(nolock) where LEFT(VILCD,6)=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("vcode", rs.getString(1));
                map.put("vname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public ArrayList getDistrictDetailsAt() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_DISTRICT_TS with(nolock)";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public ArrayList getMandalDetailsAt(String district) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_BLOCK_TS with(nolock) where LEFT(BLKCD,4)=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("mandal_ID", rs.getString(1));
                map.put("mandal_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public int getvalidatingAadharNum(String mobile, String aadhar) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select count (distinct  aadhar)  from TBL_TWSH_Registration with(nolock) where  mobile=? and aadhar not in (?)");
            stmt.setString(1, mobile);
            stmt.setString(2, aadhar);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return count;
    }

    public String getDetails(RegisterForm rForm) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        String count = "0";
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select GRADECODE  from TWSH_NR_AUGUST_2019 with(nolock) where  REGISTRATIONNO=?");
            stmt.setString(1, rForm.getHallticket());
            rs = stmt.executeQuery();
            if (rs != null) {
                if (rs.next() == true) {
                    count = rs.getString(1).trim();
                } else {
                    count = "0";
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return count;
    }

    public int getTimeStatus(String Aadhar, String date, String batch, String grade) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            if (grade.equalsIgnoreCase("TEL") || grade.equalsIgnoreCase("TTL") || grade.equalsIgnoreCase("TUL") || grade.equalsIgnoreCase("THL")) {
                stmt = con.prepareStatement("select count(*)  from TBL_TWSH_Registration with(nolock) where Aadhar=? and ExaminationDate=? and ExaminationBatch=? and grade in('TEL','TTL','TUL','THL')");
            } else if (grade.equalsIgnoreCase("TEH") || grade.equalsIgnoreCase("TTH") || grade.equalsIgnoreCase("TUH") || grade.equalsIgnoreCase("THH") || grade.equalsIgnoreCase("TEJ")) {
                stmt = con.prepareStatement("select count(*)  from TBL_TWSH_Registration with(nolock) where Aadhar=? and ExaminationDate=? and ExaminationBatch=? and grade in('TEH','TTH','TUH','THH','TEJ')");

            } else if (grade.equalsIgnoreCase("TTHS") || grade.equalsIgnoreCase("TEHS")) {
                stmt = con.prepareStatement("select count(*)  from TBL_TWSH_Registration with(nolock) where Aadhar=? and ExaminationDate=? and ExaminationBatch=? and grade in('TEHS','TTHS')");

            }
            stmt.setString(1, Aadhar);
            stmt.setString(2, date);
            stmt.setString(3, batch);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return count;
    }

    public int getStatusExam(String Aadhar, String examination, String language) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select count(*)  from TBL_TWSH_Registration with(nolock) where Aadhar=? and Examination=? and language=?");
            stmt.setString(1, Aadhar.trim());
            stmt.setString(2, examination.trim());
            stmt.setString(3, language.trim());
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select Grade,GRADE_DESC from TWSH_GRADE_MASTER with(nolock) where rowid between '12' and '21' order by RowID asc";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public String getGrade(RegisterForm rForm) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        String grade = "";
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select GRADE_DESC from TWSH_GRADE_MASTER with(nolock) where GRADE=?");
            stmt.setString(1, rForm.getGrade());
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    grade = rs.getString(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return grade;
    }

    public boolean uploadDocs(FormFile uploadFileName, String fileName, String aadhar, String grade) {
        boolean flag = false;
        try {
            String strDirectoytemp = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + grade + "\\" + aadhar;
            if (strDirectoytemp != null && !"".equals(strDirectoytemp) && strDirectoytemp.length() > 0) { // If directory is not exists it will create
                File directorytemp = new File(strDirectoytemp);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
                File fileToCreatetemp = new File(strDirectoytemp, fileName); // Copy the file into directory
                FileOutputStream fileOutStreamtemp = new FileOutputStream(fileToCreatetemp); // Write the file content into buffer
                if (uploadFileName.getFileSize() > 0) {
                    fileOutStreamtemp.write(uploadFileName.getFileData());
                    fileOutStreamtemp.flush();
                    fileOutStreamtemp.close();
                    flag = true;
                } else {
                    flag = false;
                }
            } else {
                flag = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public String getCourseName(String course) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        String grade = "";
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select CourseName from CCIC_MasterCourseDetails with(nolock) where CCD=?");
            stmt.setString(1, course);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    grade = rs.getString(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return grade;
    }

    public int getIntercount(String aadhar, String interhallticket) throws Exception {
        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select count (distinct  aadhar)  from TBL_TWSH_Registration with(nolock) where  interhallticket=? and aadhar not in (?)");
            stmt.setString(1, interhallticket);
            stmt.setString(2, aadhar);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return count;
    }

    public HashMap<String, String> getSSCDetails(String hallticket, String Aadhar) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
//        PreparedStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        String dob = null;
        try {


            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Get_SSCDETAILS_CCIC(?,?)}");
//            cstmt = con.prepareStatement("select * from studentinfo where rgukt_rollno=? and dob=?");
            cstmt.setString(1, hallticket);
            cstmt.setString(2, Aadhar);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                map.put("bname", rs.getString(2));
                map.put("fname", rs.getString(3));
                map.put("gender", rs.getString(4));
                map.put("caste", rs.getString(5));
                map.put("dob", rs.getString(6));
                map.put("aadharcount", rs.getString(8));
                map.put("Examinationstatus", rs.getString(9).toUpperCase());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }

    public HashMap<String, String> getDataForPaymentDob(RegisterForm rform, HttpServletRequest request, String uname) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
//        PreparedStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        String dob = null;
        try {


            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Get_CCIC(?,?,?)}");
//            cstmt = con.prepareStatement("select * from studentinfo where rgukt_rollno=? and dob=?");
            cstmt.setString(1, rform.getAadhar().trim());
            cstmt.setString(2, rform.getCourse().trim());
            cstmt.setString(3, uname);
//            cstmt.setString(1, "795640259613");
//            cstmt.setString(2, "1999-12-01");
            System.out.println("1========================"+rform.getAadhar());
            System.out.println("2========================"+rform.getCourse());
            System.out.println("3========================"+uname);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("gender", rs.getString(2));
                request.setAttribute("caste", rs.getString(3));
                request.setAttribute("aadhaar", "XXXXXXXX" + rs.getString(6).substring(8, 12));
                request.setAttribute("mobile", rs.getString(3));
                request.setAttribute("email", rs.getString(4));
                request.setAttribute("amount", rs.getDouble(5));
                request.setAttribute("aadhar", rs.getString(6));
                request.setAttribute("course", rs.getString(7));
                map.put("child", "9866747477");
//                list.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
System.out.println("map========================"+map);
        return map;
    }

    public String getInstiAllowedSeats(String courseId, String loginInstituteCode) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        String str = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_CheckInstituteSeatCount_Get(?,?)}");
            cstmt.setString(1, courseId);
            cstmt.setString(2, loginInstituteCode);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                str = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return str;
    }
    
    ////new file
    
     public HashMap<String,String>   getDataEdit(RegisterForm rform,HttpServletRequest request) throws Exception {
        String name="0";
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_Institute_EDIT_Get_Data(?,?)}");
            cstmt.setString(1, rform.getHallticket());
            cstmt.setString(2, rform.getAadhar());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                name="1";
                request.setAttribute("name", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
                request.setAttribute("dob", rs.getString(3));
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("instituteCode", rs.getString(5));

                request.setAttribute("institution", rs.getString(6));
                request.setAttribute("examinationAppearing", rs.getString(7));
                request.setAttribute("courseCode", rs.getString(8));
                request.setAttribute("courseName", rs.getString(9));
                request.setAttribute("examinationDistrict", rs.getString(10));

                request.setAttribute("examinationCenter", rs.getString(11));
                request.setAttribute("hNo", rs.getString(12));
                request.setAttribute("street", rs.getString(13));
                request.setAttribute("state", rs.getString(15));
                request.setAttribute("village", rs.getString(14));

                request.setAttribute("district", rs.getString(16));
                request.setAttribute("mandal", rs.getString(17));
                request.setAttribute("pincode", rs.getString(18));
                request.setAttribute("mobile", rs.getString(19));
                request.setAttribute("email", rs.getString(20));

                request.setAttribute("aadhar", rs.getString(21));

                request.setAttribute("attendance", rs.getString(29));
                request.setAttribute("regno", rs.getString(30));
                
                if(rs.getString(32).equalsIgnoreCase("Y") || rs.getString(33).equalsIgnoreCase("Y")){
                    request.setAttribute("editData", "true");
                }else{
                    request.setAttribute("editData", "false");
                }

                request.setAttribute("qualification", rs.getString(34));
                request.setAttribute("hallticket", rs.getString(31));
                if(rs.getString(48).equalsIgnoreCase("Regular")){
                   request.setAttribute("statuscase","N");
                }else{
                     request.setAttribute("statuscase","Y");
                }
                request.setAttribute("sschallticket", rs.getString(40));
                 request.setAttribute("sscFlagDB", rs.getString(32));
                request.setAttribute("sscpassoutyear", rs.getString(41));
                
                request.setAttribute("interhallticket", rs.getString(42));
                request.setAttribute("intermonth", rs.getString(43));
                request.setAttribute("interyear", rs.getString(44));
                request.setAttribute("interFlagDB", rs.getString(33));
                request.setAttribute("cate", rs.getString(45));
                request.setAttribute("fileFlag", rs.getString(46));
                request.setAttribute("studiedDistrict", rs.getString(47));
                request.setAttribute("category", rs.getString(48));
                request.setAttribute("subjectcourses", rs.getString(49));
                request.setAttribute("subjectnames", rs.getString(50));
                map.put("name","786737763");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }
public String insertFiles(RegisterForm rform, String remoteAddress) {
        Connection con = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        String result = "";

        String fileName1 = "";
        String fileName2 = "";
        String fileName3 = "";
        String fileName4 = "";
        String fileName5 = "";
        String fileName6 = "";
        String fileName7 = "";
        String fileName8 = "";
        String subjectCode = "ALL";
        boolean flag = false;

        String hallticket = rform.getAadhar();
        String course = rform.getCourse();

        if (rform.getCategory().equalsIgnoreCase("Backlog")) {
            subjectCode = rform.getSubjectCode();
        } else {
            subjectCode = "ALL";
        }

        if (rform.getPhoto() != null && rform.getPhoto().toString().length() > 0) {
            flag = uploadDocs(rform.getPhoto(), hallticket + "_PHOTO_EDIT.jpg", hallticket, course);
            if (flag == true) {
                fileName3 = hallticket + "_PHOTO_EDIT.jpg";
            }
        }
        if (rform.getSignature() != null && rform.getSignature().toString().length() > 0) {
            flag = uploadDocs(rform.getSignature(), hallticket + "_SIGNATURE_EDIT.jpg", hallticket, course);
            if (flag == true) {
                fileName2 = hallticket + "_SIGNATURE_EDIT.jpg";
            }
        }
        if (rform.getUpload1() != null && rform.getUpload1().toString().length() > 0) {
            if (rform.getFileFlag().equals("2")) {
                fileName5 = hallticket + "_SSC_PASS_FAIL_EQUAL_EDIT.jpg";
            }else if (rform.getFileFlag().equals("3")) {
                fileName5 = hallticket + "_SSC_EDIT.jpg";
            } else if (rform.getFileFlag().equals("4") && course.equals("AC")) {
                fileName5 = hallticket + "_INTER_OR_EQUAL_EDIT.jpg";
            } else if (rform.getFileFlag().equals("4")) {
                fileName5 = hallticket + "_INTER_EDIT.jpg";
            } else if (rform.getFileFlag().equals("5")) {
                fileName5 = hallticket + "_DEGREE_OR_DIPLOMA_OR_ENGINEERING_EDIT.jpg";
            }else if (rform.getFileFlag().equals("6")) {
                fileName5 = hallticket + "_YOGA_EDIT.jpg";
            } else {
                fileName5 = hallticket + "_UPLOAD1_EDIT.jpg";
            }
            flag = uploadDocs(rform.getUpload1(), fileName5, hallticket, course);
            if (flag == true) {
                fileName1 = fileName5;
            }
        }
        String isqualification="";
        if (course.equals("IS")) {
            isqualification = rform.getQualification();
            if (rform.getUpload2() != null && rform.getUpload2().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload2(), hallticket + "_EXPERIENCE1_EDIT.jpg", hallticket, course);
                if (flag == true) {
                    fileName4 = hallticket + "_EXPERIENCE1_EDIT.jpg";
                }
            }
            if (rform.getUpload3() != null && rform.getUpload3().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload3(), hallticket + "_EXPERIENCE2_EDIT.jpg", hallticket, course);
                if (flag == true) {
                    fileName6 = hallticket + "_EXPERIENCE2_EDIT.jpg";
                }
            }
            if (rform.getUpload4() != null && rform.getUpload4().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload4(), hallticket + "_EXPERIENCE3_EDIT.jpg", hallticket, course);
                if (flag == true) {
                    fileName7 = hallticket + "_EXPERIENCE3_EDIT.jpg";
                }
            }
            if (rform.getUpload5() != null && rform.getUpload5().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload5(), hallticket + "_EXPERIENCE4_EDIT.jpg", hallticket, course);
                if (flag == true) {
                    fileName8 = hallticket + "_EXPERIENCE4_EDIT.jpg";
                }
            }
        }
        try {
            Date todate2 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate2);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_ApplicantRegistrationDetails_Edit_Institute(?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, rform.getBname());
            cstmt.setString(2, rform.getFname());
            cstmt.setString(3, dob);
            cstmt.setString(4, rform.getGender());
            cstmt.setString(5, rform.getAadhar());
            cstmt.setString(6, rform.getCategory());
            cstmt.setString(7, rform.getApdistrict());
            cstmt.setString(8, rform.getInstitutionCode());
            cstmt.setString(9, course);
            cstmt.setString(10, subjectCode);
            cstmt.setString(11, rform.getEcenter());
            cstmt.setString(12, fileName1);
            cstmt.setString(13, rform.getHouseno());
            cstmt.setString(14, rform.getLocality());
            cstmt.setString(15, rform.getVillage());
            cstmt.setString(16, rform.getState());
            if ((rform.getState().equalsIgnoreCase("Andra Pradesh")) || (rform.getState().equalsIgnoreCase("Telagana"))) {
                cstmt.setString(17, rform.getDistrict());
                cstmt.setString(18, rform.getMandal());
            } else {
                cstmt.setString(17, rform.getDistrict1());
                cstmt.setString(18, rform.getMandal1());
            }
            cstmt.setString(19, rform.getPincode());
            cstmt.setString(20, rform.getMobile());
            cstmt.setString(21, rform.getEmail());
            cstmt.setString(22, fileName2);
            cstmt.setString(23, fileName3);
            cstmt.setString(24, rform.getHallticket());
            cstmt.setString(25, remoteAddress);
            cstmt.setString(26, rform.getUserName());
            cstmt.setString(27, fileName4);
            cstmt.setString(28, rform.getSschallticket());
            cstmt.setString(29, rform.getSscyear());
            cstmt.setString(30, rform.getSscflag());
            cstmt.setString(31, rform.getInterhallticket());
            cstmt.setString(32, rform.getIntermonth());
            cstmt.setString(33, rform.getInteryear());
            cstmt.setString(34, rform.getInterflag());
            cstmt.setString(35, rform.getAttendance());
            
            cstmt.setString(36, isqualification);
            cstmt.setString(37, fileName6);
            cstmt.setString(38, fileName7);
            cstmt.setString(39, fileName8);
            cstmt.setString(40, rform.getRegno());
            
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getString(1);
            }
        } catch (Exception ex) {
            logger.info("Error================" + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
