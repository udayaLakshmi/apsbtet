/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author APTOL301655
 */
public class BarcodeDAO {

    public HashMap<String, String> getDetails(String barcode) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_BARCODE_Get_TWSH(?)}");
            cstmt.setString(1, barcode);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("statusresult", rs.getString(1));
                    map.put("examination", rs.getString(2));
                    map.put("grade", rs.getString(3));
                    map.put("paper", rs.getString(4));
                    map.put("papercount", rs.getString(5));
                    map.put("regno", rs.getString(6));
                    map.put("ebatch", rs.getString(7));
                    map.put("ecenter", rs.getString(8));
                    map.put("totalbooklets", rs.getString(9));
                    map.put("scannedbooklets", rs.getString(10));
                    map.put("tobescannedbooklets", rs.getString(11));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public HashMap<String,String> submitDetails(HttpServletRequest req, String username) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap<String,String> map = new HashMap<String,String>();
        List<HashMap> lstDetails = new ArrayList();
        String res = "0";
        try {
            int count = Integer.parseInt(req.getParameter("institute"));
            String barcode = req.getParameter("barcode");
            String regno = req.getParameter("hallticket");
            String statusresult = req.getParameter("statusresult");
            String epaper = req.getParameter("epaper");
            String sytemIp = req.getRemoteAddr();
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_barcodescanstatus_update(?,?,?,?,?,?)}");
            cstmt.setString(1, regno);
            cstmt.setString(2, barcode);
            cstmt.setString(3, statusresult);
            cstmt.setString(4, username);
            cstmt.setString(5, sytemIp);
            cstmt.setString(6, epaper.substring(0,epaper.length()-1));
            rs = cstmt.executeQuery();
            if (rs != null && rs.next()) {
             map.put("status",rs.getString(1));
             map.put("statusmsg",rs.getString(2));
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("resDDD===" + res);
        return map;
    }
}
