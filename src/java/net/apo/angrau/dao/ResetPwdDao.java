/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.ap.WithLoginDAO.*;
import com.ap.WithLoginForm.LoginForm;
import net.apo.angrau.db.DatabaseConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
//import java.util.concurrent.Semaphore;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This Class is having all login related queries
 *
 * @author 1451421
 */
public class ResetPwdDao {

    public String getMobile(String user) {
        String mb = "";
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select ModelSchoolDisrtictCode from LoginDetails where username='" + user + "'";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    mb = rs.getString(1);
                    if(mb==null)
                         mb = "123";
                }
            } else {
                mb = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mb;
        }
    }
    
    public int SaveOTP(String username, String mobile, String otp, String systemIp) throws Exception {
        int count = 0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {
            //System.out.println("username "+username + "mobile "+mobile+" " +otp+ " systemIp "+systemIp);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Password_OTP_Details_Insert(?,?,?,?)}");
            cstmt.setString(1, username);
            cstmt.setString(2, mobile);
            cstmt.setString(3, otp);
            cstmt.setString(4, systemIp);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

  
    public String getvalidatingOTP(String username, String mobile) throws Exception {

        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String otp = "";

        try {
            con = DatabaseConnection.getConnection();
            query = "select OTP  from OTP_Details where Mobile_Num='" + mobile + "' and Aadhar_Id='" + username + "'";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    otp = rs.getString(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return otp;
    }


    public int updatePassword(LoginForm LoginForm) {
        int flag = 0;
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "update LoginDetails set Password='demo', EncryptedPassword='6c5ac7b4d3bd3311f033f971196cfa75',"
                    + " UpdatedDate=getDate(),LoginFlag='N' where username='" + LoginForm.getUserName() + "' and status='Active'";
            pstmt = con.prepareStatement(query);
            flag = pstmt.executeUpdate();

            if (rs != null) {
                while (rs.next()) {
                    flag = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return flag;
        }
    }
}
