/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.sms.util.MessagePropertiesUtil;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.text.SimpleDateFormat;
import javax.imageio.ImageIO;
import net.apo.angrau.forms.RegisterForm;
import net.apo.angrau.db.DatabaseConnection;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author APTOL301294
 */
public class DashBoardDao {

    public List<HashMap> getCandidateList() throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_Dashbard_Report}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(2));
                    String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                    map.put("dob", dob);
                    map.put("name", rs.getString(1));
                    map.put("mobile", rs.getString(3));
                    map.put("caste", rs.getString(4));
                    map.put("examination", rs.getString(5));
                    map.put("grade", rs.getString(6));
                    map.put("regno", rs.getString(7));
                    map.put("aadhar", rs.getString(8));
                    map.put("gender", rs.getString(9));
                    map.put("fname", rs.getString(10));
                    map.put("payStatus", rs.getString(11));
                     map.put("type", rs.getString(12));
                     map.put("verfStatus", rs.getString(13));
                     map.put("VerfRemarks", rs.getString(14));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        HashMap<String, String> map = new HashMap<String, String>();
        try {

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Dashboard_Profile(?,?)}");
            cstmt.setString(1, rform.getHallticket());
            cstmt.setString(2, rform.getMobile().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                request.setAttribute("name", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));

                request.setAttribute("visImp", rs.getString(6));
                request.setAttribute("institution", rs.getString(7));
                request.setAttribute("examinationAppearing", rs.getString(8));
                request.setAttribute("language", rs.getString(9));
                request.setAttribute("grade", rs.getString(10));
                request.setAttribute("examinationDistrict", rs.getString(11));

                request.setAttribute("examinationCenter", rs.getString(12));
                request.setAttribute("examinationDate", rs.getString(13));
                request.setAttribute("examinationBatch", rs.getString(14));
                request.setAttribute("hNo", rs.getString(15));
                request.setAttribute("street", rs.getString(16));

                request.setAttribute("village", rs.getString(17));
                request.setAttribute("state", rs.getString(18));
                request.setAttribute("district", rs.getString(19));
                request.setAttribute("mandal", rs.getString(20));
                request.setAttribute("pincode", rs.getString(21));

                request.setAttribute("mobile", rs.getString(22));
                request.setAttribute("email", rs.getString(23));
                request.setAttribute("aadhar", rs.getString(24));

                request.setAttribute("RegNo", rs.getString(26));
                request.setAttribute("paymentRefNo", rs.getString(28));
                request.setAttribute("paymentAmount", rs.getString(29));
                request.setAttribute("paymentDate", rs.getString(30));
                Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(31));
                String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                request.setAttribute("dob", dob);
                request.setAttribute("date", new Date());
                map.put("status", rs.getString(40).toString());
                request.setAttribute("appStatus", rs.getString(40).toString());
                String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(32) + "\\" + rs.getString(24) + "\\" + rs.getString(25);
                System.out.println("filepath "+filepath);
                File file = new File(filepath);
                try {
                    if (ImageIO.read(file).getData() != null) {
                        BufferedImage image = ImageIO.read(file);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ImageIO.write(image, "jpg", baos);
                        baos.flush();
                        byte[] imageInByteArray = baos.toByteArray();
                        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                        request.setAttribute("photo", b64);
                    }else request.setAttribute("photo", "");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }
    
}