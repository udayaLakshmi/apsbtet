/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author 1582792
 */
public class PracticalMarksEntryReportDAO {

    public List<HashMap> getColleageDetails(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get_Report(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "1");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("caste", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));
                map.put("maxMarks", rs.getString(6));// Practicals
                map.put("epaper", rs.getString(7));
                map.put("practicalScored", rs.getString(8));
                req.setAttribute("epaper", rs.getString(7));
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getStudentDetailsIS(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get_Report(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "4");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("caste", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));
                map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(7));//Sessional IS-109
                map.put("epaper", rs.getString(8));// PaperCodes
                map.put("practicalScored", rs.getString(9));
                map.put("sessionalScored", rs.getString(10));
                req.setAttribute("epaper", rs.getString(8));
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails==" + lstDetails);
        return lstDetails;
    }

    public List<HashMap> getDetailsFP(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get_Report(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "5");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("caste", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));
                map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(7));//Sessional IS-109
                map.put("epaper", rs.getString(8));// PaperCodes

                map.put("maxMarks2", rs.getString(9));//Practicals IS-109
                map.put("marks2", rs.getString(10));//Sessional IS-109
                map.put("epaper2", rs.getString(11));// PaperCodes

                map.put("maxMarks3", rs.getString(12));//Practicals IS-109
                map.put("marks3", rs.getString(13));//Sessional IS-109
                map.put("epaper3", rs.getString(14));// PaperCodes

                req.setAttribute("epaper1", rs.getString(8));
                req.setAttribute("epaper2", rs.getString(11));
                req.setAttribute("epaper3", rs.getString(14));


                map.put("practicalScored1", rs.getString(15));
                map.put("practicalScored2", rs.getString(16));
                map.put("practicalScored3", rs.getString(17));
                
                map.put("sessionalScored1", rs.getString(18));
                map.put("sessionalScored2", rs.getString(19));
                map.put("sessionalScored3", rs.getString(20));

                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails=="+lstDetails);
        return lstDetails;
    }

    public List<HashMap> getDetailsID(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get_Report(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "6");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("caste", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));
                map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(7));//Sessional IS-109
                map.put("epaper", rs.getString(8));// PaperCodes

                map.put("maxMarks2", rs.getString(9));//Practicals IS-109
                map.put("marks2", rs.getString(10));//Sessional IS-109
                map.put("epaper2", rs.getString(11));// PaperCodes

                map.put("maxMarks3", rs.getString(12));//Practicals IS-109
                map.put("marks3", rs.getString(13));//Sessional IS-109
                map.put("epaper3", rs.getString(14));// PaperCodes

                map.put("maxMarks4", rs.getString(15));//Practicals IS-109
                map.put("marks4", rs.getString(16));//Sessional IS-109
                map.put("epaper4", rs.getString(17));// PaperCodes

                map.put("maxMarks5", rs.getString(18));//Practicals IS-109
                map.put("marks5", rs.getString(19));//Sessional IS-109
                map.put("epaper5", rs.getString(20));// PaperCodes

                map.put("maxMarks6", rs.getString(21));//Practicals IS-109
                map.put("marks6", rs.getString(22));//Sessional IS-109
                map.put("epaper6", rs.getString(23));// PaperCodes

                req.setAttribute("epaper1", rs.getString(8));
                req.setAttribute("epaper2", rs.getString(11));
                req.setAttribute("epaper3", rs.getString(14));
                req.setAttribute("epaper4", rs.getString(17));
                req.setAttribute("epaper5", rs.getString(20));
                req.setAttribute("epaper6", rs.getString(23));
                
                
                
                map.put("practicalScored1", rs.getString(24));
                map.put("practicalScored2", rs.getString(25));
                map.put("practicalScored3", rs.getString(26));
                map.put("practicalScored4", rs.getString(27));
                map.put("practicalScored5", rs.getString(28));
                map.put("practicalScored6", rs.getString(29));
                
                map.put("sessionalScored1", rs.getString(30));
                map.put("sessionalScored2", rs.getString(31));
                map.put("sessionalScored3", rs.getString(32));
                map.put("sessionalScored4", rs.getString(33));
                map.put("sessionalScored5", rs.getString(34));
                map.put("sessionalScored6", rs.getString(35));
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails=="+lstDetails);
        return lstDetails;
    }

    public ArrayList getcourses(String username) throws Exception {
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        try {
            con = DatabaseConnection.getConnection();
//            query = "select * from MSc_CourseMaster";
//            query = "select CourseID,COURSEName from CCIC_MasterDistrictWiseInstitutionWiseCourseDetails (nolock) where InstitutionID='" + username + "'";
//            pstmt = con.prepareStatement(query);
//            rs = pstmt.executeQuery();


            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get_Report(?,?,?)}");
            cstmt.setString(1, username);
            cstmt.setString(2, "");
            cstmt.setString(3, "2");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("courseId", rs.getString(1));
                map.put("courseName", rs.getString(2));
                list.add(map);
                map = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // System.out.println("courselist======" + list);
        return list;
    }
    
 public List<HashMap> getDetailsIntegritySafety(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get_Report(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "7");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("dob", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));

                //map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(6));//Sessional IS-109
                map.put("epaper", rs.getString(7));// PaperCodes

                // map.put("maxMarks2", rs.getString(9));//Practicals IS-109
                map.put("marks2", rs.getString(8));//Sessional IS-109
                map.put("epaper2", rs.getString(9));// PaperCodes

                // map.put("maxMarks3", rs.getString(12));//Practicals IS-109
                map.put("marks3", rs.getString(10));//Sessional IS-109
                map.put("epaper3", rs.getString(11));// PaperCodes

                // map.put("maxMarks4", rs.getString(15));//Practicals IS-109
                map.put("marks4", rs.getString(12));//Sessional IS-109
                map.put("epaper4", rs.getString(13));// PaperCodes

                // map.put("maxMarks5", rs.getString(18));//Practicals IS-109
                map.put("marks5", rs.getString(14));//Sessional IS-109
                map.put("epaper5", rs.getString(15));// PaperCodes

                // map.put("maxMarks6", rs.getString(21));//Practicals IS-109
                map.put("marks6", rs.getString(16));//Sessional IS-109
                map.put("epaper6", rs.getString(17));// PaperCodes

                map.put("marks7", rs.getString(18));//Sessional IS-109
                map.put("epaper7", rs.getString(19));// PaperCodes

                map.put("marks8", rs.getString(20));//Sessional IS-109
                map.put("epaper8", rs.getString(21));// PaperCodes

                map.put("maxMarks9", rs.getString(22));//Practicals IS-109
                map.put("marks9", rs.getString(23));//Sessional IS-109
                map.put("epaper9", rs.getString(24));// PaperCodes

                req.setAttribute("epaper1", rs.getString(7));
                req.setAttribute("epaper2", rs.getString(9));
                req.setAttribute("epaper3", rs.getString(11));
                req.setAttribute("epaper4", rs.getString(13));
                req.setAttribute("epaper5", rs.getString(15));
                req.setAttribute("epaper6", rs.getString(17));
                req.setAttribute("epaper7", rs.getString(19));
                req.setAttribute("epaper8", rs.getString(21));
                req.setAttribute("epaper9", rs.getString(24));
                
                
                map.put("sessionalScored1", rs.getString(25));
                map.put("sessionalScored2", rs.getString(26));
                map.put("sessionalScored3", rs.getString(27));
                map.put("sessionalScored4", rs.getString(28));
                map.put("sessionalScored5", rs.getString(29));
                map.put("sessionalScored6", rs.getString(30));
                 map.put("sessionalScored7", rs.getString(31));
                map.put("sessionalScored8", rs.getString(32));
                 map.put("sessionalScored9", rs.getString(33));
                 map.put("practicalScored1", rs.getString(34));
                
                
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails=="+lstDetails);
        return lstDetails;
    }

    public List<HashMap> getDetailsYo(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get_Report(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "8");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("dob", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));

                //map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(6));//Sessional IS-109
                map.put("epaper", rs.getString(7));// PaperCodes

                // map.put("maxMarks2", rs.getString(9));//Practicals IS-109
                map.put("marks2", rs.getString(8));//Sessional IS-109
                map.put("epaper2", rs.getString(9));// PaperCodes

                // map.put("maxMarks3", rs.getString(12));//Practicals IS-109
                map.put("marks3", rs.getString(10));//Sessional IS-109
                map.put("epaper3", rs.getString(11));// PaperCodes

                map.put("maxMarks4", rs.getString(12));//Practicals IS-109
                map.put("marks4", rs.getString(13));//Sessional IS-109
                map.put("epaper4", rs.getString(14));// PaperCodes




                req.setAttribute("epaper1", rs.getString(7));
                req.setAttribute("epaper2", rs.getString(9));
                req.setAttribute("epaper3", rs.getString(11));
                req.setAttribute("epaper4", rs.getString(14));
                
                   map.put("sessionalScored1", rs.getString(15));
                map.put("sessionalScored2", rs.getString(16));
                map.put("sessionalScored3", rs.getString(17));
                map.put("sessionalScored4", rs.getString(18));
                  map.put("practicalScored1", rs.getString(19));

                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails=="+lstDetails);
        return lstDetails;
    }
}
