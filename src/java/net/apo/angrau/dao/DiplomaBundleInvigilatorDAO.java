/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.log4j.Logger;

/**
 *
 * @author APTOL301655
 */
public class DiplomaBundleInvigilatorDAO {
  private static final Logger logger = Logger.getLogger(DiplomaBundleInvigilatorDAO.class);

   
    public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnectionDiploma();
            query = "select distinct Branch  From Diploma_TimeTable_Live with(nolock)";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(1));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                logger.info("e=====================" + e.getMessage());
                e.printStackTrace();
            }
        }
        return list;
    }
    public ArrayList getInvgList(String uname) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_Invigilator_Details_Diploma(?)}");
            cstmt.setString(1,uname);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("bname", rs.getString(1));
                map.put("mobile", rs.getString(2));
                map.put("email", rs.getString(3));
                map.put("grade", rs.getString(4));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public String submitDetails(HttpServletRequest req, String username, RegisterForm rForm) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap<String, String> map = new HashMap<String, String>();
        String result="";
        try {
            String sytemIp = req.getRemoteAddr();
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_Invigilator_Registration_Diploma(?,?,?,?,?,?)}");
            cstmt.setString(1, rForm.getFname());
            cstmt.setString(2, rForm.getMobile());
            cstmt.setString(3, rForm.getEmail());
            cstmt.setString(4, rForm.getGrade());
            cstmt.setString(5, username);
            cstmt.setString(6, sytemIp);
            rs = cstmt.executeQuery();
            if (rs != null && rs.next()) {
               result=rs.getString(1);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                logger.info("e=====================" + e.getMessage());
                e.printStackTrace();
            }
        }
        return result;
    }
    public String deleteDetails(RegisterForm rForm,HttpServletRequest req, String username ) {

        Connection con = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        HashMap<String, String> map = new HashMap<String, String>();
        String result="";
        try {
            String sytemIp = req.getRemoteAddr();
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareStatement("{Call Usp_Invigilator_delete_Diploma(?,?,?)}");
            cstmt.setString(1, rForm.getCoursename());
            cstmt.setString(2, rForm.getGradename());
            cstmt.setString(3, rForm.getBarcode());
            rs = cstmt.executeQuery();
            if (rs != null && rs.next()) {
               result=rs.getString(1);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                logger.info("e=====================" + e.getMessage());
                e.printStackTrace();
            }
        }
        return result;
    }
     public int  getDetailsStatus(String grade,String  mobile) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        PreparedStatement cstmt = null;
        HashMap map = null;
        int count=0;
        try {
            con = DatabaseConnection.getConnection();
            String[] gradeslist=grade.split(",");
            StringBuilder sb=new StringBuilder();
            for(int i=0;i<gradeslist.length;i++){
                sb.append("'"+gradeslist[i]+"',");
            }
            String finalgrade=sb.toString();
            cstmt = con.prepareStatement("select count(*) from TWSH_Invigilator_Registration with(nolock) where invgilatormobile=? and grade in("+finalgrade.substring(0,finalgrade.length()-1)+")");
            cstmt.setString(1, mobile);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                   count=rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }  
}
