/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.ap.DAO.*;
import com.ap.Form.RegisterForm;
import com.ap.Form.RegisterFormPhD;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author ci96067
 */
public class VerificationReportDao {

    public List<HashMap> getCandidateReport(String type, String click) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int totalAutoVerifiedCount =0;
        int totalRejectedCount =0;
        int totalAssignedCount = 0, totalNotverified = 0, totalSendToReupload = 0, totalPending2nd = 0, totalApproved = 0;
        try {
            Date date = new Date();
            String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_VerificationStatusAbstract(?)}");
            cstmt.setString(1, modifiedDate);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("name", rs.getString(1));
                    map.put("assignedCount", rs.getString(2));
                    map.put("notverified", rs.getString(3));
                    map.put("sendToReupload", rs.getString(4));
                    map.put("pending2nd", rs.getString(5));
                    map.put("approved", rs.getString(6));
                    map.put("rejected", rs.getString(7));
                    map.put("autoVerified", rs.getString(8));
                    totalAssignedCount = totalAssignedCount + Integer.parseInt(rs.getString(2));
                    totalNotverified = totalNotverified + Integer.parseInt(rs.getString(3));
                    totalSendToReupload = totalSendToReupload + Integer.parseInt(rs.getString(4));
                    totalPending2nd = totalPending2nd + Integer.parseInt(rs.getString(5));
                    totalApproved = totalApproved + Integer.parseInt(rs.getString(6));
                    
                    totalRejectedCount = totalRejectedCount + Integer.parseInt(rs.getString(7));
                    totalAutoVerifiedCount = totalAutoVerifiedCount + Integer.parseInt(rs.getString(8));
                    
                    map.put("totalRejectedCount", totalRejectedCount);
                    map.put("totalAutoVerifiedCount", totalAutoVerifiedCount);
                    
                    map.put("totalAssignedCount", totalAssignedCount);
                    map.put("totalNotverified", totalNotverified);
                    map.put("totalSendToReupload", totalSendToReupload);
                    map.put("totalPending2nd", totalPending2nd);
                    map.put("totalApproved", totalApproved);
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getCourseWiseReport() throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int totalApplications1 = 0;
        int totalSubmitted = 0;
        int totalfarq = 0;
        int totalfarvq = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_Dashboard()}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    if (rs.getString(1).equalsIgnoreCase("BiPC Stream")) {
                        map.put("course", "BiPC");
                    } else {
                        map.put("course", "MPC");
                    }
                    map.put("programme", rs.getString(1));
                    map.put("totalAplications", rs.getString(2));
                    map.put("griPlaced", rs.getString(3));
                    map.put("farq", rs.getString(4));
                    map.put("farvq", rs.getString(5));

                    totalApplications1 = totalApplications1 + Integer.parseInt(rs.getString(2));
                    totalSubmitted = totalSubmitted + Integer.parseInt(rs.getString(3));
                    totalfarq = totalfarq + Integer.parseInt(rs.getString(4));
                    totalfarvq = totalfarvq + Integer.parseInt(rs.getString(5));
                    map.put("totalApplications1", totalApplications1);
                    map.put("totalSubmitted", totalSubmitted);
                    map.put("totalfarq", totalfarq);
                    map.put("totalfarvq", totalfarvq);
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getCandidateReportNew1(String type, String click) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call proc_GraduationRpt(?,?)}");
            cstmt.setString(1, type);
            cstmt.setString(2, click);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("name", rs.getString(2));
                    map.put("hall", rs.getString(1));
                    map.put("rank", rs.getString(3));
                    map.put("caste", rs.getString(4));
                    map.put("agri", rs.getString(5));
                    map.put("dob", rs.getString(6));
                    map.put("mobile", rs.getString(7));
                    map.put("gender", rs.getString(8));
                    map.put("subMinarity", rs.getString(9));
                    map.put("type", type);
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getLoginDashBoard() throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TotalApplicantAbstractReport_Get()}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("pgTotalApplied", rs.getString(1));
                    map.put("pgNoOfCandiatesSub", rs.getString(2));
                    map.put("pgNoOfCandiatesPayMade", rs.getString(3));
                    map.put("phdTotalApplied", rs.getString(4));
                    map.put("phdNoOfCandiatesSub", rs.getString(5));
                    map.put("phdNoOfCandiatesPayMade", rs.getString(6));
                    lstDetails.add(map);
                    map = null;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "D://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }

    public void getPdfData(String type, String filepath, String filename, String flag) throws SQLException {
        createPDF(type, filepath, filename, flag);
    }

    private void createPDF(String type, String filepath, String filename, String flag) {

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list = getCandidateReport("", "");
            //special font sizes
            Font bfBold12 = new Font(FontFamily.TIMES_ROMAN, 8, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD, new BaseColor(119, 41, 4)); //MAGENTA -Pink
            Font bf12 = new Font(FontFamily.TIMES_ROMAN, 8);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            //document header attributes
            doc.addAuthor("betterThanZero");
            doc.addCreationDate();
            doc.addProducer();
            doc.addCreator("MySampleCode.com");
            doc.addTitle("Report with Column Headings");
            doc.setPageSize(PageSize.A4);

            //open document
            doc.open();

            //create a paragraph
            Paragraph paragraph = new Paragraph("             Report : TWSH Certificate verification status", bfBold12);


            //specify column widths
            float[] columnWidths = {1f, 7f, 2.8f, 2.8f, 2.8f,3f, 2.8f, 2.8f, 2.8f};
            //create PDF table with the given widths
            PdfPTable table = new PdfPTable(columnWidths);
            // set table width a percentage of the page width
            table.setWidthPercentage(90f);

            //insert column headings
            insertCell(table, "SNO", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Total Assigned", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Not Verified", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Send To Reupload", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Pending for 2nd Verification", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Verified", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Auto Verified", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Rejected", Element.ALIGN_CENTER, 1, bfBold12);
            table.setHeaderRows(1);

            int totalAplications = 0, submitted = 0, submittedv = 0, paymentMade = 0, totalApproved = 0, totalRejected = 0, totalAutoVerified = 0;
            int sno = 0;
            //just some random data to fill 
            for (int x = 0; x < list.size(); x++) {
                HashMap map = list.get(x);
                sno++;
                insertCell(table, sno + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("name") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("assignedCount") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("notverified") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("sendToReupload") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("pending2nd") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("approved") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("autoVerified") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("rejected") + "", Element.ALIGN_CENTER, 4, bf12);
                totalAplications = totalAplications + Integer.parseInt(map.get("assignedCount").toString());
                submitted = submitted + Integer.parseInt(map.get("notverified").toString());
                submittedv = submittedv + Integer.parseInt(map.get("sendToReupload").toString());
                paymentMade = paymentMade + Integer.parseInt(map.get("pending2nd").toString());
                totalApproved = totalApproved + Integer.parseInt(map.get("approved").toString());
                
                totalRejected = totalRejected + Integer.parseInt(map.get("totalRejectedCount").toString());
                totalAutoVerified = totalAutoVerified + Integer.parseInt(map.get("totalAutoVerifiedCount").toString());
                map = null;

            }
            insertCell(table, "Total", Element.ALIGN_CENTER, 2, bfBold12);
            insertCell(table, totalAplications + "", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, submitted + "", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, submittedv + "", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, paymentMade + "", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, totalApproved + "", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, totalAutoVerified + "", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, totalRejected + "", Element.ALIGN_CENTER, 1, bfBold12);

            //add the PDF table to the paragraph 
            paragraph.add(table);
            // add the paragraph to the document
            doc.add(paragraph);

        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

// public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> getData(RegisterFormPhD rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        RegisterForm regform = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {

            Date todate1 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate1);

            con = DatabaseConnection.getConnection();
            if (rform.getCourse().equalsIgnoreCase("BiPC")) {
                cstmt = con.prepareCall("{Call Usp_student_Get (?,?)}");
                cstmt.setString(1, rform.getAadhar());
                cstmt.setString(2, dob);
            } else {
                cstmt = con.prepareCall("{Call Usp_Student_Get_ENG (?,?)}");
                cstmt.setString(1, rform.getAadhar());
                cstmt.setString(2, dob);
            }

            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
                request.setAttribute("dob", rform.getDob());
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));

                String adh = "";
                String aadhar = rs.getString(6);
                if (aadhar != null && aadhar != "") {
                    adh = "xxxxxxxx" + aadhar.substring(8, 12);
                } else {
                    adh = "xxxxxxxxxxxx";
                }

                request.setAttribute("aadhaar", adh);

                // request.setAttribute("aadhaar", rs.getString(6));
                request.setAttribute("income", rs.getString(7));
                request.setAttribute("region", rs.getString(8));
                request.setAttribute("mobile", rs.getString(9));

                request.setAttribute("minority", rs.getString(10));
                request.setAttribute("minoritytype", rs.getString(11));
                request.setAttribute("phf", rs.getString(12));
//                request.setAttribute("phtype", rs.getString(13));
                request.setAttribute("ncc", rs.getString(13));
                request.setAttribute("games", rs.getString(14));
                request.setAttribute("cap", rs.getString(15));

                request.setAttribute("hallticket", rs.getString(16));
                request.setAttribute("emarks", Math.round(rs.getFloat(17)));
                request.setAttribute("rank", Math.round(rs.getFloat(18)));

                request.setAttribute("address", rs.getString(19) + "," + rs.getString(20) + ",");
                request.setAttribute("address1", rs.getString(21) + "," + rs.getString(22) + "," + rs.getString(23) + "," + rs.getString(24));
                request.setAttribute("mothername", rs.getString(25));
                request.setAttribute("email", rs.getString(26));
                // request.setAttribute("amount", "1200");

                request.setAttribute("sixDistrict", rs.getString(27));
                request.setAttribute("sixplace", rs.getString(28));
                request.setAttribute("sixinstitute", rs.getString(29));



                request.setAttribute("sevenDistrict", rs.getString(30));
                request.setAttribute("sevenplace", rs.getString(31));
                request.setAttribute("seveninstitute", rs.getString(32));

                request.setAttribute("eightDistrict", rs.getString(33));
                request.setAttribute("eightplace", rs.getString(34));
                request.setAttribute("eightinstitute", rs.getString(35));


                request.setAttribute("nineDistrict", rs.getString(36));
                request.setAttribute("nineplace", rs.getString(37));
                request.setAttribute("nineinstitute", rs.getString(38));

                request.setAttribute("tenDistrict", rs.getString(39));
                request.setAttribute("tenplace", rs.getString(40));
                request.setAttribute("teninstitute", rs.getString(41));


                request.setAttribute("interDistrict", rs.getString(42));
                request.setAttribute("interplace", rs.getString(43));
                request.setAttribute("interinstitute", rs.getString(44));

                request.setAttribute("intersDistrict", rs.getString(45));
                request.setAttribute("intersplace", rs.getString(46));
                request.setAttribute("intersinstitute", rs.getString(47));
                request.setAttribute("amount", 1200);

                request.setAttribute("nccf", rs.getString(48));
                request.setAttribute("gamesf", rs.getString(49));
                request.setAttribute("capf", rs.getString(50));

                request.setAttribute("castef", rs.getString(53));
                request.setAttribute("incomef", rs.getString(54));
                request.setAttribute("ph", rs.getString(55));
                request.setAttribute("agric", rs.getString("AgriculturalFamilyYN"));

//                System.out.println("photo==========="+ DatatypeConverter.printBase64Binary(rs.getBytes(51)));
//                System.out.println("photo===1========"+ Base64.encodeBase64URLSafeString(rs.getBytes(51)));
                request.setAttribute("amount", rs.getString(52));
                map.put("child", "9866747477");

                request.setAttribute("photo", DatatypeConverter.printBase64Binary(rs.getBytes(51)).trim());
//                list.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }
}
