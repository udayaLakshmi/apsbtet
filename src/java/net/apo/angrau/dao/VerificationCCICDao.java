/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;
import com.ap.WithLoginDAO.*;
import com.ap.Action.*;
import com.sms.util.MessagePropertiesUtil;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author APTOL301655
 */
public class VerificationCCICDao {
    public List getDocumentVerficationDetails(String username, String course, String subgroup) throws Exception {
        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_CCIC_DocumentVerificationList(?)}");
            cstmt.setString(1, username);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("regno", rs.getString(1));
                map.put("name", rs.getString(2));
                map.put("father", rs.getString(3));
                map.put("DOB", rs.getString(4));
                map.put("gender", rs.getString(5));
                map.put("mobile", rs.getString(6));
                map.put("status", rs.getString(7));
                  map.put("course", rs.getString(8));

                lstDetails.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return lstDetails;
    }

// added on 11 Nov 2020
    public List getDegreeDocumentSubmitedDetails(String username) throws Exception {
         Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_CCIC_DocumentVerificationList_Completed](?,?)}");
            cstmt.setString(1, "1");
            cstmt.setString(2, username);

            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicationId", rs.getString(1));
                map.put("name", rs.getString(2));
                map.put("father", rs.getString(3));
                map.put("DOB", rs.getString(4));
                map.put("gender", rs.getString(5));
                map.put("mobilenumber", rs.getString(6));
                map.put("status", rs.getString(7));
                 map.put("course", rs.getString(8));

                lstDetails.add(map);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return lstDetails;
    }
    
    public List<HashMap> getAcademicsDocumentDetails(RegisterForm rform, HttpServletRequest request) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_CCIC_DocumentVerificationList_Candidate](?)}");
            cstmt.setString(1, rform.getHallticket());
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                request.setAttribute("regno", rs.getString(1));
                request.setAttribute("bname", rs.getString(2));
                request.setAttribute("fname", rs.getString(3));
                request.setAttribute("dob", rs.getString(4));
                request.setAttribute("gender", rs.getString(5));
                request.setAttribute("Institute", rs.getString(6));
                request.setAttribute("category", rs.getString(7));
                request.setAttribute("mobile", rs.getString(8));
                request.setAttribute("email", rs.getString(9));
                request.setAttribute("aadhar", rs.getString(10));
                request.setAttribute("course", rs.getString(11));
                request.setAttribute("photofile", rs.getString(12));
                request.setAttribute("signaturefile", rs.getString(13));
                request.setAttribute("upload1file", rs.getString(14));
                request.setAttribute("upload2file", rs.getString(15));
                request.setAttribute("upload3file", rs.getString(16));
                request.setAttribute("upload4file", rs.getString(17));
                request.setAttribute("upload5file", rs.getString(18));
               
                
                request.setAttribute("photocfrm", rs.getString(19));
                request.setAttribute("signaturecfrm", rs.getString(20));
                request.setAttribute("upload11cfrm", rs.getString(21));
                request.setAttribute("upload12cfrm", rs.getString(22));
                request.setAttribute("upload13cfrm", rs.getString(23));
                request.setAttribute("upload14cfrm", rs.getString(24));
                request.setAttribute("upload15cfrm", rs.getString(25));
                
                request.setAttribute("finalstatus", rs.getString(26));
                
                    request.setAttribute("photormks", rs.getString(27));
                   request.setAttribute("signaturermks", rs.getString(28));
                   request.setAttribute("upload1rmks", rs.getString(29));
                   request.setAttribute("upload2rmks", rs.getString(30));
                   request.setAttribute("upload3rmks", rs.getString(31));
                   request.setAttribute("upload4rmks", rs.getString(32));
                   request.setAttribute("upload5rmks", rs.getString(33));
                   
                   request.setAttribute("fileflag", rs.getString(34));
                   request.setAttribute("updatedby", rs.getString(35));
                   request.setAttribute("coursename", rs.getString(37));
                if(rform.getVstatus().equals("New Application")){
                  request.setAttribute("vstatus", "0");
                }else if(rform.getVstatus().equals("ReUploaded Application")){
                     request.setAttribute("vstatus", "1");
                }
                try {
                    String filepath = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(11) + "\\" + rs.getString(10)+ "\\" + rs.getString(12);
                    File file = new File(filepath);
                    if (file.isFile()) {
                        BufferedImage image = ImageIO.read(file);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ImageIO.write(image, "jpg", baos);
                        baos.flush();
                        byte[] imageInByteArray = baos.toByteArray();
                        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                        request.setAttribute("photo", b64);
                    } 
                } catch (Exception e) {
                    e.printStackTrace();
                }
                map.put("district_Name", "1234");
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return lstDetails;
    }

    

    public int submitAcademicsDocumentStatusDetails(RegisterForm rform, HttpServletRequest req,String uname) {

        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int res = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call [Usp_CCIC_DocumentVerification_Update] (?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, req.getParameter("applicationno"));
            cstmt.setString(2, uname);
            cstmt.setString(3, "3");
            cstmt.setString(4, "3");
            cstmt.setString(5, req.getParameter("upload1Confirm"));
            cstmt.setString(6, req.getParameter("upload2Confirm"));
            cstmt.setString(7, req.getParameter("upload3Confirm"));
            cstmt.setString(8, req.getParameter("upload4Confirm"));
            cstmt.setString(9, req.getParameter("upload5Confirm"));
            cstmt.setString(10, req.getParameter("eligibleConfirm"));
            cstmt.setString(11, "None");
            cstmt.setString(12, "None");
         
            if (req.getParameter("upload1Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(13, req.getParameter("upload1RemarksU"));
            } else {
                cstmt.setString(13, req.getParameter("upload1Remarks"));
            }
            
            if (req.getParameter("upload2Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(14, req.getParameter("upload2RemarksU"));
            } else {
                cstmt.setString(14, req.getParameter("upload2Remarks"));
            }
            
            if (req.getParameter("upload3Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(15, req.getParameter("upload3RemarksU"));
            } else {
                cstmt.setString(15, req.getParameter("upload3Remarks"));
            }
            
            if (req.getParameter("upload4Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(16, req.getParameter("upload4RemarksU"));
            } else {
                cstmt.setString(16, req.getParameter("upload4Remarks"));
            }
            if (req.getParameter("upload5Confirm").equalsIgnoreCase("2")) {
                cstmt.setString(17, req.getParameter("upload5RemarksU"));
            } else {
                cstmt.setString(17, req.getParameter("upload5Remarks"));
            }
            cstmt.setString(18, req.getParameter("finaleligibleRemarks"));
            res = cstmt.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    
}
