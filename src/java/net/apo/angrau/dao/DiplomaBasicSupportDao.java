/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.util.List;
import java.util.HashMap;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import net.apo.angrau.db.DatabaseConnection;

/**
 *
 * @author APTOL301294
 */
public class DiplomaBasicSupportDao {
    
    public List<HashMap> getStudentInfoGet(String pin) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_Diploma_StudentInfoReport_Get(?)}");
            cstmt.setString(1, pin);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("pin", rs.getString(1));
                    map.put("name", rs.getString(2));
                    map.put("fname", rs.getString(3));
                    map.put("gender", rs.getString(4));
                    map.put("scheme", rs.getString(5));
                    map.put("brach", rs.getString(6));
                    map.put("semY", rs.getString(7));
                    map.put("cat", rs.getString(8));
                    map.put("pyear", rs.getString(9));
                    map.put("subjects", rs.getString(10));
                    map.put("examFee", rs.getString(11));
                    map.put("backLogFee", rs.getString(12));
                    map.put("lateFee", rs.getString(13));
                    map.put("tatFee", rs.getString(14));
                    map.put("cerFee", rs.getString(15));
                    map.put("cfee", rs.getString(16));
                    map.put("cadte", rs.getString(17));
                    map.put("att", rs.getString(18));
                    map.put("pStatus", rs.getString(19));
                    map.put("eStatus", rs.getString(20));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getStudentPersonalAcadamicDetailsGet(String pin) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_StudentPersonalAcadamicDetails_Get(?)}");
            cstmt.setString(1, pin);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("name", rs.getString(1));
                    map.put("fname", rs.getString(2));
                    map.put("gender", rs.getString(3));
                    map.put("dob", rs.getString(4));
                    map.put("caste", rs.getString(5));
                    map.put("TenthHallTicket", rs.getString(6));
                    map.put("TenPassYear", rs.getString(7));
                    map.put("joinDate", rs.getString(8));
                    map.put("PH", rs.getString(9));
                    map.put("idOne", rs.getString(10));
                    
                    map.put("idtwo", rs.getString(11));
                    map.put("hno", rs.getString(12));
                    map.put("streetName", rs.getString(13));
                    map.put("city", rs.getString(14));
                    map.put("mandal", rs.getString(15));
                    map.put("dist", rs.getString(16));
                    map.put("pinCode", rs.getString(17));
                    map.put("mobile", rs.getString(18));
                    map.put("email", rs.getString(19));
                    map.put("aadhar", rs.getString(20));
                    
                    map.put("AddStatus", rs.getString(21));
                    map.put("college", rs.getString(22));
                    map.put("brach", rs.getString(23));
                    map.put("scheme", rs.getString(24));
                    map.put("semYear", rs.getString(25));
                    map.put("shift", rs.getString(26));
                    map.put("section", rs.getString(27));
                    map.put("active", rs.getString(28));
                    map.put("dStatus", rs.getString(29));
                    
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
     public List<HashMap> getDiplomaInternetDataGet(String pin) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_Diploma_InternetData_Get(?)}");
            cstmt.setString(1, pin);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("scheme", rs.getString(1));
                    map.put("batch", rs.getString(2));
                    map.put("college", rs.getString(3));
                    map.put("brach", rs.getString(4));
                    map.put("pin", rs.getString(5));
                    map.put("semYear", rs.getString(6));
                    map.put("cat", rs.getString(7));
                    map.put("result", rs.getString(8));
                    map.put("total", rs.getString(9));
                    map.put("passsYear", rs.getString(10));
                    
                    map.put("sub1", rs.getString(11));
                    map.put("r1", rs.getString(12));
                    map.put("sub2", rs.getString(13));
                    map.put("r2", rs.getString(14));
                    map.put("sub3", rs.getString(15));
                    map.put("r3", rs.getString(16));
                    map.put("sub4", rs.getString(17));
                    map.put("r4", rs.getString(18));
                    map.put("sub5", rs.getString(19));
                    map.put("r5", rs.getString(20));
                    
                    map.put("sub6", rs.getString(21));
                    map.put("r6", rs.getString(22));
                    map.put("sub7", rs.getString(23));
                    map.put("r7", rs.getString(24));
                    map.put("sub8", rs.getString(25));
                    map.put("r8", rs.getString(26));
                    map.put("sub9", rs.getString(27));
                    map.put("r9", rs.getString(28));
                    map.put("sub10", rs.getString(29));
                    map.put("r10", rs.getString(30));
                    
                    map.put("sub11", rs.getString(31));
                    map.put("r11", rs.getString(32));
                    map.put("rdate", rs.getString(33));
                    map.put("fStatus", rs.getString(34));
                    map.put("eligibilty", rs.getString(35));
                    
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
}