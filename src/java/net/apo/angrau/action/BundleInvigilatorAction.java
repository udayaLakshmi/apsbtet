/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.BundleDAO;
import net.apo.angrau.dao.BundleInvigilatorDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301655
 */
public class BundleInvigilatorAction extends DispatchAction {

    private static final String SUCCESS = "success";
    BundleInvigilatorDAO dao = new BundleInvigilatorDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String uname = session.getAttribute("userName").toString();
        ArrayList gradelist = new ArrayList();
        gradelist = dao.getGradesList();
        rForm.setGradeslist(gradelist);
        ArrayList invglist = new ArrayList();
        invglist = dao.getInvgList(uname);
        request.setAttribute("invgList", invglist);
        return mapping.findForward("success");
    }

    public ActionForward getGradeStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        HttpSession session = request.getSession();
        try {
            String grade = "";
            String mobile = "";
            int k = 0;
            grade = request.getParameter("grade");
            mobile = request.getParameter("mobile");
            int paperdets = dao.getDetailsStatus(grade, mobile);
            if (paperdets > 0) {
                k = 1;
            } else {
                k = 0;
              
            }
              out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public ActionForward submitDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        HashMap<String, String> statusdets = new HashMap<String, String>();
        try {
            RegisterForm rForm = (RegisterForm) form;
//            String grade = rForm.getGrade();
            HttpSession ses = request.getSession();
            String uname = ses.getAttribute("userName").toString();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                result = dao.submitDetails(request, username, rForm);
                if (result.equalsIgnoreCase("1")) {
                    request.setAttribute("result2", "Registration Done Successfully");
                } else if (result.equalsIgnoreCase("2")) {
                    request.setAttribute("result", "Invigilator Already Registered For This Grade");
                } else {
                    request.setAttribute("result", "Registration Failed");
                }
                ArrayList gradelist = new ArrayList();
                gradelist = dao.getGradesList();
                rForm.setGradeslist(gradelist);
                ArrayList invglist = new ArrayList();
                invglist = dao.getInvgList(uname);
                request.setAttribute("invgList", invglist);
                rForm.setGrade("");
                rForm.setFname("");
                rForm.setMobile("");
                rForm.setEmail("");
                rForm.setGradename("");
                rForm.setCoursename("");
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(SUCCESS);
    }

    public ActionForward deleteRecord(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String result = "";
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
        File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession session = request.getSession();
        try {
            HttpSession ses = request.getSession();
            String uname = session.getAttribute("userName").toString();
            if (ses.getAttribute("userName") != null) {
                result = dao.deleteDetails(rForm, request, uname);
                if (result.equalsIgnoreCase("1")) {
                    request.setAttribute("result2", "Record Deleted Successfully");
                } else {
                    request.setAttribute("result", "Deletion Failed");
                }
                ArrayList gradelist = new ArrayList();
                gradelist = dao.getGradesList();
                rForm.setGradeslist(gradelist);
                ArrayList invglist = new ArrayList();
                invglist = dao.getInvgList(uname);
                request.setAttribute("invgList", invglist);
                rForm.setGrade("");
                rForm.setFname("");
                rForm.setMobile("");
                rForm.setEmail("");
                rForm.setGradename("");
                rForm.setCoursename("");
                target = "success";
            } else {
                target = "unauthorise";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }
}
