/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.HashMap;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.RegisterPrintDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class RegisterPrintAction extends DispatchAction {

    RegisterPrintDao dao = new RegisterPrintDao();
    public static final String PDF = "PGCET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setAttribute("applicationPage", "applicationPage");
        return mapping.findForward("success");
    }

    public ActionForward print(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
        try {
            if (rForm.getOtp() != null) {
                String otp = dao.getvalidatingOTP(rForm.getHallticket(), rForm.getMobile());
                if (otp.equals(rForm.getOtp())) {
                    // if (otp.equalsIgnoreCase(rForm.getOtp())) {
                    HashMap<String, String> list = dao.getData(rForm, request);
                    request.setAttribute("masterData", list);
                    target = "print";
                } else {
                    rForm.setDob("");
                    rForm.setHallticket("");
                    request.setAttribute("result", "Invalid OTP Please Try Again");
                    request.setAttribute("applicationPage", "applicationPage");
                    target = "success";
                }
            } else {
                rForm.setDob("");
                rForm.setHallticket("");
                request.setAttribute("result", "OTP Failed To Validate Please Try Again");
                request.setAttribute("applicationPage", "applicationPage");
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward(target);
    }

    public ActionForward otpPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "";

        RegisterForm rForm = (RegisterForm) form;
        try {
            //String recordstatus = dao.getStatus(rForm);
        String recordstatus ="3";
            if (recordstatus.equals("3")) {
                HashMap<String, String> list = dao.getData(rForm, request);
                request.setAttribute("masterData", list);
                target = "print";
            } else if (recordstatus.equals("1")) {
                request.setAttribute("result", "Registration Not Done");
                request.setAttribute("applicationPage", "applicationPage");
                target = "success";
            } else if (recordstatus.equals("2")) {
                request.setAttribute("result", "Payment Not Done");
                request.setAttribute("applicationPage", "applicationPage");
                target = "success";
            } else if (recordstatus.equals("0")) {
                request.setAttribute("result", "No Data Found ");
                request.setAttribute("applicationPage", "applicationPage");
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward printOLD(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
        try {
            if (rForm.getOtp() != null) {
                String otp = dao.getvalidatingOTP(rForm.getHallticket(), rForm.getMobile());
                if (otp.equals(rForm.getOtp())) {
                    // if (otp.equalsIgnoreCase(rForm.getOtp())) {
                    HashMap<String, String> list = dao.getData(rForm, request);
                    request.setAttribute("masterData", list);
                    target = "print";
                } else {
                    rForm.setDob("");
                    rForm.setHallticket("");
                    request.setAttribute("result", "Invalid OTP Please Try Again");
                    request.setAttribute("applicationPage", "applicationPage");
                    target = "success";
                }
            } else {
                rForm.setDob("");
                rForm.setHallticket("");
                request.setAttribute("result", "OTP Failed To Validate Please Try Again");
                request.setAttribute("applicationPage", "applicationPage");
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward(target);
    }
}