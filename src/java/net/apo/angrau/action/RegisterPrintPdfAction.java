/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;
import com.ap.payment.PaymentGatewayIntegration;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.RegisterPrintDao;
import net.apo.angrau.dao.RegisterPrintPdfDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301655
 */
public class RegisterPrintPdfAction extends DispatchAction{
    private static final String SUCCESS = "success";
    RegisterPrintPdfDAO dao = new RegisterPrintPdfDAO();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
   HttpSession session=request.getSession();
   String uname=session.getAttribute("userName").toString();
        List<HashMap> list = dao.getCandidateList(uname);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("success");
    }
    
     public ActionForward getDataPrint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
         File directorytemp = null;
        String temporaryfolderPth = "";
        try {
                   temporaryfolderPth = getDrivePath() + PDF;
                    if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                        directorytemp = new File(temporaryfolderPth);
                        if (!directorytemp.exists()) {
                            directorytemp.mkdirs();
                        }
                    }
                    String filename = rForm.getHallticket()+ ".pdf";
                    dao.getPdfData(temporaryfolderPth, filename, rForm, request);

                    boolean downloadstatus = false;
                    downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
                    if (downloadstatus == true) {
                        dao.deleteFile(temporaryfolderPth + filename);
                        rForm.setAadhar("");
                        request.setAttribute("result1", "Registration Letter Generated Successfully");
                        request.setAttribute("form", "form");
                    }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        unspecified(mapping,form,request,response);
        return mapping.findForward(target);
    }
     public ActionForward getPayment(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
         File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession  session=request.getSession();
        try {
              String uname=session.getAttribute("userName").toString();
                HashMap<String, String> list = dao.getDataForPaymentDob(rForm, request, uname);
                if (list != null && list.size() > 0) {
                    request.setAttribute("paymentList", list);
                    target = "payment";
                }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        unspecified(mapping,form,request,response);
        return mapping.findForward(target);
    }
       public String payment(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
////
//    public String payment(ActionForm form, String payMode, String createdBy, HttpServletRequest request,){
        System.out.println("Hii Entered into Payment..");
        RegisterForm collageForm = (RegisterForm) form;
        StringBuilder requestSB = new StringBuilder();
        String checkSumValue = "";
        String userName = "";
        String url = "";
        String link = "";
//        HttpSession session = request.getSession();
        ActionForward actionForward = new ActionForward();
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";


        double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        PaymentGatewayIntegration payAPI = new PaymentGatewayIntegration();
        System.out.println("grade==============================" + collageForm.getGrade());
        String reqEncData = payAPI.payRequestFlowMpc(collageForm.getAadhar(), collageForm.getGrade(), collageForm.getPaymentmode(), collageForm.getAmount());
        System.out.println("reqEncData======" + reqEncData);
        if (reqEncData.equals("PG_ERROR")) {
            System.out.println("payLoad====");
        } else {
            link = reqEncData;
        }
        response.sendRedirect(link);
        return null;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";
        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}
