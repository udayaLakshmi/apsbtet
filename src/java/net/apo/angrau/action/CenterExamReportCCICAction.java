/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static net.apo.angrau.action.NRSHReportAction.getDrivePath;
import net.apo.angrau.dao.CenterExamReportCCICDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
/**
 *
 * @author APTOL301294
 */
public class CenterExamReportCCICAction extends DispatchAction {

    CenterExamReportCCICDao dao = new CenterExamReportCCICDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        ArrayList gradeList = dao.getGradeList(userName);
        rForm.setGradeslist(gradeList);
        return mapping.findForward("success");
    }
    
    public ActionForward getSubject(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String courseId = request.getParameter("courseId");
            ArrayList subjectsList = dao.getSubjectList(courseId);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("subC") + "'>" + (String) subMap.get("subN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
    
      public ActionForward getExamDates(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String subCode = request.getParameter("subCode");
            ArrayList subjectsList = dao.getExamDates(subCode);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("examC") + "'>" + (String) subMap.get("examN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
    
    public ActionForward getCentrBasicData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        rForm.setUserName(userName);
        String courseCode = rForm.getGrade();
        String subCode = rForm.getSubjectCode();
        String examDate = rForm.getEdate();
        List<HashMap> list = dao.getCenteBasicData(userName, courseCode, subCode,examDate);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        ArrayList gradeList = dao.getGradeList(userName);
        rForm.setGradeslist(gradeList);
        ArrayList subList = dao.getSubjectList(courseCode);
        rForm.setSubjectList(subList);
        rForm.setSubjectCode(subCode);
        ArrayList examDateList = dao.getExamDates(subCode);
        System.out.println("examDateList"+examDateList);
        rForm.setStateList(examDateList);
        rForm.setEdate(examDate);
        request.setAttribute("courseCode", courseCode);
        request.setAttribute("subCode", subCode);
        request.setAttribute("examDate", examDate);
        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        String gradeCode = request.getParameter("gcode");
        String subCode = request.getParameter("subCode");
        return mapping.findForward("successExcel");
    }

    public ActionForward getPdfData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = null;
        File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        try {
            temporaryfolderPth = getDrivePath() + PDF;
            if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                directorytemp = new File(temporaryfolderPth);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
            }
            filename = "CCIC_ATTENDANCE_SHEET_"+userName+".pdf";
            String courseCode = request.getParameter("courseCode");
            String subCode = request.getParameter("subCode");
            String examDate = request.getParameter("examDate");
            dao.getPdfDataTW(temporaryfolderPth, filename, userName, courseCode, subCode,examDate);

            boolean downloadstatus = false;
            downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
            if (downloadstatus == true) {
                dao.deleteFile(temporaryfolderPth + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}