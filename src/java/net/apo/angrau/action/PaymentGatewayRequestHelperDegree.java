/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.payment.DecryptEcrypte;
import com.ap.payment.PaymentGateWayDto;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import net.apo.angrau.db.DatabaseConnection;
import org.apache.log4j.Logger;

/**
 *
 * @author 1582792
 */
public class PaymentGatewayRequestHelperDegree {
    static Logger LOG = Logger.getLogger(PaymentGatewayRequestHelperDegree.class.getName());
//    private final String PSURL = PayConfig.PGRESPURL();
    private final String CLIENT_ID;
    private final String SERVICE_ID;
    private final String DEPT_ID;
    private final String CHECKSUM_KEY;
    private final String RETURN_SC_URL;
    private final String RETURN_FL_URL;
    private final String APP_REQ_CODE;
    private final PaymentGateWayDto DTO;

    public PaymentGatewayRequestHelperDegree(PaymentGateWayDto dto) {
        this.APP_REQ_CODE = dto.getRequestID();
        this.CLIENT_ID = dto.getClientid();
        this.SERVICE_ID = dto.getServiceID();
        this.DEPT_ID = dto.getDeptID();
        this.CHECKSUM_KEY = dto.getChecksum();
        this.RETURN_SC_URL =  dto.getReturnURL();
        this.RETURN_FL_URL =  dto.getFailureURL();
        this.DTO = dto;
    }

    
//parameters: 1234567899999|100|1001|6512|NB|0.0|5.0|2005.9|||address|AP|AndhraPradesh|500072|||YESCHCKERDA||NA|NA|NA|NA|NA
 //   public String[] pgRequest() {
    
    public String[] pgRequest (String applicationServiceNumber,String payMode,double amount){
        try {
            String ACCESS = "UAT";
            String[] sArry = new String[3];
            String checkSumData ="YESCHCKERDA";
            StringBuilder requestSB = new StringBuilder();
            DecryptEcrypte encDec = new DecryptEcrypte();
            Connection con = null;
              double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
            CallableStatement stmt = null;
            ResultSet rs = null;
        String requestId = null;
        String mobile = null;
        String name = null;
            System.out.println("dddd"+applicationServiceNumber);
            try{
                con = DatabaseConnection.getConnection();
                stmt = con.prepareCall("{call proc_PaymentMode_Degree(?,?,?)}");
                stmt.setDouble(1, amount);
                stmt.setString(2, payMode);
                stmt.setString(3, applicationServiceNumber);
                System.out.println("oooooooo"+payMode);
                System.out.println("yyyyyyyyy"+applicationServiceNumber);
                rs = stmt.executeQuery();
                if (rs != null & rs.next()) {

                    ConvenienceCharges = rs.getDouble(2);
                    System.out.println("ConvenienceCharges"+ConvenienceCharges);
                    GST_Charges = rs.getDouble(3);
                    totConvGst = rs.getDouble(4);
                    totalFinalAmount = rs.getDouble(5);
                    requestId = rs.getString(6);
                    name = rs.getString(7);
                    System.out.println("name"+name);
                    mobile = rs.getString(8);
                    System.out.println("mobile"+mobile);
                    // name mobi
                    //
                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
            
            
            
            String parameters = requestSB.append(requestId).append("|").append("100").append("|").append("1001")
                    .append("|").append("6512").append("|").append(payMode).append("|").append(amount)
                    .append("|").append(totConvGst).append("|").append(totalFinalAmount).append("|").append("https://pgadmissions2020angrau.aptonline.in/ANGRAU/paymentSuccess.do")
                    .append("|").append(name).append("|").append("Address").append("|").append("AP")
                    .append("|").append("AP").append("|").append("500000").append("|").append("test@gmail.com")
                    .append("|").append(mobile).append("|").append(checkSumData).append("|").append("https://pgadmissions2020angrau.aptonline.in/ANGRAU/paymentFailurePage.do")
                    .append("|").append("NA").append("|").append("NA").append("|").append("NA")
                    .append("|").append("NA").append("|").append("NA").toString();

            
           // String check = APP_REQ_CODE+"#"+CLIENT_ID+"#"+SERVICE_ID+"#"+DTO.getPaymentMode()+"#"+DTO.getBaseAmount()+"#"+CHECKSUM_KEY+"#"+ ProjectConfig.ACCESS+"#"+DTO.getPayType();
             String check = requestId+"#"+100+"#"+6512+"#"+payMode+"#"+amount+"#"+741852963+"#"+ACCESS+"#"+payMode;
           
            System.out.println("parameters: "+parameters);
            System.out.println("check: "+check);
            
            sArry[0] = encDec.encrypt(parameters);
            sArry[1] = encDec.encrypt(check);
            return (sArry);
        } catch (Exception e) {
            LOG.error("e ",e);
            e.printStackTrace();
            return null;
        }
    }
}
