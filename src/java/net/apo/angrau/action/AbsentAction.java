/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.ApplicationStatusDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1820530
 */
public class AbsentAction extends DispatchAction {

    private static final String SUCCESS = "success";

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        ApplicationStatusDAO applicationStatusDAO = ApplicationStatusDAO.getInstance();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        String roleId = (String) session.getAttribute("RoleId");
        String gradeCode = "";
        String batchCode = "";
        String status = "2";
        
        
        if ("13".equalsIgnoreCase(roleId)) {
//        if(userName.equalsIgnoreCase("cs1")){
            List<HashMap> list = applicationStatusDAO.getApplicationStatusReport(userName, gradeCode, batchCode, status);

            System.out.println("presentList :" + list);

            if (list != null && !list.isEmpty()) {
                request.setAttribute("listData", list);
            } else {
                request.setAttribute("result", "No Details Found");
            }
            request.setAttribute("centerLogin", "centerLogin");
              request.setAttribute("uName", userName);
        } else {

            List<HashMap> list = applicationStatusDAO.getApplicationStatusReport("0", gradeCode, batchCode, status);

            System.out.println("presentList :" + list);

            if (list != null && !list.isEmpty()) {
                request.setAttribute("listData", list);
            } else {
                request.setAttribute("result", "No Details Found");
            }
             request.setAttribute("uName", "0");

        }
        
        request.setAttribute("gcode", gradeCode);
        request.setAttribute("bcode", batchCode);
        ArrayList centerlist = applicationStatusDAO.getCenterList();
        request.setAttribute("centerlist", centerlist);
        return mapping.findForward("success");
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        ApplicationStatusDAO dao = ApplicationStatusDAO.getInstance();
        String userName = session.getAttribute("userName").toString();

        String gradeCode = rForm.getGrade();
        String batchCode = rForm.getBatchCode();
        String centerCode;

        String status = "2";//2 for absent
        String roleId = (String) session.getAttribute("RoleId");
        if ("13".equalsIgnoreCase(roleId)) {
            request.setAttribute("centerLogin", "centerLogin");
        } else {
            centerCode = rForm.getCenterCode();
            userName = centerCode;
        }
        rForm.setUserName(userName);
        List<HashMap> list = dao.getApplicationStatusReport(userName, gradeCode, batchCode, status);
         System.out.println("AbsentList :" + list);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        request.setAttribute("gcode", gradeCode);
        request.setAttribute("bcode", batchCode);
        request.setAttribute("uName", userName);
//        ArrayList gradelist = dao.getGradeList();
//        rForm.setGradeslist(gradelist);
//        ArrayList batchlist = dao.getBatchList();
//        rForm.setBatchlist(batchlist);
        ArrayList centerlist = dao.getCenterList();
        request.setAttribute("centerlist", centerlist);
        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        ApplicationStatusDAO dao = ApplicationStatusDAO.getInstance();
        String gradeCode = request.getParameter("gcode");
        String batchCode = request.getParameter("bcode");
        String userName = request.getParameter("uName");
        String status = "2"; // 2 for absent
        List<HashMap> list = dao.getApplicationStatusReport(userName, gradeCode, batchCode, status);
        
          System.out.println("AbsentList :" + list);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("AbsentReportExcel");
    }
}