/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.ApplicationStatusDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1820530
 */
public class CCICAbsentAction extends DispatchAction {
    
     private static final String SUCCESS = "success";

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        ApplicationStatusDAO dao = ApplicationStatusDAO.getInstance();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        String roleId = (String) session.getAttribute("RoleId");
        String gradeCode="";
        String batchCode="";
        String status ="2"; // 2 for absent
        
           List<HashMap> list = dao.getApplicationStatusCCICReport(userName,roleId, status);

            System.out.println("absentList from unspecified :" + list);

            if (list != null && !list.isEmpty()) {
                request.setAttribute("listData", list);
            } else {
                request.setAttribute("result", "No Details Found");
            }
                            
        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        ApplicationStatusDAO dao = ApplicationStatusDAO.getInstance();

        String userName = session.getAttribute("userName").toString();
        String roleId = (String) session.getAttribute("RoleId");
        String gradeCode="";
        String batchCode="";
        
        String status = "2"; // 2 for absent
        List<HashMap> list = dao.getApplicationStatusCCICReport(userName,roleId, status);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("CCICAbsentReportExcel");
    }
    
    
}
