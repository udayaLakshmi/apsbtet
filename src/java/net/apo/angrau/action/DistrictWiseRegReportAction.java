/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.DistrictWiseRegReportDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class DistrictWiseRegReportAction extends DispatchAction {

    DistrictWiseRegReportDao dao = new DistrictWiseRegReportDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<HashMap> list = dao.getDistrictWiseReport();
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("success");
    }
    
    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<HashMap> list = dao.getDistrictWiseReport();
        if (list != null && !list.isEmpty()) {
            request.setAttribute("instData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("successExcel");
    }
   
}