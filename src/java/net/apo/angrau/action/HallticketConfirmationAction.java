/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.HallticketConfirmationDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301655
 */
public class HallticketConfirmationAction extends DispatchAction {
     private static final String SUCCESS = "success";
    HallticketConfirmationDAO dao = new HallticketConfirmationDAO();
   public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
   HttpSession session=request.getSession();
   String uname=session.getAttribute("userName").toString();
        List<HashMap> list = dao.getCandidateList(uname);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("success");
    }  
     public ActionForward submitDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        try {
         
            RegisterForm rForm = (RegisterForm) form;
            HttpSession ses = request.getSession();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                result = dao.submitDetails(request, username);
                if ("1".equals(result)) {
                    request.setAttribute("result2", "Data Successfully Saved");
                } else {
                    request.setAttribute("result", "Data Insertion Failed");
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(SUCCESS);
    }

}
