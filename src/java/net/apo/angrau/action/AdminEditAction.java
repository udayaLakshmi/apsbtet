/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.sms.util.MessagePropertiesUtil;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.dao.AdminEditDao;
import net.apo.angrau.dao.RegisterDao;
import net.apo.angrau.dao.RegisterationAfterLoginDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class AdminEditAction extends DispatchAction {

    AdminEditDao dao = new AdminEditDao();
    RegisterationAfterLoginDAO rdao = new RegisterationAfterLoginDAO();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target="invalidservice";
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("RoleId").toString().equalsIgnoreCase("10")) {
                List<HashMap> list = dao.getCandidateList();
                if (list != null && !list.isEmpty()) {
                    request.setAttribute("listData", list);
                } else {
                    request.setAttribute("listData1", list);
                }
//        List<HashMap> list1 = dao.getCandidatePendingList();
//        if (list1 != null && !list1.isEmpty()) {
//            request.setAttribute("listData2", list1);
//        } else {
//            request.setAttribute("listData3", list1);
//        }
                target="success";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward getDataPrint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "profileEdit";
        RegisterForm rForm = (RegisterForm) form;
        String language = "";
        String examination = "";
        String examinationname = "";
        String gradename = "";
        ArrayList stateList = new ArrayList();
        if (rForm.getGrade().toUpperCase().startsWith("T")) {
            examination = "TW";
            examinationname = "TypeWriting";
        } else {
            examination = "SH";
            examinationname = "ShortHand";
        }
        String letter = Character.toString(rForm.getGrade().toUpperCase().charAt(1));
        if (letter.startsWith("T")) {
            language = "Telugu";
        } else if (letter.startsWith("E")) {
            language = "English";
        } else if (letter.startsWith("H")) {
            language = "Hindi";
        } else if (letter.startsWith("U")) {
            language = "Urdu";
        }
        rForm.setExamination(examination);
        rForm.setLanguage(language);
        HashMap<String, String> list = dao.getData(rForm, request);
        request.setAttribute("masterData", list);
        ArrayList distlist = dao.getcenterDistDetails(examination);
        rForm.setDistLists(distlist);
        stateList = dao.getStateDetails();
        rForm.setStateList(stateList);
        return mapping.findForward(target);
    }

    public ActionForward getMandalsAt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        ArrayList mandalsList = new ArrayList();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            String state = request.getParameter("state");
            if (state.equals("Andra Pradesh")) {
                mandalsList = rdao.getMandalDetails(district);
            } else if (state.equals("Telagana")) {
                mandalsList = rdao.getMandalDetailsAt(district);
            }
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("mandal_ID") + "'>" + (String) mandalMap.get("mandal_Name") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getDistrictListAt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        ArrayList mandalsList = new ArrayList();
        try {
            String state = request.getParameter("state");
            if (state.equals("Andra Pradesh")) {
                mandalsList = rdao.getDistrictDetails();
            } else if (state.equals("Telagana")) {
                mandalsList = rdao.getDistrictDetailsAt();
            }
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("district_ID") + "'>" + (String) mandalMap.get("district_Name") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward validatingaadharNum(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        try {
            String mobile = request.getParameter("mobile");
            String email = request.getParameter("email");
            String aadharNum = request.getParameter("aadharNum");
            int k = rdao.getvalidatingAadharNum(mobile, aadharNum);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward getStatus(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadharNum");
            String grade = request.getParameter("grade");
            String language = "";
            String examination = "";
            String examinationname = "";
            String gradename = "";
            if (grade.toUpperCase().startsWith("T")) {
                examination = "TW";
                examinationname = "TypeWriting";
            } else {
                examination = "SH";
                examinationname = "ShortHand";
            }
            String letter = Character.toString(grade.toUpperCase().charAt(1));
            if (letter.startsWith("T")) {
                language = "Telugu";
            } else if (letter.startsWith("E")) {
                language = "English";
            } else if (letter.startsWith("H")) {
                language = "Hindi";
            } else if (letter.startsWith("U")) {
                language = "Urdu";
            }
            rForm.setAadhar(aadharNum);
            rForm.setGrade(grade);
            rForm.setExamination(examination);
            rForm.setLanguage(language);
            int k = rdao.getStatus(rForm);
            out.println(k + "_" + aadharNum + "_" + examination + "_" + language + "_" + grade + "_" + examinationname);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward getTimeStatus(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadharNum");
            String date = request.getParameter("date");
            String batch = request.getParameter("batch");
            String grade = request.getParameter("grade");
            int k = rdao.getTimeStatus(aadharNum, date, batch, grade);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward statusexam(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadhar");
            String language = request.getParameter("language");
            String examination = request.getParameter("examination");
            int k = rdao.getStatusExam(aadharNum, examination, language);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward submitData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String result = "";
        HttpSession session = request.getSession();
        try {
            RegisterForm rForm = (RegisterForm) form;
            String remoteAddress = request.getRemoteAddr();
            String uname = session.getAttribute("userName").toString();
            result = dao.getUpdateCandidateInfo(rForm, remoteAddress, uname);
            if (result.equalsIgnoreCase("1")) {
                request.setAttribute("result1", "Data Successfully Updated");
            } else {
                request.setAttribute("result", "Data Failed to Saved");
                return mapping.findForward("success");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward("success");
    }

    public String getImageInBase64Format(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filepath = null;
        String b64 = "";
        ByteArrayOutputStream baos = null;
        BufferedImage image = null;
        String aadhar = request.getParameter("aadhar");
        String filename = request.getParameter("filename");
        String grade = request.getParameter("grade");
        PrintWriter pw = response.getWriter();
        try {
            filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + grade + "\\" + aadhar + "\\" + filename;
            File file = new File(filepath);
            if (!file.exists()) {
                return "";
            }
            image = ImageIO.read(file);
            baos = new ByteArrayOutputStream();
            int lastIndex = file.getName().lastIndexOf('.');
            // get extension
            String extenstion = file.getName().substring(lastIndex);

            if (extenstion != null && extenstion.equalsIgnoreCase(".jpg")) {
                ImageIO.write(image, "jpg", baos);
            } else {
                ImageIO.write(image, "png", baos);
            }
            baos.flush();
            byte[] imageInByteArray = baos.toByteArray();
            b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
            pw.write(b64);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                baos.close();
            }

            if (image != null) {
            }
        }
        return null;
    }

    public ActionForward getCentersDistrict(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String examination = request.getParameter("examination");
            String grade = request.getParameter("grade");
            ArrayList centerlist = new RegisterDao().getcenterDistDetails(examination, grade);

            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("district_ID") + "'>" + (String) centerMap.get("district_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCenters(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            String examination = request.getParameter("examination");
            ArrayList centerlist = new RegisterDao().getcenterDetails(district, examination);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("ccode") + "'>" + (String) centerMap.get("cname") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
}