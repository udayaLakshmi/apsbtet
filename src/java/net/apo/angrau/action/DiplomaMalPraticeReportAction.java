/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static net.apo.angrau.action.NRReportAction.PDF;
import net.apo.angrau.dao.ApplicationStatusDiplomaAbsentDAO;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1820530
 */
public class DiplomaMalPraticeReportAction extends DispatchAction {

    ApplicationStatusDiplomaAbsentDAO dao = ApplicationStatusDiplomaAbsentDAO.getInstance();

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();

        String userName = session.getAttribute("userName").toString();
        String roleId = (String) session.getAttribute("RoleId");

        List<HashMap> list = dao.getNRReport(userName, roleId, "3");

        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        request.setAttribute("role", roleId);
        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        String roleId = (String) session.getAttribute("RoleId");
        List<HashMap> list = dao.getNRReport(userName, roleId, "3");
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("malpracticeReportExcel");
    }

    public ActionForward getPdfData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = null;
        File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        String roleId = (String) session.getAttribute("RoleId");

        try {
            temporaryfolderPth = getDrivePath() + PDF;
            if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                directorytemp = new File(temporaryfolderPth);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
            }

            filename = "SBTET_Diploma_MALPRACTICE_Report.pdf";
            dao.getPdfDataNR(temporaryfolderPth, filename, userName, roleId, "3");

            boolean downloadstatus = false;
            downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
            if (downloadstatus == true) {
                dao.deleteFile(temporaryfolderPth + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}