/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static net.apo.angrau.action.NRReportAction.PDF;
import net.apo.angrau.dao.DiplomaExamDateWiseReportDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class DiplomaExamDateWiseReportAction extends DispatchAction {

    DiplomaExamDateWiseReportDAO dao = DiplomaExamDateWiseReportDAO.getInstance();

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();

        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        String roleId = (String) session.getAttribute("RoleId");

        ArrayList courseWiseList = dao.getCourseList(userName);
        rForm.setCourseWiseList(courseWiseList);
        request.setAttribute("courseWiseList", courseWiseList);
        request.setAttribute("role", roleId);
        return mapping.findForward("success");
    }

    public ActionForward getBranchList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String userName = session.getAttribute("userName").toString();
            String courseId = request.getParameter("courseId");
            ArrayList subjectsList = dao.getBrachList(courseId,userName);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("braC") + "'>" + (String) subMap.get("braN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        String roleId = (String) session.getAttribute("RoleId");

        String course = rForm.getCourse();
        String subCode = rForm.getBatchCode();
       
        request.setAttribute("courceCode", course);
        request.setAttribute("subCode", subCode);
        request.setAttribute("uName", userName);
        request.setAttribute("cCode", "");

        List<HashMap> list = dao.getNRReport(userName, course, subCode, "");

        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }

        ArrayList courseWiseList = dao.getCourseList(userName);
        //ArrayList centerlist = dao.getccicCenterList(course, subCode);
        ArrayList subjectList = dao.getSubjectList(course);
        rForm.setSubjectList(subjectList);
        rForm.setCourseWiseList(courseWiseList);

        ArrayList branchList = dao.getBrachList(course,userName);
        rForm.setBatchlist(branchList);
        rForm.setBatchCode(subCode);

        request.setAttribute("courseWiseList", courseWiseList);
        request.setAttribute("centerlist", "");
        request.setAttribute("subjectList", subjectList);
        request.setAttribute("role", roleId);

        return mapping.findForward("success");
    }

    public ActionForward getSubject(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String courseId = request.getParameter("courseId");
            ArrayList subjectsList = dao.getSubjectList(courseId);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("subC") + "'>" + (String) subMap.get("subN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCenterList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String courseId = request.getParameter("courseId");
            String branchC = request.getParameter("branchC");
            ArrayList subjectsList = dao.getccicCenterList(courseId, branchC);
            request.setAttribute("centerlist", subjectsList);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("centercode") + "'>" + (String) subMap.get("centerName") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();

        String userName = session.getAttribute("userName").toString();

        String course = request.getParameter("courceCode");
        String subCode = request.getParameter("subCode");
        String centerCode = request.getParameter("cCode");

        List<HashMap> list = dao.getNRReport(userName, course, subCode, centerCode);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("NRReportExcel");
    }

    public ActionForward getPdfData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = null;
        File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession session = request.getSession();

        String roleId = (String) session.getAttribute("RoleId");
        String userName = session.getAttribute("userName").toString();

        String course = request.getParameter("courceCode");
        String subCode = request.getParameter("subCode");
        String centerCode = request.getParameter("cCode");

        try {
            temporaryfolderPth = getDrivePath() + PDF;
            if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                directorytemp = new File(temporaryfolderPth);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
            }

            filename = "DateWiseReport.pdf";
            dao.getPdfDataNR(temporaryfolderPth, filename, userName, course, subCode, centerCode);

            boolean downloadstatus = false;
            downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
            if (downloadstatus == true) {
                dao.deleteFile(temporaryfolderPth + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}