/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.WithLoginAction.*;
import com.ap.Action.*;
import com.sms.util.MessagePropertiesUtil;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.dao.VerificationDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import sun.misc.BASE64Decoder;

/**
 *
 * @author APTOL301655
 */
public class VerificationAction  extends DispatchAction{
   
    private static final String SUCCESS = "success";
    private final VerificationDAO dao = new VerificationDAO();
  
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String username = null;
        String roleid = null;
        HttpSession session = null;
        session = request.getSession();
        username = (String) session.getAttribute("userName");
        roleid = (String) session.getAttribute("RoleId");
        List list = dao.getDocumentVerficationDetails(username,"ALL","ALL");
         request.setAttribute("course", "ALL");
         request.setAttribute("subgroup", "ALL");
        if (list != null && !list.isEmpty()) {
            request.setAttribute("twshdetails", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
         List submittedList = dao.getDegreeDocumentSubmitedDetails(username);
//List submittedList=null;
        if (submittedList != null && !submittedList.isEmpty()) {
            request.setAttribute("submittedList", submittedList);
        } else {
            //request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("success");
    }

    public ActionForward getDocumentDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        String username = null;
        String roleid = null;
        HttpSession session = null;
        session = request.getSession();
        List list =null;
        try {

            RegisterForm rForm = (RegisterForm) form;
                username = (String) session.getAttribute("userName");
                 String roleId=(String) session.getAttribute("RoleId");
                if ("3".equalsIgnoreCase(roleId) ||"1".equalsIgnoreCase(roleId)) {
                  list=  dao.getAcademicsDocumentDetails(rForm, request);
                    target="success1";
                }
                if(list != null && !list.isEmpty()){
                 request.setAttribute("doculist", 1);
                }else{
                request.setAttribute("doculist", 0);
                }
          
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public ActionForward submitAcademicsDocumentVerificationData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int result = 0;
        try {
            HttpSession session=request.getSession();
            String uname=session.getAttribute("userName").toString();
            RegisterForm rForm = (RegisterForm) form;
            result = dao.submitAcademicsDocumentStatusDetails(rForm, request,uname);
            if (result == 1) {
                request.setAttribute("result1", "Data Successfully Saved");
            } else {
                request.setAttribute("result", "Data Insertion Failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(SUCCESS);
    }

   
    public String getImageInBase64Format(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
        // System.out.println("=====getImageInBase64Format======");
        String filepath = null;
        String b64 = "";
        ByteArrayOutputStream baos = null;
        BufferedImage image = null;
        String aadhar = request.getParameter("aadhar");
        String filename = request.getParameter("filename");
        String grade = request.getParameter("grade");
        PrintWriter pw = response.getWriter();
        try {
            filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + grade+ "\\" + aadhar  + "\\" + filename;
          System.out.println("filepath================"+filepath);
            File file = new File(filepath);

            if (!file.exists()) {
                return "";
            }

            image = ImageIO.read(file);

            baos = new ByteArrayOutputStream();

            int lastIndex = file.getName().lastIndexOf('.');

            // get extension
            String extenstion = file.getName().substring(lastIndex);

            if (extenstion != null && extenstion.equalsIgnoreCase(".jpg")) {
                ImageIO.write(image, "jpg", baos);
            } else {
                ImageIO.write(image, "png", baos);
            }
            // ImageIO.write(image, "jpg", baos);
            baos.flush();
            byte[] imageInByteArray = baos.toByteArray();
            b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
            // System.out.println("b64==="+b64);
            pw.write(b64);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                baos.close();
            }

            if (image != null) {
            }
        }
        return null;
    }

public ActionForward downloadFiles(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ByteArrayOutputStream baos = null;
        try {
            if (request.getParameter("filename") != null && request.getParameter("filename") != "") {
                String aadhar = request.getParameter("aadhar");
                String grade = request.getParameter("grade");
                String filename = request.getParameter("filename");
                String   filepath =MessagePropertiesUtil.FIlE_PATH+"\\"+grade+"\\"+aadhar+"\\"+filename;
                System.out.println("filePath==================="+filepath);
                BufferedInputStream fif = null;
                File dir = new File(filepath);
                FileInputStream fin = new FileInputStream(dir);
                fif = new BufferedInputStream(fin);
                 response.setContentType("image/jpeg");
                 response.setHeader("Content-Disposition", "attachment; filename=" +filename);
                ServletOutputStream stream = response.getOutputStream();
                fif = new BufferedInputStream(fin);
                int data;
                while ((data = fif.read()) != -1) {
                    stream.write(data);
                }
                fif.close();
                stream.flush();
                stream.close();

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
//        unspecified(mapping, form, request, response);
        return mapping.findForward(SUCCESS);
    }
}
