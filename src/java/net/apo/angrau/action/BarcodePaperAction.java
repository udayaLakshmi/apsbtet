/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.BarcodeDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301655
 */
public class BarcodePaperAction extends DispatchAction{
    private static final String SUCCESS = "success";
    BarcodeDAO dao = new BarcodeDAO();
   public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
   HttpSession session=request.getSession();
   String uname=session.getAttribute("userName").toString();
      RegisterForm rForm = new RegisterForm();
      rForm.setBarcode("");
        return mapping.findForward("success");
    }  
   public ActionForward getStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        HttpSession session=request.getSession();
        try {
             String barcode="";
            int k=0;
            barcode = request.getParameter("barcode");
             HashMap<String,String>   paperdets = dao.getDetails(barcode);
                if (paperdets.size() > 0) {
                          k = 1;
                          session.setAttribute("count",paperdets.get("papercount"));
                    out.println(paperdets.get("statusresult")+ "_"+ paperdets.get("examination") + "_" + paperdets.get("grade") + "_" + paperdets.get("paper") + "_" + paperdets.get("papercount")+ "_" + paperdets.get("regno")+ "_" + paperdets.get("ebatch")+ "_" + paperdets.get("ecenter")+ "_" + paperdets.get("totalbooklets")+ "_" + paperdets.get("scannedbooklets")+ "_" + paperdets.get("tobescannedbooklets"));
                    }else{
                        k=0;
                          out.println(k);
                    }
          out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
     public ActionForward submitDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        HashMap<String,String> statusdets=new HashMap<String,String>();
        try {
         
            RegisterForm rForm = (RegisterForm) form;
            HttpSession ses = request.getSession();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                statusdets = dao.submitDetails(request, username);
                rForm.setBarcode("");
                if (statusdets.get("status").toString().equalsIgnoreCase("1")) {
                    request.setAttribute("result2", statusdets.get("statusmsg"));
                } 
                else {
                    request.setAttribute("result", statusdets.get("statusmsg"));
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(SUCCESS);
    }
}
